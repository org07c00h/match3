var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var BlockTypes;
    (function (BlockTypes) {
        BlockTypes[BlockTypes["DISABLED"] = 0] = "DISABLED";
        BlockTypes[BlockTypes["ACTIVE"] = 1] = "ACTIVE";
    })(BlockTypes = mygame.BlockTypes || (mygame.BlockTypes = {}));
    ;
    ;
    ;
    var Field = (function (_super) {
        __extends(Field, _super);
        function Field(game, x, y, playState) {
            var _this = _super.call(this, game, x, y) || this;
            _this._fieldUpperBound = [];
            _this._followGem1 = false;
            _this._followGem2 = false;
            _this._followGem3 = false;
            _this._background = _this.game.add.sprite(-15, -15, "atlas2", "field");
            _this._background.inputEnabled = true;
            _this._background.anchor.set(0, 0);
            _this.addChild(_this._background);
            _this._maxColorStreak = 0;
            _this._boomAnimation = false;
            _this._sequences = [];
            _this._playState = playState;
            _this._tweenArray = [];
            _this._tempSprite = [];
            _this._destroyed = 0;
            _this._turnOffMask = false;
            _this._firstMatch = false;
            _this._finalWasFired = false;
            _this._gameArray = mygame.GameConfig.gameArray;
            _this._fieldUpperBound = [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
            _this._streak = [];
            for (var i = 0; i < 6; i++) {
                _this._streak.push(0);
            }
            _this._disabled = false;
            _this._bgContainer = _this.game.add.sprite(0, 0);
            _this._bgContainer.anchor.set(0.5, 0);
            _this.addChild(_this._bgContainer);
            _this._container = _this.game.add.sprite(0, 0);
            _this._container.anchor.set(0.5, 0);
            _this.addChild(_this._container);
            _this._myMask = _this.game.add.graphics(0, 0);
            _this.addChild(_this._myMask);
            _this._container.mask = _this._myMask;
            _this._myMask.beginFill(0xffffff);
            _this._myMask.drawRect(mygame.GameConfig.BLOCK_SIZE, 0, 5 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE);
            _this._myMask.drawRect(0, mygame.GameConfig.BLOCK_SIZE, 7 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE * 3);
            _this._myMask.drawRect(mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE * 4, 5 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE);
            _this._myMask.endFill();
            _this.drawField();
            _this._selectedGem = null;
            if (window["collectable"]) {
                _this._counter = 0;
            }
            else {
                _this._counter = 0;
            }
            _this.game.input.onUp.add(function () {
                _this.game.input.deleteMoveCallback(_this.gemMove, _this);
                if ((_this._selectedGem !== null && _this._selectedGem !== undefined) && _this._selectedGem.isBomb && (_this._pickedGem == null)) {
                    _this._removeMap = [];
                    for (var i = 0; i < _this._gameArray.length; i++) {
                        _this._removeMap[i] = [];
                        for (var j = 0; j < _this._gameArray[i].length; j++) {
                            _this._removeMap[i].push(0);
                        }
                    }
                    _this.setOff(_this._selectedGem.mytype - 5);
                    _this.destroyGems();
                }
            }, _this);
            _this._canClick = true;
            if (window["particleOn"]) {
            }
            _this.createLightning();
            _this.game.time.events.add(window["restartBlinkTime"], _this.blink, _this);
            _this._createBomb = false;
            _this._condensationPoint = new Phaser.Point(0, 0);
            return _this;
        }
        Field.prototype.createLightning = function () {
            this._lightnings = [];
            var angles = [0, -90, 90, 180];
            var _loop_1 = function (i) {
                var lightning = this_1.game.add.sprite(100, 100, "atlas2", "light_line_10001");
                lightning.anchor.set(0, 0.5);
                lightning.scale.x = 5;
                lightning.alpha = 0;
                lightning.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(function () {
                    lightning.alpha = 0;
                }, this_1);
                lightning.angle = angles[i];
                this_1._container.addChild(lightning);
                this_1._lightnings.push(lightning);
            };
            var this_1 = this;
            for (var i = 0; i < 4; i++) {
                _loop_1(i);
            }
        };
        Object.defineProperty(Field.prototype, "firstMatchCallback", {
            set: function (val) {
                this._firstMatchCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "endMatchCallback", {
            set: function (val) {
                this._endMatchCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "disableField", {
            set: function (val) {
                this._disabled = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "destroyCallback", {
            set: function (val) {
                this._cb = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "finalCallback", {
            set: function (val) {
                this._finalCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Field.prototype.blink = function () {
            var id = this.game.rnd.between(1, 5);
            var arr = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype == id) {
                        arr.push(this._gameArray[i][j]);
                    }
                }
            }
            arr.forEach(function (x) { return x.playBlink(); });
            this.game.time.events.add(window["restartBlinkTime"], this.blink, this);
        };
        Field.prototype.gemMove = function (event, pX, pY) {
            var distX = pX - this._selectedPoint.x;
            var distY = pY - this._selectedPoint.y;
            var deltaRow = 0;
            var deltaCol = 0;
            if (Math.abs(distX) > mygame.GameConfig.BLOCK_SIZE / 2) {
                if (distX > 0) {
                    deltaCol = 1;
                }
                else {
                    deltaCol = -1;
                }
            }
            else {
                if (Math.abs(distY) > mygame.GameConfig.BLOCK_SIZE / 2) {
                    if (distY > 0) {
                        deltaRow = 1;
                    }
                    else {
                        deltaRow = -1;
                    }
                }
            }
            if (deltaRow + deltaCol != 0) {
                var k = this._selectedGem.row + deltaRow;
                var h = this._selectedGem.col + deltaCol;
                if (k >= 0 && k < this._gameArray.length && h >= 0 && h < this._gameArray[0].length) {
                    this._pickedGem = this._gameArray[this._selectedGem.row + deltaRow][this._selectedGem.col + deltaCol];
                    if (this._pickedGem.mytype > 0) {
                        this._selectedGem.deselect();
                        this.swapGems(true);
                        this.game.input.deleteMoveCallback(this.gemMove, this);
                    }
                    else {
                        this._pickedGem = null;
                    }
                }
                else {
                    this._pickedGem = null;
                }
            }
        };
        Field.prototype.drawField = function () {
            var _this = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                var _loop_2 = function (j) {
                    var type = this_2._gameArray[i][j];
                    var x = mygame.GameConfig.BLOCK_SIZE * (j + 0.5);
                    var y = mygame.GameConfig.BLOCK_SIZE * (i + 0.5);
                    if (type > 0) {
                        var sprite_1 = new mygame.Gem(this_2.game, x, y, type);
                        this_2._container.addChild(sprite_1);
                        sprite_1._sprite.inputEnabled = true;
                        sprite_1.row = i;
                        sprite_1.col = j;
                        sprite_1._sprite.events.onInputDown.add(function (obj, pointer) {
                            var tutorialCondition;
                            tutorialCondition = window["tutorial"] && (sprite_1.row == 2) && (sprite_1.mytype == 2) || !window["tutorial"];
                            tutorialCondition = tutorialCondition || window["tutorial"] && (sprite_1.col == 3) && (sprite_1.mytype == 4);
                            if (_this._canClick && tutorialCondition && !_this._disabled) {
                                if (_this._selectedGem == null) {
                                    _this._selectedGem = sprite_1;
                                    _this._selectedPoint = new Phaser.Point(pointer.x, pointer.y);
                                    sprite_1.select();
                                    _this.game.input.addMoveCallback(_this.gemMove, _this);
                                }
                                else {
                                    _this.gemSelect(sprite_1);
                                }
                            }
                        }, this_2);
                        this_2._gameArray[i][j] = sprite_1;
                    }
                    else {
                        this_2._gameArray[i][j] = new mygame.Gem(this_2.game, x, y, 0);
                        this_2._gameArray[i][j].row = i;
                        this_2._gameArray[i][j].col = j;
                    }
                };
                var this_2 = this;
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    _loop_2(j);
                }
            }
            if (window["tutorial"]) {
                this._border = this.game.add.sprite(2.5 * mygame.GameConfig.BLOCK_SIZE, 2.5 * mygame.GameConfig.BLOCK_SIZE, "atlas2", "selection");
                this._border.scale.set(0.6, 0.6);
                this._border.anchor.set(0.5);
                this._borderTween = this.game.add.tween(this._border.scale).to({
                    x: .6 + .2,
                    y: .6 + .2
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._container.addChild(this._border);
            }
        };
        Field.prototype.endGame = function () {
            var _this = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    try {
                        this._gameArray[i][j]._sprite.inputEnabled = false;
                    }
                    catch (e) {
                        continue;
                    }
                }
            }
            setTimeout(function () { _this._finalCallback(); }, 1000);
        };
        Field.prototype.clearTweens = function () {
            if (this._tweenArray.length > 0) {
                for (var i = 0; i < this._tweenArray.length; i++) {
                    if (this._tweenArray[i] != null)
                        this.game.tweens.remove(this._tweenArray[i]);
                }
            }
            this._tweenArray = [];
            if (this._tempSprite.length > 0) {
                for (var i = 0; i < this._tempSprite.length; i++) {
                    if (this._tempSprite[i] != null)
                        this._tempSprite[i].destroy(true);
                }
            }
            this._tempSprite = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] != null) {
                        this._gameArray[i][j].clearTweens();
                    }
                }
            }
        };
        Field.prototype.replay = function () {
            this._disabled = false;
            this._finalWasFired = false;
            if (window["collectable"]) {
                this._counter = 0;
            }
            else {
                this._counter = 0;
            }
        };
        Field.prototype.gemSelect = function (gem) {
            if (this._selectedGem == null) {
                return;
            }
            if (this._selectedGem.isSame(gem)) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                return;
            }
            if (this._selectedGem.isNext(gem)) {
                this._selectedGem.deselect();
                this._pickedGem = gem;
                this.swapGems();
            }
            else {
                this._selectedGem.deselect();
                this._selectedGem = gem;
                gem.select();
            }
        };
        Field.prototype.resetBoard = function () {
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[0].length; j++) {
                    if (this._gameArray[i][j].mytype > 0) {
                        this._gameArray[i][j].mytype = mygame.GameConfig.gameArrayLandscape[i][j];
                    }
                }
            }
            this._canClick = true;
        };
        Field.prototype.moveExists = function () {
            var str = "";
            var k = 6;
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype < 0) {
                        str += "0";
                    }
                    else {
                        str += this._gameArray[i][j].mytype;
                    }
                    if (this._gameArray[i][j].mytype > 5) {
                        return true;
                    }
                }
                str += k;
                k++;
                if (k > 9) {
                    k = 6;
                }
                str += "\n";
            }
            var myRe = /(\d)(?:.|(?:.|\n){9}|(?:.|\n){6})?\1\1|(\d)\2(?:.|(?:.|\n){9}|(?:.|\n){6})?\2|(\d)(?:.|\n){7}\3(?:.|(?:.|\n){9})\3|(\d)(?:.|(?:.|\n){9})\4(?:.|\n){7}\4|(\d)(?:(?:.|\n){7,9}|(?:.|\n){17})\5(?:.|\n){8}\5|(\d)(?:.|\n){8}\6(?:(?:.|\n){7,9}|(?:.|\n){17})\6/;
            if (myRe.exec(str) == null) {
                return false;
            }
            return true;
        };
        Field.prototype.swapGems = function (swapBack) {
            var _this = this;
            if (swapBack === void 0) { swapBack = true; }
            if (window["tutorial"]) {
                var minCol = Math.min(this._selectedGem.col, this._pickedGem.col);
                var maxCol = Math.max(this._selectedGem.col, this._pickedGem.col);
                if (Math.abs(this._pickedGem.col - this._selectedGem.col) != 1 || (minCol != 2 && maxCol != 3)) {
                    this._selectedGem = null;
                    this._pickedGem = null;
                    return;
                }
                else {
                    window["tutorial"] = false;
                    for (var i = 0; i < this._gameArray.length; i++) {
                        for (var j = 0; j < this._gameArray[i].length; j++) {
                            this._playState.destroyTutorial();
                            this._borderTween.stop();
                            this._border.destroy();
                        }
                    }
                }
            }
            this._canClick = false;
            this._coolAnimation = true;
            this._verticalAnimation = false;
            this._gameArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gameArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;
            var rowSel = this._selectedGem.row;
            var colSel = this._selectedGem.col;
            var rowPic = this._pickedGem.row;
            var colPic = this._pickedGem.col;
            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            var posPicked = new Phaser.Point((colPic + 0.5) * mygame.GameConfig.BLOCK_SIZE, (rowPic + 0.5) * mygame.GameConfig.BLOCK_SIZE);
            var posSelected = new Phaser.Point((colSel + 0.5) * mygame.GameConfig.BLOCK_SIZE, (rowSel + 0.5) * mygame.GameConfig.BLOCK_SIZE);
            var tweenSelected = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y
            }, mygame.GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);
            this._tweenArray.push(tweenSelected);
            var tweenPicked = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y
            }, mygame.GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);
            this._tweenArray.push(tweenPicked);
            var type1 = this._selectedGem.mytype;
            var type2 = this._pickedGem.mytype;
            tweenPicked.onComplete.add(function () {
                if (_this._selectedGem !== null && _this._selectedGem !== undefined && _this._pickedGem !== undefined && _this._pickedGem !== null && (_this._selectedGem.isBomb || _this._pickedGem.isBomb)) {
                    _this._removeMap = [];
                    for (var i = 0; i < _this._gameArray.length; i++) {
                        _this._removeMap[i] = [];
                        for (var j = 0; j < _this._gameArray[i].length; j++) {
                            _this._removeMap[i].push(0);
                        }
                    }
                    if (_this._selectedGem.isBomb) {
                        _this.setOff(_this._selectedGem.mytype - 5, _this._selectedGem.row, _this._selectedGem.col);
                        _this.destroyGems();
                    }
                    else {
                        _this._selectedGem = _this._pickedGem;
                        _this.setOff(_this._selectedGem.mytype - 5, _this._selectedGem.row, _this._selectedGem.col);
                        _this.destroyGems();
                    }
                }
                else {
                    if (_this.isMatchOnBoard(rowPic, colPic, type1, rowSel, colSel, type2)) {
                        _this.handleMatches();
                        _this._selectedGem = null;
                        _this._pickedGem = null;
                    }
                    else if (swapBack) {
                        _this.swapGems(false);
                    }
                }
            }, this);
            tweenPicked.start();
            tweenSelected.start();
            if (!swapBack) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                this._pickedGem = null;
                this._canClick = true;
            }
        };
        Field.prototype.isMatchOnBoard = function (row1, col1, type1, row2, col2, typr2) {
            return this.isMatch(row1, col1, type1) || this.isMatch(row2, col2, typr2);
        };
        Field.prototype.isMatch = function (row, col, type) {
            return this.isHorizontal(row, col, type) || this.isVertical(row, col, type);
        };
        Field.prototype.isHorizontal = function (row, col, type) {
            var start;
            var end;
            var streak = 0;
            for (var i = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gameArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (var j = start; j >= end; j--) {
                    if (this._gameArray[row][j].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        };
        Field.prototype.setOff = function (range, x, y) {
            if (x == null) {
                if (!this._selectedGem.isBomb || range < 1) {
                    return;
                }
                if (!this._canClick) {
                    return;
                }
            }
            this._canClick = false;
            this._boomAnimation = true;
            var p;
            if (x !== undefined && x !== null) {
                p = new Phaser.Point(x, y);
            }
            else {
                p = new Phaser.Point(this._selectedGem.row, this._selectedGem.col);
                this._selectedGem.stopTween();
            }
            this._bombPosition = p;
            this._selectedGem = null;
            this._pickedGem = null;
            this._bombRange = range;
            switch (range) {
                case 1:
                    {
                        for (var i = -1; i < 2; i++) {
                            var k = void 0;
                            var h = void 0;
                            if (p.x + i >= 0 && p.x + i < this._gameArray.length) {
                                k = p.x + i;
                                h = p.y;
                                if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                    this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                                }
                                if (this._gameArray[k][h].mytype > 0) {
                                    this._removeMap[k][h] = 1;
                                }
                            }
                            if (p.y + i >= 0 && p.y + i < this._gameArray[0].length) {
                                k = p.x;
                                h = p.y + i;
                                if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                    this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                                }
                                if (this._gameArray[k][h].mytype > 0) {
                                    this._removeMap[k][h] = 1;
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    {
                    }
                    break;
            }
            this._coolAnimation = false;
        };
        Field.prototype.isVertical = function (row, col, type) {
            var start;
            var end;
            var streak = 0;
            for (var j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gameArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (var i = start; i >= end; i--) {
                    if (this._gameArray[i][col].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        };
        Field.prototype.handleMatches = function () {
            var _this = this;
            if (!this._firstMatch) {
                this._firstMatch = true;
                if (this._firstMatchCallback) {
                    this._firstMatchCallback();
                }
            }
            this._removeMap = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                this._removeMap[i] = [];
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    this._removeMap[i].push(0);
                }
            }
            this.handleHorizontalMatches();
            this.handleVerticalMatches();
            this._sequences.forEach(function (x) {
                for (var i = 0; i < x.length; i++) {
                    var p = x.get(i);
                    _this._removeMap[p.x][p.y] = 1;
                }
            });
            this.destroyGems();
            this._coolAnimation = false;
        };
        Field.prototype.destroyGems = function () {
            var _this = this;
            this._destroyed = 0;
            var condensationPoint;
            var condensationType;
            if (this._selectedGem != null) {
                if (this._removeMap[this._selectedGem.row][this._selectedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._selectedGem.x, this._selectedGem.y);
                    condensationType = this._selectedGem.mytype;
                    this._condensationPoint.set(this._selectedGem.row, this._selectedGem.col);
                    var i = 0;
                    while (i < this._sequences.length) {
                        var s = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                }
                else if (this._removeMap[this._pickedGem.row][this._pickedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._pickedGem.x, this._pickedGem.y);
                    condensationType = this._pickedGem.mytype;
                    this._condensationPoint.set(this._pickedGem.row, this._pickedGem.col);
                    var i = 0;
                    while (i < this._sequences.length) {
                        var s = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                }
                else {
                    condensationPoint = new Phaser.Point(1, 1);
                    condensationType = 0;
                }
            }
            if (this._sequences.length > 0) {
                if (this._sequences.length == 1 && this._sequences[0].length > 3 && condensationPoint) {
                    condensationPoint.x = (this._condensationPoint.y + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    condensationPoint.y = (this._condensationPoint.x + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    this._sequences.forEach(function (s) {
                        s.forEach(function (p) {
                            var gem = _this._gameArray[p.x][p.y];
                            _this.game.add.tween(gem).to({
                                x: condensationPoint.x,
                                y: condensationPoint.y
                            }, 300, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                                _this.destroyGem(gem, false, true);
                            });
                        });
                        _this.playMatchSound(s.length);
                    });
                }
                else {
                    this._sequences.forEach(function (s) {
                        s.forEach(function (p) {
                            var gem = _this._gameArray[p.x][p.y];
                            _this.destroyGem(gem);
                        });
                        _this.playMatchSound(s.length);
                    });
                }
                this._destroyed = 0;
                this.game.time.events.add(400, this.makeGemsFall, this);
            }
            else if (this._bombRange == 1) {
                var x = this._bombPosition.x;
                var y = this._bombPosition.y;
                for (var i = 0; i < this._removeMap.length; i++) {
                    for (var j = 0; j < this._removeMap[i].length; j++) {
                        if (this._removeMap[i][j] > 0 && !(i == x && j == y)) {
                            this.destroyGem(this._gameArray[i][j], true);
                        }
                    }
                }
                if (window["sounds"]) {
                    this.game.sound.play("1", 0.4);
                }
                this._gameArray[x][y].playDestroy(1);
                this.game.time.events.add(750, this.makeGemsFall, this);
            }
            else {
                var x = this._bombPosition.x;
                var y = this._bombPosition.y;
                var i = this._bombPosition.x;
                var j = this._bombPosition.x;
                var k = this._bombPosition.y;
                var h = this._bombPosition.y;
                for (var d = 1; d < Math.max(this._gameArray.length, this._gameArray[0].length); d++) {
                    if (i - d >= 0 && this._gameArray[i - d][y].mytype > 0) {
                        this.destroyGem(this._gameArray[i - d][y]);
                        this._removeMap[i - d][y] = 1;
                    }
                    if (j + d < this._gameArray.length && this._gameArray[j + d][y].mytype > 0) {
                        this.destroyGem(this._gameArray[j + d][y]);
                        this._removeMap[j + d][y] = 1;
                    }
                    if (k - d >= 0 && this._gameArray[x][k - d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][k - d]);
                        this._removeMap[x][k - d] = 1;
                    }
                    if (h + d < this._gameArray[0].length && this._gameArray[x][h + d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][h + d]);
                        this._removeMap[x][h + d] = 1;
                    }
                }
                this._removeMap[x][y] = 1;
                var bomb_1 = this._gameArray[x][y];
                this._gameArray[x][y].playDestroy(0);
                this.game.time.events.add(750, this.makeGemsFall, this);
                if (window["sounds"]) {
                    this.game.sound.play("12", 0.4);
                }
                this._lightnings.forEach(function (x) {
                    x.alpha = 1;
                    x.position.set(bomb_1.x, bomb_1.y);
                    x.play("play");
                });
            }
            this._bombRange = 0;
        };
        Field.prototype.playMatchSound = function (n) {
            var num = n - 3;
            if (num < 0) {
                return;
            }
            var sounds = [17, 16, 15, 14];
            if (num > sounds.length) {
                num = sounds.length - 1;
            }
            if (window["sounds"]) {
                this.game.sound.play(sounds[num].toString(), 0.4);
            }
        };
        Field.prototype.destroyGem = function (gem, bomb, condensation) {
            var _this = this;
            if (bomb === void 0) { bomb = false; }
            if (condensation === void 0) { condensation = false; }
            var N;
            if (this._bombRange > 1) {
                N = 1;
            }
            else {
                N = this.game.rnd.between(3, 5);
            }
            this._counter++;
            if (gem.mytype > 0) {
                var _loop_3 = function (i) {
                    var phi = this_3.game.rnd.between(0, Phaser.Math.PI2);
                    var vec = new Phaser.Point(Math.sin(phi), Math.cos(phi));
                    var r = this_3.game.rnd.between(30, 50);
                    vec.setMagnitude(r);
                    var worldPos = gem.worldPosition.clone();
                    vec.x += worldPos.x;
                    vec.y += worldPos.y;
                    var p;
                    if (gem.mytype > 5) {
                        var t = +gem._sprite.frameName.split("_")[1];
                        p = new mygame.GemPatricle(this_3.game, vec.x, vec.y, t);
                    }
                    else {
                        p = new mygame.GemPatricle(this_3.game, vec.x, vec.y, gem.mytype);
                    }
                    this_3.game.world.addChild(p);
                    var toPos = this_3._playState.getGurrentElementPosition("foo");
                    var timeAnimation = 900 + this_3.game.rnd.integerInRange(-100, 200);
                    this_3.game.add.tween(p.scale).to({
                        x: 3.5,
                        y: 3.5
                    }, timeAnimation / 2, Phaser.Easing.Sinusoidal.InOut, true, 0, 0, true);
                    this_3.game.add.tween(p).to({
                        x: [p.x, Math.max((p.x - toPos.x), (toPos.x - p.x)) / 2 + this_3.game.rnd.integerInRange(-400, 800), toPos.x],
                        y: [p.y, Math.max((p.y - toPos.y), (toPos.y - p.y)) / 2 + this_3.game.rnd.integerInRange(-400, 400), toPos.y]
                    }, timeAnimation, Phaser.Easing.Sinusoidal.Out, true, 100).interpolation(function (v, k) { return Phaser.Math.bezierInterpolation(v, k); }).onComplete.add(function () {
                        if (i == N - 1) {
                            _this._cb(1, 1);
                        }
                        if (window["sounds"] && Math.random() < 0.5) {
                            var sounds = ["21", "22", "23"];
                            _this.game.sound.play(Phaser.ArrayUtils.getRandomItem(sounds), 0.2);
                        }
                        p.destroy();
                    }, this_3);
                };
                var this_3 = this;
                for (var i = 0; i < N; i++) {
                    _loop_3(i);
                }
            }
            if (condensation) {
                gem.playDestroy(22);
            }
            else {
                if (!bomb) {
                    gem.playDestroy(0);
                }
                else {
                    gem.playDestroy(0);
                }
            }
        };
        Field.prototype.render = function () {
            this.game.debug.text("foobar 100", 10, 20);
            if (this._gameArray) {
                this._gameArray[2][2].render();
            }
        };
        Field.prototype.handleHorizontalMatches = function () {
            var _loop_4 = function (i) {
                var colorStreak = 1;
                var currentColor = -100500;
                var startStreak = 0;
                for (var j = 0; j < this_4._gameArray[i].length; j++) {
                    if (this_4._gameArray[i][j].mytype == currentColor && this_4._gameArray[i][j].mytype > 0) {
                        colorStreak++;
                        if (colorStreak >= 4) {
                            this_4._createBomb = true;
                            this_4._maxColorStreak = Math.max(this_4._maxColorStreak, colorStreak);
                        }
                    }
                    if (this_4._gameArray[i][j].mytype != currentColor || j == this_4._gameArray[i].length - 1) {
                        if (colorStreak >= 3) {
                            var seq = new mygame.Sequence();
                            this_4._sequences.push(seq);
                            var currentId = this_4._sequences.length - 1;
                            var _loop_5 = function (k) {
                                var id = currentId;
                                this_4._sequences.forEach(function (s, n) {
                                    if (s.contains(i, startStreak + k)) {
                                        id = n;
                                    }
                                });
                                if (id != currentId) {
                                    this_4._sequences[id].merge(seq);
                                    seq = this_4._sequences[id];
                                    currentId = id;
                                }
                                else {
                                    seq.add(i, startStreak + k);
                                }
                            };
                            for (var k = 0; k < colorStreak; k++) {
                                _loop_5(k);
                            }
                        }
                        startStreak = j;
                        colorStreak = 1;
                        currentColor = this_4._gameArray[i][j].mytype;
                    }
                }
            };
            var this_4 = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                _loop_4(i);
            }
        };
        Field.prototype.handleVerticalMatches = function () {
            var _loop_6 = function (i) {
                var colorStreak = 1;
                var currentColor = -100500;
                var startStreak = 0;
                for (var j = 0; j < this_5._gameArray.length; j++) {
                    if (this_5._gameArray[j][i].mytype == currentColor && this_5._gameArray[j][i].mytype > 0) {
                        colorStreak++;
                        if (colorStreak >= 4) {
                            this_5._createBomb = true;
                            this_5._maxColorStreak = Math.max(this_5._maxColorStreak, colorStreak);
                        }
                    }
                    if (this_5._gameArray[j][i].mytype != currentColor || j == this_5._gameArray.length - 1) {
                        if (colorStreak >= 3) {
                            var seq = new mygame.Sequence();
                            this_5._sequences.push(seq);
                            var currentId = this_5._sequences.length - 1;
                            var _loop_7 = function (k) {
                                var id = currentId;
                                this_5._sequences.forEach(function (s, n) {
                                    if (s.contains(startStreak + k, i)) {
                                        id = n;
                                    }
                                });
                                if (id != currentId) {
                                    this_5._sequences[id].merge(seq);
                                    seq = this_5._sequences[id];
                                    currentId = id;
                                }
                                else {
                                    seq.add(startStreak + k, i);
                                }
                            };
                            for (var k = 0; k < colorStreak; k++) {
                                _loop_7(k);
                            }
                        }
                        startStreak = j;
                        colorStreak = 1;
                        currentColor = this_5._gameArray[j][i].mytype;
                    }
                }
            };
            var this_5 = this;
            for (var i = 0; i < this._gameArray[0].length; i++) {
                _loop_6(i);
            }
        };
        Field.prototype.checkGlobalMatch = function () {
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype <= 0) {
                        continue;
                    }
                    if (this.isMatch(i, j, this._gameArray[i][j].mytype)) {
                        return true;
                    }
                }
            }
            return false;
        };
        Field.prototype.swap = function (x1, y1, x2, y2, dx) {
            if (dx === void 0) { dx = 0; }
            var temp = this._gameArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;
            this._gameArray[x1][y1] = this._gameArray[x2][y2];
            this._gameArray[x1][y1].row = x1 - dx;
            this._gameArray[x1][y1].col = y1;
            this._gameArray[x2][y2] = temp;
        };
        Field.prototype.makeGemsFall = function () {
            var _this = this;
            if (this._destroyed > 0) {
                this.game.time.events.add(200, this.makeGemsFall, this);
            }
            this._sequences.forEach(function (seq) {
                if (seq.isBomb) {
                    var p = seq.bombPosition;
                    _this._removeMap[p.x][p.y] = 0;
                    var gem = _this._gameArray[p.x][p.y];
                    try {
                        _this.game.world.removeChild(gem);
                        _this._container.addChild(gem);
                    }
                    catch (e) {
                    }
                    gem.x = (gem.col + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    gem.y = (gem.row + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    gem.spawnBoom(seq.length);
                    _this._maxColorStreak = 0;
                    _this._createBomb = false;
                    _this._boomAnimation = false;
                }
            });
            if (this._turnOffMask) {
                this._turnOffMask = false;
                this._container.mask = this._myMask;
            }
            this._sequences.length = 0;
            var holes = this.findHoles();
            var tweens = [];
            var fallen = 0;
            for (var k = 0; k < holes.length; k++) {
                var hole = holes[k];
                var above = void 0;
                var start = void 0;
                var end = void 0;
                if (hole.hasHoleAbove) {
                    above = holes[k + 1];
                    start = above.row + above.length - hole.length;
                    end = hole.row - start;
                }
                else {
                    start = 0;
                    end = hole.row - this._fieldUpperBound[hole.col];
                }
                for (var i = 0; i < end; i++) {
                    this.swap(hole.row - 1 - i, hole.col, hole.row + hole.length - 1 - i, hole.col);
                }
                if (!hole.hasHoleAbove) {
                    for (var i = this._fieldUpperBound[hole.col]; i < this._fieldUpperBound[hole.col] + hole.length; i++) {
                        var gem = this._gameArray[i][hole.col];
                        var rnd = this.game.rnd.between(1, 5);
                        gem.mytype = rnd;
                        gem.x = (hole.col + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - hole.length + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[hole.col];
                    end = hole.row + hole.length;
                }
                else {
                    var len = hole.row - (above.row + above.length - hole.length);
                    start = hole.row + hole.length - len;
                    end = hole.row + hole.length;
                }
                var _loop_8 = function (i) {
                    var gem = this_6._gameArray[i][hole.col];
                    fallen++;
                    var tween = this_6.game.add.tween(gem).to({
                        y: gem.y + hole.length * mygame.GameConfig.BLOCK_SIZE
                    }, mygame.GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, false);
                    this_6._tweenArray.push(tween);
                    tween.onComplete.add(function () {
                        _this._gameArray[gem.row][gem.col] = gem;
                        fallen--;
                        if (fallen == 0) {
                            if (_this.checkGlobalMatch()) {
                                _this.handleMatches();
                            }
                            else {
                                if (!_this.moveExists()) {
                                    _this.resetBoard();
                                }
                                else {
                                    _this._canClick = true;
                                }
                                _this._selectedGem = null;
                                if (!_this._finalWasFired) {
                                    if (_this._counter >= window["totalElemets"]) {
                                        _this._finalWasFired = true;
                                        mygame.GameConfig.COUNTERS_PUBLIC = [0, window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0, 100, 100];
                                        _this.endGame();
                                    }
                                }
                            }
                        }
                    });
                    tweens.push(tween);
                };
                var this_6 = this;
                for (var i = start; i < end; i++) {
                    _loop_8(i);
                }
            }
            tweens.forEach(function (x) { return x.start(); });
        };
        Field.prototype.createFlyTween = function (gem, flyawayPoint, delay) {
            var _this = this;
            var scaleTween = this.game.add.tween(gem.scale).to({
                x: mygame.GameConfig.PANEL_GEM_SCALE,
                y: mygame.GameConfig.PANEL_GEM_SCALE
            }, mygame.GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false);
            this._tweenArray.push(scaleTween);
            scaleTween.onComplete.addOnce(function () {
                gem.scale.setTo(1);
            }, this);
            var flyTween = this.game.add.tween(gem).to({
                x: flyawayPoint.x,
                y: flyawayPoint.y
            }, mygame.GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false, delay);
            this._tweenArray.push(flyTween);
            this._gem1ToFollow = gem;
            flyTween.onStart.addOnce(function () {
                if (window["particleOn"]) {
                    if (_this._destroyed * 100 == (delay + 100)) {
                        _this._fruitSpecialEmmiter1.x = _this._gem1ToFollow.x;
                        _this._fruitSpecialEmmiter1.y = _this._gem1ToFollow.y;
                        _this._fruitSpecialEmmiter1.on = true;
                        _this._followGem1 = true;
                    }
                }
                scaleTween.start();
            });
            flyTween.onComplete.add(function (obj, tween) {
                if (gem) {
                    if (window["particleOn"]) {
                        if (_this._destroyed == 1) {
                            _this._fruitSpecialEmmiter1.on = false;
                            _this._followGem1 = false;
                        }
                        for (var i = void 0; i < _this._tempSprite.length; i++) {
                            if (_this._tempSprite[i] == gem) {
                                _this._tempSprite.splice(i, 1);
                                break;
                            }
                        }
                    }
                    _this.game.world.removeChild(gem);
                    _this._container.addChild(gem);
                    gem.position.set(-1500, -1500);
                    _this._destroyed--;
                    _this._cb(gem.mytype, 1);
                    _this._counter++;
                    if (_this._boomAnimation) {
                        _this._boomAnimation = false;
                    }
                    if (_this._destroyed == 0) {
                        _this.makeGemsFall();
                    }
                }
            }, this);
            return flyTween;
        };
        Field.prototype.updateSpecialGemEmmiter = function () {
            if (this._followGem1) {
                this._fruitSpecialEmmiter1.x = this._gem1ToFollow.x;
                this._fruitSpecialEmmiter1.y = this._gem1ToFollow.y;
            }
        };
        Object.defineProperty(Field.prototype, "gameArray", {
            get: function () {
                var result = [];
                for (var i = 0; i < this._gameArray.length; i++) {
                    result.push([]);
                    for (var j = 0; j < this._gameArray[i].length; j++) {
                        result[i][j] = this._gameArray[i][j].mytype;
                    }
                }
                return result;
            },
            enumerable: true,
            configurable: true
        });
        Field.prototype.generateFlyAwayTween = function (block, condensationPoint, flyawayPoint, delay) {
            this._container.removeChild(block);
            this.addChild(block);
            var flyAwayTween = this.createFlyTween(block, flyawayPoint, delay);
            if (this._verticalAnimation) {
                return flyAwayTween;
            }
            else {
                var condensateTween = this.game.add.tween(block).to({
                    x: condensationPoint.x
                }, mygame.GameConfig.CONDENSATION_TIME, Phaser.Easing.Linear.None, false);
                this._tweenArray.push(condensateTween);
                condensateTween.onComplete.add(function (sprite, tween) {
                    flyAwayTween.start();
                });
                return condensateTween;
            }
        };
        Field.prototype.condensate = function (point) {
            var condenstationStack = [];
            var delay = 0;
            for (var i = 0; i < this._removeMap.length; i++) {
                for (var j = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._destroyed++;
                        var block = this._gameArray[i][j];
                        var flyAwayPoint = void 0;
                        if (block.mytype == 2) {
                            flyAwayPoint = this._playState.getGurrentElementPosition("second");
                        }
                        else {
                            flyAwayPoint = this._playState.getGurrentElementPosition("first");
                        }
                        condenstationStack.push(this.generateFlyAwayTween(block, point, flyAwayPoint, delay));
                        condenstationStack[condenstationStack.length - 1].onStart.addOnce(function () {
                        });
                        delay += 100;
                    }
                }
            }
            condenstationStack.forEach(function (el) {
                el.start();
            });
        };
        Field.prototype.findHoles = function () {
            var result = [];
            for (var j = 0; j < this._gameArray[0].length; j++) {
                for (var i = this._gameArray.length - 1; i >= 0; i--) {
                    if (this._removeMap[i][j] > 0) {
                        var length_1 = 1;
                        var row = i;
                        var found = true;
                        var wasPushed = false;
                        for (var k = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] > 0) {
                                length_1++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            }
                            else if (found) {
                                result.push({ row: row, col: j, length: length_1, hasHoleAbove: false });
                                found = false;
                                wasPushed = true;
                                var id = result.length - 2;
                                if (id >= 0 && result[id].col == result[id + 1].col) {
                                    result[id].hasHoleAbove = true;
                                }
                            }
                        }
                        if (!wasPushed) {
                            result.push({ row: row, col: j, length: length_1, hasHoleAbove: false });
                            var id = result.length - 2;
                            if (id >= 0 && result[id].col == result[id + 1].col) {
                                result[id].hasHoleAbove = true;
                            }
                        }
                        break;
                    }
                }
            }
            return result;
        };
        Field.prototype.update = function () {
            if (window["particleOn"]) {
            }
        };
        Field.prototype.destroy = function () {
            var _this = this;
            this.clearTweens();
            setTimeout(function () {
                _this.game.input.onUp.removeAll(_this);
                _this.game.tweens.removeFrom(_this, true);
                _super.prototype.destroy.call(_this, true);
            }, 10);
        };
        return Field;
    }(Phaser.Sprite));
    mygame.Field = Field;
})(mygame || (mygame = {}));

var mygame;
(function (mygame) {
    var GameConfig = (function () {
        function GameConfig() {
        }
        GameConfig.deepClone = function (m) {
            var result = [];
            for (var i = 0; i < m.length; i++) {
                result.push([]);
                for (var j = 0; j < m[0].length; j++) {
                    result[i].push(m[i][j]);
                }
            }
            return result;
        };
        GameConfig.transpose = function (m) {
            var result = [];
            for (var i = 0; i < m[0].length; i++) {
                result.push([]);
                for (var j = 0; j < m.length; j++) {
                    result[i].push(m[j][i]);
                }
            }
            return result;
        };
        GameConfig.enableDrag = function (sprite) {
            return;
        };
        return GameConfig;
    }());
    GameConfig.BLOCK_SIZE = 92;
    GameConfig.SWAP_TIME = 150;
    GameConfig.DESTROY_TIME = 150;
    GameConfig.FALLDOWN_TIME = 400;
    GameConfig.FLY_TIME = 500 * 1.15;
    GameConfig.CONDENSATION_TIME = 300;
    GameConfig.COUNTERS = [0, 2, 1, 0, 0, 0, 0, 0];
    GameConfig.COUNTERS_PUBLIC = [0, 2, 1, 0, 0, 0, 0, 0];
    GameConfig.NAMES = ["apple", "apple", "grapes", "flower", "pear", "leaf"];
    GameConfig.PANEL_GEM_SCALE = 0.8;
    GameConfig.APPLE_POS = new Phaser.Point(100, -60);
    GameConfig.GRAPES_POS = new Phaser.Point(370, -60);
    GameConfig.UNLOCK_OBJ_PER_FRUITS = 3;
    GameConfig.GEM_DEFAULT_SCALE = 1;
    GameConfig.gameArrayLandscape = [
        [-1, 5, 3, 2, 4, 1, -1],
        [2, 3, 3, 2, 1, 1, 5],
        [3, 1, 2, 4, 4, 5, 1],
        [4, 3, 5, 2, 4, 5, 1],
        [-1, 5, 4, 1, 5, 4, -1]
    ];
    mygame.GameConfig = GameConfig;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Gem = (function (_super) {
        __extends(Gem, _super);
        function Gem(game, x, y, type) {
            var _this = _super.call(this, game, x, y) || this;
            _this._tempTweenArray = [];
            _this._blinks = [];
            _this._mytype = type;
            _this._spriteName = mygame.GameConfig.NAMES[_this._mytype];
            if (type > 0) {
                _this._canPlayBlibk = true;
                _this._sprite = _this.game.add.sprite(0, 0, "atlas2", "F_" + _this._mytype);
                _this._sprite.anchor.set(0.5);
                _this.addChild(_this._sprite);
                _this._boom = _this.game.add.sprite(0, 0, "atlas2", "F_1");
                _this._boom.anchor.set(0.5);
                _this._boom.visible = false;
                _this.addChild(_this._boom);
                _this._destroyAnimation = _this._sprite.animations.add("set off", Phaser.Animation.generateFrameNames("F_", _this._mytype, _this._mytype), 18, false);
                _this._destroyAnimation.onStart.add(function () {
                    _this._sprite.alpha = 1;
                }, _this);
                _this._bombBoomAnimation = _this._boom.animations.add("b_bomb", Phaser.Animation.generateFrameNames("bb_", 1, 10), 18, false);
                _this._border = _this.game.add.sprite(0, 0, "atlas2", "selection");
                _this._border.anchor.set(0.5);
                _this._border.scale.set(0.7);
                _this.addChild(_this._border);
                _this._border.alpha = 0;
                _this._destroyAnimation.onComplete.add(function () {
                    _this.animationEnd();
                }, _this);
                _this._bombTween = _this.game.add.tween(_this._sprite.scale).to({
                    x: 1.1,
                    y: 1.1
                }, 500, Phaser.Easing.Sinusoidal.InOut, false, 0, -1, true);
                _this._sprite.alpha = 1;
                _this._lightningSprite = _this.game.add.sprite(0, 0, "atlas2", "light_line_10001");
                _this._lightningSprite.anchor.set(0.5);
                _this._lightningSprite.alpha = 0;
                _this._lightningSprite.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(function () {
                    _this._lightningSprite.alpha = 0;
                }, _this);
                _this.addChild(_this._lightningSprite);
                _this.createBlink();
                _this.createRemoveAnimation();
                _this.createBombAnimation();
            }
            return _this;
        }
        Gem.prototype.createBombAnimation = function () {
            var _this = this;
            this._bomb = this.game.add.sprite(0, 0, "atlas2", "boom_8");
            this._bomb.scale.set(1.5);
            this._bomb.anchor.set(0.5);
            this._bomb.alpha = 0;
            this.addChild(this._bomb);
            var animation = this._bomb.animations.add("play", Phaser.Animation.generateFrameNames("boom_", 8, 29), 30);
            this._bombAnimation = animation;
            animation.onStart.add(function () {
                _this._canPlayBlibk = false;
                _this._blinkSprite.alpha = 0;
                if (_this._mytype <= 5) {
                    _this._blinks[_this._mytype - 1].stop();
                }
                _this._sprite.alpha = 0;
                _this._bomb.alpha = 1;
            });
            animation.onComplete.add(function () {
                _this._bomb.alpha = 0;
            });
        };
        Gem.prototype.createRemoveAnimation = function () {
            var _this = this;
            this._removeSprite = this.game.add.sprite(0, 0, "atlas2", "remove_anim_10001");
            this._removeSprite.anchor.set(0.5);
            this._removeSprite.alpha = 0;
            this.addChild(this._removeSprite);
            var animation = this._removeSprite.animations.add("play", Phaser.Animation.generateFrameNames("remove_anim_", 10001, 10018), 24);
            this._removeAnimation = animation;
            animation.onStart.add(function () {
                _this._canPlayBlibk = false;
                _this._blinkSprite.alpha = 0;
                if (_this._mytype <= 5) {
                    _this._blinks[_this._mytype - 1].stop();
                }
                _this._sprite.alpha = 0;
                _this._removeSprite.alpha = 1;
            });
            animation.onComplete.add(function () {
                _this._removeSprite.alpha = 0;
            });
        };
        Gem.prototype.render = function () {
            this.game.debug.spriteBounds(this._sprite);
            this.game.debug.spriteInfo(this._sprite, 100, 100, "#00ff00");
        };
        Gem.prototype.playBlink = function () {
            if (this._mytype > 0 && this._canPlayBlibk && this._mytype <= 5) {
                this._blinkSprite.alpha = 1;
                this._blinks[this._mytype - 1].play();
            }
        };
        Gem.prototype.createBlink = function () {
            var _this = this;
            this._blinkSprite = this.game.add.sprite(0, 0, "atlas2", "yellow_blink_10013");
            this._blinkSprite.alpha = 0;
            this._blinkSprite.anchor.set(0.5);
            this.addChild(this._blinkSprite);
            var names = ["blue", "green", "pink", "red", "yellow"];
            for (var i = 0; i < 5; i++) {
                var animation = this._blinkSprite.animations.add("blink_" + names[i], Phaser.Animation.generateFrameNames(names[i] + "_blink_", 10001, 10025), 12);
                animation.onStart.add(function () {
                    _this._blinkSprite.alpha = 1;
                }, this);
                animation.onComplete.add(function () {
                    _this._blinkSprite.alpha = 0;
                }, this);
                this._blinks.push(animation);
            }
        };
        Gem.prototype.animationEnd = function () {
            if (!this._onCompleteFired) {
                this._onCompleteFired = true;
                this._sprite.alpha = 0;
                if (this._destroyCallback) {
                    this._onCompleteFired = true;
                    this._destroyCallback();
                }
            }
        };
        Gem.prototype.boomAnimation = function () {
            this._sprite.alpha = 0;
            if (this._mytype >= 6) {
                this._bombBoomAnimation.play();
            }
            else {
                this._boomAnimation.play();
            }
        };
        Gem.prototype.spawnBoom = function (streakCounter) {
            if (streakCounter > 3) {
                this._border.alpha = 0;
                if (streakCounter == 4) {
                    this._boom.loadTexture("atlas2", "bomb_icon");
                    this._boom.anchor.set(0.45, 0.6);
                    this._sprite.alpha = 1;
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._mytype = 6;
                }
                else {
                    this._boom.loadTexture("atlas2", "wert_horiz_icon");
                    this._boom.anchor.set(0.5);
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._sprite.alpha = 1;
                    this._mytype = 7;
                }
                this._sprite.alpha = 1;
                this._onCompleteFired = false;
                this._destroyAnimation.stop();
            }
        };
        Gem.prototype.stopTween = function () {
            this._bombTween.pause();
            this._sprite.loadTexture("atlas2", "F_2");
            this._sprite.alpha = 0;
        };
        Object.defineProperty(Gem.prototype, "row", {
            get: function () {
                return this._row;
            },
            set: function (val) {
                this._row = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "col", {
            get: function () {
                return this._col;
            },
            set: function (val) {
                this._col = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "mytype", {
            get: function () {
                return this._mytype;
            },
            set: function (val) {
                this._border.alpha = 0;
                if (!this._onCompleteFired) {
                    this._destroyAnimation.stop();
                    this._onCompleteFired = true;
                }
                this._destroyAnimation.stop();
                this._mytype = val;
                if (val < 6) {
                    this._canPlayBlibk = true;
                    this._sprite.alpha = 1;
                    this._bombTween.pause();
                    this._sprite.position.set(0, 0);
                    this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE, mygame.GameConfig.GEM_DEFAULT_SCALE);
                    this._spriteName = mygame.GameConfig.NAMES[this._mytype];
                    this._sprite.loadTexture("atlas2", "F_" + this._mytype);
                }
                else {
                    this._bombTween.start();
                    this._spriteName = "rocket";
                    this._sprite.loadTexture("atlas2", "bomb_icon");
                    this._sprite.scale.set(1, 1);
                }
                this._sprite.alpha = 1;
                this._onCompleteFired = false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "onDestroy", {
            set: function (val) {
                this._destroyCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "isBomb", {
            get: function () {
                return this._mytype >= 6;
            },
            enumerable: true,
            configurable: true
        });
        Gem.prototype.foo = function () {
        };
        Gem.prototype.lightning = function (angle) {
            this._canPlayBlibk = false;
            this._blinkSprite.alpha = 0;
            this._lightningSprite.angle = angle;
            this._lightningSprite.alpha = 1;
            this._lightningSprite.play("play");
            this._sprite.alpha = 0;
        };
        Gem.prototype.playDestroy = function (bombRange) {
            if (bombRange === void 0) { bombRange = 0; }
            this._border.alpha = 0;
            this._boom.visible = false;
            if (this.mytype == 6) {
                this._bombTween.pause();
            }
            if (bombRange == 1) {
                this._bomb.alpha = 1;
                this._bombAnimation.play();
            }
            else if (bombRange == 0) {
                this._removeSprite.tint = 0xffffff;
                this._removeAnimation.play();
            }
            else {
            }
        };
        Gem.prototype.select = function () {
            if (!window["tutorial"]) {
                this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE + 0.2);
                if (this.mytype < 6) {
                    this._border.alpha = 1;
                }
            }
        };
        Gem.prototype.deselect = function () {
            this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE);
            this._border.alpha = 0;
        };
        Gem.prototype.isSame = function (a) {
            return (this._row == a.row) && (this._col == a.col);
        };
        Gem.prototype.isNext = function (gem2) {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        };
        return Gem;
    }(Phaser.Sprite));
    mygame.Gem = Gem;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var GemPatricle = (function (_super) {
        __extends(GemPatricle, _super);
        function GemPatricle(game, x, y, type) {
            var _this = _super.call(this, game, x, y) || this;
            _this.PARTICLE_NAMES = ["blue", "green", "pink", "red", "yellow"];
            _this._sprite = _this.game.add.sprite(0, 0, "atlas2", "particle_" + _this.PARTICLE_NAMES[type - 1]);
            _this.addChild(_this._sprite);
            _this._sprite.anchor.set(0.5);
            return _this;
        }
        GemPatricle.prototype.destoy = function () {
            this._sprite.destroy();
            _super.prototype.destroy.call(this);
        };
        return GemPatricle;
    }(Phaser.Sprite));
    mygame.GemPatricle = GemPatricle;
})(mygame || (mygame = {}));

var mygame;
(function (mygame) {
    var Lang = (function () {
        function Lang() {
            this.CONGRAT = { en: 'Congratulations!', de: '  Herzlichen Gluckwunsch !' };
            this.PLOT = { en: 'The plot\nis restored!', de: 'Ein Grundstuck\nrestauriert!' };
            this.TUTORIAL = { en: 'Collect fruits to upgrade garden!', de: 'Sammeln Sie Fruchte,\num den Garten zu verbessern!' };
        }
        Object.defineProperty(Lang, "Instance", {
            get: function () {
                if (this.instance === null || this.instance === undefined) {
                    this.instance = new Lang();
                }
                return this.instance;
            },
            enumerable: true,
            configurable: true
        });
        return Lang;
    }());
    Lang.loc = 'en';
    mygame.Lang = Lang;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Panel = (function (_super) {
        __extends(Panel, _super);
        function Panel(game, x, y) {
            var _this = _super.call(this, game, x, y) || this;
            _this._tweenRun = false;
            _this._delta = 0;
            _this._unclocksCounter = 0;
            _this._total = window["totalElemets"];
            _this._counter = 0;
            _this._statusText = _this.game.add.bitmapText(0, -73, "font", _this._counter + "/" + _this._total, 60);
            _this._statusText.anchor.set(0.5);
            _this._bigHouse = [];
            _this._smallHouse = [];
            _this._bridge = [];
            _this._fireworks = [];
            _this._myMask = _this.game.add.graphics(0, 0);
            _this.addChild(_this._myMask);
            _this._city = _this.game.add.sprite(0, 0, "atlas2", "cityCorrect");
            _this._city.anchor.set(0.5, 1);
            _this._city.mask = _this._myMask;
            _this._orange = _this.game.add.sprite(0, -275, "atlas2", "gold_city_back0X25");
            _this._orange.anchor.set(0.5, 0.5);
            _this._orange.scale.set(4, 4);
            _this.addChild(_this._orange);
            _this.addChild(_this._city);
            _this._fruitsCounter = 0;
            _this._finishWasFired = false;
            _this._fruitScale = 0.8;
            mygame.GameConfig.COUNTERS_PUBLIC = [999, 999, 999, 999, 999, 999];
            _this._scorePlank = _this.game.add.sprite(0, 200, "atlas2", "scorePlank");
            _this._scorePlank.anchor.set(0.5, 1);
            _this.addChild(_this._scorePlank);
            _this.createBigHouses();
            _this.createSmallHouses();
            _this.createBridges();
            _this.createProgressBar();
            _this._scorePlank.addChild(_this._statusText);
            _this._delta = 0;
            _this._timer = 0;
            _this._sleepTime = 1;
            if (mygame.Core.isLandscape) {
                _this.setLandscape();
            }
            else {
                _this.setPortrait();
            }
            return _this;
        }
        Panel.prototype.createBigHouses = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-212, -654, "atlas2", "bigHouse_" + postfix[i]);
                this._bigHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[-23, -544], [-165, -500], [-150, -642]];
            var _loop_1 = function (i) {
                var firework = this_1.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_1);
                this_1._city.addChild(firework);
                this_1._fireworks.push(firework);
            };
            var this_1 = this;
            for (var i = 0; i < 3; i++) {
                _loop_1(i);
            }
        };
        Panel.prototype.createSmallHouses = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-5, -565, "atlas2", "smallHouse_" + postfix[i]);
                this._smallHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[233, -511], [135, -480], [182, -535]];
            var _loop_2 = function (i) {
                var firework = this_2.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_2);
                this_2._city.addChild(firework);
                this_2._fireworks.push(firework);
            };
            var this_2 = this;
            for (var i = 0; i < 3; i++) {
                _loop_2(i);
            }
        };
        Panel.prototype.createBridges = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-43, -303, "atlas2", "bridge_" + postfix[i]);
                this._bridge.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[17, -276], [25, -330], [83, -300]];
            var _loop_3 = function (i) {
                var firework = this_3.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_3);
                this_3._city.addChild(firework);
                this_3._fireworks.push(firework);
            };
            var this_3 = this;
            for (var i = 0; i < 3; i++) {
                _loop_3(i);
            }
        };
        Panel.prototype.createProgressBar = function () {
            this._progressBar = this.game.add.sprite(0, 0);
            var firstBar = this.game.add.sprite(0, 0, "scoreMetr");
            firstBar.anchor.set(1, 1);
            var secondBar = this.game.add.sprite(0, 0, "scoreMetr");
            secondBar.scale.set(-1, 1);
            secondBar.anchor.set(1, 1);
            var w = firstBar.width * 2 - 30;
            var h = firstBar.height - 8;
            var bg = this.game.add.graphics(0, 0);
            bg.beginFill(0x232724)
                .drawRect(-w / 2, -h - 4, w, h)
                .endFill();
            bg.cacheAsBitmap = true;
            this._greenBar = this.game.add.graphics(0, 0);
            this._progressBar.addChild(bg);
            this._progressBar.addChild(this._greenBar);
            this._progressBar.addChild(firstBar);
            this._progressBar.addChild(secondBar);
            this._scorePlank.addChild(this._progressBar);
        };
        Object.defineProperty(Panel.prototype, "greenbarValue", {
            set: function (val) {
                var w = 141 * 2 - 30;
                var h = 46 - 14;
                this._greenBar.clear()
                    .beginFill(0x37b54e)
                    .drawRect(-w / 2, -h - 4, w * val, h)
                    .endFill();
            },
            enumerable: true,
            configurable: true
        });
        Panel.prototype.scaleFruits = function (val) {
        };
        Object.defineProperty(Panel.prototype, "finishScreenCallback", {
            set: function (val) {
                this._finishCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Panel.prototype.updateStatus = function () {
            if (this._counter > this._total) {
                this._counter = this._total;
            }
            this._statusText.text = this._counter + "/" + this._total;
            this.greenbarValue = this._counter / this._total;
        };
        Panel.prototype.replay = function () {
            this._finishWasFired = false;
            this._delta = 0;
            this._counter = 0;
            this._unclocksCounter = 0;
        };
        Panel.prototype.updateCounter = function (type, delta) {
            this._delta += delta;
            this._sleepTime = 200;
        };
        Panel.prototype.setLandscape = function () {
            this._myMask.clear();
            this._orange.angle = 0;
            this._orange.position.set(0, -275);
            this._city.position.set(-35, 50);
            this._city.scale.set(1, 1);
            this._myMask.y = -270 - 3;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._myMaskSize[0] / 2, -Panel._myMaskSize[1] / 2, Panel._myMaskSize[0], Panel._myMaskSize[1], 20);
            this._myMask.endFill();
            this._scorePlank.position.set(0, 70);
        };
        Panel.prototype.setPortrait = function () {
            this._city.position.set(-10, 150);
            this._city.scale.set(1.1, 1.1);
            this._myMask.position.set(0, -240);
            this._myMask.clear();
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._otherMaskSize[0] / 2, -Panel._otherMaskSize[1] / 2, Panel._otherMaskSize[0], Panel._otherMaskSize[1], 20);
            this._myMask.endFill();
            this._orange.angle = 90;
            this._orange.position.set(0, -240);
            this._scorePlank.position.set(0, 85);
        };
        Panel.prototype.getElementPosition = function (type) {
            if (mygame.Core.isLandscape) {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 70);
            }
            else {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 50);
            }
        };
        Panel.prototype.initFruits = function () {
        };
        Panel.prototype.unlockObject = function () {
            var _this = this;
            switch (this._unclocksCounter) {
                case 0:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_4 = function (i) {
                            this_4.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_4);
                        };
                        var this_4 = this;
                        for (var i = this._fireworks.length - 3; i < this._fireworks.length; i++) {
                            _loop_4(i);
                        }
                        this.game.add.tween(this._bridge[1]).to({
                            alpha: 0.7
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._bridge[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._bridge[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
                case 1:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_5 = function (i) {
                            this_5.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_5);
                        };
                        var this_5 = this;
                        for (var i = this._fireworks.length - 3 * 2; i < this._fireworks.length - 3; i++) {
                            _loop_5(i);
                        }
                        this.game.add.tween(this._smallHouse[1]).to({
                            alpha: 0.7
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._smallHouse[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._smallHouse[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
                case 2:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_6 = function (i) {
                            this_6.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_6);
                        };
                        var this_6 = this;
                        for (var i = 0; i < 3; i++) {
                            _loop_6(i);
                        }
                        this.game.add.tween(this._bigHouse[1]).to({
                            alpha: 1
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._bigHouse[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._bigHouse[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
            }
            this._unclocksCounter++;
        };
        Panel.prototype.hideText = function (text) {
        };
        Panel.prototype.update = function () {
            var dt = this.game.time.elapsedMS;
            if (this._timer > 0) {
                this._timer -= dt;
            }
            if (this._delta > 0) {
                this._timer = this._sleepTime;
                this._counter++;
                this._delta--;
                if (this._counter >= Math.floor(this._total * window["firstSpawn"]) && this._unclocksCounter == 0) {
                    this.unlockObject();
                }
                if (this._counter >= Math.floor(this._total * window["secondSpawn"]) && this._unclocksCounter == 1) {
                    this.unlockObject();
                }
                if (this._counter >= Math.floor(this._total * window["thirdSpawn"]) && this._unclocksCounter == 2) {
                    this.unlockObject();
                }
                this.updateStatus();
            }
        };
        return Panel;
    }(Phaser.Sprite));
    Panel._myMaskSize = [550 - 30, 700 - 54 + 2];
    Panel._otherMaskSize = [700 - 54 + 2, 550 - 30];
    mygame.Panel = Panel;
})(mygame || (mygame = {}));

var mygame;
(function (mygame) {
    var Sequence = (function () {
        function Sequence() {
            this._array = [];
            this._bombPosition = null;
        }
        Sequence.prototype.add = function (x, y) {
            if (!this.contains(x, y)) {
                this._array.push(new Phaser.Point(x, y));
            }
        };
        Object.defineProperty(Sequence.prototype, "bombPosition", {
            get: function () {
                return this._bombPosition;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sequence.prototype, "isBomb", {
            get: function () {
                if (this._array.length > 3) {
                    var n = Math.floor(this._array.length / 2);
                    if (this._bombPosition == null) {
                        this._bombPosition = new Phaser.Point(this._array[n].x, this._array[n].y);
                    }
                    return true;
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Sequence.prototype.setBombPosition = function (x, y) {
            this._bombPosition = new Phaser.Point(x, y);
        };
        Sequence.prototype.forEach = function (x) {
            this._array.forEach(x);
        };
        Sequence.prototype.contains = function (x, y) {
            for (var i = 0; i < this._array.length; i++) {
                var p = this._array[i];
                if (p.x == x && p.y == y) {
                    return true;
                }
            }
            return false;
        };
        Sequence.prototype.get = function (i) {
            return this._array[i];
        };
        Sequence.prototype.push = function (x, y) {
            this.add(x, y);
        };
        Sequence.prototype.merge = function (other) {
            var len = other.length;
            for (var i = 0; i < len; i++) {
                this._array.push(other.pop());
            }
        };
        Object.defineProperty(Sequence.prototype, "length", {
            get: function () {
                return this._array.length;
            },
            enumerable: true,
            configurable: true
        });
        Sequence.prototype.pop = function () {
            return this._array.pop();
        };
        return Sequence;
    }());
    mygame.Sequence = Sequence;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Boot = (function (_super) {
        __extends(Boot, _super);
        function Boot() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Boot.prototype.preload = function () {
            this.load.crossOrigin = 'anonymous';
            var baseURL = window['baseURL'];
            this.game.load.image('preloaderBar', baseURL + "assets/loader_plank.png");
            this.game.load.image("scoreMetr", baseURL + "assets/scoreMetr.png");
            this.game.load.image("logo", baseURL + "assets/logo.png");
        };
        Boot.prototype.create = function () {
            mygame.Controller.Instance.orientation = mygame.Controller.LANDSCAPE;
            if (window["orientation"] == "p")
                mygame.Controller.Instance.orientation = mygame.Controller.PORTRAIT;
            this.game.input.touch.preventDefault = false;
            this.game.stage.backgroundColor = 0x1d1d1d;
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            this.game.state.start('Preloader');
        };
        return Boot;
    }(Phaser.State));
    mygame.Boot = Boot;
})(mygame || (mygame = {}));

var mygame;
(function (mygame) {
    var Controller = (function () {
        function Controller() {
            this.balance = 500;
            this.dealTime = 5;
            this.playerMoney = 3000;
            this.soundsEnabled = true;
            this.width = 1280;
            this.height = 720;
            this.orientation = Controller.LANDSCAPE;
        }
        Object.defineProperty(Controller, "Instance", {
            get: function () {
                if (this.instance === null || this.instance === undefined) {
                    this.instance = new Controller();
                }
                return this.instance;
            },
            enumerable: true,
            configurable: true
        });
        Controller.prototype.playSound = function (soundName) {
            if (this.soundsEnabled) {
            }
        };
        return Controller;
    }());
    Controller.LANDSCAPE = "landscape";
    Controller.PORTRAIT = "portrait";
    mygame.Controller = Controller;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            var _this = _super.call(this, 1280, 720, Phaser.CANVAS, 'mdsp-creative', null, false, true) || this;
            if (window["innerMode"] == "client") {
                window["customClientWidth"] = function () {
                    return document.documentElement.clientWidth;
                };
                window["customClienHeight"] = function () {
                    return document.documentElement.clientHeight;
                };
            }
            mygame.Core.init(1280, 720);
            _this.state.add('Boot', mygame.Boot, false);
            _this.state.add('Preloader', mygame.Preloader, false);
            _this.state.add('PlayState', mygame.PlayState, false);
            _this.state.start('Boot');
            return _this;
        }
        return Game;
    }(Phaser.Game));
    mygame.Game = Game;
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var version = '1.0.0';
    var PlayState = (function (_super) {
        __extends(PlayState, _super);
        function PlayState() {
            var _this = _super.call(this, true) || this;
            _this._winTweens = [];
            return _this;
        }
        PlayState.prototype.create = function () {
            var _this = this;
            mygame.GameConfig.COUNTERS_PUBLIC = [0, window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            this.game.time.advancedTiming = true;
            this.game.stage.backgroundColor = "#000000";
            this._bg = this.game.add.sprite(0, 0, "bg");
            this._bg.anchor.set(0.5);
            var bgContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                .myScale(1.2)
                .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                .end();
            bgContainer.addChild(this._bg);
            this._winscreenShowed = false;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.smoothed = true;
            this._canShowEndScreen = false;
            mygame.Controller.Instance.width = getSize().width;
            mygame.Controller.Instance.height = getSize().height;
            this._container = new mygame.OSprite(mygame.Core.centerX + 200, 0)
                .otherXY(mygame.Core.centerY, 0)
                .end();
            this.game.world.addChild(this._container);
            this._borders = [];
            this._borders[0] = this.game.add.sprite(mygame.Core.width, mygame.Core.height, "atlas2", "ramk_rd");
            this._borders[0].anchor.set(1, 1);
            this._borders[1] = this.game.add.sprite(0, 0, "atlas2", "ramk_lt");
            this._borders[3] = this.game.add.sprite(mygame.Core.width, 0, "atlas2", "ramk_rt");
            this._borders[3].anchor.set(1, 0);
            ;
            this._borders[2] = this.game.add.sprite(0, mygame.Core.height, "atlas2", "ramk_ld");
            this._borders[2].anchor.set(0, 1);
            this._borders.forEach(function (x) { return _this.game.world.addChild(x); });
            if (window["collectable"]) {
                this._gamePanelOsprite = new mygame.OSprite(0, 0)
                    .myTopOffset(640)
                    .otherXY(mygame.Core.centerY, 0)
                    .otherTopOffset(230 + 400 - 35)
                    .myLeftOffset(300)
                    .end();
                this._panel = new mygame.Panel(this.game, 0, 0);
                if (this.game.device.iPad) {
                    this._gamePanelOsprite.myTopOffset(750).end();
                }
                this._gamePanelOsprite.addChild(this._panel);
                this._panel.initFruits();
            }
            mygame.GameConfig.gameArray = mygame.GameConfig.deepClone(mygame.GameConfig.gameArrayLandscape);
            this._logoOsprite = new mygame.OSprite(920, 0)
                .myTopOffset(5)
                .otherXY(mygame.Core.centerY, 0)
                .otherTopOffset(5)
                .end();
            this._logo = this.game.add.sprite(0, 0, "logo");
            this._logo.anchor.set(0.5, 0);
            this._logoOsprite.addChild(this._logo);
            if (window["collectable"]) {
                this._container = new mygame.OSprite(mygame.Core.centerX + 400 - 85, mygame.Core.centerY - 70 - 25)
                    .otherXY(mygame.Core.centerY + 30, mygame.Core.centerX + 220 - 25)
                    .end();
                this._field = new mygame.Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = function (type, delta) {
                    _this._panel.updateCounter(type, delta);
                };
                this._field.finalCallback = this.showWinScreen.bind(this);
            }
            else {
                this._container = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX - 80)
                    .end();
                this._field = new mygame.Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = function (type, delta) {
                    type + 1;
                };
                this._container.myScale(1).otherScale(1.15).end();
            }
            this.game.world.addChild(this._logoOsprite);
            this._field.firstMatchCallback = function () {
            };
            this._field.pivot.set(100 * mygame.GameConfig.gameArray[0].length / 2, 100 * mygame.GameConfig.gameArray[1].length / 5);
            this._playFree = this.game.add.sprite(0, 0, "button");
            if (window["particleOn"]) {
                this._fruitFallEmitter = this.game.add.emitter(0, -100, Math.ceil(4600 / window["fruitSpawRate"]));
                this._fruitFallEmitter.makeParticles("atlas2", ["F_1", "F_2", "F_3", "F_4", "F_5"]);
                this._fruitFallEmitter.minParticleSpeed.setTo(0, 200 * window["fruitSpawSpeed"]);
                this._fruitFallEmitter.maxParticleSpeed.setTo(0, 300 * window["fruitSpawSpeed"]);
                this.createEmitter();
            }
            this._playFreeBtnOsprite = new mygame.OSprite(mygame.Core.centerX + 300, 0).myBottomOffset(150).myScale(1).end();
            if (this.game.device.iPad) {
                this._playFreeBtnOsprite.myBottomOffset(300).end();
            }
            this._playFreeBtnOsprite.otherXY(mygame.Core.centerY, 0).otherBottomOffset(140).otherScale(0.95).end();
            this._playFree.anchor.set(0.5, 0.1);
            var buttonText = this.game.add.bitmapText(0, 25, "font", window["buttonText"], 50);
            buttonText.anchor.set(0.5, 0.1);
            this._playFree.addChild(buttonText);
            this._playFree.inputEnabled = true;
            this._playFree.events.onInputDown.add(function () {
                window["trackClick"]();
            }, this);
            this._playFreeBtnOsprite.addChild(this._playFree);
            if (!window["collectable"]) {
                this._playFree.alpha = 0;
                this._playFree.y = 300;
            }
            if (!mygame.Core.isLandscape) {
                this.onPortret();
            }
            if (window["tutorial"]) {
                this._playFreeBtnOsprite.visible = false;
                this._tutorialOver = this.game.make.sprite(-630 + 15 + 5, -160 + 40 - 5, "atlas2", "tutorial_mask");
                this._tutorialOver.scale.set(10);
                this._tutorialOver.alpha = 0.8;
                this._field.addChild(this._tutorialOver);
                var tutorialUP = this.game.add.graphics(0, -128);
                this._tutorialOver.addChild(tutorialUP);
                tutorialUP.beginFill(0x000000, 1);
                tutorialUP.drawRect(0, 0, 256, 128);
                tutorialUP.endFill();
                var tutorialDOWN = this.game.add.graphics(0, 72);
                this._tutorialOver.addChild(tutorialDOWN);
                tutorialDOWN.beginFill(0x000000, 1);
                tutorialDOWN.drawRect(0, 0, 128, 128);
                tutorialDOWN.endFill();
                var tutorialLEFT = this.game.add.graphics(-128, 0);
                this._tutorialOver.addChild(tutorialLEFT);
                tutorialLEFT.beginFill(0x000000, 1);
                tutorialLEFT.drawRect(0, 0, 128, 128);
                tutorialLEFT.endFill();
                var tutorialRIGHT = this.game.add.graphics(128, 0);
                this._tutorialOver.addChild(tutorialRIGHT);
                tutorialRIGHT.beginFill(0x000000, 1);
                tutorialRIGHT.drawRect(0, 0, 128, 128);
                tutorialRIGHT.endFill();
                this._hand = this.game.add.sprite(100 * 4 + 10 - mygame.GameConfig.BLOCK_SIZE * 2, 100 * 2 - 70, "atlas2", "Tutorial-arrow");
                this._hand.anchor.set(0.5);
                this._handTween = this.game.add.tween(this._hand).to({
                    y: 80
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._field.addChild(this._hand);
                this._tutorialContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .myBottomOffset(0)
                    .myLeftOffset(35)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                    .otherRightOffset(500 - 30)
                    .otherBottomOffset(0)
                    .otherScale(0.75)
                    .end();
                this.game.world.addChild(this._tutorialContainer);
                this._charTutorialSprite = this.game.make.sprite(0, 0, "atlas2", "tutorial_dedula");
                this._charTutorialSprite.anchor.set(0, 1);
                this._tutorialContainer.addChild(this._charTutorialSprite);
                this._charTutorialSprite.position.set(0, this._charTutorialSprite.width + 100);
                this.game.add.tween(this._charTutorialSprite).to({
                    y: 0
                }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                this._bubbleTutorialText = this.game.add.bitmapText(this._charTutorialSprite.width / 2, -200 - 50, "font2", window["tutorialText"][0], 35);
                this._bubbleTutorialText.maxWidth = 500;
                this._bubbleTutorialText.anchor.setTo(0.5, 0.5);
                this._bubbleTutorialText.align = "center";
                this._bubbleTutorialText.tint = 0x733601;
                this._charTutorialSprite.addChild(this._bubbleTutorialText);
            }
            setTimeout(function () { _this._field.endGame(); }, window["endGameTimer"]);
            this._timerInstall = 0;
            window['gameStarted'];
            this.game.time.events.loop(Phaser.Timer.SECOND, this.tick, this);
        };
        PlayState.prototype.createEmitter = function () {
            this._safeEmitter = this.game.add.emitter(0, 60, 70);
            this._safeEmitter.makeParticles("atlas2", ["particle"]);
            this._safeEmitter.setAlpha(1, 0, 1000);
            this._safeEmitter.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            this._safeEmitter.setXSpeed(-50, 50);
            this._safeEmitter.setYSpeed(-50, 50);
            this._safeEmitter.gravity = 0;
            this._safeEmitter.start(false, 1000, 2);
            this._safeEmitter.width = this._logo.width;
            this._safeEmitter.height = this._logo.height;
            this._logo.addChild(this._safeEmitter);
            var buttonParcticle = this.game.add.emitter(0, 50, 70);
            buttonParcticle.makeParticles("atlas2", ["particle"]);
            buttonParcticle.setAlpha(1, 0, 1000);
            buttonParcticle.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            buttonParcticle.setXSpeed(-50, 50);
            buttonParcticle.setYSpeed(-50, 50);
            buttonParcticle.gravity = 0;
            buttonParcticle.start(false, 1000, 2);
            buttonParcticle.width = this._playFree.width - 300;
            buttonParcticle.height = this._playFree.height - 50;
            this._playFree.addChild(buttonParcticle);
        };
        PlayState.prototype.tick = function () {
            this._timerInstall++;
            window['playTime'] = this._timerInstall;
        };
        PlayState.prototype.getGurrentElementPosition = function (type) {
            return this._panel.getElementPosition(type);
        };
        PlayState.prototype.update = function () {
            if (window["collectable"]) {
                this._panel.update();
            }
            if (this._field) {
                this._field.update();
            }
            if (window["particleOn"]) {
            }
        };
        PlayState.prototype.destroyTutorial = function () {
            var _this = this;
            this._handTween.stop();
            this._hand.destroy();
            this._playFreeBtnOsprite.visible = true;
            this.game.add.tween(this._charTutorialSprite).to({
                y: this._charTutorialSprite.height + 100
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                _this._tutorialContainer.destroy(true);
            }, this);
            this.game.add.tween(this._tutorialOver).to({
                alpha: 0
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                _this._tutorialOver.destroy(true);
            }, this);
        };
        PlayState.prototype.render = function () {
        };
        PlayState.prototype.repositionEmitter = function () {
            var dx;
            if (mygame.Core.isLandscape) {
                dx = mygame.Core.centerX - 20;
            }
            else {
                dx = mygame.Core.centerY - 20;
            }
            this._fruitFallEmitter.emitX = this.game.rnd.between(-dx, dx);
            this.game.time.events.add(window["fruitSpawRate"], this.repositionEmitter.bind(this));
        };
        PlayState.prototype.showWinScreen = function () {
            if (!this._winscreenShowed) {
                if (window["sounds"]) {
                    this.game.sound.play("5", 0.4);
                }
                this._winscreenShowed = true;
                this._playFree.visible = false;
                this._playFree.inputEnabled = false;
                this._playFreeBtnOsprite.removeChild(this._playFree);
                this._panelContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                    .end();
                this.game.stage.addChild(this._panelContainer);
                this._backOverlay = this.game.add.graphics(0, 0);
                this._panelContainer.addChild(this._backOverlay);
                this._backOverlay.beginFill(0x251d1a, 0.8);
                this._backOverlay.drawRect(-1024, -1024, 2048, 2048);
                this._backOverlay.endFill();
                if (window["particleOn"]) {
                    this._panelContainer.addChild(this._fruitFallEmitter);
                    if (mygame.Core.isLandscape) {
                        this._fruitFallEmitter.emitY = -mygame.Core.centerY - 100;
                    }
                    else {
                        this._fruitFallEmitter.emitY = -mygame.Core.centerX - 100;
                    }
                    this._fruitFallEmitter.start(false, 4600, window["fruitSpawRate"]);
                    this.repositionEmitter();
                }
                this._panelSprite = this.game.make.sprite(0, 0, "atlas2", "final_popup");
                this._panelSprite.anchor.set(0.5);
                this._sunraySprite = this.game.make.sprite(0, 0, "atlas2", "lucksherry_sunray");
                this._sunraySprite.scale.set(7);
                this._sunraySprite.anchor.set(0.5);
                this._sunraySprite.alpha = 0.6;
                this._panelContainer.addChild(this._sunraySprite);
                this.game.add.tween(this._sunraySprite).to({ angle: 360 }, 18500, Phaser.Easing.Linear.None, true, 0, -1);
                this._panelContainer.addChild(this._panelSprite);
                this._panelSprite.addChild(this._playFree);
                this._playFree.position.set(0, 177 + 40);
                this._playFree.anchor.x = 0.5;
                this._playFree.scale.set(0.85, 0.85);
                this._playFree.visible = true;
                this._playFree.inputEnabled = true;
                this._congrateText = this.game.add.bitmapText(0, 45, "font2", window["tutorialText"][1], 50);
                this._congrateText.anchor.setTo(0.5, 0.5);
                this._congrateText.align = "center";
                this._congrateText.tint = 0x733601;
                this._panelSprite.addChild(this._congrateText);
                this._plotText = this.game.add.bitmapText(0, 108 + 35, "font2", window["tutorialText"][2], 50);
                this._plotText.maxWidth = 500;
                this._plotText.anchor.setTo(0.5, 0.5);
                this._plotText.align = "center";
                this._plotText.tint = 0x733601;
                this._panelSprite.addChild(this._plotText);
                this._panelContainer.alpha = 0;
                this._panelSprite.scale.set(0.1, 0.1);
                this.game.add.tween(this._panelSprite.scale).to({ x: 1, y: 1 }, 700, Phaser.Easing.Back.Out, true);
                this.game.add.tween(this._panelContainer).to({ alpha: 1 }, 700, Phaser.Easing.Sinusoidal.Out, true);
            }
        };
        PlayState.prototype.onLandscape = function () {
            if (this._bg) {
                this._borders[0].position.set(mygame.Core.width, mygame.Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(mygame.Core.width, 0);
                this._borders[2].position.set(0, mygame.Core.height);
            }
            if (this._panel) {
                this._panel.setLandscape();
                if (window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -mygame.Core.centerY - 100;
                }
            }
        };
        PlayState.prototype.onPortret = function () {
            if (this._bg) {
                this._borders[0].position.set(mygame.Core.width, mygame.Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(mygame.Core.width, 0);
                this._borders[2].position.set(0, mygame.Core.height);
            }
            if (this._panel) {
                this._panel.setPortrait();
                if (window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -mygame.Core.centerX - 100;
                }
            }
        };
        return PlayState;
    }(mygame.OState));
    mygame.PlayState = PlayState;
    function getSize(log) {
        if (log === void 0) { log = false; }
        var w = 0;
        var h = 0;
        var deW = 0;
        var deH = 0;
        if (!(document.documentElement.clientWidth == 0)) {
            deW = document.documentElement.clientWidth;
            deH = document.documentElement.clientHeight;
        }
        w = deW;
        h = deH;
        if (window.innerWidth > window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        return { width: w, height: h };
    }
    function sendEvent(value, params) {
        if (params === void 0) { params = null; }
        window["trackEvent"](value, params);
    }
    function ClickInstall() {
        window["trackClick"]();
    }
})(mygame || (mygame = {}));

var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Preloader = (function (_super) {
        __extends(Preloader, _super);
        function Preloader() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        Preloader.prototype.preload = function () {
            var _this = this;
            this.preloadContainer = this.add.sprite(0, 0, "");
            this.preloadContainer.anchor.set(0.5, 0.5);
            this.preloadLogo = this.add.image(0, 0, 'logo');
            this.preloadContainer.addChild(this.preloadLogo);
            this.preloadLogo.position.set(-this.preloadLogo.width / 2, 0);
            this.preloadLogo.y -= 70 - 20;
            this.preloadBar = this.add.image(0, 0, 'preloaderBar');
            this.preloadContainer.addChild(this.preloadBar);
            this.preloadBar.position.set(-this.preloadBar.width / 2, 0);
            this.preloadBar.y += 40 + 20;
            this.preloadBarBackgroundL = this.add.image(this.preloadBar.x, this.preloadBar.y, 'scoreMetr');
            this.preloadContainer.addChild(this.preloadBarBackgroundL);
            this.preloadBarBackgroundR = this.add.image(this.preloadBar.x + this.preloadBarBackgroundL.width * 2, this.preloadBar.y, 'scoreMetr');
            this.preloadContainer.addChild(this.preloadBarBackgroundR);
            this.preloadBarBackgroundR.scale.set(-1, 1);
            this.load.setPreloadSprite(this.preloadBar);
            this.ChangeSize();
            var baseURL = window['baseURL'];
            this.game.load.image("bg", baseURL + "assets/" + window["bg"]);
            this.game.load.atlas("atlas2", baseURL + "assets/atlas.png", "assets/atlas.json");
            this.game.load.image("button", baseURL + "assets/" + window["button"]);
            this.game.load.bitmapFont('font', baseURL + "assets/font_big_gradient.png", baseURL + "assets/font_big_gradient.fnt");
            this.game.load.bitmapFont('font2', baseURL + "assets/font_small_white.png", baseURL + "assets/font_small_white.fnt");
            if (window["sounds"]) {
                var sounds = [1, 3, 5, 12, 14, 15, 16, 17, 19, 21, 22, 23];
                sounds.forEach(function (x) { return _this.game.load.audio(x.toString(), baseURL + "assets/sounds/" + x + ".mp3"); });
            }
        };
        Preloader.prototype.shutdown = function () {
            this.preloadLogo.destroy();
            this.preloadLogo = null;
            this.preloadBar.destroy();
            this.preloadBar = null;
            this.preloadBarBackgroundL.destroy();
            this.preloadBarBackgroundL = null;
            this.preloadBarBackgroundR.destroy();
            this.preloadBarBackgroundR = null;
            this.preloadContainer.destroy();
            this.preloadContainer = null;
        };
        Preloader.prototype.create = function () {
            mygame.Core.begin(this.game, !this.game.device.desktop);
            this.game.state.start('PlayState');
        };
        Preloader.prototype.ChangeSize = function () {
            this.game.scale.setGameSize(window.innerWidth, window.innerHeight);
            if (window.innerHeight > window.innerWidth) {
                this.preloadContainer.scale.set((window.innerWidth * 0.85) / 469, (window.innerWidth * 0.85) / 469);
            }
            else {
                this.preloadContainer.scale.set((window.innerWidth * 0.85) / 469, (window.innerWidth * 0.85) / 469);
            }
            this.preloadContainer.position.set(window.innerWidth / 2, window.innerHeight / 2 - 40);
        };
        return Preloader;
    }(Phaser.State));
    mygame.Preloader = Preloader;
})(mygame || (mygame = {}));
