/**
 * Created by DEaDA on 3/25/17.
 */
module mygame {
    export class Preloader extends Phaser.State {
        preloadLogo;
        preloadBar;
        preloadBarBackgroundL;
        preloadBarBackgroundR;
        preloadContainer;

        preload() {
            this.preloadContainer = this.add.sprite(0, 0, "");
            this.preloadContainer.anchor.set(0.5, 0.5)
            this.preloadLogo = this.add.image(0, 0, 'logo');
            this.preloadContainer.addChild(this.preloadLogo);
            this.preloadLogo.position.set(-this.preloadLogo.width/2, 0)
            this.preloadLogo.y -= 70 - 20;

            this.preloadBar = this.add.image(0, 0, 'preloaderBar');
            this.preloadContainer.addChild(this.preloadBar);
            this.preloadBar.position.set(-this.preloadBar.width/2, 0)
            this.preloadBar.y += 40 + 20;
            this.preloadBarBackgroundL = this.add.image(this.preloadBar.x, this.preloadBar.y, 'scoreMetr');   
            this.preloadContainer.addChild(this.preloadBarBackgroundL);         
            this.preloadBarBackgroundR = this.add.image(this.preloadBar.x+this.preloadBarBackgroundL.width*2, this.preloadBar.y, 'scoreMetr');    
            this.preloadContainer.addChild(this.preloadBarBackgroundR); 
            this.preloadBarBackgroundR.scale.set(-1,1);                  
            
            this.load.setPreloadSprite(this.preloadBar);


            this.ChangeSize();

            let baseURL = window['baseURL'];

            
            this.game.load.image("bg", baseURL + "assets/" + window["bg"]);
            this.game.load.atlas("atlas2", baseURL + "assets/atlas.png", "assets/atlas.json");
            this.game.load.image("button", baseURL + "assets/" + window["button"]);

            this.game.load.bitmapFont('font', baseURL + "assets/font_big_gradient.png", baseURL + "assets/font_big_gradient.fnt");
            this.game.load.bitmapFont('font2', baseURL + "assets/font_small_white.png", baseURL + "assets/font_small_white.fnt");

            if (window["sounds"]) {
                let sounds: number[] = [1, 3, 5, 12, 14, 15, 16, 17, 19, 21, 22, 23];
                sounds.forEach(x => this.game.load.audio(x.toString(), baseURL + "assets/sounds/" + x + ".mp3"));
            }
        }

        public shutdown(): void {
            this.preloadLogo.destroy();
            this.preloadLogo = null;
            this.preloadBar.destroy();
            this.preloadBar = null;
            this.preloadBarBackgroundL.destroy();
            this.preloadBarBackgroundL = null;
            this.preloadBarBackgroundR.destroy();
            this.preloadBarBackgroundR = null;
            this.preloadContainer.destroy();
            this.preloadContainer = null;
        }

        create() {
            Core.begin(this.game, !this.game.device.desktop);
            this.game.state.start('PlayState');
        }

        ChangeSize() {
            this.game.scale.setGameSize(window.innerWidth, window.innerHeight);
            
            if (window.innerHeight > window.innerWidth) {
                //console.log("PORT",window.innerWidth, window.innerHeight);
                this.preloadContainer.scale.set((window.innerWidth*0.85)/469, (window.innerWidth*0.85)/469);
                //console.log(window.innerWidth*0.7);
                // this.preloadContainer.position.set(window.innerWidth/2,window.innerHeight/2);
            } else {
                //console.log("LAND",window.innerWidth, window.innerHeight);
                this.preloadContainer.scale.set((window.innerWidth*0.85)/469, (window.innerWidth*0.85)/469);
                //console.log(window.innerWidth*1.3);
                //this.preloadContainer.position.set(window.innerWidth/2, window.innerHeight/2);
                
                //this.preloadContainer.position.set(0,0);
            }

            this.preloadContainer.position.set(window.innerWidth/2,window.innerHeight/2-40);

        }
    }
}