module mygame {
    export class GameConfig {
        public static readonly BLOCK_SIZE: number = 92;
        public static readonly SWAP_TIME: number = 150;
        public static readonly DESTROY_TIME: number = 150;
        public static readonly FALLDOWN_TIME: number = 400;
        public static readonly FLY_TIME: number = 500 * 1.15;
        public static readonly CONDENSATION_TIME: number = 300;
        public static readonly COUNTERS: number[] = [0, 2, 1, 0, 0, 0, 0, 0];
        public static COUNTERS_PUBLIC: number[] = [0, 2, 1, 0, 0, 0, 0, 0];
        public static readonly NAMES: string[] = ["apple", "apple", "grapes", "flower", "pear", "leaf"];
        public static readonly PANEL_GEM_SCALE: number = 0.8;
        public static readonly APPLE_POS: Phaser.Point = new Phaser.Point(100, -60);
        public static readonly GRAPES_POS: Phaser.Point = new Phaser.Point(370, -60);
        public static readonly UNLOCK_OBJ_PER_FRUITS: number = 3;
        public static readonly GEM_DEFAULT_SCALE: number = 1;
        public static gameArray: number[][];

         public static gameArrayLandscape: number[][] = [
            [-1, 5, 3, 2, 4, 1, -1],
            [2, 3, 3, 2, 1, 1, 5],
            [3, 1, 2, 4, 4, 5, 1],
            [4, 3, 5, 2, 4, 5, 1],
            [-1, 5, 4, 1, 5, 4, -1]
            ];

        // public static gameArrayLandscape: number[][] = [
        //     [-1, 5, 3, 2, 4, 1, -1],
        //     [2, 3, 3, 2, 1, 1, 5],
        //     [3, 1, 2, 4, 4, 5, 1],
        //     [4, 3, 5, 2, 4, 5, 1],
        //     [-1, 5, 4, 1, 5, 4, -1]
        //     ];

        public static deepClone(m: number[][]): number[][] {
            let result = [];
            for (let i = 0; i < m.length; i++) {
                result.push([]);
                for(let j = 0; j < m[0].length; j++) {
                    result[i].push(m[i][j]);
                }
            }
            return result;
        }

        public  static transpose(m: number[][]): number[][] {
            let result = [];
            for (let i = 0; i < m[0].length; i++) {
                result.push([]);
                for (let j = 0; j < m.length; j++) {
                    result[i].push(m[j][i]);
                }
            }
            return result;
        }

        public static enableDrag(sprite: any): void {
            return;
            // sprite.inputEnabled = true;
            // sprite.input.enableDrag();
            // sprite.events.onDragUpdate.add(() => {
            // });
        }

    }
}