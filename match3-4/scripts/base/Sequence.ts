module mygame {
    export class Sequence {
        private _array: Phaser.Point[];
        private _bombPosition: Phaser.Point;

        constructor() {
            this._array = [];
            this._bombPosition = null;
        }

        public add(x: number, y: number): void {
            if (!this.contains(x, y)) {
                this._array.push(new Phaser.Point(x, y));
            }
        }

        public get bombPosition(): Phaser.Point {
            return this._bombPosition;
        }

        public get isBomb(): boolean {
            if (this._array.length > 3) {
                let n: number = Math.floor(this._array.length / 2);
                if (this._bombPosition == null) {
                    this._bombPosition = new Phaser.Point(this._array[n].x, this._array[n].y);
                }

                return true;
            }
            return false;
        }

        public setBombPosition(x: number, y: number): void {
            this._bombPosition = new Phaser.Point(x, y);
        }

        public forEach(x: (value: Phaser.Point, index: number, array: Phaser.Point[]) => void): void {
            this._array.forEach(x);
        }

        public contains(x: number, y: number): boolean {
            for (let i: number = 0; i < this._array.length; i++) {
                let p: Phaser.Point = this._array[i];
                if (p.x == x && p.y == y) {
                    return true;
                }
            }
            return false;
        }

        public get(i: number): Phaser.Point {
            return this._array[i];
        }

        public push(x: number, y: number): void {
            this.add(x, y);
        }

        public merge(other: Sequence): void {
            let len: number = other.length;
            for (let i: number = 0; i < len; i++) {
                this._array.push(other.pop());
            }
        }

        public get length(): number {
            return this._array.length;
        }

        public pop(): Phaser.Point {
            return this._array.pop();
        }
    }
}
