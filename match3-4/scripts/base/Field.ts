module mygame {
    export enum BlockTypes {
        DISABLED,
        ACTIVE
    };

    export interface Block {
        col: number,
        row: number,
        sprite: Phaser.Sprite;
        state: BlockTypes;
        type: number;
    };

    export interface Hole {
        col: number;
        row: number;
        length: number;
        hasHoleAbove: boolean;
    };

    export class Field extends Phaser.Sprite {
        private _gameArray: any[][];
        private _selectedGem: Gem;
        private _canClick: boolean;
        private _pickedGem: Gem;
        private _removeMap: number[][];
        private _destroyed: number;
        private _fieldUpperBound: number[] = [];
        private _cb: Function;
        private _coolAnimation: boolean;
        private _verticalAnimation: boolean;
        private _counter: number;
        private _streak: number[];
        private _container: Phaser.Sprite;
        private _myMask: Phaser.Graphics;
        private _turnOffMask: boolean;
        private _selectedPoint: Phaser.Point;
        private _bgContainer: Phaser.Sprite;

        private _firstMatch: boolean;
        private _firstMatchCallback: Function;
        private _endMatchCallback: Function;
        private _disabled: boolean;
        private _finalWasFired: boolean;
        private _finalCallback: Function;
        private _playState: PlayState;
        private _tweenArray: Phaser.Tween[];
        private _tempSprite: Phaser.Sprite[];

        private _fruit1Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit2Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit3Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit4Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit5Emmiter: Phaser.Particles.Arcade.Emitter;

        private _fruitSpecialEmmiter1: Phaser.Particles.Arcade.Emitter;
        private _fruitSpecialEmmiter2: Phaser.Particles.Arcade.Emitter;
        private _fruitSpecialEmmiter3: Phaser.Particles.Arcade.Emitter;

        private _gem1ToFollow: Gem;
        private _gem2ToFollow: Gem;
        private _gem3ToFollow: Gem;

        private _followGem1: boolean;
        private _followGem2: boolean;
        private _followGem3: boolean;

        private _maxColorStreak: number;
        private _border: Phaser.Sprite;
        private _borderTween: Phaser.Tween;

        private _condensationPoint: Phaser.Point;
        private _createBomb: boolean;
        private _boomAnimation: boolean;
        private _background: Phaser.Sprite;
        private _bombRange: number;

        private _sequences: Sequence[];
        private _bombPosition: Phaser.Point;

        private _lightnings: Phaser.Sprite[];

        constructor(game: Phaser.Game, x: number, y: number, playState: PlayState) {
            super(game, x, y);

            this._followGem1 = false;
            this._followGem2 = false;
            this._followGem3 = false;

            this._background = this.game.add.sprite(-15, -15, "atlas2", "field");
            this._background.inputEnabled = true;
            this._background.anchor.set(0, 0);
            this.addChild(this._background);


            this._maxColorStreak = 0;
            this._boomAnimation = false;
            this._sequences = [];

            this._playState = playState;
            this._tweenArray = [];
            this._tempSprite = [];

            this._destroyed = 0;
            this._turnOffMask = false;
            this._firstMatch = false;
            this._finalWasFired = false;
            this._gameArray = GameConfig.gameArray;
            this._fieldUpperBound = [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
            this._streak = [];
            for (let i: number = 0; i < 6; i++) {
                this._streak.push(0);
            }
            this._disabled = false;

            this._bgContainer = this.game.add.sprite(0, 0);
            this._bgContainer.anchor.set(0.5, 0);
            this.addChild(this._bgContainer);

            this._container = this.game.add.sprite(0, 0);
            this._container.anchor.set(0.5, 0);
            this.addChild(this._container);
            this._myMask = this.game.add.graphics(0, 0);
            this.addChild(this._myMask);
            this._container.mask = this._myMask;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRect(GameConfig.BLOCK_SIZE, 0, 5 * GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE);
            this._myMask.drawRect(0, GameConfig.BLOCK_SIZE, 7 * GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE * 3);
            this._myMask.drawRect(GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE * 4, 5 * GameConfig.BLOCK_SIZE, GameConfig.BLOCK_SIZE);
            this._myMask.endFill();

            this.drawField();
            this._selectedGem = null;
            if (window["collectable"]) {
                this._counter = 0;
                // this._counters = [0,window["firstElement"],0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            } else {
                this._counter = 0;
                // this._counters = [-1, -1, -1, -1, -1, -1, -1, -1];
            }

            this.game.input.onUp.add(() => {
                this.game.input.deleteMoveCallback(this.gemMove, this);
                if ((this._selectedGem !== null && this._selectedGem !== undefined) && this._selectedGem.isBomb && (this._pickedGem == null)) {
                    this._removeMap = [];
                    for (let i: number = 0; i < this._gameArray.length; i++) {
                        this._removeMap[i] = [];
                        for (let j: number = 0; j < this._gameArray[i].length; j++) {
                            this._removeMap[i].push(0);
                        }
                    }
                        this.setOff(this._selectedGem.mytype - 5);
                        this.destroyGems();
                }
            }, this);
            this._canClick = true;

            if(window["particleOn"]) {
                // this._fruit1Emmiter = this.game.add.emitter(0, 0, 50);
                // this.addChild(this._fruit1Emmiter);
                // this._fruit1Emmiter.makeParticles("animation", Phaser.Animation.generateFrameNames("smoke_", 10001, 10015));
                //this._fruit1Emmiter.gravity.set(0, 0);
                // this._fruit1Emmiter.minParticleSpeed.setTo(-300, -300);
                // this._fruit1Emmiter.maxParticleSpeed.setTo(300, 300);
                // this._fruit1Emmiter.setScale(1.2, 0, 1.2, 0, 1200, Phaser.Easing.Quintic.Out);

                // this._fruit2Emmiter = this.game.add.emitter(0, 0, 50);
                // this.addChild(this._fruit2Emmiter);
                // this._fruit2Emmiter.makeParticles("atlas", "F_2");
                // //this._fruit2Emmiter.gravity.set(0, 0);
                // this._fruit2Emmiter.minParticleSpeed.setTo(-300, -300);
                // this._fruit2Emmiter.maxParticleSpeed.setTo(300, 300);
                // this._fruit2Emmiter.setScale(1.2, 0, 1.2, 0, 1200, Phaser.Easing.Quintic.Out);

                // this._fruit3Emmiter = this.game.add.emitter(0, 0, 50);
                // this.addChild(this._fruit3Emmiter);
                // this._fruit3Emmiter.makeParticles("atlas", "F_3");
                // //this._fruit3Emmiter.gravity.set(0, 0);
                // this._fruit3Emmiter.minParticleSpeed.setTo(-300, -300);
                // this._fruit3Emmiter.maxParticleSpeed.setTo(300, 300);
                // this._fruit3Emmiter.setScale(1.2, 0, 1.2, 0, 1200, Phaser.Easing.Quintic.Out);

                // this._fruit4Emmiter = this.game.add.emitter(0, 0, 50);
                // this.addChild(this._fruit4Emmiter);
                // this._fruit4Emmiter.makeParticles("atlas", "F_4");
                // //this._fruit4Emmiter.gravity.set(0, 0);
                // this._fruit4Emmiter.minParticleSpeed.setTo(-300, -300);
                // this._fruit4Emmiter.maxParticleSpeed.setTo(300, 300);
                // this._fruit4Emmiter.setScale(1.2, 0, 1.2, 0, 1200, Phaser.Easing.Quintic.Out);

                // this._fruit5Emmiter = this.game.add.emitter(0, 0, 50);
                // this.addChild(this._fruit5Emmiter);
                // this._fruit5Emmiter.makeParticles("atlas", "F_5");
                // //this._fruit5Emmiter.gravity.set(0, 0);
                // this._fruit5Emmiter.minParticleSpeed.setTo(-300, -300);
                // this._fruit5Emmiter.maxParticleSpeed.setTo(300, 300);
                // this._fruit5Emmiter.setScale(1.2, 0, 1.2, 0, 1200, Phaser.Easing.Quintic.Out);

                // this._fruitSpecialEmmiter1 = this.game.add.emitter(0, 0, 250);
                // this.game.world.addChild(this._fruitSpecialEmmiter1);
                // this._fruitSpecialEmmiter1.makeParticles("animation", Phaser.Animation.generateFrameNames("smoke_", 10001, 10015));
                // // this._fruitSpecialEmmiter1.makeParticles("atlas", "F_1"); //1, 3, 4
                // this._fruitSpecialEmmiter1.gravity = 0;
                // this._fruitSpecialEmmiter1.minParticleSpeed.setTo(0, 0);
                // this._fruitSpecialEmmiter1.maxParticleSpeed.setTo(0, 0);
                // this._fruitSpecialEmmiter1.setRotation(0, 0);
                // this._fruitSpecialEmmiter1.setScale(1, 0.0, 1, 0.0, 1000, Phaser.Easing.Quintic.Out);
                // // this._fruitSpecialEmmiter1.

                // this._fruitSpecialEmmiter1.start(false, 500, 10);
                // this._fruitSpecialEmmiter1.on = false;

                // this._fruitSpecialEmmiter2 = this.game.add.emitter(0, 0, 250);
                // this.game.world.addChild(this._fruitSpecialEmmiter2);
                // this._fruitSpecialEmmiter2.makeParticles("atlas", "F_3"); //1, 3, 4
                // //this._fruitSpecialEmmiter2.gravity.set(0, 0);
                // this._fruitSpecialEmmiter2.minParticleSpeed.setTo(-300, -300);
                // this._fruitSpecialEmmiter2.maxParticleSpeed.setTo(300, 300);
                // this._fruitSpecialEmmiter2.setScale(1.2, 0, 1.2, 0, 600, Phaser.Easing.Quintic.Out);

                // this._fruitSpecialEmmiter2.start(false, 500, 5);
                // this._fruitSpecialEmmiter2.on = false;

                // this._fruitSpecialEmmiter3 = this.game.add.emitter(0, 0, 250);
                // this.game.world.addChild(this._fruitSpecialEmmiter3);
                // this._fruitSpecialEmmiter3.makeParticles("atlas", "F_4"); //1, 3, 4
                // //this._fruitSpecialEmmiter3.gravity.set(0, 0);
                // this._fruitSpecialEmmiter3.minParticleSpeed.setTo(-300, -300);
                // this._fruitSpecialEmmiter3.maxParticleSpeed.setTo(300, 300);
                // this._fruitSpecialEmmiter3.setScale(1.2, 0, 1.2, 0, 600, Phaser.Easing.Quintic.Out);

                // this._fruitSpecialEmmiter3.start(false, 500, 5);
                // this._fruitSpecialEmmiter3.on = false;
            }
            this.createLightning();

            this.game.time.events.add(window["restartBlinkTime"], this.blink, this);

            this._createBomb = false;
            this._condensationPoint = new Phaser.Point(0, 0);
        }

        private createLightning(): void {
            this._lightnings = [];
            let angles: number[] = [0, -90, 90, 180];
            for (let i: number = 0; i < 4; i++) {
                let lightning: Phaser.Sprite = this.game.add.sprite(100, 100, "atlas2", "light_line_10001");
                lightning.anchor.set(0, 0.5);
                lightning.scale.x = 5;
                lightning.alpha = 0;
                lightning.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(() => {
                    lightning.alpha = 0;
                }, this);
                lightning.angle = angles[i];
                this._container.addChild(lightning);
                this._lightnings.push(lightning);
                // let sprite: Phaser.Sprite = this.game.add.sprite()
            }
        }

        public set firstMatchCallback(val: Function) {
            this._firstMatchCallback = val;
        }

        public set endMatchCallback(val: Function) {
            this._endMatchCallback = val;
        }

        public set disableField(val: boolean) {
            this._disabled = val;
        }

        public set destroyCallback(val: Function) {
            this._cb = val;
        }

        public set finalCallback(val: Function) {
            this._finalCallback = val;
        }

        private blink(): void {
            let id: number = this.game.rnd.between(1, 5);
            let arr: Gem[] = [];
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype == id) {
                        arr.push(this._gameArray[i][j]);
                    }
                }
            }
            arr.forEach(x => x.playBlink());

            this.game.time.events.add(window["restartBlinkTime"], this.blink, this);
            // this._gameArray[]
        }

        private gemMove(event: any, pX: number, pY: number): void {
            let distX: number = pX - this._selectedPoint.x ;
            let distY: number = pY - this._selectedPoint.y;

            let deltaRow = 0;
            let deltaCol = 0;

            if (Math.abs(distX) > GameConfig.BLOCK_SIZE /2 ) {
                if (distX > 0) {
                    deltaCol = 1
                } else {
                    deltaCol = -1;
                }
            } else {
                if (Math.abs(distY) > GameConfig.BLOCK_SIZE / 2) {
                    if (distY > 0) {
                        deltaRow = 1;
                    } else {
                        deltaRow = -1;
                    }
                }
            }

            if (deltaRow + deltaCol != 0) {
                let k: number = this._selectedGem.row + deltaRow;
                let h: number = this._selectedGem.col + deltaCol;
                if (k >= 0 && k < this._gameArray.length && h >= 0 && h < this._gameArray[0].length) {
                    this._pickedGem = this._gameArray[this._selectedGem.row + deltaRow][this._selectedGem.col + deltaCol];
                    if (this._pickedGem.mytype > 0) {
                        this._selectedGem.deselect();
                        this.swapGems(true);
                        this.game.input.deleteMoveCallback(this.gemMove, this);
                    } else {
                        this._pickedGem = null;
                    }
                } else {
                    this._pickedGem = null;
                }
            }

        }



        private drawField(): void {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    let type: number = this._gameArray[i][j];
                    let x: number = GameConfig.BLOCK_SIZE * (j + 0.5);
                    let y: number = GameConfig.BLOCK_SIZE * (i + 0.5);
                    if (type > 0) {
                        let sprite: Gem = new Gem(this.game, x, y, type);
                        // sprite.tint = 0x00;
                        this._container.addChild(sprite);
                        sprite._sprite.inputEnabled = true;
                        sprite.row = i;
                        sprite.col = j;

                        // this.game.debug.body(sprite);

                        sprite._sprite.events.onInputDown.add((obj: Phaser.Sprite, pointer: Phaser.Pointer) => {
                            let tutorialCondition;
                            tutorialCondition = window["tutorial"] && (sprite.row == 2) && (sprite.mytype == 2) || !window["tutorial"];
                            tutorialCondition = tutorialCondition || window["tutorial"] && (sprite.col == 3) && (sprite.mytype == 4);

                            if (this._canClick && tutorialCondition && !this._disabled) {
                                if (this._selectedGem == null) {
                                    this._selectedGem = sprite;
                                    this._selectedPoint = new Phaser.Point(pointer.x, pointer.y);
                                    sprite.select();
                                    this.game.input.addMoveCallback(this.gemMove, this);
                                } else {
                                    this.gemSelect(sprite);
                                }
                            }

                        }, this);
                            /*if (!((j == 2 || j == 3) && sprite.mytype == 2) && window["tutorial"]) {
                                if(!(j == 3 && i == 2)) {
                                    sprite.alpha = 0.4;
                                }
                            }*/

                        this._gameArray[i][j] = sprite;
                    } else {
                        this._gameArray[i][j] = new Gem(this.game, x, y, 0);
                        this._gameArray[i][j].row = i;
                        this._gameArray[i][j].col = j;
                    }

                }
            }
            if (window["tutorial"]) {

                this._border = this.game.add.sprite(2.5 * GameConfig.BLOCK_SIZE, 2.5 * GameConfig.BLOCK_SIZE, "atlas2", "selection");
                this._border.scale.set(0.6, 0.6);
                this._border.anchor.set(0.5);
                this._borderTween = this.game.add.tween(this._border.scale).to({
                     x: .6 + .2,
                     y: .6 + .2
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._container.addChild(this._border);



            }

        }

        public endGame() {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    try {
                        this._gameArray[i][j]._sprite.inputEnabled = false;
                    } catch (e) {
                        continue;
                    }
                }
            }

            setTimeout(()=>{this._finalCallback();}, 1000);
        }

        public clearTweens(): void {
            if(this._tweenArray.length > 0) {
                for(let i: number = 0; i < this._tweenArray.length; i ++) {
                    if(this._tweenArray[i] != null) this.game.tweens.remove(this._tweenArray[i]);
                }
            }
            this._tweenArray = [];

            if(this._tempSprite.length > 0) {
                for(let i: number = 0; i < this._tempSprite.length; i ++) {
                    if(this._tempSprite[i] != null) this._tempSprite[i].destroy(true);
                }
            }
            this._tempSprite = [];

            for (let i = 0; i < this._gameArray.length; i++) {
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] != null) {
                        this._gameArray[i][j].clearTweens();
                    }
                }
            }


        }


        public replay(): void {
            this._disabled = false;
            this._finalWasFired = false;
            if (window["collectable"]) {
                this._counter = 0;
                // this._counters = [0,window["firstElement"],0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            } else {
                this._counter = 0;
                // this._counters = [-1, -1, -1, -1, -1, -1, -1, -1];
            }
        }

        private gemSelect(gem: Gem): void {
            if (this._selectedGem == null) {
                return;
            }

            if (this._selectedGem.isSame(gem)) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                return;
            }

            if (this._selectedGem.isNext(gem)) {
                this._selectedGem.deselect();
                this._pickedGem = gem;
                this.swapGems();
            } else {
                this._selectedGem.deselect();
                this._selectedGem = gem;
                gem.select();
            }

        }

        private resetBoard(): void {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[0].length; j++) {
                    if (this._gameArray[i][j].mytype > 0) {
                        this._gameArray[i][j].mytype = GameConfig.gameArrayLandscape[i][j];
                    }
                }
            }
            this._canClick = true;
        }

        private moveExists(): boolean {
            let str: string = "";
            let k = 6;
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype < 0) {
                        str += "0";
                    } else {
                        str += this._gameArray[i][j].mytype;
                    }
                    if (this._gameArray[i][j].mytype > 5) {
                        return true;
                    }
                }
                // for (let h: number = 0; h < 2; h++) {
                str += k;
                k++;
                if (k > 9) {
                    k = 6;
                }
                str += "\n";
            }
            let myRe: RegExp = /(\d)(?:.|(?:.|\n){9}|(?:.|\n){6})?\1\1|(\d)\2(?:.|(?:.|\n){9}|(?:.|\n){6})?\2|(\d)(?:.|\n){7}\3(?:.|(?:.|\n){9})\3|(\d)(?:.|(?:.|\n){9})\4(?:.|\n){7}\4|(\d)(?:(?:.|\n){7,9}|(?:.|\n){17})\5(?:.|\n){8}\5|(\d)(?:.|\n){8}\6(?:(?:.|\n){7,9}|(?:.|\n){17})\6/;
            if (myRe.exec(str) == null) {
                return false;
            }
            return true;
        }

        private swapGems(swapBack: boolean = true): void {
            if (window["tutorial"]) {

                let minCol: number = Math.min(this._selectedGem.col, this._pickedGem.col);
                let maxCol: number = Math.max(this._selectedGem.col, this._pickedGem.col);
                if (Math.abs(this._pickedGem.col - this._selectedGem.col) != 1 || (minCol != 2 && maxCol != 3)) {
                    this._selectedGem = null;
                    this._pickedGem = null;
                    return;
                } else {
                    window["tutorial"] = false;
                    for (let i: number = 0; i < this._gameArray.length; i++) {
                        for (let j: number = 0; j < this._gameArray[i].length; j++) {
                            //this._gameArray[i][j].alpha = 1;
                            this._playState.destroyTutorial();
                            this._borderTween.stop();
                            this._border.destroy();
                        }
                    }

                }
            }
            this._canClick = false;
            this._coolAnimation = true;
            this._verticalAnimation = false;
            this._gameArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gameArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;

            let rowSel: number = this._selectedGem.row;
            let colSel: number = this._selectedGem.col;
            let rowPic: number = this._pickedGem.row;
            let colPic: number = this._pickedGem.col;

            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            // save poistions
            let posPicked: Phaser.Point = new Phaser.Point((colPic + 0.5) * GameConfig.BLOCK_SIZE, (rowPic + 0.5) * GameConfig.BLOCK_SIZE,);
            let posSelected: Phaser.Point = new Phaser.Point((colSel + 0.5) * GameConfig.BLOCK_SIZE, (rowSel + 0.5) * GameConfig.BLOCK_SIZE);

            // add tweens
            let tweenSelected: Phaser.Tween = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            this._tweenArray.push(tweenSelected);

            let tweenPicked: Phaser.Tween = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            this._tweenArray.push(tweenPicked);

            // temp information
            let type1: number = this._selectedGem.mytype;
            let type2: number = this._pickedGem.mytype;


            tweenPicked.onComplete.add(() => {

                if (this._selectedGem !== null && this._selectedGem !== undefined && this._pickedGem !== undefined && this._pickedGem !== null && (this._selectedGem.isBomb || this._pickedGem.isBomb)) {
                    this._removeMap = [];
                    for (let i: number = 0; i < this._gameArray.length; i++) {
                        this._removeMap[i] = [];
                        for (let j: number = 0; j < this._gameArray[i].length; j++) {
                            this._removeMap[i].push(0);
                        }
                    }
                    if (this._selectedGem.isBomb) {
                        this.setOff(this._selectedGem.mytype - 5, this._selectedGem.row, this._selectedGem.col);
                        this.destroyGems();
                    } else {
                        this._selectedGem = this._pickedGem;
                        this.setOff(this._selectedGem.mytype - 5, this._selectedGem.row, this._selectedGem.col);
                        this.destroyGems();
                    }
                } else {
                    if (this.isMatchOnBoard(rowPic, colPic, type1, rowSel, colSel, type2)) {
                        this.handleMatches();
                        this._selectedGem = null;
                        this._pickedGem = null;
                    } else if (swapBack) {
                        this.swapGems(false);
                    }
                }
            }, this);

            tweenPicked.start();
            tweenSelected.start();

            if (!swapBack) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                this._pickedGem = null;
                this._canClick = true;
            }


        }

        private isMatchOnBoard(row1: number, col1: number, type1: number, row2: number, col2: number, typr2: number): boolean {
            return this.isMatch(row1, col1, type1) || this.isMatch(row2, col2, typr2);
        }

        private isMatch(row: number, col: number, type: number): boolean {
            return this.isHorizontal(row, col, type) || this.isVertical(row, col, type);
        }

        private isHorizontal(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;


            for (let i: number = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gameArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let j: number = start; j >= end; j--) {
                    if (this._gameArray[row][j].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }

            return false;
        }

        private setOff(range: number, x?: number, y?: number): void {
            if (x == null) {
                if (!this._selectedGem.isBomb || range < 1) {
                    return;
                }
                if (!this._canClick) {
                    return;
                }
            }
            this._canClick = false;
            this._boomAnimation = true;

            let p: Phaser.Point;
            if (x !== undefined && x !== null) {
                p = new Phaser.Point(x, y);
            } else {
                p = new Phaser.Point(this._selectedGem.row, this._selectedGem.col);
                this._selectedGem.stopTween();
            }
            this._bombPosition = p;
            this._selectedGem = null;
            this._pickedGem = null;
            this._bombRange = range;
            switch (range) {
                case 1: {
                    for (let i: number = - 1; i < 2; i++) {
                        let k: number;
                        let h: number;
                        if (p.x + i >= 0 && p.x + i < this._gameArray.length) {
                            k = p.x + i;
                            h = p.y;

                            if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                            }
                            if (this._gameArray[k][h].mytype > 0) {
                                this._removeMap[k][h] = 1;
                            }

                        }
                        if (p.y + i >= 0 && p.y + i < this._gameArray[0].length) {
                            k = p.x;
                            h = p.y + i;
                            //
                            if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                            }
                            if (this._gameArray[k][h].mytype > 0) {
                                this._removeMap[k][h] = 1;
                            }
                        }
                    }
                } break;

                case 2: {
                    // let k: number;
                    // let h: number;
                    // for (let i: number = - 2; i < 3; i++) {
                    //     if (p.x + i >= 0 && p.x + i < this._gameArray.length) {
                    //         k = p.x + i;
                    //         h = p.y;

                    //         if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                    //             this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                    //         }
                    //         if (this._gameArray[k][h].mytype > 0) {
                    //             this._removeMap[k][h] = 1;
                    //         }
                    //     }
                    //     if (p.y + i >= 0 && p.y + i < this._gameArray[0].length) {
                    //         k = p.x;
                    //         h = p.y + i;

                    //         if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                    //             this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                    //         }
                    //         if (this._gameArray[k][h].mytype > 0) {
                    //             this._removeMap[k][h] = 1;
                    //         }
                    //     }
                    // }

                    // for (let i: number = -1; i < 2; i++) {
                    //     for (let j: number = -1; j < 2; j++) {
                    //         if (p.x + i >= 0 && p.x + i < this._gameArray.length) {
                    //             if (p.y + j >= 0 && p.y + j < this._gameArray[0].length) {
                    //                 k = p.x + i;
                    //                 h = p.y + j;

                    //                 if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                    //                     this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                    //                 }
                    //                 if (this._gameArray[k][h].mytype > 0) {
                    //                     this._removeMap[k][h] = 1;
                    //                 }
                    //             }
                    //         }
                    //     }
                    // }

                } break;
            }
            this._coolAnimation = false;
        }

        private isVertical(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;

            for (let j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gameArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let i: number = start; i >= end; i--) {
                    if (this._gameArray[i][col].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }


            return false;
        }

        private handleMatches(): void {
            if (!this._firstMatch) {
                this._firstMatch = true;
                if(this._firstMatchCallback) {
                    this._firstMatchCallback();
                }

            }
            this._removeMap = [];

            for (let i: number = 0; i < this._gameArray.length; i++) {
                this._removeMap[i] = [];
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    this._removeMap[i].push(0);
                }
            }

            this.handleHorizontalMatches();
            this.handleVerticalMatches();
            this._sequences.forEach(x => {
                for (let i: number = 0; i < x.length; i++) {
                    let p: Phaser.Point = x.get(i);
                    this._removeMap[p.x][p.y] = 1;
                }
            });
            this.destroyGems();
            this._coolAnimation = false;
        }

        private destroyGems(): void {
            this._destroyed = 0;
            let condensationPoint: Phaser.Point;
            let condensationType: number;
            if (this._selectedGem != null) {
                if (this._removeMap[this._selectedGem.row][this._selectedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._selectedGem.x, this._selectedGem.y);
                    condensationType = this._selectedGem.mytype;
                    this._condensationPoint.set(this._selectedGem.row, this._selectedGem.col);
                    let i: number = 0;
                    while (i < this._sequences.length) {
                        let s: Sequence = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                } else if (this._removeMap[this._pickedGem.row][this._pickedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._pickedGem.x, this._pickedGem.y);
                    condensationType = this._pickedGem.mytype;
                    this._condensationPoint.set(this._pickedGem.row, this._pickedGem.col);
                    let i: number = 0;
                    while (i < this._sequences.length) {
                        let s: Sequence = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                } else {
                    condensationPoint = new Phaser.Point(1, 1);
                    condensationType = 0;
                }

            }
            // debugger;
            if (this._sequences.length > 0) {
                if (this._sequences.length == 1 && this._sequences[0].length > 3 && condensationPoint) {
                    condensationPoint.x = (this._condensationPoint.y + 0.5) * GameConfig.BLOCK_SIZE;
                    condensationPoint.y = (this._condensationPoint.x + 0.5)* GameConfig.BLOCK_SIZE;
                     this._sequences.forEach(s => {
                        s.forEach((p: Phaser.Point) => {
                            let gem: Gem = this._gameArray[p.x][p.y];
                            this.game.add.tween(gem).to({
                                x: condensationPoint.x,
                                y: condensationPoint.y
                            }, 300, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                                this.destroyGem(gem, false, true);
                            })

                            // this._counter++;
                            // this._cb(1, 1);
                        });
                        this.playMatchSound(s.length);
                    });
                } else {
                    this._sequences.forEach(s => {
                        s.forEach((p: Phaser.Point) => {
                            let gem: Gem = this._gameArray[p.x][p.y];
                            this.destroyGem(gem);
                        });
                        this.playMatchSound(s.length);
                    });
                }

                this._destroyed = 0;

                this.game.time.events.add(400, this.makeGemsFall, this);
            } else if (this._bombRange == 1) {
                let x: number = this._bombPosition.x;
                let y: number = this._bombPosition.y;
                for (let i: number = 0; i < this._removeMap.length; i++) {
                    for (let j: number = 0; j < this._removeMap[i].length; j++) {
                        if (this._removeMap[i][j] > 0 && !(i == x && j == y)) {
                            this.destroyGem(this._gameArray[i][j], true);
                            // this._counter--;
                        }
                    }
                }
if (window["sounds"]) {
                this.game.sound.play("1", 0.4);
}

                this._gameArray[x][y].playDestroy(1);
                this.game.time.events.add(750, this.makeGemsFall, this);
            } else {
                // this._bombPosition
                let x: number = this._bombPosition.x;
                let y: number = this._bombPosition.y;

                let i: number = this._bombPosition.x;
                let j: number = this._bombPosition.x;

                let k: number = this._bombPosition.y;
                let h: number = this._bombPosition.y;

                for (let d: number = 1; d < Math.max(this._gameArray.length, this._gameArray[0].length); d++) {
                    if (i - d >= 0 && this._gameArray[i - d][y].mytype >0) {
                        this.destroyGem(this._gameArray[i - d][y]);
                        this._removeMap[i -d][y] = 1;
                    }
                    if (j + d < this._gameArray.length && this._gameArray[j + d][y].mytype > 0) {
                        this.destroyGem(this._gameArray[j + d][y]);
                        this._removeMap[j + d][y] = 1;
                    }
                    if (k - d >= 0 && this._gameArray[x][k - d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][k - d]);
                        this._removeMap[x][k - d] = 1;
                    }
                    if (h + d < this._gameArray[0].length && this._gameArray[x][h + d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][h + d]);
                        this._removeMap[x][h + d] = 1;
                    }
                }
                this._removeMap[x][y] = 1;
                let bomb: Gem = this._gameArray[x][y];
                this._gameArray[x][y].playDestroy(0);
                this.game.time.events.add(750, this.makeGemsFall, this);
if (window["sounds"]) {
                this.game.sound.play("12", 0.4);
}
                this._lightnings.forEach(x => {
                    x.alpha = 1;
                    x.position.set(bomb.x, bomb.y);
                    x.play("play");
                });
            }
            this._bombRange =0;
        }

        private playMatchSound(n: number): void {
            let num: number = n - 3;
            if (num < 0) {
                return;
            }
            let sounds: number[] = [17, 16, 15, 14];
            if (num > sounds.length) {
                num = sounds.length - 1;
            }
if (window["sounds"]) {
            this.game.sound.play(sounds[num].toString(), 0.4);
}
        }

        private destroyGem(gem: Gem, bomb: boolean = false, condensation: boolean = false): void {
            let N: number;
            if (this._bombRange > 1) {
                N = 1;
            } else {
                N = this.game.rnd.between(3, 5);
            }
            this._counter++;

            if (gem.mytype > 0) {
                for (let i: number = 0; i < N; i++) {

                    let phi: number = this.game.rnd.between(0, Phaser.Math.PI2);
                    let vec: Phaser.Point = new Phaser.Point(Math.sin(phi), Math.cos(phi));
                    let r: number = this.game.rnd.between(30, 50);
                    vec.setMagnitude(r);
                    let worldPos: PIXI.Point = gem.worldPosition.clone();
                    vec.x += worldPos.x;
                    vec.y += worldPos.y;
                    let p: GemPatricle ;
                    if (gem.mytype > 5) {
                        let t: number = +gem._sprite.frameName.split("_")[1];
                        // debugger;
                        p = new GemPatricle(this.game, vec.x, vec.y, t);
                    } else {
                        p = new GemPatricle(this.game, vec.x, vec.y, gem.mytype);
                    }
                    this.game.world.addChild(p);
                    let toPos: Phaser.Point = this._playState.getGurrentElementPosition("foo");
                    let timeAnimation: number = 900 + this.game.rnd.integerInRange(-100,200)

                    this.game.add.tween(p.scale).to({
                        x: 3.5,
                        y: 3.5
                    }, timeAnimation/2, Phaser.Easing.Sinusoidal.InOut, true, 0, 0, true);

                    this.game.add.tween(p).to({
                        x: [p.x, Math.max((p.x-toPos.x), (toPos.x-p.x))/2+this.game.rnd.integerInRange(-400,800),toPos.x],
                        y: [p.y, Math.max((p.y-toPos.y), (toPos.y-p.y))/2+this.game.rnd.integerInRange(-400,400),toPos.y]
                    }, timeAnimation, Phaser.Easing.Sinusoidal.Out, true, 100).interpolation(function(v,k){return Phaser.Math.bezierInterpolation(v,k)}).onComplete.add(() => {
                        if (i == N - 1) {
                            this._cb(1, 1);
                        }
                        if (window["sounds"] && Math.random() < 0.5) {
                            // console.log("henlo");
                            let sounds: string[] = ["21", "22", "23"];
                            this.game.sound.play(Phaser.ArrayUtils.getRandomItem(sounds), 0.2);
                        }
                        p.destroy();
                    }, this);

                }
            }
            if (condensation) {
                gem.playDestroy(22);
            } else {
                if (!bomb) {
                    gem.playDestroy(0);
                } else {
                    gem.playDestroy(0);
                }
            }
        }

        public render(): void {
            // super.render();
            // this.debu
            this.game.debug.text("foobar 100", 10, 20);
            if (this._gameArray) {
                this._gameArray[2][2].render();
                // this.game.debug.spriteBounds();
                // this.game.debug.spriteCorners(this._gameArray[2][2], true, true);
            }
        }

        private handleHorizontalMatches(): void {
             for (let i: number = 0; i < this._gameArray.length; i++) {
                  let colorStreak: number = 1;
                  let currentColor: number = -100500;
                  let startStreak: number = 0;
                  for (let j: number = 0; j < this._gameArray[i].length; j++) {
                       if (this._gameArray[i][j].mytype == currentColor && this._gameArray[i][j].mytype > 0) {
                            colorStreak++;
                            if (colorStreak >= 4) {
                                this._createBomb = true;
                                this._maxColorStreak = Math.max(this._maxColorStreak, colorStreak);
                            }
                       }
                       if (this._gameArray[i][j].mytype != currentColor || j == this._gameArray[i].length - 1) {
                            if (colorStreak >= 3) {
                                let seq: Sequence = new Sequence();
                                this._sequences.push(seq);

                                // if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                //     this._coolAnimation = false;
                                // }

                                let currentId: number = this._sequences.length - 1;

                                for(let k: number = 0; k < colorStreak; k++) {
                                    let id: number = currentId
                                    this._sequences.forEach((s: Sequence, n: number) => {
                                        if (s.contains(i, startStreak + k)) {
                                            id = n;
                                        }
                                    });
                                    if (id != currentId) {
                                        this._sequences[id].merge(seq);
                                        seq = this._sequences[id];
                                        currentId = id;
                                    } else {
                                        seq.add(i, startStreak + k);
                                    }
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[i][j].mytype;
                       }
                  }
             }
        }

         private handleVerticalMatches(): void {
            for (let i: number = 0; i < this._gameArray[0].length; i++) {
                    let colorStreak: number = 1;
                    let currentColor: number = -100500;
                    let startStreak: number = 0;
                    for (let j: number = 0; j < this._gameArray.length; j++) {
                        if (this._gameArray[j][i].mytype == currentColor && this._gameArray[j][i].mytype > 0) {
                            colorStreak++;
                            if (colorStreak >= 4) {
                                this._createBomb = true;
                                this._maxColorStreak = Math.max(this._maxColorStreak, colorStreak);
                            }
                        }
                        if (this._gameArray[j][i].mytype != currentColor || j == this._gameArray.length - 1) {
                            if (colorStreak >= 3) {
                                let seq: Sequence = new Sequence();
                                this._sequences.push(seq);
                                let currentId: number = this._sequences.length - 1;

                                // if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                //     this._coolAnimation = false;
                                // } else {
                                    // this._verticalAnimation = true;
                                // }
                                for(let k: number = 0; k < colorStreak; k++) {
                                    let id: number = currentId

                                    this._sequences.forEach((s: Sequence, n: number) => {
                                        if (s.contains(startStreak + k, i)) {
                                            id = n;
                                        }
                                    });

                                    if (id != currentId) {
                                        this._sequences[id].merge(seq);
                                        seq = this._sequences[id];
                                        currentId = id;
                                    } else {
                                        seq.add(startStreak + k, i);
                                    }
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[j][i].mytype;
                       }
                  }
             }
        }

        private checkGlobalMatch(): boolean {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype <= 0) {
                        continue;
                    }
                    if (this.isMatch(i, j, this._gameArray[i][j].mytype)){
                        return true;
                    }
                }
            }
            return false;
        }

        private swap(x1: number, y1: number, x2: number, y2: number, dx:number = 0): void {
            let temp: Block = this._gameArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;

            this._gameArray[x1][y1] = this._gameArray[x2][y2];

            this._gameArray[x1][y1].row = x1 - dx;
            this._gameArray[x1][y1].col = y1;

            this._gameArray[x2][y2] = temp;
        }

        private makeGemsFall(): void {
            if (this._destroyed > 0) {
                this.game.time.events.add(200, this.makeGemsFall, this);
            }
            this._sequences.forEach((seq: Sequence) => {
                if (seq.isBomb) {
                    let p: Phaser.Point = seq.bombPosition;
                    this._removeMap[p.x][p.y] = 0;
                    let gem: Gem = this._gameArray[p.x][p.y];
                    try {
                        this.game.world.removeChild(gem);
                        this._container.addChild(gem);
                    } catch (e) {

                    }
                    gem.x = (gem.col + 0.5) * GameConfig.BLOCK_SIZE;
                    gem.y = (gem.row + 0.5) * GameConfig.BLOCK_SIZE;
                    gem.spawnBoom(seq.length);
                    this._maxColorStreak = 0;
                    this._createBomb = false;
                    this._boomAnimation = false;
                }
            });
            if (this._turnOffMask) {
                this._turnOffMask = false;
                this._container.mask = this._myMask;
            }
            this._sequences.length = 0;
            let holes: Hole[] = this.findHoles();
            let tweens: Phaser.Tween[] = [];
            let fallen: number = 0;

            for (let k: number = 0; k < holes.length; k++) {
                let hole: Hole = holes[k];
                let above: Hole;
                let start: number;
                let end: number;
                if (hole.hasHoleAbove) {
                    above = holes[k + 1];
                    start = above.row + above.length - hole.length;
                    end = hole.row - start;
                } else {
                    start = 0;
                    end = hole.row - this._fieldUpperBound[hole.col];
                }
                for (let i: number = 0; i < end; i++) {
                    this.swap(hole.row - 1 - i, hole.col, hole.row + hole.length - 1 - i, hole.col);
                }
                if (!hole.hasHoleAbove) {
                    for (let i = this._fieldUpperBound[hole.col]; i < this._fieldUpperBound[hole.col] + hole.length; i++) {
                        let gem: Gem = this._gameArray[i][hole.col];
                        let rnd: number = this.game.rnd.between(1, 5);
                        gem.mytype = rnd;

                        gem.x = (hole.col + 0.5) * GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - hole.length + 0.5) * GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[hole.col];
                    end = hole.row + hole.length;
                } else {
                    let len: number = hole.row - (above.row + above.length - hole.length);
                    start = hole.row + hole.length - len;
                    end = hole.row + hole.length;
                }
                for (let i: number = start; i < end; i++) {

                    let gem: Gem = this._gameArray[i][hole.col];
                    fallen++;
                    let tween: Phaser.Tween = this.game.add.tween(gem).to({
                        y: gem.y + hole.length * GameConfig.BLOCK_SIZE
                    }, GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, false);
                    this._tweenArray.push(tween);
                    tween.onComplete.add(() => {
                        this._gameArray[gem.row][gem.col] = gem;
                        fallen--;
                        if (fallen == 0) {
                            if(this.checkGlobalMatch()){
                                this.handleMatches();
                            } else {
                                if (!this.moveExists()) {
                                    this.resetBoard();
                                } else {
                                    this._canClick = true;
                                }
                                this._selectedGem = null;
                                if (!this._finalWasFired) {
                                    // let counter: number = 0;
                                    // for (let i: number = 0; i < this._counters.length; i++) {
                                        if (this._counter >= window["totalElemets"]) {
                                            // counter++;
                                        // }

                                        // if (counter == this._counters.length - 2) {
                                            this._finalWasFired = true;
                                            GameConfig.COUNTERS_PUBLIC = [0,window["firstElement"],0, window["secondElement"], window["thirdElement"], 0, 100, 100];
                                            this.endGame();
                                        // }
                                    }
                                }

                                // for (let i = 0; i < this._gameArray.length; i++) {
                                //     let str = "";
                                //     for (let j = 0; j < this._gameArray[i].length; j++) {
                                //         str += this._gameArray[i][j].mytype + "\t";
                                //     }
                                // }
                            }
                        }
                    });
                    tweens.push(tween);
               }
            }

            tweens.forEach(x => x.start());
        }

        private createFlyTween(gem: Gem, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {
            let scaleTween: Phaser.Tween = this.game.add.tween(gem.scale).to({
                x: GameConfig.PANEL_GEM_SCALE,
                y: GameConfig.PANEL_GEM_SCALE
            }, GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false);

            this._tweenArray.push(scaleTween);

            scaleTween.onComplete.addOnce(() => {
                gem.scale.setTo(1);
            }, this);

            let flyTween: Phaser.Tween = this.game.add.tween(gem).to({
                x: flyawayPoint.x,
                y: flyawayPoint.y
            }, GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false, delay);

            this._tweenArray.push(flyTween);

            // if(gem.mytype == 1) {?
                this._gem1ToFollow = gem;
            // } else if (gem.mytype == 3) {
            //     this._gem2ToFollow = gem;
            // } else if (gem.mytype == 4) {
            //     this._gem3ToFollow = gem;
            // }

            flyTween.onStart.addOnce(() => {
                if(window["particleOn"]) {
                    // if (gem.mytype == 1) {
                        // if (this._gem1ToFollow == gem) {
                            if (this._destroyed * 100 == (delay + 100)) {
                            this._fruitSpecialEmmiter1.x = this._gem1ToFollow.x;
                            this._fruitSpecialEmmiter1.y = this._gem1ToFollow.y;

                            this._fruitSpecialEmmiter1.on = true;
                            this._followGem1 = true;
                        }
                    //     }
                    // } else if (gem.mytype == 3) {
                    //     if (this._gem2ToFollow == gem) {

                    //         this._fruitSpecialEmmiter2.x = this._gem2ToFollow.x;
                    //         this._fruitSpecialEmmiter2.y = this._gem2ToFollow.y;

                    //         this._fruitSpecialEmmiter2.on = true;
                    //         this._followGem2 = true;
                    //     }
                    // } else if (gem.mytype == 4) {
                    //     if (this._gem3ToFollow == gem) {


                    //         this._fruitSpecialEmmiter3.x = this._gem3ToFollow.x;
                    //         this._fruitSpecialEmmiter3.y = this._gem3ToFollow.y;

                    //         this._fruitSpecialEmmiter3.on = true;
                    //         this._followGem3 = true;
                    //     }
                    // }
                }
                scaleTween.start()
            });

            flyTween.onComplete.add((obj: Phaser.Sprite, tween: Phaser.Tween) => {
                if(gem) {
                    if(window["particleOn"]) {
                        // if (gem.mytype == 1) {
                            // if (this._gem1ToFollow == gem) {
                            if (this._destroyed == 1) {
                                this._fruitSpecialEmmiter1.on = false;
                                this._followGem1 = false;
                            }
                            // }
                        // } else if (gem.mytype == 3) {
                        //     if (this._gem2ToFollow == gem) {
                        //         this._fruitSpecialEmmiter2.on = false;
                        //         this._followGem2 = false;
                        //     }
                        // } else if (gem.mytype == 4) {
                        //     if (this._gem3ToFollow == gem) {
                        //         this._fruitSpecialEmmiter3.on = false;
                        //         this._followGem3 = false;
                        //     }
                        // }

                        for (let i: number; i < this._tempSprite.length; i++) {
                            if (this._tempSprite[i] == gem) {
                                this._tempSprite.splice(i, 1);
                                break;
                            }
                        }
                    }
                    this.game.world.removeChild(gem);
                    this._container.addChild(gem);
                    gem.position.set(-1500, -1500);
                    this._destroyed--;
                    //
                    this._cb(gem.mytype, 1);
                    this._counter++;
                    // this._counters[gem.mytype]--;
                    if (this._boomAnimation) {
                        this._boomAnimation = false;
                    }
                    if (this._destroyed == 0) {
                        this.makeGemsFall();
                    }
                }
            }, this);

            return flyTween;
        }

        private updateSpecialGemEmmiter(){
                if (this._followGem1) {
                    this._fruitSpecialEmmiter1.x = this._gem1ToFollow.x;
                    this._fruitSpecialEmmiter1.y = this._gem1ToFollow.y;
                }

                // if (this._followGem2) {
                //     this._fruitSpecialEmmiter2.x = this._gem2ToFollow.x;
                //     this._fruitSpecialEmmiter2.y = this._gem2ToFollow.y;
                // }

                // if (this._followGem3) {
                //     this._fruitSpecialEmmiter3.x = this._gem3ToFollow.x;
                //     this._fruitSpecialEmmiter3.y = this._gem3ToFollow.y;
                // }
        }

        public get gameArray(): number[][] {
            let result = [];
            for (let i: number = 0; i < this._gameArray.length; i++) {
                result.push([]);
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    result[i][j] = this._gameArray[i][j].mytype;
                }
            }

            return result;
        }

        private generateFlyAwayTween(block: Gem, condensationPoint: Phaser.Point, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {

            this._container.removeChild(block);
            this.addChild(block);

            let flyAwayTween: Phaser.Tween = this.createFlyTween(block, flyawayPoint, delay);

            if (this._verticalAnimation) {

                return flyAwayTween;
            } else {
                let condensateTween: Phaser.Tween = this.game.add.tween(block).to({
                    x: condensationPoint.x
                }, GameConfig.CONDENSATION_TIME, Phaser.Easing.Linear.None, false);
                this._tweenArray.push(condensateTween);
                condensateTween.onComplete.add((sprite: Phaser.Sprite, tween: Phaser.Tween) => {
                    flyAwayTween.start();
                });
            return condensateTween;
            }
        }

         private condensate(point: Phaser.Point): void {
            let condenstationStack: Phaser.Tween[] = [];
            let delay: number = 0;
            for (let i: number = 0; i < this._removeMap.length; i++) {
                for (let j: number = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._destroyed++;

                        let block: Gem = this._gameArray[i][j];
                        let flyAwayPoint: Phaser.Point;
                        if (block.mytype == 2) {
                            flyAwayPoint = this._playState.getGurrentElementPosition("second");
                        } else {
                            flyAwayPoint = this._playState.getGurrentElementPosition("first");
                        }
                        condenstationStack.push(this.generateFlyAwayTween(block, point, flyAwayPoint, delay));
                        condenstationStack[condenstationStack.length-1].onStart.addOnce(()=>{

                        });
                        delay += 100;

                    }
                }
            }
            condenstationStack.forEach((el: Phaser.Tween) => {
                el.start();
            });
        }

        private findHoles(): Hole[] {
            let result: Hole[] = [];

            for (let j: number = 0; j < this._gameArray[0].length; j++) {
                for (let i: number = this._gameArray.length - 1; i >= 0; i--) {
                    if (this._removeMap[i][j] > 0) {
                        // let's find length of the hole
                        let length: number = 1;
                        let row: number = i;
                        let found: boolean = true;
                        let wasPushed: boolean = false;
                        for (let k = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] >0) {
                                length++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            } else if (found) {
                                result.push({row: row, col: j, length: length, hasHoleAbove: false});
                                found = false;
                                wasPushed = true;
                                let id: number = result.length - 2;
                                if (id >= 0 && result[id].col == result[id + 1].col) {
                                    result[id].hasHoleAbove = true;
                                }
                            }
                        }

                        if (!wasPushed) {
                            result.push({row: row, col: j, length: length, hasHoleAbove: false});
                            let id: number = result.length - 2;
                            if (id >= 0 && result[id].col == result[id + 1].col) {
                                result[id].hasHoleAbove = true;
                            }
                        }

                        break;
                    }
                }
            }

            return result;
        }

        update() {
            if(window["particleOn"]) {
                /*this._fruit1Emmiter.update();
                this._fruit2Emmiter.update();
                this._fruit3Emmiter.update();
                this._fruit4Emmiter.update();
                this._fruit5Emmiter.update();
                this._fruitSpecialEmmiter1.update();
                this._fruitSpecialEmmiter2.update();
                this._fruitSpecialEmmiter3.update();*/
                // this.updateSpecialGemEmmiter();
            }
        }

        public destroy(): void {
            this.clearTweens();

            setTimeout(()=>{
                this.game.input.onUp.removeAll(this);
                this.game.tweens.removeFrom(this, true);
                super.destroy(true);
            }, 10)
        }


    }
}