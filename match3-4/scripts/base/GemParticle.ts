module mygame {
    export class GemPatricle extends Phaser.Sprite {
        private _sprite: Phaser.Sprite;
        private PARTICLE_NAMES: string[] = ["blue", "green", "pink", "red", "yellow"];

        constructor(game: Phaser.Game, x: number, y: number, type: number) {
            super(game, x, y);
            this._sprite = this.game.add.sprite(0, 0, "atlas2", "particle_" + this.PARTICLE_NAMES[type - 1]);
            this.addChild(this._sprite);
            this._sprite.anchor.set(0.5);
        }

        public destoy(): void {
            this._sprite.destroy();
            super.destroy();
        }
    }
}