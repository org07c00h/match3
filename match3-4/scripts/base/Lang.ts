/**
 * Created by DEaDA on 6/9/17.
 */

module mygame {
    export class Lang {
        private static instance: Lang;

        private constructor() {
        }

        static get Instance() {
            if (this.instance === null || this.instance === undefined) {
                this.instance = new Lang();
            }
            return this.instance;
        }
        public static loc:string = 'en';

        public CONGRAT: Object = {en: 'Congratulations!', de: '  Herzlichen Gluckwunsch !'};
        public PLOT: Object = {en: 'The plot\nis restored!', de: 'Ein Grundstuck\nrestauriert!'};
        public TUTORIAL: Object = {en: 'Collect fruits to upgrade garden!', de: 'Sammeln Sie Fruchte,\num den Garten zu verbessern!'};


    }
}
