module mygame {
    export class Panel extends Phaser.Sprite {
        private _sprite: Phaser.Sprite;
        // private _myCounter: number[];

        // private _apple: Phaser.Sprite;
        // private _grapes: Phaser.Sprite;
        // private _leaf: Phaser.Sprite;

        // private _appleText: Phaser.BitmapText;
        // private _grapeText: Phaser.BitmapText;
        // private _leafText: Phaser.BitmapText;

        private _delta: number;
        private _timer: number;
        private _sleepTime: number;
        private _fruitsCounter: number;
        private _finishCallback: Function;
        private _finishWasFired: boolean;

        // private _appleTween: Phaser.Tween;
        // private _grapesTween: Phaser.Tween;
        // private _leafTween: Phaser.Tween;

        private _fruitScale: number;
        // private _border: Phaser.Sprite;
        private _city: Phaser.Sprite;
        private _myMask: Phaser.Graphics;

        // private _leafs: Phaser.Sprite[];

        // private _leaf1: Phaser.Sprite;
        // private _benches: Phaser.Sprite[];
        // private _brightBenches: Phaser.Sprite[];
        // private _fontains: Phaser.Sprite[];
        // private _trash: Phaser.Sprite;
        // private _trash2: Phaser.Sprite;

        private _orange: Phaser.Sprite;

        private _unclocksCounter: number;
        private static _myMaskSize: number[] = [550 - 30, 700 - 54 + 2];
        private static _otherMaskSize: number[] = [700 - 54 + 2, 550 - 30];

        private _scorePlank: Phaser.Sprite;
        private _progressBar: Phaser.Sprite;
        private _greenBar: Phaser.Graphics;
        private _total: number;
        private _counter;
        private _statusText: Phaser.BitmapText;
        private _bigHouse: Phaser.Sprite[];
        private _smallHouse: Phaser.Sprite[];
        private _bridge: Phaser.Sprite[];

        private _fireworks: Phaser.Sprite[];
        private _tweenRun: boolean = false;



        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this._delta = 0;
            this._unclocksCounter = 0;
            // this._benches = new Array(2);
            // this._brightBenches = new Array(2);
            // this._fontains = new Array(2);
            this._total = window["totalElemets"];
            this._counter = 0;
            this._statusText = this.game.add.bitmapText(0, -73 , "font", this._counter + "/" + this._total, 60);
            this._statusText.anchor.set(0.5);
            this._bigHouse = [];
            this._smallHouse = [];
            this._bridge = [];
            this._fireworks = [];


            this._myMask = this.game.add.graphics(0, 0);
            this.addChild(this._myMask);


            this._city = this.game.add.sprite(0, 0, "atlas2", "cityCorrect");
            this._city.anchor.set(0.5, 1);
            this._city.mask = this._myMask;


            this._orange = this.game.add.sprite(0, -275, "atlas2", "gold_city_back0X25");
            this._orange.anchor.set(0.5, 0.5);
            this._orange.scale.set(4, 4);

            this.addChild(this._orange);
            this.addChild(this._city);



            this._fruitsCounter = 0;
            this._finishWasFired = false;
            this._fruitScale = 0.8;



            // this._myCounter = [0,window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0];

            GameConfig.COUNTERS_PUBLIC = [999, 999, 999, 999, 999, 999];




            this._scorePlank = this.game.add.sprite(0, 200, "atlas2", "scorePlank");
            this._scorePlank.anchor.set(0.5, 1);
            this.addChild(this._scorePlank);

            this.createBigHouses();
            this.createSmallHouses();
            this.createBridges();


            this.createProgressBar()

            this._scorePlank.addChild(this._statusText);



            this._delta = 0;
            // for (let i: number = 0; i < this._delta.length; i++) {
            //     this._delta[i] = 0;
            // }

            this._timer = 0;
            this._sleepTime = 1;

            if (Core.isLandscape) {
                this.setLandscape();
            } else {
                this.setPortrait();
            }
        }

        private createBigHouses(): void {
            let postfix: string[] = ["start", "end"];
            for (let i: number = 0; i < 2; i++) {
                let sprite: Phaser.Sprite = this.game.add.sprite(-212, -654, "atlas2", "bigHouse_" + postfix[i]);
                this._bigHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1-i;
            }
            let pos: number[][] = [[-23, -544], [-165, -500], [-150,-642]];
            for (let i: number = 0; i < 3; i++) {
                let firework: Phaser.Sprite = this.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(() => {
                    this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this);
                // firework
                // firework.play("play", 24, false);

                this._city.addChild(firework);
                this._fireworks.push(firework);
            }
            // this._bigHouse[1].blendMode = PIXI.blendModes.ADD;
        }

        private createSmallHouses(): void {
            let postfix: string[] = ["start", "end"];
            for (let i: number = 0; i < 2; i++) {
                let sprite: Phaser.Sprite = this.game.add.sprite(-5, -565, "atlas2", "smallHouse_" + postfix[i]);
                this._smallHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1-i;
            }

            let pos: number[][] = [[233, -511], [135, -480], [182,-535]];
            for (let i: number = 0; i < 3; i++) {
                let firework: Phaser.Sprite = this.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(() => {
                    this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this);
                // firework
                // firework.play("play", 24, false);

                this._city.addChild(firework);
                this._fireworks.push(firework);
            }
            // this._smallHouse[1].blendMode = PIXI.blendModes.ADD;
        }

        private createBridges(): void {
            let postfix: string[] = ["start", "end"];
            for (let i: number = 0; i < 2; i++) {
                let sprite: Phaser.Sprite = this.game.add.sprite(-43, -303, "atlas2", "bridge_" + postfix[i]);
                this._bridge.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1-i;
            }
            let pos: number[][] = [[17, -276], [25, -330], [83,-300]];
            for (let i: number = 0; i < 3; i++) {
                let firework: Phaser.Sprite = this.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(() => {
                    this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this);
                // firework
                // firework.play("play", 24, false);

                this._city.addChild(firework);
                this._fireworks.push(firework);
            }
            // this._bridge[1].blendMode = PIXI.blendModes.ADD;
        }

        private createProgressBar(): void {
            this._progressBar = this.game.add.sprite(0, 0);
            let firstBar: Phaser.Sprite = this.game.add.sprite(0, 0, "scoreMetr");
            firstBar.anchor.set(1,1);

            let secondBar: Phaser.Sprite = this.game.add.sprite(0, 0, "scoreMetr")
            secondBar.scale.set(-1, 1);
            secondBar.anchor.set(1, 1);
            // this._progressBar.anchor.set(1, 1);

            let w: number = firstBar.width * 2 - 30;
            let h: number = firstBar.height - 8;

            let bg: Phaser.Graphics = this.game.add.graphics(0, 0);
            bg.beginFill(0x232724)
                .drawRect(-w / 2, -h - 4, w, h)
                .endFill();

            bg.cacheAsBitmap = true;

            this._greenBar  = this.game.add.graphics(0, 0);

            this._progressBar.addChild(bg);
            this._progressBar.addChild(this._greenBar);
            this._progressBar.addChild(firstBar);
            this._progressBar.addChild(secondBar);

            this._scorePlank.addChild(this._progressBar);

            // this.greenbarValue = 1;
        }

        private set greenbarValue(val: number)
        {
            let w: number = 141 * 2 - 30;
            let h: number = 46 - 14;

            this._greenBar.clear()
                .beginFill(0x37b54e)
                .drawRect(-w/2, -h - 4, w * val, h)
                .endFill();
        }

        public scaleFruits(val: number): void {
            // this._apple.scale.set(val);
            // this._fruitScale = val;
            // this._grapes.scale.set(val);
        }

        public set finishScreenCallback(val: Function) {
            this._finishCallback = val;
        }

        private updateStatus(): void {
            if (this._counter > this._total) {
                this._counter = this._total;
            }
            this._statusText.text =this._counter + "/" + this._total;
            this.greenbarValue = this._counter / this._total;

        }

        public replay(): void {
            this._finishWasFired = false;
            // this._myCounter = [0,window["firstElement"],0, window["secondElement"], 0, 0]
            this._delta = 0;
            this._counter = 0;
            this._unclocksCounter = 0;
            // this._delta = new Array(this._myCounter.length);
            // for (let i: number = 0; i < this._delta.length; i++) {
                // this._delta[i] = 0;
            // }
            // this._appleText.text = "0/" + window["firstElement"].toString();
            // this._grapeText.text = "0/" + window["secondElement"].toString();
        }

        public updateCounter(type: number, delta: number): void {
            this._delta += delta;
            // if (window["collectable"]) {
            //     if ((type == 1 || type == 3 || type == 4) && this._myCounter[type] > 0) {
                    // this._delta[type] += delta;
                    this._sleepTime = 200;
                // }
            // }
        }

        public setLandscape(): void {
            // this._border.angle = 0;
            this._myMask.clear();
            // this._trash.scale.set(1);
            // this._trash2.scale.set(1);
            this._orange.angle = 0;
            this._orange.position.set(0, -275);

            this._city.position.set(-35, 50);
            this._city.scale.set(1, 1);
            this._myMask.y = -270 - 3;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._myMaskSize[0] / 2, -Panel._myMaskSize[1] / 2, Panel._myMaskSize[0] , Panel._myMaskSize[1] , 20);
            // this._myMask.drawRoundedRect(-500 / 2, -600, 545, 688 - 2, 30);
            this._myMask.endFill();

            // this._border.position.set(0, 70 - 688 / 2);

            // this._leaf1.position.set(-300, -200);
            // this._leaf1.angle = 0;

            this._scorePlank.position.set(0, 70);
            // this._scorePlank.anchor.set(0.5, 1);
        }

        public setPortrait(): void {
            // this._border.angle = 90;
            // this._trash.scale.set(1.2);
            // this._trash2.scale.set(1.2)

            this._city.position.set(-10, 150);
            // Ga
            this._city.scale.set(1.1, 1.1);

            // this._border.position.set(0, 70 - 688 / 2 + 65 + 10);
            this._myMask.position.set(0, -240);
            this._myMask.clear();
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._otherMaskSize[0] / 2, -Panel._otherMaskSize[1] / 2, Panel._otherMaskSize[0] , Panel._otherMaskSize[1] , 20);
            this._myMask.endFill();
            this._orange.angle = 90;
            this._orange.position.set(0, -240);
            // this._orange.bringToTop

            // this._leaf1.position.set(-230, 360);
            // this._leaf1.angle = -90;
            this._scorePlank.position.set(0, 85);

        }

        public getElementPosition(type: string): Phaser.Point {
            // if(type == "first") {
            //     return new Phaser.Point(this._apple.worldPosition.x, this._apple.worldPosition.y);
            // } else if (type == "second") {
            //     return new Phaser.Point(this._leaf.worldPosition.x, this._leaf.worldPosition.y);
            // } else {
            //     return new Phaser.Point(this._grapes.worldPosition.x, this._grapes.worldPosition.y);
            // }
            if (Core.isLandscape) {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 70);
            } else {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 50);
            }

        }

        public initFruits(): void {

        }

        private unlockObject(): void {
            switch (this._unclocksCounter) {
                case 0: {
if (window["sounds"]) {
                    this.game.sound.play("3", 0.4);
}
if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
}
                    // this._bigHouse
                    for (let i: number = this._fireworks.length - 3; i < this._fireworks.length; i++) {
                        this.game.time.events.add(100 * i, () => {
// if (window["sounds"]) {
//                             this.game.sound.play("19", 0.4);
// }
                            this._fireworks[i].alpha = 1;
                            this._fireworks[i].play("play", 24);
                        }, this);

                    }
                    this.game.add.tween(this._bridge[1]).to({
                        alpha: 0.7
                    }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                        this.game.add.tween(this._bridge[1]).to({
                            alpha: 1
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        this.game.add.tween(this._bridge[0]).to({
                            alpha: 0
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);

                        // this._bridge[0].visible = false;
                    }, this);
                    // this.game.add.tween(this._trash2).to({
                    //     alpha: 0.7
                    // }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //     this.game.add.tween(this._trash).to({
                    //         alpha: 0
                    //     }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //         this._trash.visible = false;
                    //     }, this);
                    //     this.game.add.tween(this._trash2).to({
                    //         alpha: 0
                    //     }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //         this._trash2.blendMode = PIXI.blendModes.NORMAL;
                    //     })
                    // }, this);

                } break;

                case 1: {
if (window["sounds"]) {
                    this.game.sound.play("3", 0.4);
}
if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
}
                    for (let i: number = this._fireworks.length - 3 * 2; i < this._fireworks.length - 3; i++) {
                        this.game.time.events.add(100 * i, () => {
                            this._fireworks[i].alpha = 1;
                            this._fireworks[i].play("play", 24);
// if (window["sounds"]) {
//                             this.game.sound.play("19", 0.4);
// }
                        }, this);

                    }
                    this.game.add.tween(this._smallHouse[1]).to({
                        alpha: 0.7
                    }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                        this.game.add.tween(this._smallHouse[1]).to({
                            alpha: 1
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        this.game.add.tween(this._smallHouse[0]).to({
                            alpha: 0
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                    }, this);
                    // this._benches.forEach((el: Phaser.Sprite, i: number) => {
                    //     el.scale.multiply(1.3, 1.3);
                    //     // el.y -= 500;
                    //     el.alpha = 1;
                    //     // this.game.add.tween(el).to({
                    //     //     y: el.y + 500
                    //     // }, 500, Phaser.Easing.Sinusoidal.InOut, true, 200 * i).onComplete.addOnce(() => {
                    //         this.game.add.tween(el.scale).to({
                    //             x: 1 - 2 * i,
                    //             y: 1
                    //         }, 1000, Phaser.Easing.Back.Out, true);
                    //         this.game.add.tween(this._brightBenches[i]).to({
                    //             alpha: 0
                    //         }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //             this._brightBenches[i].blendMode = PIXI.blendModes.NORMAL;
                    //         });
                    //     // }, this);
                    // });
                } break;

                case 2: {
if (window["sounds"]) {
                    this.game.sound.play("3", 0.4);
}
if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
}
                    for (let i: number = 0; i < 3; i++) {
                        this.game.time.events.add(100 * i, () => {
                            this._fireworks[i].alpha = 1;
                            this._fireworks[i].play("play", 24);

                        }, this);
                    }
                    this.game.add.tween(this._bigHouse[1]).to({
                        alpha: 1
                    }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {

                        this.game.add.tween(this._bigHouse[1]).to({
                            alpha: 1
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        this.game.add.tween(this._bigHouse[0]).to({
                            alpha: 0
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                    }, this);
                    // let el: Phaser.Sprite = this._fontains[0];
                    // // el.y -= 500;
                    // el.alpha = 1;
                    // el.scale.set(1.3);

                    // this._fontains[1].alpha = 0.7;

                    // // this.game.add.tween(el).to({
                    // //     y: el.y + 500
                    // // }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //     this.game.add.tween(el.scale).to({
                    //         x: 1,
                    //         y: 1
                    //     }, 1000, Phaser.Easing.Back.Out, true);

                    //     this.game.add.tween(this._fontains[1]).to({
                    //         alpha: 0
                    //     }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    //         this._fontains[1].blendMode = PIXI.blendModes.NORMAL;
                    //     }, this);

                    // // }, this);

                } break;
            }
            this._unclocksCounter++;
        }

        private hideText(text: Phaser.BitmapText): void {
            // if (text.alpha > 0) {
            //     this.game.add.tween(text).to({
            //         alpha: 0
            //     }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
            //         // let checkMark: Phaser.Sprite = this.game.add.sprite(text.x + 30, text.y - 10, "atlas", "Quest-Board-02");
            //         // checkMark.alpha = 0;
            //         // this.game.add.tween(checkMark).to({
            //         //     alpha: 1
            //         // }, 200, Phaser.Easing.Sinusoidal.InOut, true);
            //         // checkMark.anchor.set(0.5);
            //         // this.addChild(checkMark);
            //     });
            // }
        }

        public update(): void {
            let dt: number = this.game.time.elapsedMS;

            if (this._timer > 0) {
                this._timer -= dt;
            }

            if (this._delta > 0) {

                this._timer = this._sleepTime;
                this._counter++;
                this._delta--;

                // if (!this._tweenRun) {
                //     this._tweenRun = true;

                //     this.game.add.tween(this._scorePlank).to({
                //         angle: -5
                //     }, 50, Phaser.Easing.Bounce.InOut, true).onComplete.addOnce(() => {
                //         this.game.add.tween(this._scorePlank).to({
                //             angle: 5
                //         }, 100, Phaser.Easing.Bounce.InOut, true).onComplete.addOnce(() => {
                //             this.game.add.tween(this._scorePlank).to({
                //                 angle: 0
                //             }, 50, Phaser.Easing.Bounce.InOut, true).onComplete.addOnce(() => {
                //                 this._tweenRun = false;
                //             })
                //         }, this);
                //     })
                // }


                if (this._counter >= Math.floor(this._total * window["firstSpawn"]) && this._unclocksCounter == 0) {
                    this.unlockObject();
                }

                if (this._counter >= Math.floor(this._total * window["secondSpawn"]) && this._unclocksCounter == 1) {
                    this.unlockObject();
                }

                if (this._counter >= Math.floor(this._total * window["thirdSpawn"]) && this._unclocksCounter == 2) {
                    this.unlockObject();
                }

                this.updateStatus();

                // if (this._delta[1] > 0) {
                //     this._delta[1]--;
                //     this._myCounter[1]--;

                //     if (this._myCounter[1] <= 0) {
                //         // this._appleText.text = window["firstElement"].toString() +"/"+ window["firstElement"].toString();
                //         // this.hideText(this._appleText);

                //         this.unlockObject();

                //     } else {

                //         // this._appleTween.start();
                //         // this._appleText.text = (window["firstElement"] - this._myCounter[1]).toString() +"/"+ window["firstElement"].toString();

                //     }
                // }

                // if (this._delta[3] > 0) {
                //     this._delta[3]--;
                //     this._myCounter[3]--;
                //     if (this._myCounter[3] <= 0) {
                //         // this._leafText.text = window["secondElement"].toString() + "/" + window["secondElement"].toString();
                //         this.unlockObject();
                //         // this.hideText(this._leafText);
                //     } else {

                //         // this._leafTween.start();
                //         // this._leafText.text = (window["secondElement"].toString() - this._myCounter[3]).toString() + "/" + window["secondElement"].toString();
                //     }
                // }

                // if (this._delta[4] > 0) {
                //     this._delta[4]--;
                //     this._myCounter[4]--;
                //     if (this._myCounter[4] <= 0) {
                //         // this._grapeText.text = window["thirdElement"].toString() + "/" + window["thirdElement"].toString();
                //         this.unlockObject();
                //         // this.hideText(this._grapeText);
                //     } else {

                //         // this._grapesTween.start();
                //         // this._grapeText.text = (window["thirdElement"].toString() - this._myCounter[4]).toString() + "/" + window["thirdElement"].toString();
                //     }
                // }
            }
        }
    }
}