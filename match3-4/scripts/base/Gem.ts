module mygame {
    export class Gem extends Phaser.Sprite {
        private _mytype: number;
        public _sprite: Phaser.Sprite;
        private _spriteName: string;
        private _destroyCallback: Function;
        private _destroyAnimation: Phaser.Animation;
        private _row: number;
        private _col: number;
        private _onCompleteFired: boolean;
        private _tempTweenArray: Phaser.Tween[];
        private _bombTween: Phaser.Tween;
        private _border: Phaser.Sprite;
        private _boomAnimation: Phaser.Animation;
        private _bombBoomAnimation: Phaser.Animation;

        private _boom: Phaser.Sprite;
        private _blinks: Phaser.Animation[];
        private _blinkSprite: Phaser.Sprite;
        private _canPlayBlibk: boolean;
        private _removeSprite: Phaser.Sprite;
        private _removeAnimation: Phaser.Animation;
        private _lightningSprite: Phaser.Sprite;
        private _bomb: Phaser.Sprite;
        private _bombAnimation: Phaser.Animation;



        constructor(game: Phaser.Game, x: number, y: number, type: number) {
            super(game, x, y);
            this._tempTweenArray = [];
            this._blinks = [];
            this._mytype = type;
            this._spriteName = GameConfig.NAMES[this._mytype];
            if (type > 0) {
                this._canPlayBlibk = true;
                this._sprite = this.game.add.sprite(0, 0, "atlas2", "F_" + this._mytype);
                this._sprite.anchor.set(0.5);
                // this._sprite.tint = 0x00;
                // this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
                this.addChild(this._sprite);

                this._boom = this.game.add.sprite(0, 0, "atlas2", "F_1");
                this._boom.anchor.set(0.5);
                this._boom.visible = false;
                this.addChild(this._boom);
                this._destroyAnimation = this._sprite.animations.add("set off", Phaser.Animation.generateFrameNames("F_", this._mytype, this._mytype), 18, false);
                this._destroyAnimation.onStart.add(() => {
                    this._sprite.alpha = 1;
                }, this);

                // this._boomAnimation = this._boom.animations.add("bomb", Phaser.Animation.generateFrameNames("b_", 1, 10), 18, false);

                // this._boomAnimation.onComplete.add(() => {
                //     // this._boom.visible = false;
                //     this.animationEnd();
                // }, this);

                this._bombBoomAnimation = this._boom.animations.add("b_bomb", Phaser.Animation.generateFrameNames("bb_", 1, 10), 18, false);
                // this._bombBoomAnimation.onComplete.add(() => {
                //     this._boom.visible = false;
                //     this.animationEnd();
                // }, this);

                this._border = this.game.add.sprite(0, 0, "atlas2", "selection");
                this._border.anchor.set(0.5);
                this._border.scale.set(0.7);
                this.addChild(this._border);
                this._border.alpha = 0;

                this._destroyAnimation.onComplete.add(() => {
                    this.animationEnd();
                }, this);
                // this._bombTween = new Array(2);

                this._bombTween = this.game.add.tween(this._sprite.scale).to({
                    x: 1.1,
                    y: 1.1
                }, 500, Phaser.Easing.Sinusoidal.InOut, false, 0, -1, true);
                this._sprite.alpha = 1;

                this._lightningSprite = this.game.add.sprite(0, 0, "atlas2", "light_line_10001");
                this._lightningSprite.anchor.set(0.5);
                this._lightningSprite.alpha = 0;
                this._lightningSprite.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(() => {
                    this._lightningSprite.alpha = 0;
                }, this);
                this.addChild(this._lightningSprite);

                this.createBlink();
                this.createRemoveAnimation();
                this.createBombAnimation();
                // try {
                    // this._blinks[this._mytype - 1].play();
                // } catch (e) {
                //     debugger;
                // }
            }
        }

        private createBombAnimation(): void {
            this._bomb = this.game.add.sprite(0, 0, "atlas2", "boom_8");
            this._bomb.scale.set(1.5);
            this._bomb.anchor.set(0.5);
            this._bomb.alpha = 0;
            this.addChild(this._bomb);

            let animation: Phaser.Animation = this._bomb.animations.add("play", Phaser.Animation.generateFrameNames("boom_", 8, 29), 30);
            this._bombAnimation = animation;
            animation.onStart.add(() => {
                this._canPlayBlibk = false;
                this._blinkSprite.alpha = 0;
                if (this._mytype <= 5) {
                    this._blinks[this._mytype - 1].stop();
                }
                this._sprite.alpha = 0;
                this._bomb.alpha = 1;
            });
            animation.onComplete.add(() => {
                // this._sprite.alpha = 1;
                this._bomb.alpha = 0;
            });
        }

        private createRemoveAnimation(): void {
            this._removeSprite = this.game.add.sprite(0, 0, "atlas2", "remove_anim_10001");
            this._removeSprite.anchor.set(0.5);
            this._removeSprite.alpha = 0;
            this.addChild(this._removeSprite);

            let animation: Phaser.Animation = this._removeSprite.animations.add("play", Phaser.Animation.generateFrameNames("remove_anim_", 10001, 10018), 24);
            this._removeAnimation =animation;
            animation.onStart.add(() => {
                this._canPlayBlibk = false;
                this._blinkSprite.alpha = 0;
                if (this._mytype <= 5) {
                    this._blinks[this._mytype - 1].stop();
                }
                this._sprite.alpha = 0;
                this._removeSprite.alpha = 1;
            });
            animation.onComplete.add(() => {
                // this._sprite.alpha = 1;
                this._removeSprite.alpha = 0;
            });

        }

        public render(): void {
            this.game.debug.spriteBounds(this._sprite);
            this.game.debug.spriteInfo(this._sprite, 100, 100, "#00ff00");
        }

        public playBlink(): void {
            // debugger;
            if (this._mytype > 0 && this._canPlayBlibk && this._mytype <= 5) {
                this._blinkSprite.alpha = 1;
                this._blinks[this._mytype - 1].play();
            }
        }

        private createBlink(): void {
            this._blinkSprite = this.game.add.sprite(0, 0, "atlas2", "yellow_blink_10013");
            this._blinkSprite.alpha = 0;
            this._blinkSprite.anchor.set(0.5);
            // this._blinkSprite.tint = 0x00;
            this.addChild(this._blinkSprite);
            let names: string[] = ["blue", "green", "pink", "red", "yellow"];
            for (let i: number = 0; i < 5; i++) {
                let animation: Phaser.Animation = this._blinkSprite.animations.add("blink_" + names[i], Phaser.Animation.generateFrameNames(names[i] + "_blink_", 10001, 10025), 12);
                animation.onStart.add(() => {
                    this._blinkSprite.alpha = 1;
                }, this);
                animation.onComplete.add(() => {
                    this._blinkSprite.alpha = 0;
                }, this);

                this._blinks.push(animation);
            }
        }

        private animationEnd(): void {
            if (!this._onCompleteFired) {
                this._onCompleteFired = true;
                this._sprite.alpha = 0;

                if (this._destroyCallback) {
                    this._onCompleteFired = true;
                    this._destroyCallback();
                }
            }
        }

        public boomAnimation(): void {
            // this._boom.visible = true;
            this._sprite.alpha = 0;
            if (this._mytype >= 6) {
                this._bombBoomAnimation.play();
            } else {
                this._boomAnimation.play();
            }

        }

        public spawnBoom(streakCounter: number): void {
            // streakCounter = 5;
            if (streakCounter > 3) {
                this._border.alpha = 0;
                // this._sprite.scale.set(1.5);
                if (streakCounter == 4) {
                    this._boom.loadTexture("atlas2", "bomb_icon");
                    this._boom.anchor.set(0.45, 0.6);
                    this._sprite.alpha = 1;
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._mytype = 6;
                } else {
                    this._boom.loadTexture("atlas2", "wert_horiz_icon");
                    this._boom.anchor.set(0.5);
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._sprite.alpha = 1;
                    // this._sprite.loadTexture("atlas", "bomb");
                    this._mytype = 7;
                }
                // this.game.add.tween(this._sprite.scale).to({
                //     x: 1,
                //     y: 1
                // }, 400, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                //     // this.game.time.events.add(100, () => {
                //         this._sprite.scale.set(0.7);
                //         this._bombTween = this.game.add.tween(this._sprite.scale).to({
                //             x: 1.3,
                //             y: 1.3
                //         }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);

                //     // }, this);

                // }, this);

                this._sprite.alpha = 1;
                this._onCompleteFired = false;
                this._destroyAnimation.stop();
            }
        }

        public stopTween(): void {
            this._bombTween.pause();
            this._sprite.loadTexture("atlas2", "F_2");
            this._sprite.alpha = 0;
        }

        public set row(val: number) {
            this._row = val;
        }

        public set col(val: number) {
            this._col = val;
        }

        public get col(): number {
            return this._col;
        }

        public get row(): number {
            return this._row;
        }

        public get mytype(): number {
            return this._mytype;
        }

        public set onDestroy(val: Function) {
            this._destroyCallback = val;
        }

        public get isBomb(): boolean {
            return this._mytype >= 6;
        }

        public set mytype(val: number) {
            this._border.alpha = 0;

            if (!this._onCompleteFired) {
                this._destroyAnimation.stop();
                this._onCompleteFired = true;
            }

            this._destroyAnimation.stop();
            this._mytype = val;


            if (val < 6) {
                this._canPlayBlibk = true;
                this._sprite.alpha = 1;
                this._bombTween.pause();
                this._sprite.position.set(0, 0);
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE, GameConfig.GEM_DEFAULT_SCALE);
                this._spriteName = GameConfig.NAMES[this._mytype];
                this._sprite.loadTexture("atlas2", "F_" + this._mytype);

            } else {
                this._bombTween.start();
                this._spriteName = "rocket";
                this._sprite.loadTexture("atlas2", "bomb_icon");
                this._sprite.scale.set(1, 1);
            }
            this._sprite.alpha = 1;
            this._onCompleteFired = false;
        }

        public foo(): void {
        }

        public lightning(angle: number): void {
            this._canPlayBlibk = false;
            this._blinkSprite.alpha = 0;
            this._lightningSprite.angle = angle;
            this._lightningSprite.alpha = 1;
            this._lightningSprite.play("play");
            this._sprite.alpha = 0;
        }

        public playDestroy(bombRange: number = 0): void {
            this._border.alpha = 0;
            this._boom.visible = false;
            if (this.mytype == 6) {
                this._bombTween.pause();
            }
            // let tween: Phaser.Tween = this.game.add.tween(this._sprite.scale).to({x: 0.15, y: 0.15}, 100, Phaser.Easing.Sinusoidal.Out, true);
            // this._tempTweenArray.push(tween);
            if (bombRange == 1) {
                // debugger;
                this._bomb.alpha = 1;
                this._bombAnimation.play();
                // this._bomb.tint = 0xe74c3c;
                // this._removeAnimation.play();
            } else if (bombRange == 0) {
                this._removeSprite.tint = 0xffffff;
                this._removeAnimation.play();
            } else {
                // молния
            }

            // this._removeSprite.play("play");
            // tween.onComplete.addOnce(() => {
            //     this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
            //     this._destroyAnimation.play();
            // });



        }

        public select(): void {
            if (!window["tutorial"]) {
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE + 0.2);
                if (this.mytype < 6) {
                    this._border.alpha = 1;
                }
            }
        }

        public deselect(): void {
            this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
            this._border.alpha = 0;
        }

        public isSame(a: Gem): boolean {
            return (this._row == a.row) && (this._col == a.col);
        }

        public isNext(gem2: Gem): boolean {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        }
    }
}