/**
 * Created by DEaDA on 3/25/17.
 */
/// <reference path="../lib/phaser.d.ts" />
/// <reference path="Preloader.ts" />
/// <reference path="PlayState.ts" />
/// <reference path="Controller.ts" />
/// <reference path="PlayState.ts" />
/// <reference path="../lib/core.d.ts" />
module mygame {
    export class Boot extends Phaser.State {
        preload() {
            this.load.crossOrigin = 'anonymous';

            let baseURL = window['baseURL'];

            this.game.load.image('preloaderBar', baseURL + "assets/loader_plank.png");
            this.game.load.image("scoreMetr", baseURL + "assets/scoreMetr.png");
            this.game.load.image("logo", baseURL + "assets/logo.png");
        }

        create() {
            Controller.Instance.orientation = Controller.LANDSCAPE;
            if (window["orientation"] == "p")
                Controller.Instance.orientation = Controller.PORTRAIT;
            this.game.input.touch.preventDefault = false;
            this.game.stage.backgroundColor = 0x1d1d1d;
            this.input.maxPointers = 1;
            this.stage.disableVisibilityChange = true;
            //Core.begin(this.game, !this.game.device.desktop);
            this.game.state.start('Preloader');


        }
    }
}