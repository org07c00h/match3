/**
 * Created by DEaDA on 3/25/17.
 */

module mygame {
    import ScaleManager = Phaser.ScaleManager;
    import Sprite = Phaser.Sprite;
    import Game = Phaser.Game;
    import Group = Phaser.Group;
    import Tween = Phaser.Tween;
    import Sound = Phaser.Sound;
    import PhaserTextStyle = Phaser.PhaserTextStyle;
    import Point = Phaser.Point;

    let version: string = '1.0.0';
    // let bg: BackGround;
    export class PlayState extends OState {
        private _bg: Phaser.Sprite;
        private _field: Field;
        private _panel: Panel;
        private _logo: Phaser.Sprite;
        private _container: OSprite;
        private _playFree: Phaser.Sprite;
        private _winTweens: Phaser.Tween[];
        private _canShowEndScreen: boolean;

        private _logoOsprite: OSprite;
        private _gamePanelOsprite: OSprite;
        private _playFreeBtnOsprite: OSprite;
        private _borders: Phaser.Sprite[];
        // private _bench: Phaser.Sprite;
        // private _rake: Phaser.Sprite;

        private _backOverlay: Phaser.Graphics;
        private _panelContainer: OSprite;
        private _panelSprite: Phaser.Sprite;
        private _sunraySprite: Phaser.Sprite;
        private _congrateText: Phaser.BitmapText;
        private _plotText: Phaser.BitmapText;

        private _tutorialOver: Phaser.Sprite;
        private _hand: Phaser.Sprite;
        private _handTween: Phaser.Tween;

        private _tutorialContainer: OSprite;
        private _charTutorialSprite: Phaser.Sprite;
        // private _bubbleTutorialSprite: Phaser.Sprite;
        private _bubbleTutorialText: Phaser.BitmapText;

        private _winscreenShowed: boolean;
        private _fruitFallEmitter: Phaser.Particles.Arcade.Emitter;
        private _timerInstall: number;
        private _safeEmitter: Phaser.Particles.Arcade.Emitter;

        constructor() {
            super(true);
            this._winTweens = [];
        }

        create() {
            GameConfig.COUNTERS_PUBLIC = [0,window["firstElement"],0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            this.game.time.advancedTiming = true;
            this.game.stage.backgroundColor = "#000000";

            this._bg = this.game.add.sprite(0, 0, "bg");
            this._bg.anchor.set(0.5);
            let bgContainer: OSprite = new OSprite(Core.centerX, Core.centerY)
                .myScale(1.2)
                .otherXY(Core.centerY, Core.centerX)
                .end();
            bgContainer.addChild(this._bg);


            this._winscreenShowed = false;

            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.game.stage.smoothed = true;
            this._canShowEndScreen = false;

            Controller.Instance.width = getSize().width;
            Controller.Instance.height = getSize().height;


            this._container = new OSprite(Core.centerX + 200, 0)
                .otherXY(Core.centerY, 0)
                .end();
            this.game.world.addChild(this._container);

            this._borders = [];
            this._borders[0] = this.game.add.sprite(Core.width, Core.height, "atlas2", "ramk_rd");
            this._borders[0].anchor.set(1, 1);
            // this._borders[0].tint = 0x00;

            this._borders[1] = this.game.add.sprite(0, 0, "atlas2", "ramk_lt");
            // this._borders[1].scale.set(-1, -1);
            // this._borders[1].tint = 0x00;

            this._borders[3] = this.game.add.sprite(Core.width, 0, "atlas2", "ramk_rt");
            this._borders[3].anchor.set(1, 0);
            // this._borders[3].scale.set(1, 0)
            ;
            // this._borders[3].tint = 0x00;
            //this._borders[3].alpha = 0;

            this._borders[2] = this.game.add.sprite(0, Core.height, "atlas2", "ramk_ld");
            this._borders[2].anchor.set(0, 1);
            // this._borders[2].scale.set(-1, 1);

            // this._bench = this.game.add.sprite(Core.width, 0, "atlas", "Back-04");
            // this._bench.anchor.set(1, 0);
            // this.game.world.addChild(this._bench);

            // this._rake = this.game.add.sprite(Core.width, Core.height, "atlas", "Back-05");
            // this._rake.anchor.set(1, 1);
            // this.game.world.addChild(this._rake);


            this._borders.forEach(x => this.game.world.addChild(x));



            // init panel
            if (window["collectable"]) {
                this._gamePanelOsprite = new OSprite(0, 0)
                    .myTopOffset(640)
                    .otherXY(Core.centerY, 0)
                    .otherTopOffset(230 + 400 - 35)
                    .myLeftOffset(300)
                    .end();

                this._panel = new Panel(this.game, 0, 0);

                if (this.game.device.iPad) {
                    this._gamePanelOsprite.myTopOffset(750).end();
                }

                this._gamePanelOsprite.addChild(this._panel);

                this._panel.initFruits();
            }

            GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayLandscape);

            this._logoOsprite = new OSprite(920, 0)
                .myTopOffset(5)
                // .myScale(0.65)
                .otherXY(Core.centerY, 0)
                .otherTopOffset(5)
                // .otherScale(0.6)
                .end();

            this._logo = this.game.add.sprite(0, 0, "logo");
            this._logo.anchor.set(0.5, 0);
            this._logoOsprite.addChild(this._logo);






            if (window["collectable"]) {

                this._container = new OSprite(Core.centerX + 400 - 85, Core.centerY - 70 - 25)
                    .otherXY(Core.centerY + 30, Core.centerX + 220 - 25)
                    .end();

                this._field = new Field(this.game, 0, 0, this);

                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    this._panel.updateCounter(type, delta);
                };
                this._field.finalCallback = this.showWinScreen.bind(this);

            } else {

                this._container = new OSprite(Core.centerX, Core.centerY )
                    .otherXY(Core.centerY, Core.centerX - 80)
                    .end();

                this._field = new Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    type + 1;
                };


                this._container.myScale(1).otherScale(1.15).end();
            }
            this.game.world.addChild(this._logoOsprite);
            this._field.firstMatchCallback = () => {
                // this.game.add.tween(this._playFree.scale).to({x: 1.1, y: 1.1}, window["buttonPlaySpeed"], Phaser.Easing.Sinusoidal.Out, true, 0, -1, true);
            }

            this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 5);
            this._playFree = this.game.add.sprite(0, 0, "button");

            if(window["particleOn"]) {
                this._fruitFallEmitter = this.game.add.emitter(0, -100, Math.ceil(4600 / window["fruitSpawRate"]));
                this._fruitFallEmitter.makeParticles("atlas2", ["F_1", "F_2", "F_3", "F_4", "F_5"]);
                //this._fruitFallEmitter.setYSpeed(200 * window["fruitSpawSpeed"], 300 * window["fruitSpawSpeed"]);
                //this._fruitFallEmitter.gravity.set(0, 0);
                this._fruitFallEmitter.minParticleSpeed.setTo(0, 200 * window["fruitSpawSpeed"]);
                this._fruitFallEmitter.maxParticleSpeed.setTo(0, 300 * window["fruitSpawSpeed"]);

                this.createEmitter();
            }

            // init button
            this._playFreeBtnOsprite = new OSprite(Core.centerX + 300, 0).myBottomOffset(150).myScale(1).end();

            if (this.game.device.iPad) {
                this._playFreeBtnOsprite.myBottomOffset(300).end();
            }

            this._playFreeBtnOsprite.otherXY(Core.centerY, 0).otherBottomOffset(140).otherScale(0.95).end();


            this._playFree.anchor.set(0.5, 0.1);
            let buttonText: Phaser.BitmapText = this.game.add.bitmapText(0, 25, "font", window["buttonText"], 50);
            buttonText.anchor.set(0.5, 0.1);
            // buttonText.tint = 0;
            this._playFree.addChild(buttonText);
            // this._playFree.scale.set(1.3);


            this._playFree.inputEnabled = true;
            this._playFree.events.onInputDown.add(() => {
                window["trackClick"]();
            }, this);


            this._playFreeBtnOsprite.addChild(this._playFree);
            if (!window["collectable"]) {
                this._playFree.alpha = 0;
                this._playFree.y = 300;
            }


            if (!Core.isLandscape) {
                this.onPortret();
            }

            /*this._field.addChild(this._playFreeBtnOsprite);
            this._field.addChild(this._logoOsprite);*/



            if(window["tutorial"]) {

                this._playFreeBtnOsprite.visible = false;

                // this._tutorialOver = this.game.make.sprite(-630 + 15, -160 + 40, "bannerMask");
                this._tutorialOver = this.game.make.sprite(-630 + 15 + 5, -160 + 40 - 5, "atlas2", "tutorial_mask");
                this._tutorialOver.scale.set(10);
                this._tutorialOver.alpha = 0.8;
                this._field.addChild(this._tutorialOver);

                let tutorialUP: Phaser.Graphics = this.game.add.graphics(0, -128);
                this._tutorialOver.addChild(tutorialUP);
                tutorialUP.beginFill(0x000000, 1);
                tutorialUP.drawRect(0, 0, 256, 128);
                tutorialUP.endFill();

                let tutorialDOWN: Phaser.Graphics = this.game.add.graphics(0, 72);
                this._tutorialOver.addChild(tutorialDOWN);
                tutorialDOWN.beginFill(0x000000, 1);
                tutorialDOWN.drawRect(0, 0, 128, 128);
                tutorialDOWN.endFill();

                let tutorialLEFT: Phaser.Graphics = this.game.add.graphics(-128, 0);
                this._tutorialOver.addChild(tutorialLEFT);
                tutorialLEFT.beginFill(0x000000, 1);
                tutorialLEFT.drawRect(0, 0, 128, 128);
                tutorialLEFT.endFill();

                let tutorialRIGHT: Phaser.Graphics = this.game.add.graphics(128, 0);
                this._tutorialOver.addChild(tutorialRIGHT);
                tutorialRIGHT.beginFill(0x000000, 1);
                tutorialRIGHT.drawRect(0, 0, 128, 128);
                tutorialRIGHT.endFill();

                this._hand = this.game.add.sprite(100 * 4 + 10 - GameConfig.BLOCK_SIZE * 2, 100 * 2 - 70, "atlas2", "Tutorial-arrow");

                this._hand.anchor.set(0.5);
                this._handTween = this.game.add.tween(this._hand).to({
                    y: 80
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._field.addChild(this._hand);

                this._tutorialContainer = new OSprite(Core.centerX, Core.centerY)
                    .myBottomOffset(0)
                    .myLeftOffset(35)
                    .otherXY(Core.centerY, Core.centerX)
                    .otherRightOffset(500 - 30)
                    .otherBottomOffset(0)
                    .otherScale(0.75)
                    .end();

                this.game.world.addChild(this._tutorialContainer);

                this._charTutorialSprite = this.game.make.sprite(0, 0, "atlas2", "tutorial_dedula");
                this._charTutorialSprite.anchor.set(0, 1);
                this._tutorialContainer.addChild(this._charTutorialSprite);
                this._charTutorialSprite.position.set(0, this._charTutorialSprite.width + 100);

                this.game.add.tween(this._charTutorialSprite).to({
                    y: 0
                }, 750, Phaser.Easing.Sinusoidal.InOut, true);

                // this._bubbleTutorialSprite = this.game.make.sprite(window["bubbleX"], window["bubbleY"], "atlas", "tutor_bubble");
                // this._bubbleTutorialSprite.anchor.set(0, 0.5);
                // this._charTutorialSprite.addChild(this._bubbleTutorialSprite);

                this._bubbleTutorialText = this.game.add.bitmapText(this._charTutorialSprite.width / 2, -200 - 50, "font2", window["tutorialText"][0], 35);
                this._bubbleTutorialText.maxWidth = 500;
                this._bubbleTutorialText.anchor.setTo(0.5, 0.5);
                this._bubbleTutorialText.align = "center";
                this._bubbleTutorialText.tint = 0x733601;
                this._charTutorialSprite.addChild(this._bubbleTutorialText);

            }




            setTimeout(()=>{this._field.endGame();}, window["endGameTimer"]);

            this._timerInstall = 0;
            window['gameStarted'];
            this.game.time.events.loop(Phaser.Timer.SECOND, this.tick, this);

            // let boom: Phaser.Sprite = this.game.add.sprite(Core.centerX, Core.centerY, "boom_anim", "boom_1");
            // boom.scale.set(1.5);
            // let animation: Phaser.Animation = boom.animations.add("play", Phaser.Animation.generateFrameNames("boom_", 1, 38), 30, true);
            // animation.play();
            // this.game.world.addChild(boom);
        }

        private createEmitter(): void {
            this._safeEmitter = this.game.add.emitter(0, 60, 70);
            this._safeEmitter.makeParticles("atlas2", ["particle"]);
            //this._safeEmitter.minParticleSpeed.setTo(-160, -70);
            //this._safeEmitter.maxParticleSpeed.setTo(160, 70);
            this._safeEmitter.setAlpha(1, 0, 1000);
            this._safeEmitter.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            this._safeEmitter.setXSpeed(-50, 50);
            this._safeEmitter.setYSpeed(-50, 50);
            this._safeEmitter.gravity = 0;
            this._safeEmitter.start(false, 1000, 2);
            // this._safeEmitter.on = false;
            this._safeEmitter.width = this._logo.width;
            this._safeEmitter.height = this._logo.height;
            this._logo.addChild(this._safeEmitter)

            let buttonParcticle = this.game.add.emitter(0, 50, 70);
            buttonParcticle.makeParticles("atlas2", ["particle"]);
            //buttonParcticle.minParticleSpeed.setTo(-160, -70);
            //buttonParcticle.maxParticleSpeed.setTo(160, 70);
            buttonParcticle.setAlpha(1, 0, 1000);
            buttonParcticle.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            buttonParcticle.setXSpeed(-50, 50);
            buttonParcticle.setYSpeed(-50, 50);
            buttonParcticle.gravity = 0;
            buttonParcticle.start(false, 1000, 2);
            // buttonParcticle.on = false;
            buttonParcticle.width = this._playFree.width - 300;
            buttonParcticle.height = this._playFree.height - 50;

            this._playFree.addChild(buttonParcticle);

            // this._safeEmitter.on = true;
        }


        private tick() {
            this._timerInstall ++;
            window['playTime'] =  this._timerInstall;
        }

        public getGurrentElementPosition(type: string): Phaser.Point {
            return this._panel.getElementPosition(type)
        }

        public update(): void {
            if (window["collectable"]) {
                this._panel.update();
            }

            if(this._field) {
                this._field.update();
            }

            if(window["particleOn"]) {
                //this._fruitFallEmitter.update();
            }
        }

        public destroyTutorial() {
            this._handTween.stop();
            this._hand.destroy();

            this._playFreeBtnOsprite.visible = true;

            this.game.add.tween(this._charTutorialSprite).to({
                y: this._charTutorialSprite.height + 100
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                this._tutorialContainer.destroy(true);
            }, this);

            this.game.add.tween(this._tutorialOver).to({
                alpha: 0
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                this._tutorialOver.destroy(true);
            }, this);
        }

        public render(): void {
            // super.render();
            // var rect = new Phaser.Rectangle( 100, 100, 100, 100 ) ;

            // this.game.debug.geom( rect, 'rgba(255,0,0,1)' ) ;
            // // this.game.debug.geom( circle, 'rgba(255,255,0,1)' ) ;
            // // this.game.debug.geom( point, 'rgba(255,255,255,1)' ) ;
            // // this.game.debug.pixel( 200, 280, 'rgba(0,255,255,1)' ) ;
            // this.game.debug.text( "This is debug text", 100, 380, 'rgba(0,255,255,1)' );
            // // this.game.debug.text("foobar 100", 10, 20, rg);
            // if (this._field) {
            //     this._field.render();
            // }
        }

        // public update(): void


        private repositionEmitter(): void {
            let dx: number;
            if (Core.isLandscape) {
                dx = Core.centerX - 20;
            } else {
                dx = Core.centerY - 20;
            }
            this._fruitFallEmitter.emitX = this.game.rnd.between(-dx, dx);

            this.game.time.events.add(window["fruitSpawRate"], this.repositionEmitter.bind(this));
        }

        public showWinScreen(): void {

            if(!this._winscreenShowed){
if (window["sounds"]) {
                this.game.sound.play("5", 0.4);
}

                this._winscreenShowed = true;

                this._playFree.visible = false;
                this._playFree.inputEnabled = false;

                this._playFreeBtnOsprite.removeChild(this._playFree);

                this._panelContainer = new OSprite(Core.centerX, Core.centerY)
                    .otherXY(Core.centerY, Core.centerX)
                    .end();
                this.game.stage.addChild(this._panelContainer);

                this._backOverlay = this.game.add.graphics(0, 0);
                this._panelContainer.addChild(this._backOverlay);
                this._backOverlay.beginFill(0x251d1a, 0.8);
                this._backOverlay.drawRect(-1024, -1024, 2048, 2048);
                this._backOverlay.endFill();

                if(window["particleOn"]) {
                    this._panelContainer.addChild(this._fruitFallEmitter);
                    if (Core.isLandscape) {
                        this._fruitFallEmitter.emitY = -Core.centerY - 100;
                    } else {
                        this._fruitFallEmitter.emitY = -Core.centerX - 100;
                    }
                    this._fruitFallEmitter.start(false, 4600, window["fruitSpawRate"]);
                    this.repositionEmitter();
                }


                this._panelSprite = this.game.make.sprite(0, 0, "atlas2", "final_popup");
                this._panelSprite.anchor.set(0.5);




                this._sunraySprite = this.game.make.sprite(0, 0, "atlas2", "lucksherry_sunray");
                this._sunraySprite.scale.set(7);
                this._sunraySprite.anchor.set(0.5);
                this._sunraySprite.alpha = 0.6;
                this._panelContainer.addChild(this._sunraySprite);
                this.game.add.tween(this._sunraySprite).to({angle: 360}, 18500, Phaser.Easing.Linear.None, true, 0, -1)
                this._panelContainer.addChild(this._panelSprite);

                this._panelSprite.addChild(this._playFree);
                this._playFree.position.set(0, 177 + 40);
                this._playFree.anchor.x = 0.5;
                this._playFree.scale.set(0.85, 0.85);
                this._playFree.visible = true;
                this._playFree.inputEnabled = true;

                this._congrateText = this.game.add.bitmapText(0, 45, "font2", window["tutorialText"][1], 50);
                this._congrateText.anchor.setTo(0.5, 0.5);
                this._congrateText.align = "center";
                this._congrateText.tint = 0x733601;
                this._panelSprite.addChild(this._congrateText);


                this._plotText = this.game.add.bitmapText(0, 108 + 35, "font2", window["tutorialText"][2], 50);
                this._plotText.maxWidth = 500;
                this._plotText.anchor.setTo(0.5, 0.5);
                this._plotText.align = "center";
                this._plotText.tint = 0x733601;
                this._panelSprite.addChild(this._plotText);

                // this.game.add.tween(this._playFree.scale).to({x: 0.9, y: 0.9}, window["buttonPlaySpeed"], Phaser.Easing.Sinusoidal.Out, true, 0, -1, true);

                this._panelContainer.alpha = 0;
                this._panelSprite.scale.set(0.1, 0.1);

                this.game.add.tween(this._panelSprite.scale).to({x: 1, y: 1}, 700, Phaser.Easing.Back.Out, true);
                this.game.add.tween(this._panelContainer).to({alpha: 1}, 700, Phaser.Easing.Sinusoidal.Out, true);
            }
        }

        public onLandscape(): void {
            if (this._bg) {
                this._borders[0].position.set(Core.width, Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(Core.width, 0);
                this._borders[2].position.set(0, Core.height);
                // this._rake.position.set(Core.width - 50, Core.height);
                // this._rake.angle = 0;
                // this.game.world.setChildIndex(this._rake, 0);
                // this._bench.position.set(Core.width, 0);
                // this._bench.scale.set(1, 1);

                // this._borders[2].position.set(0, Core.height);
                // this._borders[1].position.set(Core.width, 0);
                // this._borders[0].position.set(Core.width, Core.height);
                // this._borders[3].alpha = 0;
                // this._borders[0].alpha = 1;
            }

            if (this._panel) {
                this._panel.setLandscape();

                if(window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -Core.centerY - 100;
                }
            }
        }

        public onPortret(): void {
            if (this._bg) {
                // this._rake.position.set(0, 1200);
                // this.game.world.setChildIndex(this._rake, this.game.world.children.length - 6);
                // this._rake.angle = 90;
                // this._bench.position.set(Core.width, Core.height);
                // this._bench.scale.set(1, -1);
                // this._borders[2].position.set(this._borders[0].width, Core.height - this._borders[0].height);
                // this._borders[1].position.set(this._borders[0].width, this._borders[0].height);
                // this._borders[0].position.set(Core.width, Core.height);
                // this._borders[3].alpha = 1;
                // this._borders[0].alpha = 0;

                this._borders[0].position.set(Core.width, Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(Core.width, 0);
                this._borders[2].position.set(0, Core.height);

            }

            if (this._panel) {
                this._panel.setPortrait();

                if(window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -Core.centerX - 100;
                }
            }
        }

    }

    function getSize(log = false) {
        let w = 0;
        let h = 0;
        let deW = 0;
        let deH = 0;
        if (!(document.documentElement.clientWidth == 0)) {
            deW = document.documentElement.clientWidth;
            deH = document.documentElement.clientHeight;
        }

        w = deW;
        h = deH;
        if (window.innerWidth > window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        return {width: w, height: h};
    }

    function sendEvent(value, params = null) {
        window["trackEvent"](value, params);
    }

    function ClickInstall() {
        window["trackClick"]();
    }
}