var mygame;
(function (mygame) {
    var Lang = (function () {
        function Lang() {
            this.CONGRAT = { en: 'Congratulations!', de: '  Herzlichen Gluckwunsch !' };
            this.PLOT = { en: 'The plot\nis restored!', de: 'Ein Grundstuck\nrestauriert!' };
            this.TUTORIAL = { en: 'Collect fruits to upgrade garden!', de: 'Sammeln Sie Fruchte,\num den Garten zu verbessern!' };
        }
        Object.defineProperty(Lang, "Instance", {
            get: function () {
                if (this.instance === null || this.instance === undefined) {
                    this.instance = new Lang();
                }
                return this.instance;
            },
            enumerable: true,
            configurable: true
        });
        return Lang;
    }());
    Lang.loc = 'en';
    mygame.Lang = Lang;
})(mygame || (mygame = {}));
