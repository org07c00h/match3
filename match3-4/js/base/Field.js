var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var BlockTypes;
    (function (BlockTypes) {
        BlockTypes[BlockTypes["DISABLED"] = 0] = "DISABLED";
        BlockTypes[BlockTypes["ACTIVE"] = 1] = "ACTIVE";
    })(BlockTypes = mygame.BlockTypes || (mygame.BlockTypes = {}));
    ;
    ;
    ;
    var Field = (function (_super) {
        __extends(Field, _super);
        function Field(game, x, y, playState) {
            var _this = _super.call(this, game, x, y) || this;
            _this._fieldUpperBound = [];
            _this._followGem1 = false;
            _this._followGem2 = false;
            _this._followGem3 = false;
            _this._background = _this.game.add.sprite(-15, -15, "atlas2", "field");
            _this._background.inputEnabled = true;
            _this._background.anchor.set(0, 0);
            _this.addChild(_this._background);
            _this._maxColorStreak = 0;
            _this._boomAnimation = false;
            _this._sequences = [];
            _this._playState = playState;
            _this._tweenArray = [];
            _this._tempSprite = [];
            _this._destroyed = 0;
            _this._turnOffMask = false;
            _this._firstMatch = false;
            _this._finalWasFired = false;
            _this._gameArray = mygame.GameConfig.gameArray;
            _this._fieldUpperBound = [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0];
            _this._streak = [];
            for (var i = 0; i < 6; i++) {
                _this._streak.push(0);
            }
            _this._disabled = false;
            _this._bgContainer = _this.game.add.sprite(0, 0);
            _this._bgContainer.anchor.set(0.5, 0);
            _this.addChild(_this._bgContainer);
            _this._container = _this.game.add.sprite(0, 0);
            _this._container.anchor.set(0.5, 0);
            _this.addChild(_this._container);
            _this._myMask = _this.game.add.graphics(0, 0);
            _this.addChild(_this._myMask);
            _this._container.mask = _this._myMask;
            _this._myMask.beginFill(0xffffff);
            _this._myMask.drawRect(mygame.GameConfig.BLOCK_SIZE, 0, 5 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE);
            _this._myMask.drawRect(0, mygame.GameConfig.BLOCK_SIZE, 7 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE * 3);
            _this._myMask.drawRect(mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE * 4, 5 * mygame.GameConfig.BLOCK_SIZE, mygame.GameConfig.BLOCK_SIZE);
            _this._myMask.endFill();
            _this.drawField();
            _this._selectedGem = null;
            if (window["collectable"]) {
                _this._counter = 0;
            }
            else {
                _this._counter = 0;
            }
            _this.game.input.onUp.add(function () {
                _this.game.input.deleteMoveCallback(_this.gemMove, _this);
                if ((_this._selectedGem !== null && _this._selectedGem !== undefined) && _this._selectedGem.isBomb && (_this._pickedGem == null)) {
                    _this._removeMap = [];
                    for (var i = 0; i < _this._gameArray.length; i++) {
                        _this._removeMap[i] = [];
                        for (var j = 0; j < _this._gameArray[i].length; j++) {
                            _this._removeMap[i].push(0);
                        }
                    }
                    _this.setOff(_this._selectedGem.mytype - 5);
                    _this.destroyGems();
                }
            }, _this);
            _this._canClick = true;
            if (window["particleOn"]) {
            }
            _this.createLightning();
            _this.game.time.events.add(window["restartBlinkTime"], _this.blink, _this);
            _this._createBomb = false;
            _this._condensationPoint = new Phaser.Point(0, 0);
            return _this;
        }
        Field.prototype.createLightning = function () {
            this._lightnings = [];
            var angles = [0, -90, 90, 180];
            var _loop_1 = function (i) {
                var lightning = this_1.game.add.sprite(100, 100, "atlas2", "light_line_10001");
                lightning.anchor.set(0, 0.5);
                lightning.scale.x = 5;
                lightning.alpha = 0;
                lightning.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(function () {
                    lightning.alpha = 0;
                }, this_1);
                lightning.angle = angles[i];
                this_1._container.addChild(lightning);
                this_1._lightnings.push(lightning);
            };
            var this_1 = this;
            for (var i = 0; i < 4; i++) {
                _loop_1(i);
            }
        };
        Object.defineProperty(Field.prototype, "firstMatchCallback", {
            set: function (val) {
                this._firstMatchCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "endMatchCallback", {
            set: function (val) {
                this._endMatchCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "disableField", {
            set: function (val) {
                this._disabled = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "destroyCallback", {
            set: function (val) {
                this._cb = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Field.prototype, "finalCallback", {
            set: function (val) {
                this._finalCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Field.prototype.blink = function () {
            var id = this.game.rnd.between(1, 5);
            var arr = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype == id) {
                        arr.push(this._gameArray[i][j]);
                    }
                }
            }
            arr.forEach(function (x) { return x.playBlink(); });
            this.game.time.events.add(window["restartBlinkTime"], this.blink, this);
        };
        Field.prototype.gemMove = function (event, pX, pY) {
            var distX = pX - this._selectedPoint.x;
            var distY = pY - this._selectedPoint.y;
            var deltaRow = 0;
            var deltaCol = 0;
            if (Math.abs(distX) > mygame.GameConfig.BLOCK_SIZE / 2) {
                if (distX > 0) {
                    deltaCol = 1;
                }
                else {
                    deltaCol = -1;
                }
            }
            else {
                if (Math.abs(distY) > mygame.GameConfig.BLOCK_SIZE / 2) {
                    if (distY > 0) {
                        deltaRow = 1;
                    }
                    else {
                        deltaRow = -1;
                    }
                }
            }
            if (deltaRow + deltaCol != 0) {
                var k = this._selectedGem.row + deltaRow;
                var h = this._selectedGem.col + deltaCol;
                if (k >= 0 && k < this._gameArray.length && h >= 0 && h < this._gameArray[0].length) {
                    this._pickedGem = this._gameArray[this._selectedGem.row + deltaRow][this._selectedGem.col + deltaCol];
                    if (this._pickedGem.mytype > 0) {
                        this._selectedGem.deselect();
                        this.swapGems(true);
                        this.game.input.deleteMoveCallback(this.gemMove, this);
                    }
                    else {
                        this._pickedGem = null;
                    }
                }
                else {
                    this._pickedGem = null;
                }
            }
        };
        Field.prototype.drawField = function () {
            var _this = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                var _loop_2 = function (j) {
                    var type = this_2._gameArray[i][j];
                    var x = mygame.GameConfig.BLOCK_SIZE * (j + 0.5);
                    var y = mygame.GameConfig.BLOCK_SIZE * (i + 0.5);
                    if (type > 0) {
                        var sprite_1 = new mygame.Gem(this_2.game, x, y, type);
                        this_2._container.addChild(sprite_1);
                        sprite_1._sprite.inputEnabled = true;
                        sprite_1.row = i;
                        sprite_1.col = j;
                        sprite_1._sprite.events.onInputDown.add(function (obj, pointer) {
                            var tutorialCondition;
                            tutorialCondition = window["tutorial"] && (sprite_1.row == 2) && (sprite_1.mytype == 2) || !window["tutorial"];
                            tutorialCondition = tutorialCondition || window["tutorial"] && (sprite_1.col == 3) && (sprite_1.mytype == 4);
                            if (_this._canClick && tutorialCondition && !_this._disabled) {
                                if (_this._selectedGem == null) {
                                    _this._selectedGem = sprite_1;
                                    _this._selectedPoint = new Phaser.Point(pointer.x, pointer.y);
                                    sprite_1.select();
                                    _this.game.input.addMoveCallback(_this.gemMove, _this);
                                }
                                else {
                                    _this.gemSelect(sprite_1);
                                }
                            }
                        }, this_2);
                        this_2._gameArray[i][j] = sprite_1;
                    }
                    else {
                        this_2._gameArray[i][j] = new mygame.Gem(this_2.game, x, y, 0);
                        this_2._gameArray[i][j].row = i;
                        this_2._gameArray[i][j].col = j;
                    }
                };
                var this_2 = this;
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    _loop_2(j);
                }
            }
            if (window["tutorial"]) {
                this._border = this.game.add.sprite(2.5 * mygame.GameConfig.BLOCK_SIZE, 2.5 * mygame.GameConfig.BLOCK_SIZE, "atlas2", "selection");
                this._border.scale.set(0.6, 0.6);
                this._border.anchor.set(0.5);
                this._borderTween = this.game.add.tween(this._border.scale).to({
                    x: .6 + .2,
                    y: .6 + .2
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._container.addChild(this._border);
            }
        };
        Field.prototype.endGame = function () {
            var _this = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    try {
                        this._gameArray[i][j]._sprite.inputEnabled = false;
                    }
                    catch (e) {
                        continue;
                    }
                }
            }
            setTimeout(function () { _this._finalCallback(); }, 1000);
        };
        Field.prototype.clearTweens = function () {
            if (this._tweenArray.length > 0) {
                for (var i = 0; i < this._tweenArray.length; i++) {
                    if (this._tweenArray[i] != null)
                        this.game.tweens.remove(this._tweenArray[i]);
                }
            }
            this._tweenArray = [];
            if (this._tempSprite.length > 0) {
                for (var i = 0; i < this._tempSprite.length; i++) {
                    if (this._tempSprite[i] != null)
                        this._tempSprite[i].destroy(true);
                }
            }
            this._tempSprite = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] != null) {
                        this._gameArray[i][j].clearTweens();
                    }
                }
            }
        };
        Field.prototype.replay = function () {
            this._disabled = false;
            this._finalWasFired = false;
            if (window["collectable"]) {
                this._counter = 0;
            }
            else {
                this._counter = 0;
            }
        };
        Field.prototype.gemSelect = function (gem) {
            if (this._selectedGem == null) {
                return;
            }
            if (this._selectedGem.isSame(gem)) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                return;
            }
            if (this._selectedGem.isNext(gem)) {
                this._selectedGem.deselect();
                this._pickedGem = gem;
                this.swapGems();
            }
            else {
                this._selectedGem.deselect();
                this._selectedGem = gem;
                gem.select();
            }
        };
        Field.prototype.resetBoard = function () {
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[0].length; j++) {
                    if (this._gameArray[i][j].mytype > 0) {
                        this._gameArray[i][j].mytype = mygame.GameConfig.gameArrayLandscape[i][j];
                    }
                }
            }
            this._canClick = true;
        };
        Field.prototype.moveExists = function () {
            var str = "";
            var k = 6;
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype < 0) {
                        str += "0";
                    }
                    else {
                        str += this._gameArray[i][j].mytype;
                    }
                    if (this._gameArray[i][j].mytype > 5) {
                        return true;
                    }
                }
                str += k;
                k++;
                if (k > 9) {
                    k = 6;
                }
                str += "\n";
            }
            var myRe = /(\d)(?:.|(?:.|\n){9}|(?:.|\n){6})?\1\1|(\d)\2(?:.|(?:.|\n){9}|(?:.|\n){6})?\2|(\d)(?:.|\n){7}\3(?:.|(?:.|\n){9})\3|(\d)(?:.|(?:.|\n){9})\4(?:.|\n){7}\4|(\d)(?:(?:.|\n){7,9}|(?:.|\n){17})\5(?:.|\n){8}\5|(\d)(?:.|\n){8}\6(?:(?:.|\n){7,9}|(?:.|\n){17})\6/;
            if (myRe.exec(str) == null) {
                return false;
            }
            return true;
        };
        Field.prototype.swapGems = function (swapBack) {
            var _this = this;
            if (swapBack === void 0) { swapBack = true; }
            if (window["tutorial"]) {
                var minCol = Math.min(this._selectedGem.col, this._pickedGem.col);
                var maxCol = Math.max(this._selectedGem.col, this._pickedGem.col);
                if (Math.abs(this._pickedGem.col - this._selectedGem.col) != 1 || (minCol != 2 && maxCol != 3)) {
                    this._selectedGem = null;
                    this._pickedGem = null;
                    return;
                }
                else {
                    window["tutorial"] = false;
                    for (var i = 0; i < this._gameArray.length; i++) {
                        for (var j = 0; j < this._gameArray[i].length; j++) {
                            this._playState.destroyTutorial();
                            this._borderTween.stop();
                            this._border.destroy();
                        }
                    }
                }
            }
            this._canClick = false;
            this._coolAnimation = true;
            this._verticalAnimation = false;
            this._gameArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gameArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;
            var rowSel = this._selectedGem.row;
            var colSel = this._selectedGem.col;
            var rowPic = this._pickedGem.row;
            var colPic = this._pickedGem.col;
            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            var posPicked = new Phaser.Point((colPic + 0.5) * mygame.GameConfig.BLOCK_SIZE, (rowPic + 0.5) * mygame.GameConfig.BLOCK_SIZE);
            var posSelected = new Phaser.Point((colSel + 0.5) * mygame.GameConfig.BLOCK_SIZE, (rowSel + 0.5) * mygame.GameConfig.BLOCK_SIZE);
            var tweenSelected = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y
            }, mygame.GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);
            this._tweenArray.push(tweenSelected);
            var tweenPicked = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y
            }, mygame.GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);
            this._tweenArray.push(tweenPicked);
            var type1 = this._selectedGem.mytype;
            var type2 = this._pickedGem.mytype;
            tweenPicked.onComplete.add(function () {
                if (_this._selectedGem !== null && _this._selectedGem !== undefined && _this._pickedGem !== undefined && _this._pickedGem !== null && (_this._selectedGem.isBomb || _this._pickedGem.isBomb)) {
                    _this._removeMap = [];
                    for (var i = 0; i < _this._gameArray.length; i++) {
                        _this._removeMap[i] = [];
                        for (var j = 0; j < _this._gameArray[i].length; j++) {
                            _this._removeMap[i].push(0);
                        }
                    }
                    if (_this._selectedGem.isBomb) {
                        _this.setOff(_this._selectedGem.mytype - 5, _this._selectedGem.row, _this._selectedGem.col);
                        _this.destroyGems();
                    }
                    else {
                        _this._selectedGem = _this._pickedGem;
                        _this.setOff(_this._selectedGem.mytype - 5, _this._selectedGem.row, _this._selectedGem.col);
                        _this.destroyGems();
                    }
                }
                else {
                    if (_this.isMatchOnBoard(rowPic, colPic, type1, rowSel, colSel, type2)) {
                        _this.handleMatches();
                        _this._selectedGem = null;
                        _this._pickedGem = null;
                    }
                    else if (swapBack) {
                        _this.swapGems(false);
                    }
                }
            }, this);
            tweenPicked.start();
            tweenSelected.start();
            if (!swapBack) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                this._pickedGem = null;
                this._canClick = true;
            }
        };
        Field.prototype.isMatchOnBoard = function (row1, col1, type1, row2, col2, typr2) {
            return this.isMatch(row1, col1, type1) || this.isMatch(row2, col2, typr2);
        };
        Field.prototype.isMatch = function (row, col, type) {
            return this.isHorizontal(row, col, type) || this.isVertical(row, col, type);
        };
        Field.prototype.isHorizontal = function (row, col, type) {
            var start;
            var end;
            var streak = 0;
            for (var i = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gameArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (var j = start; j >= end; j--) {
                    if (this._gameArray[row][j].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        };
        Field.prototype.setOff = function (range, x, y) {
            if (x == null) {
                if (!this._selectedGem.isBomb || range < 1) {
                    return;
                }
                if (!this._canClick) {
                    return;
                }
            }
            this._canClick = false;
            this._boomAnimation = true;
            var p;
            if (x !== undefined && x !== null) {
                p = new Phaser.Point(x, y);
            }
            else {
                p = new Phaser.Point(this._selectedGem.row, this._selectedGem.col);
                this._selectedGem.stopTween();
            }
            this._bombPosition = p;
            this._selectedGem = null;
            this._pickedGem = null;
            this._bombRange = range;
            switch (range) {
                case 1:
                    {
                        for (var i = -1; i < 2; i++) {
                            var k = void 0;
                            var h = void 0;
                            if (p.x + i >= 0 && p.x + i < this._gameArray.length) {
                                k = p.x + i;
                                h = p.y;
                                if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                    this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                                }
                                if (this._gameArray[k][h].mytype > 0) {
                                    this._removeMap[k][h] = 1;
                                }
                            }
                            if (p.y + i >= 0 && p.y + i < this._gameArray[0].length) {
                                k = p.x;
                                h = p.y + i;
                                if (this._gameArray[k][h].isBomb && (k != p.x || h != p.y) && this._removeMap[k][h] == 0) {
                                    this.setOff(this._gameArray[k][h].mytype - 5, k, h);
                                }
                                if (this._gameArray[k][h].mytype > 0) {
                                    this._removeMap[k][h] = 1;
                                }
                            }
                        }
                    }
                    break;
                case 2:
                    {
                    }
                    break;
            }
            this._coolAnimation = false;
        };
        Field.prototype.isVertical = function (row, col, type) {
            var start;
            var end;
            var streak = 0;
            for (var j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gameArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (var i = start; i >= end; i--) {
                    if (this._gameArray[i][col].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }
            return false;
        };
        Field.prototype.handleMatches = function () {
            var _this = this;
            if (!this._firstMatch) {
                this._firstMatch = true;
                if (this._firstMatchCallback) {
                    this._firstMatchCallback();
                }
            }
            this._removeMap = [];
            for (var i = 0; i < this._gameArray.length; i++) {
                this._removeMap[i] = [];
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    this._removeMap[i].push(0);
                }
            }
            this.handleHorizontalMatches();
            this.handleVerticalMatches();
            this._sequences.forEach(function (x) {
                for (var i = 0; i < x.length; i++) {
                    var p = x.get(i);
                    _this._removeMap[p.x][p.y] = 1;
                }
            });
            this.destroyGems();
            this._coolAnimation = false;
        };
        Field.prototype.destroyGems = function () {
            var _this = this;
            this._destroyed = 0;
            var condensationPoint;
            var condensationType;
            if (this._selectedGem != null) {
                if (this._removeMap[this._selectedGem.row][this._selectedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._selectedGem.x, this._selectedGem.y);
                    condensationType = this._selectedGem.mytype;
                    this._condensationPoint.set(this._selectedGem.row, this._selectedGem.col);
                    var i = 0;
                    while (i < this._sequences.length) {
                        var s = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                }
                else if (this._removeMap[this._pickedGem.row][this._pickedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._pickedGem.x, this._pickedGem.y);
                    condensationType = this._pickedGem.mytype;
                    this._condensationPoint.set(this._pickedGem.row, this._pickedGem.col);
                    var i = 0;
                    while (i < this._sequences.length) {
                        var s = this._sequences[i];
                        if (s.contains(this._condensationPoint.x, this._condensationPoint.y)) {
                            if (s.isBomb) {
                                s.setBombPosition(this._condensationPoint.x, this._condensationPoint.y);
                            }
                            break;
                        }
                        i++;
                    }
                }
                else {
                    condensationPoint = new Phaser.Point(1, 1);
                    condensationType = 0;
                }
            }
            if (this._sequences.length > 0) {
                if (this._sequences.length == 1 && this._sequences[0].length > 3 && condensationPoint) {
                    condensationPoint.x = (this._condensationPoint.y + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    condensationPoint.y = (this._condensationPoint.x + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    this._sequences.forEach(function (s) {
                        s.forEach(function (p) {
                            var gem = _this._gameArray[p.x][p.y];
                            _this.game.add.tween(gem).to({
                                x: condensationPoint.x,
                                y: condensationPoint.y
                            }, 300, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                                _this.destroyGem(gem, false, true);
                            });
                        });
                        _this.playMatchSound(s.length);
                    });
                }
                else {
                    this._sequences.forEach(function (s) {
                        s.forEach(function (p) {
                            var gem = _this._gameArray[p.x][p.y];
                            _this.destroyGem(gem);
                        });
                        _this.playMatchSound(s.length);
                    });
                }
                this._destroyed = 0;
                this.game.time.events.add(400, this.makeGemsFall, this);
            }
            else if (this._bombRange == 1) {
                var x = this._bombPosition.x;
                var y = this._bombPosition.y;
                for (var i = 0; i < this._removeMap.length; i++) {
                    for (var j = 0; j < this._removeMap[i].length; j++) {
                        if (this._removeMap[i][j] > 0 && !(i == x && j == y)) {
                            this.destroyGem(this._gameArray[i][j], true);
                        }
                    }
                }
                if (window["sounds"]) {
                    this.game.sound.play("1", 0.4);
                }
                this._gameArray[x][y].playDestroy(1);
                this.game.time.events.add(750, this.makeGemsFall, this);
            }
            else {
                var x = this._bombPosition.x;
                var y = this._bombPosition.y;
                var i = this._bombPosition.x;
                var j = this._bombPosition.x;
                var k = this._bombPosition.y;
                var h = this._bombPosition.y;
                for (var d = 1; d < Math.max(this._gameArray.length, this._gameArray[0].length); d++) {
                    if (i - d >= 0 && this._gameArray[i - d][y].mytype > 0) {
                        this.destroyGem(this._gameArray[i - d][y]);
                        this._removeMap[i - d][y] = 1;
                    }
                    if (j + d < this._gameArray.length && this._gameArray[j + d][y].mytype > 0) {
                        this.destroyGem(this._gameArray[j + d][y]);
                        this._removeMap[j + d][y] = 1;
                    }
                    if (k - d >= 0 && this._gameArray[x][k - d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][k - d]);
                        this._removeMap[x][k - d] = 1;
                    }
                    if (h + d < this._gameArray[0].length && this._gameArray[x][h + d].mytype > 0) {
                        this.destroyGem(this._gameArray[x][h + d]);
                        this._removeMap[x][h + d] = 1;
                    }
                }
                this._removeMap[x][y] = 1;
                var bomb_1 = this._gameArray[x][y];
                this._gameArray[x][y].playDestroy(0);
                this.game.time.events.add(750, this.makeGemsFall, this);
                if (window["sounds"]) {
                    this.game.sound.play("12", 0.4);
                }
                this._lightnings.forEach(function (x) {
                    x.alpha = 1;
                    x.position.set(bomb_1.x, bomb_1.y);
                    x.play("play");
                });
            }
            this._bombRange = 0;
        };
        Field.prototype.playMatchSound = function (n) {
            var num = n - 3;
            if (num < 0) {
                return;
            }
            var sounds = [17, 16, 15, 14];
            if (num > sounds.length) {
                num = sounds.length - 1;
            }
            if (window["sounds"]) {
                this.game.sound.play(sounds[num].toString(), 0.4);
            }
        };
        Field.prototype.destroyGem = function (gem, bomb, condensation) {
            var _this = this;
            if (bomb === void 0) { bomb = false; }
            if (condensation === void 0) { condensation = false; }
            var N;
            if (this._bombRange > 1) {
                N = 1;
            }
            else {
                N = this.game.rnd.between(3, 5);
            }
            this._counter++;
            if (gem.mytype > 0) {
                var _loop_3 = function (i) {
                    var phi = this_3.game.rnd.between(0, Phaser.Math.PI2);
                    var vec = new Phaser.Point(Math.sin(phi), Math.cos(phi));
                    var r = this_3.game.rnd.between(30, 50);
                    vec.setMagnitude(r);
                    var worldPos = gem.worldPosition.clone();
                    vec.x += worldPos.x;
                    vec.y += worldPos.y;
                    var p;
                    if (gem.mytype > 5) {
                        var t = +gem._sprite.frameName.split("_")[1];
                        p = new mygame.GemPatricle(this_3.game, vec.x, vec.y, t);
                    }
                    else {
                        p = new mygame.GemPatricle(this_3.game, vec.x, vec.y, gem.mytype);
                    }
                    this_3.game.world.addChild(p);
                    var toPos = this_3._playState.getGurrentElementPosition("foo");
                    var timeAnimation = 900 + this_3.game.rnd.integerInRange(-100, 200);
                    this_3.game.add.tween(p.scale).to({
                        x: 3.5,
                        y: 3.5
                    }, timeAnimation / 2, Phaser.Easing.Sinusoidal.InOut, true, 0, 0, true);
                    this_3.game.add.tween(p).to({
                        x: [p.x, Math.max((p.x - toPos.x), (toPos.x - p.x)) / 2 + this_3.game.rnd.integerInRange(-400, 800), toPos.x],
                        y: [p.y, Math.max((p.y - toPos.y), (toPos.y - p.y)) / 2 + this_3.game.rnd.integerInRange(-400, 400), toPos.y]
                    }, timeAnimation, Phaser.Easing.Sinusoidal.Out, true, 100).interpolation(function (v, k) { return Phaser.Math.bezierInterpolation(v, k); }).onComplete.add(function () {
                        if (i == N - 1) {
                            _this._cb(1, 1);
                        }
                        if (window["sounds"] && Math.random() < 0.5) {
                            var sounds = ["21", "22", "23"];
                            _this.game.sound.play(Phaser.ArrayUtils.getRandomItem(sounds), 0.2);
                        }
                        p.destroy();
                    }, this_3);
                };
                var this_3 = this;
                for (var i = 0; i < N; i++) {
                    _loop_3(i);
                }
            }
            if (condensation) {
                gem.playDestroy(22);
            }
            else {
                if (!bomb) {
                    gem.playDestroy(0);
                }
                else {
                    gem.playDestroy(0);
                }
            }
        };
        Field.prototype.render = function () {
            this.game.debug.text("foobar 100", 10, 20);
            if (this._gameArray) {
                this._gameArray[2][2].render();
            }
        };
        Field.prototype.handleHorizontalMatches = function () {
            var _loop_4 = function (i) {
                var colorStreak = 1;
                var currentColor = -100500;
                var startStreak = 0;
                for (var j = 0; j < this_4._gameArray[i].length; j++) {
                    if (this_4._gameArray[i][j].mytype == currentColor && this_4._gameArray[i][j].mytype > 0) {
                        colorStreak++;
                        if (colorStreak >= 4) {
                            this_4._createBomb = true;
                            this_4._maxColorStreak = Math.max(this_4._maxColorStreak, colorStreak);
                        }
                    }
                    if (this_4._gameArray[i][j].mytype != currentColor || j == this_4._gameArray[i].length - 1) {
                        if (colorStreak >= 3) {
                            var seq = new mygame.Sequence();
                            this_4._sequences.push(seq);
                            var currentId = this_4._sequences.length - 1;
                            var _loop_5 = function (k) {
                                var id = currentId;
                                this_4._sequences.forEach(function (s, n) {
                                    if (s.contains(i, startStreak + k)) {
                                        id = n;
                                    }
                                });
                                if (id != currentId) {
                                    this_4._sequences[id].merge(seq);
                                    seq = this_4._sequences[id];
                                    currentId = id;
                                }
                                else {
                                    seq.add(i, startStreak + k);
                                }
                            };
                            for (var k = 0; k < colorStreak; k++) {
                                _loop_5(k);
                            }
                        }
                        startStreak = j;
                        colorStreak = 1;
                        currentColor = this_4._gameArray[i][j].mytype;
                    }
                }
            };
            var this_4 = this;
            for (var i = 0; i < this._gameArray.length; i++) {
                _loop_4(i);
            }
        };
        Field.prototype.handleVerticalMatches = function () {
            var _loop_6 = function (i) {
                var colorStreak = 1;
                var currentColor = -100500;
                var startStreak = 0;
                for (var j = 0; j < this_5._gameArray.length; j++) {
                    if (this_5._gameArray[j][i].mytype == currentColor && this_5._gameArray[j][i].mytype > 0) {
                        colorStreak++;
                        if (colorStreak >= 4) {
                            this_5._createBomb = true;
                            this_5._maxColorStreak = Math.max(this_5._maxColorStreak, colorStreak);
                        }
                    }
                    if (this_5._gameArray[j][i].mytype != currentColor || j == this_5._gameArray.length - 1) {
                        if (colorStreak >= 3) {
                            var seq = new mygame.Sequence();
                            this_5._sequences.push(seq);
                            var currentId = this_5._sequences.length - 1;
                            var _loop_7 = function (k) {
                                var id = currentId;
                                this_5._sequences.forEach(function (s, n) {
                                    if (s.contains(startStreak + k, i)) {
                                        id = n;
                                    }
                                });
                                if (id != currentId) {
                                    this_5._sequences[id].merge(seq);
                                    seq = this_5._sequences[id];
                                    currentId = id;
                                }
                                else {
                                    seq.add(startStreak + k, i);
                                }
                            };
                            for (var k = 0; k < colorStreak; k++) {
                                _loop_7(k);
                            }
                        }
                        startStreak = j;
                        colorStreak = 1;
                        currentColor = this_5._gameArray[j][i].mytype;
                    }
                }
            };
            var this_5 = this;
            for (var i = 0; i < this._gameArray[0].length; i++) {
                _loop_6(i);
            }
        };
        Field.prototype.checkGlobalMatch = function () {
            for (var i = 0; i < this._gameArray.length; i++) {
                for (var j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype <= 0) {
                        continue;
                    }
                    if (this.isMatch(i, j, this._gameArray[i][j].mytype)) {
                        return true;
                    }
                }
            }
            return false;
        };
        Field.prototype.swap = function (x1, y1, x2, y2, dx) {
            if (dx === void 0) { dx = 0; }
            var temp = this._gameArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;
            this._gameArray[x1][y1] = this._gameArray[x2][y2];
            this._gameArray[x1][y1].row = x1 - dx;
            this._gameArray[x1][y1].col = y1;
            this._gameArray[x2][y2] = temp;
        };
        Field.prototype.makeGemsFall = function () {
            var _this = this;
            if (this._destroyed > 0) {
                this.game.time.events.add(200, this.makeGemsFall, this);
            }
            this._sequences.forEach(function (seq) {
                if (seq.isBomb) {
                    var p = seq.bombPosition;
                    _this._removeMap[p.x][p.y] = 0;
                    var gem = _this._gameArray[p.x][p.y];
                    try {
                        _this.game.world.removeChild(gem);
                        _this._container.addChild(gem);
                    }
                    catch (e) {
                    }
                    gem.x = (gem.col + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    gem.y = (gem.row + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    gem.spawnBoom(seq.length);
                    _this._maxColorStreak = 0;
                    _this._createBomb = false;
                    _this._boomAnimation = false;
                }
            });
            if (this._turnOffMask) {
                this._turnOffMask = false;
                this._container.mask = this._myMask;
            }
            this._sequences.length = 0;
            var holes = this.findHoles();
            var tweens = [];
            var fallen = 0;
            for (var k = 0; k < holes.length; k++) {
                var hole = holes[k];
                var above = void 0;
                var start = void 0;
                var end = void 0;
                if (hole.hasHoleAbove) {
                    above = holes[k + 1];
                    start = above.row + above.length - hole.length;
                    end = hole.row - start;
                }
                else {
                    start = 0;
                    end = hole.row - this._fieldUpperBound[hole.col];
                }
                for (var i = 0; i < end; i++) {
                    this.swap(hole.row - 1 - i, hole.col, hole.row + hole.length - 1 - i, hole.col);
                }
                if (!hole.hasHoleAbove) {
                    for (var i = this._fieldUpperBound[hole.col]; i < this._fieldUpperBound[hole.col] + hole.length; i++) {
                        var gem = this._gameArray[i][hole.col];
                        var rnd = this.game.rnd.between(1, 5);
                        gem.mytype = rnd;
                        gem.x = (hole.col + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - hole.length + 0.5) * mygame.GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[hole.col];
                    end = hole.row + hole.length;
                }
                else {
                    var len = hole.row - (above.row + above.length - hole.length);
                    start = hole.row + hole.length - len;
                    end = hole.row + hole.length;
                }
                var _loop_8 = function (i) {
                    var gem = this_6._gameArray[i][hole.col];
                    fallen++;
                    var tween = this_6.game.add.tween(gem).to({
                        y: gem.y + hole.length * mygame.GameConfig.BLOCK_SIZE
                    }, mygame.GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, false);
                    this_6._tweenArray.push(tween);
                    tween.onComplete.add(function () {
                        _this._gameArray[gem.row][gem.col] = gem;
                        fallen--;
                        if (fallen == 0) {
                            if (_this.checkGlobalMatch()) {
                                _this.handleMatches();
                            }
                            else {
                                if (!_this.moveExists()) {
                                    _this.resetBoard();
                                }
                                else {
                                    _this._canClick = true;
                                }
                                _this._selectedGem = null;
                                if (!_this._finalWasFired) {
                                    if (_this._counter >= window["totalElemets"]) {
                                        _this._finalWasFired = true;
                                        mygame.GameConfig.COUNTERS_PUBLIC = [0, window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0, 100, 100];
                                        _this.endGame();
                                    }
                                }
                            }
                        }
                    });
                    tweens.push(tween);
                };
                var this_6 = this;
                for (var i = start; i < end; i++) {
                    _loop_8(i);
                }
            }
            tweens.forEach(function (x) { return x.start(); });
        };
        Field.prototype.createFlyTween = function (gem, flyawayPoint, delay) {
            var _this = this;
            var scaleTween = this.game.add.tween(gem.scale).to({
                x: mygame.GameConfig.PANEL_GEM_SCALE,
                y: mygame.GameConfig.PANEL_GEM_SCALE
            }, mygame.GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false);
            this._tweenArray.push(scaleTween);
            scaleTween.onComplete.addOnce(function () {
                gem.scale.setTo(1);
            }, this);
            var flyTween = this.game.add.tween(gem).to({
                x: flyawayPoint.x,
                y: flyawayPoint.y
            }, mygame.GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false, delay);
            this._tweenArray.push(flyTween);
            this._gem1ToFollow = gem;
            flyTween.onStart.addOnce(function () {
                if (window["particleOn"]) {
                    if (_this._destroyed * 100 == (delay + 100)) {
                        _this._fruitSpecialEmmiter1.x = _this._gem1ToFollow.x;
                        _this._fruitSpecialEmmiter1.y = _this._gem1ToFollow.y;
                        _this._fruitSpecialEmmiter1.on = true;
                        _this._followGem1 = true;
                    }
                }
                scaleTween.start();
            });
            flyTween.onComplete.add(function (obj, tween) {
                if (gem) {
                    if (window["particleOn"]) {
                        if (_this._destroyed == 1) {
                            _this._fruitSpecialEmmiter1.on = false;
                            _this._followGem1 = false;
                        }
                        for (var i = void 0; i < _this._tempSprite.length; i++) {
                            if (_this._tempSprite[i] == gem) {
                                _this._tempSprite.splice(i, 1);
                                break;
                            }
                        }
                    }
                    _this.game.world.removeChild(gem);
                    _this._container.addChild(gem);
                    gem.position.set(-1500, -1500);
                    _this._destroyed--;
                    _this._cb(gem.mytype, 1);
                    _this._counter++;
                    if (_this._boomAnimation) {
                        _this._boomAnimation = false;
                    }
                    if (_this._destroyed == 0) {
                        _this.makeGemsFall();
                    }
                }
            }, this);
            return flyTween;
        };
        Field.prototype.updateSpecialGemEmmiter = function () {
            if (this._followGem1) {
                this._fruitSpecialEmmiter1.x = this._gem1ToFollow.x;
                this._fruitSpecialEmmiter1.y = this._gem1ToFollow.y;
            }
        };
        Object.defineProperty(Field.prototype, "gameArray", {
            get: function () {
                var result = [];
                for (var i = 0; i < this._gameArray.length; i++) {
                    result.push([]);
                    for (var j = 0; j < this._gameArray[i].length; j++) {
                        result[i][j] = this._gameArray[i][j].mytype;
                    }
                }
                return result;
            },
            enumerable: true,
            configurable: true
        });
        Field.prototype.generateFlyAwayTween = function (block, condensationPoint, flyawayPoint, delay) {
            this._container.removeChild(block);
            this.addChild(block);
            var flyAwayTween = this.createFlyTween(block, flyawayPoint, delay);
            if (this._verticalAnimation) {
                return flyAwayTween;
            }
            else {
                var condensateTween = this.game.add.tween(block).to({
                    x: condensationPoint.x
                }, mygame.GameConfig.CONDENSATION_TIME, Phaser.Easing.Linear.None, false);
                this._tweenArray.push(condensateTween);
                condensateTween.onComplete.add(function (sprite, tween) {
                    flyAwayTween.start();
                });
                return condensateTween;
            }
        };
        Field.prototype.condensate = function (point) {
            var condenstationStack = [];
            var delay = 0;
            for (var i = 0; i < this._removeMap.length; i++) {
                for (var j = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._destroyed++;
                        var block = this._gameArray[i][j];
                        var flyAwayPoint = void 0;
                        if (block.mytype == 2) {
                            flyAwayPoint = this._playState.getGurrentElementPosition("second");
                        }
                        else {
                            flyAwayPoint = this._playState.getGurrentElementPosition("first");
                        }
                        condenstationStack.push(this.generateFlyAwayTween(block, point, flyAwayPoint, delay));
                        condenstationStack[condenstationStack.length - 1].onStart.addOnce(function () {
                        });
                        delay += 100;
                    }
                }
            }
            condenstationStack.forEach(function (el) {
                el.start();
            });
        };
        Field.prototype.findHoles = function () {
            var result = [];
            for (var j = 0; j < this._gameArray[0].length; j++) {
                for (var i = this._gameArray.length - 1; i >= 0; i--) {
                    if (this._removeMap[i][j] > 0) {
                        var length_1 = 1;
                        var row = i;
                        var found = true;
                        var wasPushed = false;
                        for (var k = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] > 0) {
                                length_1++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            }
                            else if (found) {
                                result.push({ row: row, col: j, length: length_1, hasHoleAbove: false });
                                found = false;
                                wasPushed = true;
                                var id = result.length - 2;
                                if (id >= 0 && result[id].col == result[id + 1].col) {
                                    result[id].hasHoleAbove = true;
                                }
                            }
                        }
                        if (!wasPushed) {
                            result.push({ row: row, col: j, length: length_1, hasHoleAbove: false });
                            var id = result.length - 2;
                            if (id >= 0 && result[id].col == result[id + 1].col) {
                                result[id].hasHoleAbove = true;
                            }
                        }
                        break;
                    }
                }
            }
            return result;
        };
        Field.prototype.update = function () {
            if (window["particleOn"]) {
            }
        };
        Field.prototype.destroy = function () {
            var _this = this;
            this.clearTweens();
            setTimeout(function () {
                _this.game.input.onUp.removeAll(_this);
                _this.game.tweens.removeFrom(_this, true);
                _super.prototype.destroy.call(_this, true);
            }, 10);
        };
        return Field;
    }(Phaser.Sprite));
    mygame.Field = Field;
})(mygame || (mygame = {}));
