var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Gem = (function (_super) {
        __extends(Gem, _super);
        function Gem(game, x, y, type) {
            var _this = _super.call(this, game, x, y) || this;
            _this._tempTweenArray = [];
            _this._blinks = [];
            _this._mytype = type;
            _this._spriteName = mygame.GameConfig.NAMES[_this._mytype];
            if (type > 0) {
                _this._canPlayBlibk = true;
                _this._sprite = _this.game.add.sprite(0, 0, "atlas2", "F_" + _this._mytype);
                _this._sprite.anchor.set(0.5);
                _this.addChild(_this._sprite);
                _this._boom = _this.game.add.sprite(0, 0, "atlas2", "F_1");
                _this._boom.anchor.set(0.5);
                _this._boom.visible = false;
                _this.addChild(_this._boom);
                _this._destroyAnimation = _this._sprite.animations.add("set off", Phaser.Animation.generateFrameNames("F_", _this._mytype, _this._mytype), 18, false);
                _this._destroyAnimation.onStart.add(function () {
                    _this._sprite.alpha = 1;
                }, _this);
                _this._bombBoomAnimation = _this._boom.animations.add("b_bomb", Phaser.Animation.generateFrameNames("bb_", 1, 10), 18, false);
                _this._border = _this.game.add.sprite(0, 0, "atlas2", "selection");
                _this._border.anchor.set(0.5);
                _this._border.scale.set(0.7);
                _this.addChild(_this._border);
                _this._border.alpha = 0;
                _this._destroyAnimation.onComplete.add(function () {
                    _this.animationEnd();
                }, _this);
                _this._bombTween = _this.game.add.tween(_this._sprite.scale).to({
                    x: 1.1,
                    y: 1.1
                }, 500, Phaser.Easing.Sinusoidal.InOut, false, 0, -1, true);
                _this._sprite.alpha = 1;
                _this._lightningSprite = _this.game.add.sprite(0, 0, "atlas2", "light_line_10001");
                _this._lightningSprite.anchor.set(0.5);
                _this._lightningSprite.alpha = 0;
                _this._lightningSprite.animations.add("play", Phaser.Animation.generateFrameNames("light_line_", 10001, 10022), 24).onComplete.add(function () {
                    _this._lightningSprite.alpha = 0;
                }, _this);
                _this.addChild(_this._lightningSprite);
                _this.createBlink();
                _this.createRemoveAnimation();
                _this.createBombAnimation();
            }
            return _this;
        }
        Gem.prototype.createBombAnimation = function () {
            var _this = this;
            this._bomb = this.game.add.sprite(0, 0, "atlas2", "boom_8");
            this._bomb.scale.set(1.5);
            this._bomb.anchor.set(0.5);
            this._bomb.alpha = 0;
            this.addChild(this._bomb);
            var animation = this._bomb.animations.add("play", Phaser.Animation.generateFrameNames("boom_", 8, 29), 30);
            this._bombAnimation = animation;
            animation.onStart.add(function () {
                _this._canPlayBlibk = false;
                _this._blinkSprite.alpha = 0;
                if (_this._mytype <= 5) {
                    _this._blinks[_this._mytype - 1].stop();
                }
                _this._sprite.alpha = 0;
                _this._bomb.alpha = 1;
            });
            animation.onComplete.add(function () {
                _this._bomb.alpha = 0;
            });
        };
        Gem.prototype.createRemoveAnimation = function () {
            var _this = this;
            this._removeSprite = this.game.add.sprite(0, 0, "atlas2", "remove_anim_10001");
            this._removeSprite.anchor.set(0.5);
            this._removeSprite.alpha = 0;
            this.addChild(this._removeSprite);
            var animation = this._removeSprite.animations.add("play", Phaser.Animation.generateFrameNames("remove_anim_", 10001, 10018), 24);
            this._removeAnimation = animation;
            animation.onStart.add(function () {
                _this._canPlayBlibk = false;
                _this._blinkSprite.alpha = 0;
                if (_this._mytype <= 5) {
                    _this._blinks[_this._mytype - 1].stop();
                }
                _this._sprite.alpha = 0;
                _this._removeSprite.alpha = 1;
            });
            animation.onComplete.add(function () {
                _this._removeSprite.alpha = 0;
            });
        };
        Gem.prototype.render = function () {
            this.game.debug.spriteBounds(this._sprite);
            this.game.debug.spriteInfo(this._sprite, 100, 100, "#00ff00");
        };
        Gem.prototype.playBlink = function () {
            if (this._mytype > 0 && this._canPlayBlibk && this._mytype <= 5) {
                this._blinkSprite.alpha = 1;
                this._blinks[this._mytype - 1].play();
            }
        };
        Gem.prototype.createBlink = function () {
            var _this = this;
            this._blinkSprite = this.game.add.sprite(0, 0, "atlas2", "yellow_blink_10013");
            this._blinkSprite.alpha = 0;
            this._blinkSprite.anchor.set(0.5);
            this.addChild(this._blinkSprite);
            var names = ["blue", "green", "pink", "red", "yellow"];
            for (var i = 0; i < 5; i++) {
                var animation = this._blinkSprite.animations.add("blink_" + names[i], Phaser.Animation.generateFrameNames(names[i] + "_blink_", 10001, 10025), 12);
                animation.onStart.add(function () {
                    _this._blinkSprite.alpha = 1;
                }, this);
                animation.onComplete.add(function () {
                    _this._blinkSprite.alpha = 0;
                }, this);
                this._blinks.push(animation);
            }
        };
        Gem.prototype.animationEnd = function () {
            if (!this._onCompleteFired) {
                this._onCompleteFired = true;
                this._sprite.alpha = 0;
                if (this._destroyCallback) {
                    this._onCompleteFired = true;
                    this._destroyCallback();
                }
            }
        };
        Gem.prototype.boomAnimation = function () {
            this._sprite.alpha = 0;
            if (this._mytype >= 6) {
                this._bombBoomAnimation.play();
            }
            else {
                this._boomAnimation.play();
            }
        };
        Gem.prototype.spawnBoom = function (streakCounter) {
            if (streakCounter > 3) {
                this._border.alpha = 0;
                if (streakCounter == 4) {
                    this._boom.loadTexture("atlas2", "bomb_icon");
                    this._boom.anchor.set(0.45, 0.6);
                    this._sprite.alpha = 1;
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._mytype = 6;
                }
                else {
                    this._boom.loadTexture("atlas2", "wert_horiz_icon");
                    this._boom.anchor.set(0.5);
                    this._boom.alpha = 1;
                    this._boom.visible = true;
                    this._sprite.alpha = 1;
                    this._mytype = 7;
                }
                this._sprite.alpha = 1;
                this._onCompleteFired = false;
                this._destroyAnimation.stop();
            }
        };
        Gem.prototype.stopTween = function () {
            this._bombTween.pause();
            this._sprite.loadTexture("atlas2", "F_2");
            this._sprite.alpha = 0;
        };
        Object.defineProperty(Gem.prototype, "row", {
            get: function () {
                return this._row;
            },
            set: function (val) {
                this._row = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "col", {
            get: function () {
                return this._col;
            },
            set: function (val) {
                this._col = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "mytype", {
            get: function () {
                return this._mytype;
            },
            set: function (val) {
                this._border.alpha = 0;
                if (!this._onCompleteFired) {
                    this._destroyAnimation.stop();
                    this._onCompleteFired = true;
                }
                this._destroyAnimation.stop();
                this._mytype = val;
                if (val < 6) {
                    this._canPlayBlibk = true;
                    this._sprite.alpha = 1;
                    this._bombTween.pause();
                    this._sprite.position.set(0, 0);
                    this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE, mygame.GameConfig.GEM_DEFAULT_SCALE);
                    this._spriteName = mygame.GameConfig.NAMES[this._mytype];
                    this._sprite.loadTexture("atlas2", "F_" + this._mytype);
                }
                else {
                    this._bombTween.start();
                    this._spriteName = "rocket";
                    this._sprite.loadTexture("atlas2", "bomb_icon");
                    this._sprite.scale.set(1, 1);
                }
                this._sprite.alpha = 1;
                this._onCompleteFired = false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "onDestroy", {
            set: function (val) {
                this._destroyCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Gem.prototype, "isBomb", {
            get: function () {
                return this._mytype >= 6;
            },
            enumerable: true,
            configurable: true
        });
        Gem.prototype.foo = function () {
        };
        Gem.prototype.lightning = function (angle) {
            this._canPlayBlibk = false;
            this._blinkSprite.alpha = 0;
            this._lightningSprite.angle = angle;
            this._lightningSprite.alpha = 1;
            this._lightningSprite.play("play");
            this._sprite.alpha = 0;
        };
        Gem.prototype.playDestroy = function (bombRange) {
            if (bombRange === void 0) { bombRange = 0; }
            this._border.alpha = 0;
            this._boom.visible = false;
            if (this.mytype == 6) {
                this._bombTween.pause();
            }
            if (bombRange == 1) {
                this._bomb.alpha = 1;
                this._bombAnimation.play();
            }
            else if (bombRange == 0) {
                this._removeSprite.tint = 0xffffff;
                this._removeAnimation.play();
            }
            else {
            }
        };
        Gem.prototype.select = function () {
            if (!window["tutorial"]) {
                this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE + 0.2);
                if (this.mytype < 6) {
                    this._border.alpha = 1;
                }
            }
        };
        Gem.prototype.deselect = function () {
            this._sprite.scale.set(mygame.GameConfig.GEM_DEFAULT_SCALE);
            this._border.alpha = 0;
        };
        Gem.prototype.isSame = function (a) {
            return (this._row == a.row) && (this._col == a.col);
        };
        Gem.prototype.isNext = function (gem2) {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        };
        return Gem;
    }(Phaser.Sprite));
    mygame.Gem = Gem;
})(mygame || (mygame = {}));
