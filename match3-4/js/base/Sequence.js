var mygame;
(function (mygame) {
    var Sequence = (function () {
        function Sequence() {
            this._array = [];
            this._bombPosition = null;
        }
        Sequence.prototype.add = function (x, y) {
            if (!this.contains(x, y)) {
                this._array.push(new Phaser.Point(x, y));
            }
        };
        Object.defineProperty(Sequence.prototype, "bombPosition", {
            get: function () {
                return this._bombPosition;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Sequence.prototype, "isBomb", {
            get: function () {
                if (this._array.length > 3) {
                    var n = Math.floor(this._array.length / 2);
                    if (this._bombPosition == null) {
                        this._bombPosition = new Phaser.Point(this._array[n].x, this._array[n].y);
                    }
                    return true;
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Sequence.prototype.setBombPosition = function (x, y) {
            this._bombPosition = new Phaser.Point(x, y);
        };
        Sequence.prototype.forEach = function (x) {
            this._array.forEach(x);
        };
        Sequence.prototype.contains = function (x, y) {
            for (var i = 0; i < this._array.length; i++) {
                var p = this._array[i];
                if (p.x == x && p.y == y) {
                    return true;
                }
            }
            return false;
        };
        Sequence.prototype.get = function (i) {
            return this._array[i];
        };
        Sequence.prototype.push = function (x, y) {
            this.add(x, y);
        };
        Sequence.prototype.merge = function (other) {
            var len = other.length;
            for (var i = 0; i < len; i++) {
                this._array.push(other.pop());
            }
        };
        Object.defineProperty(Sequence.prototype, "length", {
            get: function () {
                return this._array.length;
            },
            enumerable: true,
            configurable: true
        });
        Sequence.prototype.pop = function () {
            return this._array.pop();
        };
        return Sequence;
    }());
    mygame.Sequence = Sequence;
})(mygame || (mygame = {}));
