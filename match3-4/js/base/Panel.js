var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var Panel = (function (_super) {
        __extends(Panel, _super);
        function Panel(game, x, y) {
            var _this = _super.call(this, game, x, y) || this;
            _this._tweenRun = false;
            _this._delta = 0;
            _this._unclocksCounter = 0;
            _this._total = window["totalElemets"];
            _this._counter = 0;
            _this._statusText = _this.game.add.bitmapText(0, -73, "font", _this._counter + "/" + _this._total, 60);
            _this._statusText.anchor.set(0.5);
            _this._bigHouse = [];
            _this._smallHouse = [];
            _this._bridge = [];
            _this._fireworks = [];
            _this._myMask = _this.game.add.graphics(0, 0);
            _this.addChild(_this._myMask);
            _this._city = _this.game.add.sprite(0, 0, "atlas2", "cityCorrect");
            _this._city.anchor.set(0.5, 1);
            _this._city.mask = _this._myMask;
            _this._orange = _this.game.add.sprite(0, -275, "atlas2", "gold_city_back0X25");
            _this._orange.anchor.set(0.5, 0.5);
            _this._orange.scale.set(4, 4);
            _this.addChild(_this._orange);
            _this.addChild(_this._city);
            _this._fruitsCounter = 0;
            _this._finishWasFired = false;
            _this._fruitScale = 0.8;
            mygame.GameConfig.COUNTERS_PUBLIC = [999, 999, 999, 999, 999, 999];
            _this._scorePlank = _this.game.add.sprite(0, 200, "atlas2", "scorePlank");
            _this._scorePlank.anchor.set(0.5, 1);
            _this.addChild(_this._scorePlank);
            _this.createBigHouses();
            _this.createSmallHouses();
            _this.createBridges();
            _this.createProgressBar();
            _this._scorePlank.addChild(_this._statusText);
            _this._delta = 0;
            _this._timer = 0;
            _this._sleepTime = 1;
            if (mygame.Core.isLandscape) {
                _this.setLandscape();
            }
            else {
                _this.setPortrait();
            }
            return _this;
        }
        Panel.prototype.createBigHouses = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-212, -654, "atlas2", "bigHouse_" + postfix[i]);
                this._bigHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[-23, -544], [-165, -500], [-150, -642]];
            var _loop_1 = function (i) {
                var firework = this_1.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_1);
                this_1._city.addChild(firework);
                this_1._fireworks.push(firework);
            };
            var this_1 = this;
            for (var i = 0; i < 3; i++) {
                _loop_1(i);
            }
        };
        Panel.prototype.createSmallHouses = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-5, -565, "atlas2", "smallHouse_" + postfix[i]);
                this._smallHouse.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[233, -511], [135, -480], [182, -535]];
            var _loop_2 = function (i) {
                var firework = this_2.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_2);
                this_2._city.addChild(firework);
                this_2._fireworks.push(firework);
            };
            var this_2 = this;
            for (var i = 0; i < 3; i++) {
                _loop_2(i);
            }
        };
        Panel.prototype.createBridges = function () {
            var _this = this;
            var postfix = ["start", "end"];
            for (var i = 0; i < 2; i++) {
                var sprite = this.game.add.sprite(-43, -303, "atlas2", "bridge_" + postfix[i]);
                this._bridge.push(sprite);
                this._city.addChild(sprite);
                sprite.alpha = 1 - i;
            }
            var pos = [[17, -276], [25, -330], [83, -300]];
            var _loop_3 = function (i) {
                var firework = this_3.game.add.sprite(pos[i][0], pos[i][1], "atlas2", "image_1");
                firework.anchor.set(0.5);
                firework.alpha = 0;
                firework.animations.add("play", Phaser.Animation.generateFrameNames("image_", 1, 19), 20).onComplete.add(function () {
                    _this.game.add.tween(firework.scale).to({
                        x: 1.3,
                        y: 1.3
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                    _this.game.add.tween(firework).to({
                        alpha: 0
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true);
                }, this_3);
                this_3._city.addChild(firework);
                this_3._fireworks.push(firework);
            };
            var this_3 = this;
            for (var i = 0; i < 3; i++) {
                _loop_3(i);
            }
        };
        Panel.prototype.createProgressBar = function () {
            this._progressBar = this.game.add.sprite(0, 0);
            var firstBar = this.game.add.sprite(0, 0, "scoreMetr");
            firstBar.anchor.set(1, 1);
            var secondBar = this.game.add.sprite(0, 0, "scoreMetr");
            secondBar.scale.set(-1, 1);
            secondBar.anchor.set(1, 1);
            var w = firstBar.width * 2 - 30;
            var h = firstBar.height - 8;
            var bg = this.game.add.graphics(0, 0);
            bg.beginFill(0x232724)
                .drawRect(-w / 2, -h - 4, w, h)
                .endFill();
            bg.cacheAsBitmap = true;
            this._greenBar = this.game.add.graphics(0, 0);
            this._progressBar.addChild(bg);
            this._progressBar.addChild(this._greenBar);
            this._progressBar.addChild(firstBar);
            this._progressBar.addChild(secondBar);
            this._scorePlank.addChild(this._progressBar);
        };
        Object.defineProperty(Panel.prototype, "greenbarValue", {
            set: function (val) {
                var w = 141 * 2 - 30;
                var h = 46 - 14;
                this._greenBar.clear()
                    .beginFill(0x37b54e)
                    .drawRect(-w / 2, -h - 4, w * val, h)
                    .endFill();
            },
            enumerable: true,
            configurable: true
        });
        Panel.prototype.scaleFruits = function (val) {
        };
        Object.defineProperty(Panel.prototype, "finishScreenCallback", {
            set: function (val) {
                this._finishCallback = val;
            },
            enumerable: true,
            configurable: true
        });
        Panel.prototype.updateStatus = function () {
            if (this._counter > this._total) {
                this._counter = this._total;
            }
            this._statusText.text = this._counter + "/" + this._total;
            this.greenbarValue = this._counter / this._total;
        };
        Panel.prototype.replay = function () {
            this._finishWasFired = false;
            this._delta = 0;
            this._counter = 0;
            this._unclocksCounter = 0;
        };
        Panel.prototype.updateCounter = function (type, delta) {
            this._delta += delta;
            this._sleepTime = 200;
        };
        Panel.prototype.setLandscape = function () {
            this._myMask.clear();
            this._orange.angle = 0;
            this._orange.position.set(0, -275);
            this._city.position.set(-35, 50);
            this._city.scale.set(1, 1);
            this._myMask.y = -270 - 3;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._myMaskSize[0] / 2, -Panel._myMaskSize[1] / 2, Panel._myMaskSize[0], Panel._myMaskSize[1], 20);
            this._myMask.endFill();
            this._scorePlank.position.set(0, 70);
        };
        Panel.prototype.setPortrait = function () {
            this._city.position.set(-10, 150);
            this._city.scale.set(1.1, 1.1);
            this._myMask.position.set(0, -240);
            this._myMask.clear();
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-Panel._otherMaskSize[0] / 2, -Panel._otherMaskSize[1] / 2, Panel._otherMaskSize[0], Panel._otherMaskSize[1], 20);
            this._myMask.endFill();
            this._orange.angle = 90;
            this._orange.position.set(0, -240);
            this._scorePlank.position.set(0, 85);
        };
        Panel.prototype.getElementPosition = function (type) {
            if (mygame.Core.isLandscape) {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 70);
            }
            else {
                return new Phaser.Point(this._scorePlank.worldPosition.x, this._scorePlank.worldPosition.y - 50);
            }
        };
        Panel.prototype.initFruits = function () {
        };
        Panel.prototype.unlockObject = function () {
            var _this = this;
            switch (this._unclocksCounter) {
                case 0:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_4 = function (i) {
                            this_4.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_4);
                        };
                        var this_4 = this;
                        for (var i = this._fireworks.length - 3; i < this._fireworks.length; i++) {
                            _loop_4(i);
                        }
                        this.game.add.tween(this._bridge[1]).to({
                            alpha: 0.7
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._bridge[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._bridge[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
                case 1:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_5 = function (i) {
                            this_5.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_5);
                        };
                        var this_5 = this;
                        for (var i = this._fireworks.length - 3 * 2; i < this._fireworks.length - 3; i++) {
                            _loop_5(i);
                        }
                        this.game.add.tween(this._smallHouse[1]).to({
                            alpha: 0.7
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._smallHouse[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._smallHouse[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
                case 2:
                    {
                        if (window["sounds"]) {
                            this.game.sound.play("3", 0.4);
                        }
                        if (window["sounds"]) {
                            this.game.sound.play("19", 0.4);
                        }
                        var _loop_6 = function (i) {
                            this_6.game.time.events.add(100 * i, function () {
                                _this._fireworks[i].alpha = 1;
                                _this._fireworks[i].play("play", 24);
                            }, this_6);
                        };
                        var this_6 = this;
                        for (var i = 0; i < 3; i++) {
                            _loop_6(i);
                        }
                        this.game.add.tween(this._bigHouse[1]).to({
                            alpha: 1
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                            _this.game.add.tween(_this._bigHouse[1]).to({
                                alpha: 1
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                            _this.game.add.tween(_this._bigHouse[0]).to({
                                alpha: 0
                            }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                        }, this);
                    }
                    break;
            }
            this._unclocksCounter++;
        };
        Panel.prototype.hideText = function (text) {
        };
        Panel.prototype.update = function () {
            var dt = this.game.time.elapsedMS;
            if (this._timer > 0) {
                this._timer -= dt;
            }
            if (this._delta > 0) {
                this._timer = this._sleepTime;
                this._counter++;
                this._delta--;
                if (this._counter >= Math.floor(this._total * window["firstSpawn"]) && this._unclocksCounter == 0) {
                    this.unlockObject();
                }
                if (this._counter >= Math.floor(this._total * window["secondSpawn"]) && this._unclocksCounter == 1) {
                    this.unlockObject();
                }
                if (this._counter >= Math.floor(this._total * window["thirdSpawn"]) && this._unclocksCounter == 2) {
                    this.unlockObject();
                }
                this.updateStatus();
            }
        };
        return Panel;
    }(Phaser.Sprite));
    Panel._myMaskSize = [550 - 30, 700 - 54 + 2];
    Panel._otherMaskSize = [700 - 54 + 2, 550 - 30];
    mygame.Panel = Panel;
})(mygame || (mygame = {}));
