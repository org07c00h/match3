var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var GemPatricle = (function (_super) {
        __extends(GemPatricle, _super);
        function GemPatricle(game, x, y, type) {
            var _this = _super.call(this, game, x, y) || this;
            _this.PARTICLE_NAMES = ["blue", "green", "pink", "red", "yellow"];
            _this._sprite = _this.game.add.sprite(0, 0, "atlas2", "particle_" + _this.PARTICLE_NAMES[type - 1]);
            _this.addChild(_this._sprite);
            _this._sprite.anchor.set(0.5);
            return _this;
        }
        GemPatricle.prototype.destoy = function () {
            this._sprite.destroy();
            _super.prototype.destroy.call(this);
        };
        return GemPatricle;
    }(Phaser.Sprite));
    mygame.GemPatricle = GemPatricle;
})(mygame || (mygame = {}));
