var mygame;
(function (mygame) {
    var GameConfig = (function () {
        function GameConfig() {
        }
        GameConfig.deepClone = function (m) {
            var result = [];
            for (var i = 0; i < m.length; i++) {
                result.push([]);
                for (var j = 0; j < m[0].length; j++) {
                    result[i].push(m[i][j]);
                }
            }
            return result;
        };
        GameConfig.transpose = function (m) {
            var result = [];
            for (var i = 0; i < m[0].length; i++) {
                result.push([]);
                for (var j = 0; j < m.length; j++) {
                    result[i].push(m[j][i]);
                }
            }
            return result;
        };
        GameConfig.enableDrag = function (sprite) {
            return;
        };
        return GameConfig;
    }());
    GameConfig.BLOCK_SIZE = 92;
    GameConfig.SWAP_TIME = 150;
    GameConfig.DESTROY_TIME = 150;
    GameConfig.FALLDOWN_TIME = 400;
    GameConfig.FLY_TIME = 500 * 1.15;
    GameConfig.CONDENSATION_TIME = 300;
    GameConfig.COUNTERS = [0, 2, 1, 0, 0, 0, 0, 0];
    GameConfig.COUNTERS_PUBLIC = [0, 2, 1, 0, 0, 0, 0, 0];
    GameConfig.NAMES = ["apple", "apple", "grapes", "flower", "pear", "leaf"];
    GameConfig.PANEL_GEM_SCALE = 0.8;
    GameConfig.APPLE_POS = new Phaser.Point(100, -60);
    GameConfig.GRAPES_POS = new Phaser.Point(370, -60);
    GameConfig.UNLOCK_OBJ_PER_FRUITS = 3;
    GameConfig.GEM_DEFAULT_SCALE = 1;
    GameConfig.gameArrayLandscape = [
        [-1, 5, 3, 2, 4, 1, -1],
        [2, 3, 3, 2, 1, 1, 5],
        [3, 1, 2, 4, 4, 5, 1],
        [4, 3, 5, 2, 4, 5, 1],
        [-1, 5, 4, 1, 5, 4, -1]
    ];
    mygame.GameConfig = GameConfig;
})(mygame || (mygame = {}));
