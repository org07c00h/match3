var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var mygame;
(function (mygame) {
    var version = '1.0.0';
    var PlayState = (function (_super) {
        __extends(PlayState, _super);
        function PlayState() {
            var _this = _super.call(this, true) || this;
            _this._winTweens = [];
            return _this;
        }
        PlayState.prototype.create = function () {
            var _this = this;
            mygame.GameConfig.COUNTERS_PUBLIC = [0, window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            this.game.time.advancedTiming = true;
            this.game.stage.backgroundColor = "#000000";
            this._bg = this.game.add.sprite(0, 0, "bg");
            this._bg.anchor.set(0.5);
            var bgContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                .myScale(1.2)
                .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                .end();
            bgContainer.addChild(this._bg);
            this._winscreenShowed = false;
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.smoothed = true;
            this._canShowEndScreen = false;
            mygame.Controller.Instance.width = getSize().width;
            mygame.Controller.Instance.height = getSize().height;
            this._container = new mygame.OSprite(mygame.Core.centerX + 200, 0)
                .otherXY(mygame.Core.centerY, 0)
                .end();
            this.game.world.addChild(this._container);
            this._borders = [];
            this._borders[0] = this.game.add.sprite(mygame.Core.width, mygame.Core.height, "atlas2", "ramk_rd");
            this._borders[0].anchor.set(1, 1);
            this._borders[1] = this.game.add.sprite(0, 0, "atlas2", "ramk_lt");
            this._borders[3] = this.game.add.sprite(mygame.Core.width, 0, "atlas2", "ramk_rt");
            this._borders[3].anchor.set(1, 0);
            ;
            this._borders[2] = this.game.add.sprite(0, mygame.Core.height, "atlas2", "ramk_ld");
            this._borders[2].anchor.set(0, 1);
            this._borders.forEach(function (x) { return _this.game.world.addChild(x); });
            if (window["collectable"]) {
                this._gamePanelOsprite = new mygame.OSprite(0, 0)
                    .myTopOffset(640)
                    .otherXY(mygame.Core.centerY, 0)
                    .otherTopOffset(230 + 400 - 35)
                    .myLeftOffset(300)
                    .end();
                this._panel = new mygame.Panel(this.game, 0, 0);
                if (this.game.device.iPad) {
                    this._gamePanelOsprite.myTopOffset(750).end();
                }
                this._gamePanelOsprite.addChild(this._panel);
                this._panel.initFruits();
            }
            mygame.GameConfig.gameArray = mygame.GameConfig.deepClone(mygame.GameConfig.gameArrayLandscape);
            this._logoOsprite = new mygame.OSprite(920, 0)
                .myTopOffset(5)
                .otherXY(mygame.Core.centerY, 0)
                .otherTopOffset(5)
                .end();
            this._logo = this.game.add.sprite(0, 0, "logo");
            this._logo.anchor.set(0.5, 0);
            this._logoOsprite.addChild(this._logo);
            if (window["collectable"]) {
                this._container = new mygame.OSprite(mygame.Core.centerX + 400 - 85, mygame.Core.centerY - 70 - 25)
                    .otherXY(mygame.Core.centerY + 30, mygame.Core.centerX + 220 - 25)
                    .end();
                this._field = new mygame.Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = function (type, delta) {
                    _this._panel.updateCounter(type, delta);
                };
                this._field.finalCallback = this.showWinScreen.bind(this);
            }
            else {
                this._container = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX - 80)
                    .end();
                this._field = new mygame.Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = function (type, delta) {
                    type + 1;
                };
                this._container.myScale(1).otherScale(1.15).end();
            }
            this.game.world.addChild(this._logoOsprite);
            this._field.firstMatchCallback = function () {
            };
            this._field.pivot.set(100 * mygame.GameConfig.gameArray[0].length / 2, 100 * mygame.GameConfig.gameArray[1].length / 5);
            this._playFree = this.game.add.sprite(0, 0, "button");
            if (window["particleOn"]) {
                this._fruitFallEmitter = this.game.add.emitter(0, -100, Math.ceil(4600 / window["fruitSpawRate"]));
                this._fruitFallEmitter.makeParticles("atlas2", ["F_1", "F_2", "F_3", "F_4", "F_5"]);
                this._fruitFallEmitter.minParticleSpeed.setTo(0, 200 * window["fruitSpawSpeed"]);
                this._fruitFallEmitter.maxParticleSpeed.setTo(0, 300 * window["fruitSpawSpeed"]);
                this.createEmitter();
            }
            this._playFreeBtnOsprite = new mygame.OSprite(mygame.Core.centerX + 300, 0).myBottomOffset(150).myScale(1).end();
            if (this.game.device.iPad) {
                this._playFreeBtnOsprite.myBottomOffset(300).end();
            }
            this._playFreeBtnOsprite.otherXY(mygame.Core.centerY, 0).otherBottomOffset(140).otherScale(0.95).end();
            this._playFree.anchor.set(0.5, 0.1);
            var buttonText = this.game.add.bitmapText(0, 25, "font", window["buttonText"], 50);
            buttonText.anchor.set(0.5, 0.1);
            this._playFree.addChild(buttonText);
            this._playFree.inputEnabled = true;
            this._playFree.events.onInputDown.add(function () {
                window["trackClick"]();
            }, this);
            this._playFreeBtnOsprite.addChild(this._playFree);
            if (!window["collectable"]) {
                this._playFree.alpha = 0;
                this._playFree.y = 300;
            }
            if (!mygame.Core.isLandscape) {
                this.onPortret();
            }
            if (window["tutorial"]) {
                this._playFreeBtnOsprite.visible = false;
                this._tutorialOver = this.game.make.sprite(-630 + 15 + 5, -160 + 40 - 5, "atlas2", "tutorial_mask");
                this._tutorialOver.scale.set(10);
                this._tutorialOver.alpha = 0.8;
                this._field.addChild(this._tutorialOver);
                var tutorialUP = this.game.add.graphics(0, -128);
                this._tutorialOver.addChild(tutorialUP);
                tutorialUP.beginFill(0x000000, 1);
                tutorialUP.drawRect(0, 0, 256, 128);
                tutorialUP.endFill();
                var tutorialDOWN = this.game.add.graphics(0, 72);
                this._tutorialOver.addChild(tutorialDOWN);
                tutorialDOWN.beginFill(0x000000, 1);
                tutorialDOWN.drawRect(0, 0, 128, 128);
                tutorialDOWN.endFill();
                var tutorialLEFT = this.game.add.graphics(-128, 0);
                this._tutorialOver.addChild(tutorialLEFT);
                tutorialLEFT.beginFill(0x000000, 1);
                tutorialLEFT.drawRect(0, 0, 128, 128);
                tutorialLEFT.endFill();
                var tutorialRIGHT = this.game.add.graphics(128, 0);
                this._tutorialOver.addChild(tutorialRIGHT);
                tutorialRIGHT.beginFill(0x000000, 1);
                tutorialRIGHT.drawRect(0, 0, 128, 128);
                tutorialRIGHT.endFill();
                this._hand = this.game.add.sprite(100 * 4 + 10 - mygame.GameConfig.BLOCK_SIZE * 2, 100 * 2 - 70, "atlas2", "Tutorial-arrow");
                this._hand.anchor.set(0.5);
                this._handTween = this.game.add.tween(this._hand).to({
                    y: 80
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._field.addChild(this._hand);
                this._tutorialContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .myBottomOffset(0)
                    .myLeftOffset(35)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                    .otherRightOffset(500 - 30)
                    .otherBottomOffset(0)
                    .otherScale(0.75)
                    .end();
                this.game.world.addChild(this._tutorialContainer);
                this._charTutorialSprite = this.game.make.sprite(0, 0, "atlas2", "tutorial_dedula");
                this._charTutorialSprite.anchor.set(0, 1);
                this._tutorialContainer.addChild(this._charTutorialSprite);
                this._charTutorialSprite.position.set(0, this._charTutorialSprite.width + 100);
                this.game.add.tween(this._charTutorialSprite).to({
                    y: 0
                }, 750, Phaser.Easing.Sinusoidal.InOut, true);
                this._bubbleTutorialText = this.game.add.bitmapText(this._charTutorialSprite.width / 2, -200 - 50, "font2", window["tutorialText"][0], 35);
                this._bubbleTutorialText.maxWidth = 500;
                this._bubbleTutorialText.anchor.setTo(0.5, 0.5);
                this._bubbleTutorialText.align = "center";
                this._bubbleTutorialText.tint = 0x733601;
                this._charTutorialSprite.addChild(this._bubbleTutorialText);
            }
            setTimeout(function () { _this._field.endGame(); }, window["endGameTimer"]);
            this._timerInstall = 0;
            window['gameStarted'];
            this.game.time.events.loop(Phaser.Timer.SECOND, this.tick, this);
        };
        PlayState.prototype.createEmitter = function () {
            this._safeEmitter = this.game.add.emitter(0, 60, 70);
            this._safeEmitter.makeParticles("atlas2", ["particle"]);
            this._safeEmitter.setAlpha(1, 0, 1000);
            this._safeEmitter.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            this._safeEmitter.setXSpeed(-50, 50);
            this._safeEmitter.setYSpeed(-50, 50);
            this._safeEmitter.gravity = 0;
            this._safeEmitter.start(false, 1000, 2);
            this._safeEmitter.width = this._logo.width;
            this._safeEmitter.height = this._logo.height;
            this._logo.addChild(this._safeEmitter);
            var buttonParcticle = this.game.add.emitter(0, 50, 70);
            buttonParcticle.makeParticles("atlas2", ["particle"]);
            buttonParcticle.setAlpha(1, 0, 1000);
            buttonParcticle.setScale(0.6, 0.1, 0.6, 0.1, 1000);
            buttonParcticle.setXSpeed(-50, 50);
            buttonParcticle.setYSpeed(-50, 50);
            buttonParcticle.gravity = 0;
            buttonParcticle.start(false, 1000, 2);
            buttonParcticle.width = this._playFree.width - 300;
            buttonParcticle.height = this._playFree.height - 50;
            this._playFree.addChild(buttonParcticle);
        };
        PlayState.prototype.tick = function () {
            this._timerInstall++;
            window['playTime'] = this._timerInstall;
        };
        PlayState.prototype.getGurrentElementPosition = function (type) {
            return this._panel.getElementPosition(type);
        };
        PlayState.prototype.update = function () {
            if (window["collectable"]) {
                this._panel.update();
            }
            if (this._field) {
                this._field.update();
            }
            if (window["particleOn"]) {
            }
        };
        PlayState.prototype.destroyTutorial = function () {
            var _this = this;
            this._handTween.stop();
            this._hand.destroy();
            this._playFreeBtnOsprite.visible = true;
            this.game.add.tween(this._charTutorialSprite).to({
                y: this._charTutorialSprite.height + 100
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                _this._tutorialContainer.destroy(true);
            }, this);
            this.game.add.tween(this._tutorialOver).to({
                alpha: 0
            }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(function () {
                _this._tutorialOver.destroy(true);
            }, this);
        };
        PlayState.prototype.render = function () {
        };
        PlayState.prototype.repositionEmitter = function () {
            var dx;
            if (mygame.Core.isLandscape) {
                dx = mygame.Core.centerX - 20;
            }
            else {
                dx = mygame.Core.centerY - 20;
            }
            this._fruitFallEmitter.emitX = this.game.rnd.between(-dx, dx);
            this.game.time.events.add(window["fruitSpawRate"], this.repositionEmitter.bind(this));
        };
        PlayState.prototype.showWinScreen = function () {
            if (!this._winscreenShowed) {
                if (window["sounds"]) {
                    this.game.sound.play("5", 0.4);
                }
                this._winscreenShowed = true;
                this._playFree.visible = false;
                this._playFree.inputEnabled = false;
                this._playFreeBtnOsprite.removeChild(this._playFree);
                this._panelContainer = new mygame.OSprite(mygame.Core.centerX, mygame.Core.centerY)
                    .otherXY(mygame.Core.centerY, mygame.Core.centerX)
                    .end();
                this.game.stage.addChild(this._panelContainer);
                this._backOverlay = this.game.add.graphics(0, 0);
                this._panelContainer.addChild(this._backOverlay);
                this._backOverlay.beginFill(0x251d1a, 0.8);
                this._backOverlay.drawRect(-1024, -1024, 2048, 2048);
                this._backOverlay.endFill();
                if (window["particleOn"]) {
                    this._panelContainer.addChild(this._fruitFallEmitter);
                    if (mygame.Core.isLandscape) {
                        this._fruitFallEmitter.emitY = -mygame.Core.centerY - 100;
                    }
                    else {
                        this._fruitFallEmitter.emitY = -mygame.Core.centerX - 100;
                    }
                    this._fruitFallEmitter.start(false, 4600, window["fruitSpawRate"]);
                    this.repositionEmitter();
                }
                this._panelSprite = this.game.make.sprite(0, 0, "atlas2", "final_popup");
                this._panelSprite.anchor.set(0.5);
                this._sunraySprite = this.game.make.sprite(0, 0, "atlas2", "lucksherry_sunray");
                this._sunraySprite.scale.set(7);
                this._sunraySprite.anchor.set(0.5);
                this._sunraySprite.alpha = 0.6;
                this._panelContainer.addChild(this._sunraySprite);
                this.game.add.tween(this._sunraySprite).to({ angle: 360 }, 18500, Phaser.Easing.Linear.None, true, 0, -1);
                this._panelContainer.addChild(this._panelSprite);
                this._panelSprite.addChild(this._playFree);
                this._playFree.position.set(0, 177 + 40);
                this._playFree.anchor.x = 0.5;
                this._playFree.scale.set(0.85, 0.85);
                this._playFree.visible = true;
                this._playFree.inputEnabled = true;
                this._congrateText = this.game.add.bitmapText(0, 45, "font2", window["tutorialText"][1], 50);
                this._congrateText.anchor.setTo(0.5, 0.5);
                this._congrateText.align = "center";
                this._congrateText.tint = 0x733601;
                this._panelSprite.addChild(this._congrateText);
                this._plotText = this.game.add.bitmapText(0, 108 + 35, "font2", window["tutorialText"][2], 50);
                this._plotText.maxWidth = 500;
                this._plotText.anchor.setTo(0.5, 0.5);
                this._plotText.align = "center";
                this._plotText.tint = 0x733601;
                this._panelSprite.addChild(this._plotText);
                this._panelContainer.alpha = 0;
                this._panelSprite.scale.set(0.1, 0.1);
                this.game.add.tween(this._panelSprite.scale).to({ x: 1, y: 1 }, 700, Phaser.Easing.Back.Out, true);
                this.game.add.tween(this._panelContainer).to({ alpha: 1 }, 700, Phaser.Easing.Sinusoidal.Out, true);
            }
        };
        PlayState.prototype.onLandscape = function () {
            if (this._bg) {
                this._borders[0].position.set(mygame.Core.width, mygame.Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(mygame.Core.width, 0);
                this._borders[2].position.set(0, mygame.Core.height);
            }
            if (this._panel) {
                this._panel.setLandscape();
                if (window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -mygame.Core.centerY - 100;
                }
            }
        };
        PlayState.prototype.onPortret = function () {
            if (this._bg) {
                this._borders[0].position.set(mygame.Core.width, mygame.Core.height);
                this._borders[1].position.set(0, 0);
                this._borders[3].position.set(mygame.Core.width, 0);
                this._borders[2].position.set(0, mygame.Core.height);
            }
            if (this._panel) {
                this._panel.setPortrait();
                if (window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -mygame.Core.centerX - 100;
                }
            }
        };
        return PlayState;
    }(mygame.OState));
    mygame.PlayState = PlayState;
    function getSize(log) {
        if (log === void 0) { log = false; }
        var w = 0;
        var h = 0;
        var deW = 0;
        var deH = 0;
        if (!(document.documentElement.clientWidth == 0)) {
            deW = document.documentElement.clientWidth;
            deH = document.documentElement.clientHeight;
        }
        w = deW;
        h = deH;
        if (window.innerWidth > window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        return { width: w, height: h };
    }
    function sendEvent(value, params) {
        if (params === void 0) { params = null; }
        window["trackEvent"](value, params);
    }
    function ClickInstall() {
        window["trackClick"]();
    }
})(mygame || (mygame = {}));
