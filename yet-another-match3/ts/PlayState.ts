/**
 * Created by DEaDA on 3/25/17.
 */

module mygame {
    import ScaleManager = Phaser.ScaleManager;
    import Sprite = Phaser.Sprite;
    import Game = Phaser.Game;
    import Group = Phaser.Group;
    import Tween = Phaser.Tween;
    import Sound = Phaser.Sound;
    import PhaserTextStyle = Phaser.PhaserTextStyle;
    import Point = Phaser.Point;

    let version: string = '1.0.0';
    // let bg: BackGround;
    export class PlayState extends OState {
        private _field: Field;
        private _panel: Panel;
        private _logo: Phaser.Sprite;
        private _container: OSprite;
        // private _leftContainer: OSprite;
        private _playFree: Phaser.Sprite;
        private _winScreen: OSprite;
        private _winBG: Phaser.Sprite;
        private _ostinClap: OSprite;
        private _ostinClapSprite: Phaser.Sprite;
        private _welldone: Phaser.Sprite;
        private _sunray: Phaser.Sprite;
        private _backStars: Phaser.Sprite;
        private _star: Phaser.Sprite;
        private _installNowSprite: OSprite;
        private _installNowSpriteOriginal: Phaser.Sprite;
        private _replaySprite: OSprite;
        private _finalContainer: Phaser.Sprite;
        private _winTweens: Phaser.Tween[];
        private _canShowEndScreen: boolean;
        private _background: OSprite;

        private _logoOsprite: OSprite;
        private _gamePanelOsprite: OSprite;
        private _playFreeBtnOsprite: OSprite;

        constructor() {
            super(true);
            this._winTweens = [];
        }

        create() {
            this.game.time.advancedTiming = true;

            //console.log("222");

            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.game.stage.smoothed = true;
            this._canShowEndScreen = false;

            Controller.Instance.width = getSize().width;
            Controller.Instance.height = getSize().height;


            this._container = new OSprite(Core.centerX, 0)
                .otherXY(Core.centerY, 0)
                .end();

                this._background = new OSprite(Core.centerX, Core.centerY - 650, "background")
                    .myScale(1.8)
                    .otherScale(0.75)
                    .otherXY(Core.centerY, Core.centerX)
                    .enabledBgMode()
                    .end();

            this.game.world.addChild(this._background);

            // init button
            this._playFreeBtnOsprite = new OSprite(Core.centerX, 0).myBottomOffset(70).myScale(0.65).end();
            this._playFreeBtnOsprite.otherXY(Core.centerY, 0).otherBottomOffset(73).otherScale(0.65).end();

            this._playFree = this.game.add.sprite(0, 0, "play_free");
            this._playFree.scale.set(1.3);
            this._playFree.anchor.set(0.5, 0.5);

            this._playFree.inputEnabled = true;
            this._playFree.events.onInputDown.add(() => {
                window["trackClick"]();
            }, this);


            this._playFreeBtnOsprite.addChild(this._playFree);
            if (!window["collectable"]) {
                this._playFree.alpha = 0;
                this._playFree.y = 300;
            }

            this.game.add.tween(this._playFree.scale).to({x: 1.45, y: 1.45}, window["buttonPlaySpeed"], Phaser.Easing.Sinusoidal.Out, true, 0, -1, true);


            // init panel
            if (window["collectable"]) {
                this._gamePanelOsprite = new OSprite(Core.centerX, 0).myTopOffset(160).myScale(0.9).end();
                this._gamePanelOsprite.otherXY(Core.centerY, 0).otherTopOffset(230).otherScale(1.1).end();

                this._panel = new Panel(this.game, 0, 0);

                this._gamePanelOsprite.addChild(this._panel);

                this._panel.initFruits();
            }

            // init field
            let startX: number;
            if (Core.isLandscape) {
                startX = -400;
                GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayLandscape);
                if (GameConfig.gameArray.length > 6) {
                    GameConfig.gameArray = GameConfig.transpose(GameConfig.gameArray);
                }
            } else {
                GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayPortret);
                startX = -300;
                if (GameConfig.gameArray.length < 8) {
                    GameConfig.gameArray = GameConfig.transpose(GameConfig.gameArray);
                }
            }



            if (window["collectable"]) {

                this._container = new OSprite(Core.centerX, Core.centerY + 30)
                    .otherXY(Core.centerY, Core.centerX - 30)
                    .end();

                this._field = new Field(this.game, 0, 0, this);

                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    this._panel.updateCounter(type, delta);
                };
                this._field.finalCallback = this.showWinScreen.bind(this);
                this._container.myScale(0.95).otherScale(1.05).end(); //myTopOffset(200).otherTopOffset(285)

            } else {

                this._container = new OSprite(Core.centerX, Core.centerY )
                    .otherXY(Core.centerY, Core.centerX - 80)
                    .end();

                this._field = new Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    type + 1;
                };
                this._field.firstMatchCallback = () => {
                    let tween: Phaser.Tween = this.game.add.tween(this._playFree).to({
                        y: 0,
                    }, 500, Phaser.Easing.Sinusoidal.InOut, true, window["playfreeTime"]);
                    tween.onStart.add(() => {
                        this._playFree.alpha = 1;
                    }, this);
                }

                this._container.myScale(1).otherScale(1.15).end();
            }

            if(Core.isLandscape) {
                this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 5);
            } else {
                this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 2);
            }


            if(window["collectable"]) {
                this._finalContainer = this.game.add.sprite(0, 0);
                this.initWinScreen();
            }


            this._logoOsprite = new OSprite(Core.centerX, 0).myTopOffset(10).myScale(0.65).end();
            this._logoOsprite.otherXY(Core.centerY, 0).otherTopOffset(15).otherScale(0.95).end();

            this._logo = this.game.add.sprite(0, 0, "objects", "logo.png");
            // this._logo.scale.set(1.55);
            this._logo.anchor.set(0.5, 0);
            this._logoOsprite.addChild(this._logo);

        }

        private readyToEndScreen() {
            this._canShowEndScreen = true;
        }

        private checkShowEndScreen() {
            if(this._canShowEndScreen) {
                this.showWinScreen();
            }
        }

        public getGurrentElementPosition(type: string): Phaser.Point {
            return this._panel.getElementPosition(type)
        }

        private initWinScreen(): void {
            this._winScreen = new OSprite(Core.centerX, 20)
                .otherXY(Core.centerY, 20)
                .end();
            this._finalContainer.addChild(this._winScreen);
            this._winBG = this.game.add.sprite(0, -40, "objects", "bg_final.png");
            this._winBG.anchor.set(0.5, 0);
            this._winScreen.addChild(this._winBG);

                this._ostinClap = new OSprite(0, 0)
                    .myBottomOffset(480)
                    .myLeftOffset(25)
                    .otherBottomOffset(530)
                    .otherLeftOffset(25)
                    .myScale(2.25)
                    .otherScale(2.65)
                    .otherXY(20, 700)
                    .end();

            this._ostinClapSprite = this.game.add.sprite(0, 0, "objects", "ostin_clap_1.png");
            this._ostinClap.addChild(this._ostinClapSprite);
            this._ostinClapSprite.animations.add("clap", ["ostin_clap_1.png", "ostin_clap_2.png"], 6, true);
            this._finalContainer.addChild(this._ostinClap);

                this._winBG.scale.set(6);


            // sunray
            this._sunray = this.game.add.sprite(0, 300 - 20, "objects", "stars_bg.png");
            this._sunray.anchor.set(0.5, 0.5);
            this._sunray.y += this._sunray.height / 2;
            this._winScreen.addChild(this._sunray);

            // backstars
            this._backStars = this.game.add.sprite(0, 500, "objects", "back_stars.png");
            this._backStars.anchor.set(0.5, 0.5);
            this._winScreen.addChild(this._backStars);

            // star
            this._star = this.game.add.sprite(-10, 500, "objects", "final_star.png");
            this._star.anchor.set(0.5, 0.5);
            this._winScreen.addChild(this._star);

            // welldone
            this._welldone = this.game.add.sprite(0, 180, "objects", "well_done_final.png");
            this._welldone.anchor.set(0.5, 0);
            this._winScreen.addChild(this._welldone);

            this._installNowSpriteOriginal = this.game.add.sprite(0, 0, "objects", "install_final.png");
            this._installNowSpriteOriginal.anchor.set(0.5, 0.5);

            // install now
            this._installNowSprite = new OSprite(1100 - 20, 520)
                .otherXY(600 - 50, 1035)
                .myScale(1.5)
                .otherScale(1.25)
                .end();
            this._finalContainer.addChild(this._installNowSprite);
            this._installNowSprite.events.onInputDown.add(() => {
                window["trackClick"]();
            }, this);

            this._installNowSprite.addChild(this._installNowSpriteOriginal);

            // replay
            this._replaySprite = new OSprite(1100 - 20, 650, "objects", "replay_final.png")
                .otherXY(600 - 50, 1150)
                .myScale(1.35)
                .otherScale(1.2)
                .end();

            this._replaySprite.events.onInputDown.add(() => {
                this._field.replay();
                if (window["collectable"]) {
                    this._panel.replay();
                }
                this.hideWinScreen();
            }, this);
            this._finalContainer.addChild(this._replaySprite);

            if (Core.isLandscape) {
                this._welldone.scale.set(1.25);
                this._sunray.scale.set(1.2);
            } else {
                this._welldone.scale.set(1.5);
                this._sunray.scale.set(1.3);
            }

            this._finalContainer.x = -1800;


            this._installNowSpriteOriginal.scale.set(0.85, 0.85);
            this._installNowSpriteOriginal.angle = 3;

            this._installNowSprite.alpha = 0;
            this._replaySprite.alpha = 0;
            // this._winBG.alpha = 0;
            this._container.alpha = 1;
            this._ostinClap.alpha = 0;
            this._winScreen.alpha = 0;
            this._installNowSprite.inputEnabled = true;
            this._replaySprite.inputEnabled = false;
            this._canShowEndScreen = false;
        }

        private hideWinScreen(): void {


            this._logoOsprite.otherScale(0.95).end();

            this.game.add.tween(this._logo.scale).to({x: 1, y: 1}, 500, Phaser.Easing.Sinusoidal.Out, true);

            this._installNowSprite.inputEnabled = true;
            this._replaySprite.inputEnabled = false;
            this._canShowEndScreen = false;
            this.game.add.tween(this._finalContainer).to({
                x: -1800
            }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(()=>{
                this._installNowSprite.alpha = 0;
                this._replaySprite.alpha = 0;
                this._container.alpha = 1;
                this._ostinClap.alpha = 0;
                this._winScreen.alpha = 0;

                this._installNowSpriteOriginal.scale.set(0.85, 0.85);
                this._installNowSpriteOriginal.angle = 3;

                for(let i: number = 0; i < this._winTweens.length; i++) {
                    this.game.tweens.remove(this._winTweens[i]);
                }

                this._winTweens = [];
            });
        }


        public update(): void {
            if (window["collectable"]) {
                this._panel.update();
            }

            if(this._field) {
                this._field.update();
            }
        }

        public showWinScreen(): void {
            this.game.add.tween(this._finalContainer).to({
                x: 0
            }, 500, Phaser.Easing.Sinusoidal.InOut, true);
            this._ostinClapSprite.play("clap");
            this._field.disableField = true;

            this._sunray.angle = 0;
            this._star.angle = 0;
            this._installNowSpriteOriginal.angle = 3;
            this._backStars.scale.set(1,1);
            this._installNowSpriteOriginal.scale.set(0.85, 0.85);

            this._winScreen.alpha = 1;

            this._ostinClap.alpha = 1;
            this._installNowSprite.alpha = 1;
            this._replaySprite.alpha = 1;

            this._installNowSprite.inputEnabled = true;
            this._replaySprite.inputEnabled = true;



            this._winTweens.push(
                this.game.add.tween(this._sunray).to({angle: 360}, 18500, Phaser.Easing.Linear.None, true, 0, -1)
            );

            this._winTweens[0].onComplete.addOnce(()=>{
                this._sunray.angle = 0;
            })

            this._winTweens.push(
                this.game.add.tween(this._backStars.scale).to({x: this._backStars.scale.x + 0.25, y: this._backStars.scale.y + 0.25}, 2800, Phaser.Easing.Sinusoidal.InOut, true, 0, -1)
            );

            this._winTweens[1].yoyo(true);

            this._winTweens.push(
                this.game.add.tween(this._star).to({angle: 15}, 3400, Phaser.Easing.Sinusoidal.Out, true, 0, -1)
            );

            this._winTweens[2].yoyo(true);

            this._winTweens.push(
                this.game.add.tween(this._installNowSpriteOriginal.scale).to({x: 1, y: 1}, 3400, Phaser.Easing.Sinusoidal.Out, true, 0, -1)
            );

            this._winTweens[3].yoyo(true, 3600);

            this._winTweens.push(
                this.game.add.tween(this._installNowSpriteOriginal).to({angle: -3}, 1200, Phaser.Easing.Sinusoidal.Out, true, 0, -1)
            );

            this._winTweens[4].yoyo(true, 0);

            this.game.add.tween(this._logo.scale).to({x: 1.6, y: 1.6}, 600, Phaser.Easing.Sinusoidal.Out, true);
            this._logoOsprite.otherScale(0.65).end();
        }

        public onLandscape(): void {
            if (this._welldone) {
                this._welldone.scale.set(1.25);
                this._sunray.scale.set(1.2);
            }

            if (this._field && GameConfig.gameArray.length != 4) {

                GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayLandscape);

                let y: number = this._field.y;
                this._field.destroy();

                setTimeout(()=> {

                    this._field = new Field(this.game, 0, 0, this);
                    this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 5);
                    this._container.addChild(this._field);


                    if (window["collectable"]) {


                        this._field.destroyCallback = (type: number, delta: number) => {
                            this._panel.updateCounter(type, delta);
                        };
                        this._field.finalCallback = this.showWinScreen.bind(this);
                    } else {

                        this._field.destroyCallback = (type: number, delta: number) => {
                            type + 1;
                        };

                    }
                }, 20);
            }
        }

        public onPortret(): void {
            if (this._welldone) {
                this._welldone.scale.set(1.5);
                this._sunray.scale.set(1.3);
            }

            if (this._field && GameConfig.gameArray.length != 8) {

                GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayPortret);

                let y: number = this._field.y;
                this._field.destroy();

                setTimeout(()=>{
                    this._field = new Field(this.game, 0, 0, this);
                    this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 2);
                    this._container.addChild(this._field);


                    if (window["collectable"]) {


                        this._field.destroyCallback = (type: number, delta: number) => {
                            this._panel.updateCounter(type, delta);
                        };
                        this._field.finalCallback = this.showWinScreen.bind(this);
                    } else {

                        this._field.destroyCallback = (type: number, delta: number) => {
                            type + 1;
                        };

                    }
                }, 20)


            }
        }

    }

    function getSize(log = false) {
        let w = 0;
        let h = 0;
        let deW = 0;
        let deH = 0;
        if (!(document.documentElement.clientWidth == 0)) {
            deW = document.documentElement.clientWidth;
            deH = document.documentElement.clientHeight;
        }

        w = deW;
        h = deH;
        if (window.innerWidth > window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        return {width: w, height: h};
    }

    function sendEvent(value, params = null) {
        window["trackEvent"](value, params);
    }

    function ClickInstall() {
        window["trackClick"]();
    }
}