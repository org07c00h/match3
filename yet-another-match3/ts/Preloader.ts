/**
 * Created by DEaDA on 3/25/17.
 */
module mygame {
    export class Preloader extends Phaser.State {
        preloadBar;
        preloadBarBackground;

        preload() {
            this.preloadBarBackground = this.add.image(0, 0, 'preloaderBarEmpty');
            this.preloadBar = this.add.image(0, 0, 'preloaderBar');
            this.preloadBarBackground.alignIn(this.game.world.bounds, Phaser.CENTER);
            this.preloadBar.x = Math.floor(this.preloadBarBackground.x);
            this.preloadBar.y = Math.floor(this.preloadBarBackground.y);
            this.load.setPreloadSprite(this.preloadBar);


            let baseURL = window['baseURL'];

            let currentBG: number = window["currentBG"];

            if(currentBG == 0) {
                currentBG = this.game.rnd.integerInRange(1, 3);
            }


            this.game.load.image('background', baseURL + "assets/background_"+ currentBG +".png");

            if(window["collectable"]) {
                this.game.load.atlas("objects", baseURL + "assets/collect.png", baseURL + "assets/collect.json");
            } else {
                this.game.load.atlas("objects", baseURL + "assets/no_collect.png", baseURL + "assets/no_collect.json");
            }


            if (window["myLang"] == "en") {
                this.game.load.image("play_free", baseURL + "assets/EngButton" + "_" + window["buttonPlayColor"] + ".png");
            } else {
                this.game.load.image("play_free", baseURL + "assets/DutButton" + "_" + window["buttonPlayColor"] + ".png");
            }



            this.game.load.bitmapFont('font', baseURL + "assets/font.png", baseURL + "assets/font.fnt");
        }

        create() {
            this.game.state.start('PlayState');
        }

        ChangeSize() {
            this.game.scale.setGameSize(window.innerWidth, window.innerHeight);
            this.preloadBarBackground.width = window.innerWidth;
            this.preloadBarBackground.scale.x = this.preloadBarBackground.scale.x - 0.1;
            this.preloadBarBackground.scale.y = this.preloadBarBackground.scale.x;
            this.preloadBar.scale.set(this.preloadBarBackground.scale.x, this.preloadBarBackground.scale.y);
            if (this.game.scale.width > window.innerWidth) {
                this.game.canvas.style.marginLeft = '-' + (this.game.scale.width - window.innerWidth) / 2 + 'px';
            }
            this.preloadBarBackground.alignIn(this.game.world.bounds, Phaser.CENTER);
            this.preloadBar.x = this.preloadBarBackground.x;
            this.preloadBar.y = this.preloadBarBackground.y;

        }
    }
}