module mygame {
    export class GameConfig {
        public static readonly BLOCK_SIZE: number = 100;
        public static readonly SWAP_TIME: number = 150;
        public static readonly DESTROY_TIME: number = 150;
        public static readonly FALLDOWN_TIME: number = 400;
        public static readonly FLY_TIME: number = 300;
        public static readonly CONDENSATION_TIME: number = 300;
        public static readonly COUNTERS: number[] = [0, 2, 1, 0, 0, 0];
        public static COUNTERS_PUBLIC: number[] = [0, 2, 1, 0, 0, 0];
        public static readonly NAMES: string[] = ["apple", "apple", "grapes", "flower", "pear", "leaf"];
        public static readonly PANEL_GEM_SCALE: number = 0.8;
        public static readonly APPLE_POS: Phaser.Point = new Phaser.Point(100, -60);
        public static readonly GRAPES_POS: Phaser.Point = new Phaser.Point(370, -60);
        public static readonly UNLOCK_OBJ_PER_FRUITS: number = 3;
        public static gameArray: number[][];
        public static gameArrayLandscape: number[][] = [
            [2, 3, 4, 1, 3, 2, 2, 3, 4, 4], // 1
            [3, 5, 1, 4, 1, 3, 5, 2, 3, 5], // 2
            [2, 5, 1, 3, 5, 1, 2, 3, 4, 4], // 3
            [1, 3, 2, 4, 1, 3, 2, 4, 1, 3]];
        public static gameArrayPortret: number[][] = [
            [1, 2, 5, 3, 4, 2], // 1
            [3, 4, 3, 1, 3, 5], // 2
            [ 3, 5, 1, 5, 1, 1], // 3
            [ 5, 3, 2, 2, 5, 5], // 4
            [ 2, 2, 4, 1, 2, 1], // 5
            [4, 3, 3, 1, 5, 2], // 6
            [4, 1, 2, 3, 2, 5], // 7
            [5, 5, 2, 4, 1, 1]
        ]; // 3

        public static deepClone(m: number[][]): number[][] {
            let result = [];
            for (let i = 0; i < m.length; i++) {
                result.push([]);
                for(let j = 0; j < m[0].length; j++) {
                    result[i].push(m[i][j]);
                }
            }
            return result;
        }

        public  static transpose(m: number[][]): number[][] {
            let result = [];
            for (let i = 0; i < m[0].length; i++) {
                result.push([]);
                for (let j = 0; j < m.length; j++) {
                    result[i].push(m[j][i]);
                }
            }
            return result;
        }

    }
}