module mygame {
    export class Gem extends Phaser.Sprite {
        private _mytype: number;
        private _sprite: Phaser.Sprite;
        private _spriteName: string;
        private _destroyCallback: Function;
        private _destroyAnimation: Phaser.Animation;
        private _row: number;
        private _col: number;
        private _onCompleteFired: boolean;
        private _tempTweenArray: Phaser.Tween[];

        constructor(game: Phaser.Game, x: number, y: number, type: number) {
            super(game, x, y);
            this._tempTweenArray = []
            this._mytype = type;
            this._spriteName = GameConfig.NAMES[this._mytype];
            if (type > 0) {
                this._sprite = this.game.add.sprite(0, 0, "objects", "F_" + this._mytype + ".png");
                this._sprite.anchor.set(0.5);
                // this._sprite.width = GameConfig.BLOCK_SIZE - 10;
                // this._sprite.height = GameConfig.BLOCK_SIZE - 10;
                this.addChild(this._sprite);
                // this._destroyAnimation = [null];
                // for (let i: number = 1; i < GameConfig.NAMES.length; i++) {
                this._destroyAnimation = this._sprite.animations.add("set off", Phaser.Animation.generateFrameNames("e_", 1, 7, ".png"), 18, false);
                this._destroyAnimation.onComplete.add(() => {
                    if (!this._onCompleteFired) {
                        this._onCompleteFired = true;
                        this._sprite.alpha = 0;

                        if (this._destroyCallback) {
                            this._destroyCallback();
                        }
                    }
                }, this);
                // }
            }
        }

        public set row(val: number) {
            this._row = val;
        }

        public set col(val: number) {
            this._col = val;
        }

        public get col(): number {
            return this._col;
        }

        public get row(): number {
            return this._row;
        }

        public get mytype(): number {
            return this._mytype;
        }

        public set onDestroy(val: Function) {
            this._destroyCallback = val;
        }

        public set mytype(val: number) {

            if (!this._onCompleteFired) {
                // debugger;
                this._destroyAnimation.stop();
                // this._destroyCallback();
                this._onCompleteFired = true;
                //console.log("TRIGGERED!!!");

            }
            this._destroyAnimation.stop();
            this._mytype = val;
            this._spriteName = GameConfig.NAMES[this._mytype];
            this._sprite.loadTexture("objects", "F_" + this._mytype + ".png");
            this._sprite.alpha = 1;
            this._onCompleteFired = false;
        }

        public playDestroy(): void {
            let tween: Phaser.Tween = this.game.add.tween(this._sprite.scale).to({x: 0.15, y: 0.15}, 100, Phaser.Easing.Sinusoidal.Out, true);
            this._tempTweenArray.push(tween);
            tween.onComplete.addOnce(()=>{
                this._sprite.scale.set(1);
                this._destroyAnimation.play();
            });



        }

        public clearTweens() {
            //console.log("Ckeat");
            /*if(this._tempTweenArray.length > 0) {
                for(let i: number = 0; i < this._tempTweenArray.length; i ++) {
                    if(this._tempTweenArray[i] != null) this.game.tweens.remove(this._tempTweenArray[i]);
                }
            }
            this._tempTweenArray = [];*/
        }

        public select(): void {
            // debugger;
            this._sprite.scale.set(1.2);
        }

        public deselect(): void {
            this._sprite.scale.set(1);
            // this._sprite.width = GameConfig.BLOCK_SIZE;
            // this._sprite.height = GameConfig.BLOCK_SIZE;
        }

        public isSame(a: Gem): boolean {
            return (this._row == a.row) && (this._col == a.col);
        }

        public isNext(gem2: Gem): boolean {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        }

        public update(): void {
            //console.log("it works");
        }
    }
}