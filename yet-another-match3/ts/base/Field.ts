module mygame {
    export enum BlockTypes {
        DISABLED,
        ACTIVE
    };

    export interface Block {
        col: number,
        row: number,
        sprite: Phaser.Sprite;
        state: BlockTypes;
        type: number;
    };

    export interface Hole {
        col: number;
        row: number;
        length: number;
        hasHoleAbove: boolean;
    };

    export class Field extends Phaser.Sprite {
        private _gameArray: any[][];
        private _selectedGem: Gem;
        private _canClick: boolean;
        private _pickedGem: Gem;
        private _removeMap: number[][];
        private _fastFall: boolean;
        private _destroyed: number;
        private _fieldUpperBound: number[] = [];
        private _cb: Function;
        private _coolAnimation: boolean;
        private _verticalAnimation: boolean;
        private _counters: number[];
        private _destroyedType: number;
        private _streak: number[];
        private _callbackWasFired: boolean;
        private _container: Phaser.Sprite;
        private _myMask: Phaser.Graphics;
        private _turnOffMask: boolean;
        private _selectedPoint: Phaser.Point;
        private _bgContainer: Phaser.Sprite;
        private _hand: Phaser.Sprite;
        private _handTween: Phaser.Tween;
        private _firstMatch: boolean;
        private _firstMatchCallback: Function;
        private _endMatchCallback: Function;
        private _disabled: boolean;
        private _finalWasFired: boolean;
        private _finalCallback: Function;
        private _playState: PlayState;
        private _tweenArray: Phaser.Tween[];
        private _tempSprite: Phaser.Sprite[];
        private _fruit1Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit2Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit3Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit4Emmiter: Phaser.Particles.Arcade.Emitter;
        private _fruit5Emmiter: Phaser.Particles.Arcade.Emitter;

        constructor(game: Phaser.Game, x: number, y: number, playState: PlayState) {
            super(game, x, y);

            this._playState = playState;
            this._tweenArray = [];
            this._tempSprite = [];

            this._destroyed = 0;
            this._turnOffMask = false;
            this._firstMatch = false;
            this._finalWasFired = false;
            this._gameArray = GameConfig.gameArray;
            this._fieldUpperBound = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
            this._streak = [];
            for (let i: number = 0; i < 6; i++) {
                this._streak.push(0);
            }
            this._disabled = false;

            this._bgContainer = this.game.add.sprite(0, 0);
            this._bgContainer.anchor.set(0.5, 0);
            this.addChild(this._bgContainer);

            this._container = this.game.add.sprite(0, 0);
            this._container.anchor.set(0.5, 0);
            this.addChild(this._container);
            this._myMask = this.game.add.graphics(0, 0);
            this.addChild(this._myMask);
            this._container.mask = this._myMask;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRect(0, 0, 100 * 10, 100 * 8);
            this._myMask.endFill();

            this.drawField();
            this._selectedGem = null;
            if (window["collectable"]) {
                this._counters = GameConfig.COUNTERS_PUBLIC;
            } else {
                this._counters = [-1, -1, -1, -1, -1, -1, -1];
            }
            this.game.input.onUp.add(() => {
                //ry {
                    this.game.input.deleteMoveCallback(this.gemMove, this);
                //} catch (e) {
                //    console.log("error");
                //}
            }, this);
            this._canClick = true;

            this._fruit1Emmiter = this.game.add.emitter(0, 0, 50);
            this.addChild(this._fruit1Emmiter);
            this._fruit1Emmiter.makeParticles("objects", "F_1.png");
            this._fruit1Emmiter.gravity = 0;
            this._fruit1Emmiter.minParticleSpeed.setTo(-300, -300);
            this._fruit1Emmiter.maxParticleSpeed.setTo(300, 300);
            this._fruit1Emmiter.setAlpha(1, 0.1, 1200);
            this._fruit1Emmiter.setScale(1.2, 0.1, 1.2, 0.1, 1200, Phaser.Easing.Quintic.Out);

            this._fruit2Emmiter = this.game.add.emitter(0, 0, 50);
            this.addChild(this._fruit2Emmiter);
            this._fruit2Emmiter.makeParticles("objects", "F_2.png");
            this._fruit2Emmiter.gravity = 0;
            this._fruit2Emmiter.minParticleSpeed.setTo(-300, -300);
            this._fruit2Emmiter.maxParticleSpeed.setTo(300, 300);
            this._fruit2Emmiter.setAlpha(1, 0.1, 1200);
            this._fruit2Emmiter.setScale(1.2, 0.1, 1.2, 0.1, 1200, Phaser.Easing.Quintic.Out);

            this._fruit3Emmiter = this.game.add.emitter(0, 0, 50);
            this.addChild(this._fruit3Emmiter);
            this._fruit3Emmiter.makeParticles("objects", "F_3.png");
            this._fruit3Emmiter.gravity = 0;
            this._fruit3Emmiter.minParticleSpeed.setTo(-300, -300);
            this._fruit3Emmiter.maxParticleSpeed.setTo(300, 300);
            this._fruit3Emmiter.setAlpha(1, 0.1, 1200);
            this._fruit3Emmiter.setScale(1.2, 0.1, 1.2, 0.1, 1200, Phaser.Easing.Quintic.Out);

            this._fruit4Emmiter = this.game.add.emitter(0, 0, 50);
            this.addChild(this._fruit4Emmiter);
            this._fruit4Emmiter.makeParticles("objects", "F_4.png");
            this._fruit4Emmiter.gravity = 0;
            this._fruit4Emmiter.minParticleSpeed.setTo(-300, -300);
            this._fruit4Emmiter.maxParticleSpeed.setTo(300, 300);
            this._fruit4Emmiter.setAlpha(1, 0.1, 1200);
            this._fruit4Emmiter.setScale(1.2, 0.1, 1.2, 0.1, 1200, Phaser.Easing.Quintic.Out);

            this._fruit5Emmiter = this.game.add.emitter(0, 0, 50);
            this.addChild(this._fruit5Emmiter);
            this._fruit5Emmiter.makeParticles("objects", "F_5.png");
            this._fruit5Emmiter.gravity = 0;
            this._fruit5Emmiter.minParticleSpeed.setTo(-300, -300);
            this._fruit5Emmiter.maxParticleSpeed.setTo(300, 300);
            this._fruit5Emmiter.setAlpha(1, 0.1, 1200);
            this._fruit5Emmiter.setScale(1.2, 0.1, 1.2, 0.1, 1200, Phaser.Easing.Quintic.Out);
        }

        public set firstMatchCallback(val: Function) {
            this._firstMatchCallback = val;
        }

        public set endMatchCallback(val: Function) {
            this._endMatchCallback = val;
        }

        public set disableField(val: boolean) {
            this._disabled = val;
        }

        public set destroyCallback(val: Function) {
            this._cb = val;
            // debugger;
        }

        public mydebug(): void {
            // debugger;
        }

        public set finalCallback(val: Function) {
            this._finalCallback = val;
        }

        private gemMove(event: any, pX: number, pY: number): void {
        // if (this._selectedGem == null) return;
        // if (event.id == 0 ) {
        //     console.log(window["tutorial"]);


            let distX: number = pX - this._selectedPoint.x ;
            let distY: number = pY - this._selectedPoint.y;

            let deltaRow = 0;
            let deltaCol = 0;

            if (Math.abs(distX) > GameConfig.BLOCK_SIZE /2 ) {
                if (distX > 0) {
                    deltaCol = 1
                } else {
                    deltaCol = -1;
                }
            } else {
                if (Math.abs(distY) > GameConfig.BLOCK_SIZE / 2) {
                    if (distY > 0) {
                        deltaRow = 1;
                    } else {
                        deltaRow = -1;
                    }
                }
            }

            if (deltaRow + deltaCol != 0) {
                this._pickedGem = this._gameArray[this._selectedGem.row + deltaRow][this._selectedGem.col + deltaCol];
                if (this._pickedGem.mytype > 0) {
                    this._selectedGem.deselect();
                    this.swapGems(true);
                    this.game.input.deleteMoveCallback(this.gemMove, this);
                } else {
                    this._pickedGem = null;
                }
            }
            // }

        }



        private drawField(): void {
            // debugger;
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    let type: number = this._gameArray[i][j];
                    let x: number = GameConfig.BLOCK_SIZE * (j + 0.5);
                    let y: number = GameConfig.BLOCK_SIZE * (i + 0.5);
                    if (type > 0) {
                        let sprite: Gem = new Gem(this.game, x, y, type);
                        let gemBG: Phaser.Sprite = this.game.add.sprite(x, y, "objects", "tile.png");
                        gemBG.anchor.set(0.5);
                        this._bgContainer.addChild(gemBG);
                        this._container.addChild(sprite);
                        sprite.inputEnabled = true;
                        sprite.row = i;
                        sprite.col = j;

                        sprite.events.onInputDown.add((obj: Phaser.Sprite, pointer: Phaser.Pointer) => {
                            let tutorialCondition;
                            if (this._gameArray.length == 8) {
                                tutorialCondition= window["tutorial"] && (sprite.row == 1) && (sprite.mytype == 1) || !window["tutorial"];
                                tutorialCondition = tutorialCondition || window["tutorial"] && (sprite.row == 2) && (sprite.mytype == 5);
                            } else {
                                 tutorialCondition = window["tutorial"] && (sprite.row == 0) && (sprite.mytype == 1) || !window["tutorial"];
                                tutorialCondition = tutorialCondition || window["tutorial"] && (sprite.row == 1) && (sprite.mytype == 4);
                            }
                            if (this._canClick && tutorialCondition && !this._disabled) {
                                if (this._selectedGem == null) {
                                    this._selectedGem = sprite;
                                    this._selectedPoint = new Phaser.Point(pointer.x, pointer.y);
                                    sprite.select();
                                    this.game.input.addMoveCallback(this.gemMove, this);
                                } else {
                                    this.gemSelect(sprite);
                                }
                            }

                        }, this);
                        if (this._gameArray.length == 8) {
                            if (!((i == 1 || i == 2) && sprite.mytype == 1) && window["tutorial"]) {
                                sprite.alpha = 0.2;
                            }
                        } else {
                            if (!((i == 1 || i == 0) && sprite.mytype == 1) && window["tutorial"]) {
                                sprite.alpha = 0.2;
                            }
                        }

                        this._gameArray[i][j] = sprite;
                    } else {
                        let grass: Phaser.Sprite = this.game.add.sprite(x, y, "grass");
                        grass.width = grass.height = GameConfig.BLOCK_SIZE;
                        grass.anchor.set(0.5);
                        this.addChild(grass);
                        this._gameArray[i][j] = new Gem(this.game, x, y, 0);
                        this._gameArray[i][j].row = i;
                        this._gameArray[i][j].col = j;
                    }

                }
            }
            if (window["tutorial"]) {
                this._hand = this.game.add.sprite(100 * 4 - 20, 100 * 2 - 20, "objects", "hand.png");

                this._hand.anchor.set(0.5);
                if (this._gameArray.length == 8) {
                    this._handTween = this.game.add.tween(this._hand).to({
                        y: 280
                    }, 500, Phaser.Easing.Linear.None, true, 0, -1, true);
                } else {
                    this._handTween = this.game.add.tween(this._hand).to({
                        y: 80
                    }, 500, Phaser.Easing.Linear.None, true, 0, -1, true);
                }
                this.addChild(this._hand);
            }
        }

        public clearTweens(): void {
            if(this._tweenArray.length > 0) {
                for(let i: number = 0; i < this._tweenArray.length; i ++) {
                    if(this._tweenArray[i] != null) this.game.tweens.remove(this._tweenArray[i]);
                }
            }
            this._tweenArray = [];

            if(this._tempSprite.length > 0) {
                for(let i: number = 0; i < this._tempSprite.length; i ++) {
                    if(this._tempSprite[i] != null) this._tempSprite[i].destroy(true);
                }
            }
            this._tempSprite = [];

            if(this._pickedGem != null) this._pickedGem.clearTweens();
            if(this._selectedGem != null) this._selectedGem.clearTweens();

            for (let i = 0; i < this._gameArray.length; i++) {
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] != null) {
                        this._gameArray[i][j].clearTweens();
                    }
                }
            }


        }


        public replay(): void {
            this._disabled = false;
            this._finalWasFired = false;
            if (window["collectable"]) {
                this._counters = GameConfig.COUNTERS_PUBLIC;
            } else {
                this._counters = [-1, -1, -1, -1, -1, -1, -1];
            }
        }

        private gemSelect(gem: Gem): void {
            if (this._selectedGem == null) {
               // console.log("something strange");
                return;
            }

            if (this._selectedGem.isSame(gem)) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                return;
            }

            if (this._selectedGem.isNext(gem)) {
                this._selectedGem.deselect(); // set scale
                this._pickedGem = gem;
                this.swapGems();
               // console.log("swap!");
            } else {
                this._selectedGem.deselect();
                this._selectedGem = gem;
                gem.select();
            }

        }

        private swapGems(swapBack: boolean = true): void {
            if (window["tutorial"]) {
                if (Math.abs(this._pickedGem.row - this._selectedGem.row) != 1) {
                    return;
                } else {
                    window["tutorial"] = false;
                    for (let i: number = 0; i < this._gameArray.length; i++) {
                        for (let j: number = 0; j < this._gameArray[i].length; j++) {
                            this._gameArray[i][j].alpha = 1;
                            this._handTween.stop();
                            this._hand.destroy();
                        }
                    }

                }
            }
            this._canClick = false;
            this._coolAnimation = true;
            this._verticalAnimation = false;
            this._gameArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gameArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;

            let rowSel: number = this._selectedGem.row;
            let colSel: number = this._selectedGem.col;
            let rowPic: number = this._pickedGem.row;
            let colPic: number = this._pickedGem.col;

            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            // save poistions
            let posPicked: Phaser.Point = new Phaser.Point((colPic + 0.5) * GameConfig.BLOCK_SIZE, (rowPic + 0.5) * GameConfig.BLOCK_SIZE,);
            let posSelected: Phaser.Point = new Phaser.Point((colSel + 0.5) * GameConfig.BLOCK_SIZE, (rowSel + 0.5) * GameConfig.BLOCK_SIZE);

            // add tweens
            let tweenSelected: Phaser.Tween = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            this._tweenArray.push(tweenSelected);

            let tweenPicked: Phaser.Tween = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            this._tweenArray.push(tweenPicked);

            // temp information
            let type1: number = this._selectedGem.mytype;
            let type2: number = this._pickedGem.mytype;


            tweenPicked.onComplete.add(() => {
                 if (this.isMatchOnBoard(rowPic, colPic, type1, rowSel, colSel, type2)) {
                     //console.log("DESTROY!!!");
                     this.handleMatches();
                     this._selectedGem = null;
                     this._pickedGem = null;
                 } else if (swapBack) {
                     this.swapGems(false);
                 }
            }, this);

            tweenPicked.start();
            tweenSelected.start();

            if (!swapBack) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                this._pickedGem = null;
                this._canClick = true;
            }


        }

        private isMatchOnBoard(row1: number, col1: number, type1: number, row2: number, col2: number, typr2: number): boolean {
            return this.isMatch(row1, col1, type1) || this.isMatch(row2, col2, typr2);
        }

        private isMatch(row: number, col: number, type: number): boolean {
            return this.isHorizontal(row, col, type) || this.isVertical(row, col, type);
        }

        private isHorizontal(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;


            for (let i: number = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gameArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let j: number = start; j >= end; j--) {
                    if (this._gameArray[row][j].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }

            return false;
        }

        private isVertical(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;

            for (let j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gameArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let i: number = start; i >= end; i--) {
                    if (this._gameArray[i][col].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }


            return false;
        }

        private debugShow(): void {
            for (let i = 0; i < this._gameArray.length; i++) {
                let str = "";
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] !== -1) {
                        str += this._gameArray[i][j].mytype + " ";
                    } else {
                        str += " -1 "
                    }

                }
            }
        }

        private handleMatches(): void {
            if (!this._firstMatch && !window["collectable"]) {
                this._firstMatch = true;
                if(this._firstMatchCallback) {
                    this._firstMatchCallback();
                }

            }
            this._removeMap = [];

            for (let i: number = 0; i < this._gameArray.length; i++) {
                this._removeMap[i] = [];
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    this._removeMap[i].push(0);
                }
            }

            this.handleHorizontalMatches();
            this.handleVerticalMatches();
            this.destroyGems();
            this._coolAnimation = false;
        }

        private destroyGems(): void {
            this._destroyed = 0;
            let condensationPoint: Phaser.Point;
            let condensationType: number;
            if (this._selectedGem != null) {
                if (this._removeMap[this._selectedGem.row][this._selectedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._selectedGem.x, this._selectedGem.y);
                    condensationType = this._selectedGem.mytype;
                } else if (this._removeMap[this._pickedGem.row][this._pickedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._pickedGem.x, this._pickedGem.y);
                    condensationType = this._pickedGem.mytype;
                } else {
                    condensationPoint = new Phaser.Point(1, 1);
                    condensationType = 0;
                }
            }
            this._coolAnimation = false
            if (this._coolAnimation) {
                this.condensate(condensationPoint);
            } else {
                setTimeout(()=>{
                let gemsToDestroy: Gem[] = [];
                let tweens: Phaser.Tween[] = [];
                let delay: number = 0;
                for (let i = 0; i < this._removeMap.length; i++) {
                    for (let j = 0; j < this._removeMap[i].length; j++) {
                        if (this._removeMap[i][j] > 0) {
                            this._destroyed++;
                            let gem: Gem = this._gameArray[i][j];

                            if ((gem.mytype == 1 || gem.mytype == 2) && this._counters[gem.mytype] > 0) {
                                let point: Phaser.Point;
                                if (gem.mytype == 1) {
                                    point = this._playState.getGurrentElementPosition("first");
                                } else {
                                    point = this._playState.getGurrentElementPosition("second");
                                }
                                // debugger;

                                // remove sprite from current layer
                                let xPos: number = gem.worldPosition.x;
                                let yPos: number = gem.worldPosition.y;

                                this._container.removeChild(gem);
                                this.game.world.addChild(gem);
                                this._tempSprite.push(gem);
                                gem.position.set(xPos, yPos);
                                //this.addChild(gem);
                                let flyTween: Phaser.Tween = this.createFlyTween(gem, point, delay);
                                delay += 100;
                                tweens.push(flyTween);
                            } else {
                                gem.onDestroy = () => {
                                    this._destroyed--;
                                    this._counters[gem.mytype] -= 1;
                                    this._cb(gem.mytype, 1);
                                    if (this._destroyed == 0) {

                                        this.makeGemsFall();
                                        // this.makeGemsFall();
                                    }
                                };
                                gemsToDestroy.push(gem);
                            }

                        }
                    }

                }

                let deleyForParticle: number = 0;

                gemsToDestroy.forEach((x) => {
                    x.playDestroy()
                    //console.log(x.mytype);

                    switch (x.mytype) {
                        case 1:
                            this._fruit1Emmiter.x = x.x;
                            this._fruit1Emmiter.y = x.y;

                            this._fruit1Emmiter.start(true, 1100, null, 7);
                            break;
                        case 2:
                            this._fruit2Emmiter.x = x.x;
                            this._fruit2Emmiter.y = x.y;

                            this._fruit2Emmiter.start(true, 1100, null, 7);
                            break;
                        case 3:
                            this._fruit3Emmiter.x = x.x;
                            this._fruit3Emmiter.y = x.y;

                            this._fruit3Emmiter.start(true, 1100, null, 7);
                            break;
                        case 4:
                            this._fruit4Emmiter.x = x.x;
                            this._fruit4Emmiter.y = x.y;

                            this._fruit4Emmiter.start(true, 1100, null, 7);
                            break;
                        case 5:
                            this._fruit5Emmiter.x = x.x;
                            this._fruit5Emmiter.y = x.y;

                            this._fruit5Emmiter.start(true, 1100, null, 7);
                            break;
                    }



                });

                tweens.forEach(x => x.start());
                }, 10);
            }
        }

        private handleHorizontalMatches(): void {
             for (let i: number = 0; i < this._gameArray.length; i++) {
                  let colorStreak: number = 1;
                  let currentColor: number = -100500;
                  let startStreak: number = 0;
                  for (let j: number = 0; j < this._gameArray[i].length; j++) {
                       if (this._gameArray[i][j].mytype == currentColor && this._gameArray[i][j].mytype > 0) {
                            colorStreak++;
                       }
                       if (this._gameArray[i][j].mytype != currentColor || j == this._gameArray[i].length - 1) {
                            if (colorStreak >= 3) {

                                if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                    this._coolAnimation = false;

                                }

                                for(let k: number = 0; k < colorStreak; k++) {
                                    this._removeMap[i][startStreak + k]++;
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[i][j].mytype;
                       }
                  }
             }
        }

         private handleVerticalMatches(): void {
            for (let i: number = 0; i < this._gameArray[0].length; i++) {
                    let colorStreak: number = 1;
                    let currentColor: number = -100500;
                    let startStreak: number = 0;
                    for (let j: number = 0; j < this._gameArray.length; j++) {
                        try {
                        if (this._gameArray[j][i].mytype == currentColor && this._gameArray[j][i].mytype > 0) {
                            colorStreak++;
                        }
                        } catch(e) {
                            // debugger;
                        }
                        if (this._gameArray[j][i].mytype != currentColor || j == this._gameArray.length - 1) {
                            if (colorStreak >= 3) {

                                if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                    this._coolAnimation = false;
                                } else {
                                    this._verticalAnimation = true;
                                }
                                for(let k: number = 0; k < colorStreak; k++) {
                                    this._removeMap[startStreak + k][i]++;
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[j][i].mytype;
                       }
                  }
             }
        }

        private checkGlobalMatch(): boolean {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype <= 0) {
                        continue;
                    }
                    if (this.isMatch(i, j, this._gameArray[i][j].mytype)){
                        return true;
                    }
                }
            }
            return false;
        }

        private swap(x1: number, y1: number, x2: number, y2: number, dx:number = 0): void {
            let temp: Block = this._gameArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;

            this._gameArray[x1][y1] = this._gameArray[x2][y2];

            this._gameArray[x1][y1].row = x1 - dx;
            this._gameArray[x1][y1].col = y1;

            this._gameArray[x2][y2] = temp;
        }

        private makeGemsFall(): void {
            if (this._destroyed > 0) {
                this.game.time.events.add(200, this.makeGemsFall, this);
            }
            if (this._turnOffMask) {
                this._turnOffMask = false;
                this._container.mask = this._myMask;
            }
            let holes: Hole[] = this.findHoles();
            let tweens: Phaser.Tween[] = [];
            let fallen: number = 0;

            // holes.forEach((hole: Hole) => {
            for (let k: number = 0; k < holes.length; k++) {
                let hole: Hole = holes[k];
                let above: Hole;
                let start: number;
                let end: number;
                if (hole.hasHoleAbove) {
                    above = holes[k + 1];
                    start = above.row + above.length - hole.length;
                    end = hole.row - start;
                } else {
                    start = 0;
                    end = hole.row - this._fieldUpperBound[hole.col];
                }
                for (let i: number = 0; i < end; i++) {
                    this.swap(hole.row - 1 - i, hole.col, hole.row + hole.length - 1 - i, hole.col);
                }
                if (!hole.hasHoleAbove) {
                    for (let i = this._fieldUpperBound[hole.col]; i < this._fieldUpperBound[hole.col] + hole.length; i++) {
                        let gem: Gem = this._gameArray[i][hole.col];
                        let rnd: number = this.game.rnd.between(1, 5);
                        gem.mytype = rnd;

                        gem.x = (hole.col + 0.5) * GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - hole.length + 0.5) * GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[hole.col];
                    end = hole.row + hole.length;
                } else {
                    let len: number = hole.row - (above.row + above.length - hole.length);
                    start = hole.row + hole.length - len;
                    end = hole.row + hole.length;
                }
                for (let i: number = start; i < end; i++) {
                    try {
                        this._gameArray[i][hole.col];
                    } catch (e) {
                        // debugger;
                    }
                    let gem: Gem = this._gameArray[i][hole.col];
                    fallen++;
                    let tween: Phaser.Tween = this.game.add.tween(gem).to({
                        y: gem.y + hole.length * GameConfig.BLOCK_SIZE
                    }, GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, false);
                    this._tweenArray.push(tween);
                    tween.onComplete.add(() => {
                        this._gameArray[gem.row][gem.col] = gem;
                        fallen--;
                        if (fallen == 0) {
                            if(this.checkGlobalMatch()){
                                this.handleMatches();
                            } else {
                                this._canClick = true;
                                this._selectedGem = null;
                                if (!this._finalWasFired) {
                                    let counter: number = 0;
                                    for (let i: number = 0; i < this._counters.length; i++) {
                                        if (this._counters[i] <= 0) {
                                            counter++;
                                        }
                                        if (counter == this._counters.length) {
                                            this._finalWasFired = true;
                                            GameConfig.COUNTERS_PUBLIC = [0,window["firstElement"],window["secondElement"], 0, 0, 0];
                                            if(this._finalCallback) this._finalCallback();
                                        }
                                    }
                                }

                                for (let i = 0; i < this._gameArray.length; i++) {
                                    let str = "";
                                    for (let j = 0; j < this._gameArray[i].length; j++) {
                                        str += this._gameArray[i][j].mytype + "\t";
                                    }
                                }
                            }
                        }
                    });
                    tweens.push(tween);
               }
            }

            tweens.forEach(x => x.start());
        }

        private createFlyTween(gem: Gem, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {
            let scaleTween: Phaser.Tween = this.game.add.tween(gem.scale).to({
                x: GameConfig.PANEL_GEM_SCALE,
                y: GameConfig.PANEL_GEM_SCALE
            }, GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false);

            this._tweenArray.push(scaleTween);

            scaleTween.onComplete.addOnce(() => {
                gem.scale.setTo(1);
            }, this);

            let flyTween: Phaser.Tween = this.game.add.tween(gem).to({
                x: flyawayPoint.x,
                y: flyawayPoint.y
            }, GameConfig.FLY_TIME, Phaser.Easing.Linear.None, false, delay);

            this._tweenArray.push(flyTween);

            flyTween.onStart.addOnce(() => {
                scaleTween.start()
            });

            flyTween.onComplete.add((obj: Phaser.Sprite, tween: Phaser.Tween) => {
                if(gem) {
                    for(let i: number; i < this._tempSprite.length; i++) {
                        if(this._tempSprite[i] == gem) {
                            this._tempSprite.splice(i, 1);
                            break;
                        }
                    }
                    this.game.world.removeChild(gem);
                    this._container.addChild(gem);
                    gem.position.set(-1500, -1500);
                    this._destroyed--;
                    this._cb(gem.mytype, 1);
                    this._counters[gem.mytype]--;
                    if (this._destroyed == 0) {
                        this.makeGemsFall();
                    }
                }
            }, this);

            return flyTween;
        }

        public get gameArray(): number[][] {
            let result = [];
            for (let i: number = 0; i < this._gameArray.length; i++) {
                result.push([]);
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    result[i][j] = this._gameArray[i][j].mytype;
                }
            }

            return result;
        }

        private generateFlyAwayTween(block: Gem, condensationPoint: Phaser.Point, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {

            this._container.removeChild(block);
            this.addChild(block);

            let flyAwayTween: Phaser.Tween = this.createFlyTween(block, flyawayPoint, delay);

            if (this._verticalAnimation) {

                return flyAwayTween;
            } else {
                let condensateTween: Phaser.Tween = this.game.add.tween(block).to({
                    x: condensationPoint.x
                }, GameConfig.CONDENSATION_TIME, Phaser.Easing.Linear.None, false);
                this._tweenArray.push(condensateTween);
                condensateTween.onComplete.add((sprite: Phaser.Sprite, tween: Phaser.Tween) => {
                    flyAwayTween.start();
                });
            return condensateTween;
            }
        }

         private condensate(point: Phaser.Point): void {
            let condenstationStack: Phaser.Tween[] = [];
            let delay: number = 0;
            for (let i: number = 0; i < this._removeMap.length; i++) {
                for (let j: number = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._destroyed++;

                        let block: Gem = this._gameArray[i][j];
                        let flyAwayPoint: Phaser.Point;
                        if (block.mytype == 2) {
                            flyAwayPoint = this._playState.getGurrentElementPosition("second");
                        } else {
                            flyAwayPoint = this._playState.getGurrentElementPosition("first");
                        }
                        condenstationStack.push(this.generateFlyAwayTween(block, point, flyAwayPoint, delay));
                        condenstationStack[condenstationStack.length-1].onStart.addOnce(()=>{

                        });
                        delay += 100;

                    }
                }
            }
            condenstationStack.forEach((el: Phaser.Tween) => {
                el.start();
            });
        }

        private findHoles(): Hole[] {
            let result: Hole[] = [];

            for (let j: number = 0; j < this._gameArray[0].length; j++) {
                for (let i: number = this._gameArray.length - 1; i >= 0; i--) {
                    if (this._removeMap[i][j] > 0) {
                        // let's find length of the hole
                        let length: number = 1;
                        let row: number = i;
                        let found: boolean = true;
                        let wasPushed: boolean = false;
                        for (let k = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] >0) {
                                length++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            } else if (found) {
                                result.push({row: row, col: j, length: length, hasHoleAbove: false});
                                found = false;
                                wasPushed = true;
                                let id: number = result.length - 2;
                                if (id >= 0 && result[id].col == result[id + 1].col) {
                                    result[id].hasHoleAbove = true;
                                }
                            }
                        }

                        if (!wasPushed) {
                            result.push({row: row, col: j, length: length, hasHoleAbove: false});
                            let id: number = result.length - 2;
                            if (id >= 0 && result[id].col == result[id + 1].col) {
                                result[id].hasHoleAbove = true;
                            }
                        }

                        break;
                    }
                }
            }

            return result;
        }

        update() {
            /*this._fruitEmmiter.forEachAlive((p)=>{
                if(p.lifespan < this._fruitEmmiter.lifespan * 0.3) {
                    p.alpha *= 0.9 ;
                }
            }, this);*/
        }

        public destroy(): void {
            this.clearTweens();

            setTimeout(()=>{
                this.game.input.onUp.removeAll(this);
                this.game.tweens.removeFrom(this, true);
                super.destroy(true);
            }, 10)
        }


    }
}