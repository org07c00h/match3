module mygame {
    export class Panel extends Phaser.Sprite {
        private _sprite: Phaser.Sprite;
        private _apple: Phaser.Sprite;
        private _grapes: Phaser.Sprite;
        private _counters: number[];
        private _appleText: Phaser.BitmapText;
        private _grapeText: Phaser.BitmapText;
        private _delta: number[];
        private _timer: number;
        private _sleepTime: number;
        private _unlockCallback: Function;
        private _fruitsCounter: number;
        private _finishCallback: Function;
        private _finishWasFired: boolean;
        private _appleTween: Phaser.Tween;
        private _grapesTween: Phaser.Tween;
        private _fruitScale: number;


        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);

            this._sprite = this.game.add.sprite(0, 0, "objects", "panel.png");
            this._sprite.anchor.set(0.5);
            this.addChild(this._sprite);
            this._fruitsCounter = 0;
            this._finishWasFired = false;
            this._fruitScale = 0.8;



            this._counters = [0,window["firstElement"],window["secondElement"], 0, 0, 0]

            GameConfig.COUNTERS_PUBLIC = this._counters;


            this._appleText = this.game.add.bitmapText(-140, 5, "font", "0/" + this._counters[1].toString(), 32);
            this._appleText.anchor.setTo(0, 0.5);
            this.addChild(this._appleText);


            this._grapeText = this.game.add.bitmapText(140, 5, "font", "0/" + this._counters[2].toString(), 32);
            this._grapeText.anchor.setTo(0, 0.5);
            this.addChild(this._grapeText);

            this._delta = new Array(this._counters.length);
            for (let i: number = 0; i < this._delta.length; i++) {
                this._delta[i] = 0;
            }

            this._timer = 0;
            this._sleepTime = 1;
        }

        public scaleFruits(val: number): void {
            this._apple.scale.set(val);
            this._fruitScale = val;
            this._grapes.scale.set(val);
        }

        public set finishScreenCallback(val: Function) {
            this._finishCallback = val;
        }

        public replay(): void {
            this._finishWasFired = false;
            this._counters = [0,window["firstElement"],window["secondElement"], 0, 0, 0]
            this._delta = new Array(this._counters.length);
            for (let i: number = 0; i < this._delta.length; i++) {
                this._delta[i] = 0;
            }
            this._appleText.text = "0/" + window["firstElement"].toString();
            this._grapeText.text = "0/" + window["secondElement"].toString();
        }

        public updateCounter(type: number, delta: number): void {
            // debugger;
            if (window["collectable"]) {
                if ((type == 1 || type == 2) && this._counters[type] > 0) {
                    // if (this._counters[type] >)
                    //console.log(`UPDATE COUNTER = ${type} : ${delta}`);
                    // debugger;
                    this._delta[type] += delta;
                    this._sleepTime = 200;

                }
            }
        }
        
        public getElementPosition(type: string): Phaser.Point {
            if(type == "first") {
               // console.log(this._apple.worldPosition.x, this._apple.worldPosition.y);
                return new Phaser.Point(this._apple.worldPosition.x, this._apple.worldPosition.y);
            } else {
               // console.log(this._grapes.worldPosition.x, this._grapes.worldPosition.y);
                return new Phaser.Point(this._grapes.worldPosition.x, this._grapes.worldPosition.y);
            }
        }

        public initFruits(): void {
            this._apple = this.game.add.sprite(this.x - this._sprite.width / 2 + 45 * 2.35, this.y - 2, "objects", "F_1.png");
            this._apple.scale.setTo(0.8);
            this._apple.anchor.setTo(0.5);

            this.parent.addChild(this._apple);

            this._grapes = this.game.add.sprite(this.x + 80, this.y - 2, "objects", "F_2.png");
            this._grapes.scale.setTo(0.8);
            this._grapes.anchor.setTo(0.5);

            this.parent.addChild(this._grapes);

            this._appleTween = this.game.add.tween(this._apple.scale).to({
                x: this._fruitScale + 0.2,
                y: this._fruitScale + 0.2
            }, 150, Phaser.Easing.Linear.None, false);
            this._appleTween.onComplete.add(() => {
                this._apple.scale.set(this._fruitScale);
            });

            this._grapesTween = this.game.add.tween(this._grapes.scale).to({
                x: this._fruitScale + 0.2,
                y: this._fruitScale + 0.2
            }, 150, Phaser.Easing.Linear.None, false);
            this._grapesTween.onComplete.add(() => {
                this._grapes.scale.set(this._fruitScale);
            }, this);
        }

        public update(): void {
            let dt: number = this.game.time.elapsedMS;

            if (this._timer > 0) {
                this._timer -= dt;
            }



            if ((this._delta[1] > 0 || this._delta[2] > 0)){

                this._timer = this._sleepTime;
                this._fruitsCounter++;

                if (this._delta[1] > 0) {
                    this._delta[1]--;
                    this._counters[1]--;

                    if (this._counters[1] <= 0) {
                        this._appleText.text = window["firstElement"].toString() +"/"+ window["firstElement"].toString();

                    } else {

                        this._appleTween.start();
                        this._appleText.text = (window["firstElement"] - this._counters[1]).toString() +"/"+ window["firstElement"].toString();
                    }
                }
                if (this._delta[2] > 0) {
                    this._delta[2]--;
                    this._counters[2]--;
                    if (this._counters[2] <= 0) {
                        this._grapeText.text = window["secondElement"].toString() + "/" + window["secondElement"].toString();
                    } else {

                        this._grapesTween.start();
                        this._grapeText.text = (window["secondElement"].toString() - this._counters[2]).toString() + "/" + window["secondElement"].toString();
                    }
                }
            }
        }
    }
}