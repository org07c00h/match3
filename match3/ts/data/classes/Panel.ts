module TProject {
    export class Panel extends Phaser.Sprite {
        private _sprite: Phaser.Sprite;
        private _apple: Phaser.Sprite;
        private _grapes: Phaser.Sprite;
        private _counters: number[];
        private _appleText: Phaser.Text;
        private _grapeText: Phaser.Text;
        private _delta: number[];
        private _timer: number;
        private _sleepTime: number;
        private _unlockCallback: Function;
        private _fruitsCounter: number;


        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);

            this._sprite = this.game.add.sprite(0, 0, "panel");
            this._sprite.width = 45 * 8;
            this._sprite.height = 45;
            this.addChild(this._sprite);
            this._fruitsCounter = 0;


            // apples and grapes
            this._counters = GameConfig.COUNTERS.slice();

            this._appleText = this.game.add.text(45 * 2, GameConfig.BLOCK_SIZE / 2 + 5, this._counters[1].toString(), {
                fontSize: "18px",
                fontWeight: "bold",
                fill: "#ffffff",
                stroke: '#2c2cB9',
                strokeThickness: 7
            });
            this._appleText.anchor.setTo(0.5);
            this.addChild(this._appleText);

            this._grapeText = this.game.add.text(300, GameConfig.BLOCK_SIZE / 2 + 5, this._counters[2].toString(), {
                fontSize: "18px",
                fontWeight: "bold",
                fill: "#ffffff",
                stroke: '#2c2cB9',
                strokeThickness: 7
            });
            this._grapeText.anchor.setTo(0.5);
            this.addChild(this._grapeText);

            this._delta = new Array(this._counters.length);
            for (let i: number = 0; i < this._delta.length; i++) {
                this._delta[i] = 0;
            }

            this._timer = 0;
            this._sleepTime = 1;
        }

        public set unlockCallback(val: Function) {
            this._unlockCallback = val;
        }

        public updateCounter(type: number, delta: number): void {
            if ((type == 1 || type == 2) && this._counters[type] > 0) {
                // if (this._counters[type] >)
                console.log(`UPDATE COUNTER = ${type} : ${delta}`);
                // debugger;
                this._delta[type] += delta;
                this._sleepTime = 200;

            }
        }

        public initFruits(): void {
            this._apple = this.game.add.sprite(45 + this.x, GameConfig.BLOCK_SIZE / 2 + this.y, "fruits", "apple0");
            this._apple.scale.setTo(0.8);
            this._apple.anchor.setTo(0.5);
            this._apple.animations.add("play", Phaser.Animation.generateFrameNames("apple", 0, 3), 24).onComplete.add(() => {
                this._apple.loadTexture("fruits", "apple0");
            }, this);
            this.parent.addChild(this._apple);

            this._grapes = this.game.add.sprite(250 + this.x, GameConfig.BLOCK_SIZE / 2 + this.y, "fruits", "grapes0");
            this._grapes.scale.setTo(0.8);
            this._grapes.anchor.setTo(0.5);
            this._grapes.animations.add("play", Phaser.Animation.generateFrameNames("grapes", 0, 3), 24).onComplete.add(() => {
                this._grapes.loadTexture("fruits", "grapes0");
            }, this);
            this.parent.addChild(this._grapes);
        }

        private checkFruitCounter(): void {
            if (this._fruitsCounter >= GameConfig.UNLOCK_OBJ_PER_FRUITS) {
                this._unlockCallback();
                this._fruitsCounter -= GameConfig.UNLOCK_OBJ_PER_FRUITS;
            }
        }

        public update(): void {
            let dt: number = this.game.time.elapsedMS;

            if (this._timer > 0) {
                this._timer -= dt;
            }



            if ((this._delta[1] > 0 || this._delta[2] > 0)){
                this._timer = this._sleepTime;
                this._fruitsCounter++;
                this.checkFruitCounter();
                if (this._delta[1] > 0) {
                    this._delta[1]--;
                    this._counters[1]--;

                    if (this._counters[1] <= 0) {
                        this._appleText.text = "finish";
                    } else {
                        this._apple.play("play");
                        this._appleText.text = this._counters[1].toString();
                    }
                }
                if (this._delta[2] > 0) {
                    this._delta[2]--;
                    this._counters[2]--;
                    if (this._counters[2] <= 0) {
                        this._grapeText.text = "finish";
                    } else {
                        this._grapes.play("play");
                        this._grapeText.text = this._counters[2].toString();
                    }
                }
            }
        }
    }
}