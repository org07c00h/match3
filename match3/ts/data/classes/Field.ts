module TProject {
    export enum BlockTypes {
        DISABLED,
        ACTIVE
    };

    export interface Block {
        col: number,
        row: number,
        sprite: Phaser.Sprite;
        state: BlockTypes;
        type: number;
    };

    export interface Hole {
        col: number;
        row: number;
        length: number;
        hasHoleAbove: boolean;
    };

    export class Field extends Phaser.Sprite {
        private _gameArray: any[][];
        private _selectedGem: Gem;
        private _canClick: boolean;
        private _pickedGem: Gem;
        private _removeMap: number[][];
        private _fastFall: boolean;
        private _destroyed: number;
        private _fieldUpperBound: number[] = [];
        private _cb: Function;
        private _coolAnimation: boolean;
        private _verticalAnimation: boolean;
        private _counters: number[];
        private _destroyedType: number;
        private _streak: number[];
        private _callbackWasFired: boolean;
        private _container: Phaser.Sprite;
        private _myMask: Phaser.Graphics;
        private _turnOffMask: boolean;
        private _selectedPoint: Phaser.Point;

        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this._destroyed = 0;
            this._turnOffMask = false;
            // this._gameArray = [
            // [-1, -1, 3, 2, 2, 5, -1, -1],
            // [-1, 3, 2, 5, 1, 2, 1, -1],
            // [ 2, 5, 1, 3, 2, 1, 3, 3],
            // [ 4, 3, 1, 1, 3, 2, 2, 4],
            // [ 4, 2, 4, 2, 1, 4, 3, 5],
            // [ -1, 5, 5, 4, 5, 3, 4, -1],
            // [ -1, -1, 3, 2, 3, 5, -1, -1]
            // ];
            // this._gameArray = [
            // [-1, -1, 5, 5, 1, 2, -1, -1],
            // [-1, 3, 1, 1, 5, 3, 5, -1],
            // [2, 1, 4, 4, 5, 3, 4, 4],
            // [1, 5, 3, 5, 1, 4, 4, 2],
            // [5, 4, 1, 1, 2, 5, 2, 1],
            // [-1, 3, 2, 3, 4, 2, 4, -1],
            // [-1, -1, 2, 3, 2, 1, -1, -1]];
            this._gameArray = [
            [-1, -1, 4, 4, 3, 2, -1, -1],
            [-1, 5, 5, 1, 4, 3, 2, -1],
            [2, 5, 1, 4, 1, 1, 4, 2],
            [1, 3, 2, 4, 1, 3, 3, 5],
            [5, 1, 5, 5, 4, 2, 4, 2],
            [-1, 1, 2, 1, 1, 5, 5, -1],
            [-1, -1, 2, 4, 2, 1, -1, -1]];
            // this._gameArray = [
            //     [-1, -1, 5, 3, 4, 2, -1, -1], // 1
            //     [-1,  4, 3, 2, 3, 5,  5, -1], // 2
            //     [ 1,  5, 2, 5, 5, 5,  1,  5], // 3
            //     [ 5,  3, 2, 2, 1, 1,  5,  1], // 4
            //     [ 2,  2, 4, 1, 1, 1,  2,  2], // 1
            //     [-1,  3, 3, 1, 5, 2,  4, -1], // 2
            //     [-1, -1, 2, 3, 2, 5, -1, -1], // 3
            // ];
            this._fieldUpperBound = [2, 1, 0, 0, 0, 0, 1, 2];
            this._streak = [];
            for (let i: number = 0; i < 6; i++) {
                this._streak.push(0);
            }

            this._container = this.game.add.sprite(0, 0);
            this.addChild(this._container);

            this.game.input.keyboard.onDownCallback = () => {

                this.mydebug();
            }
            this._myMask = this.game.add.graphics(0, 0);
            this.addChild(this._myMask);
            this._container.mask = this._myMask;
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRect(45 *2, 0, 45 * 4, 45 * this._gameArray.length);
            this._myMask.drawRect(0, 45 * 2, 45, 45 * 3);
            this._myMask.drawRect(45 * 7, 45 * 2, 45, 45 * 3);
            this._myMask.drawRect(45, 45, 45 * 6, 45 * 5);
            this._myMask.endFill();

            this.drawField();
            this._selectedGem = null;

            this._counters = GameConfig.COUNTERS.slice();
            this.game.input.onUp.add(() => {
                this.game.input.deleteMoveCallback(this.gemMove, this);
            }, this);
            this._canClick = true;
        }

        public set destroyCallback(val: Function) {
            this._cb = val;
        }

        public mydebug(): void {
            debugger;
        }

        private gemMove(event: any, pX: number, pY: number): void {
        // if (this._selectedGem == null) return;
        // if (event.id == 0 ) {
            let distX: number = pX - this._selectedPoint.x ;
            let distY: number = pY - this._selectedPoint.y;

            let deltaRow = 0;
            let deltaCol = 0;

            if (Math.abs(distX) > GameConfig.BLOCK_SIZE /2 ) {
                if (distX > 0) {
                    deltaCol = 1
                } else {
                    deltaCol = -1;
                }
            } else {
                if (Math.abs(distY) > GameConfig.BLOCK_SIZE / 2) {
                    if (distY > 0) {
                        deltaRow = 1;
                    } else {
                        deltaRow = -1;
                    }
                }
            }

            if (deltaRow + deltaCol != 0) {
                this._pickedGem = this._gameArray[this._selectedGem.row + deltaRow][this._selectedGem.col + deltaCol];
                if (this._pickedGem.mytype > 0) {
                    this._selectedGem.deselect();
                    this.swapGems(true);
                    this.game.input.deleteMoveCallback(this.gemMove, this);
                } else {
                    this._pickedGem = null;
                }
            }
            // }

        }



        private drawField(): void {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    let type: number = this._gameArray[i][j];
                    let x: number = GameConfig.BLOCK_SIZE * (j + 0.5);
                    let y: number = GameConfig.BLOCK_SIZE * (i + 0.5);
                    // console.log(x, y);
                    if (type > 0) {
                        let sprite: Gem = new Gem(this.game, x, y, type);
                        this._container.addChild(sprite);
                        sprite.inputEnabled = true;
                        sprite.row = i;
                        sprite.col = j;

                        sprite.events.onInputDown.add((obj: Phaser.Sprite, pointer: Phaser.Pointer) => {
                            if (this._canClick) {
                                if (this._selectedGem == null) {
                                    this._selectedGem = sprite;
                                    this._selectedPoint = new Phaser.Point(pointer.x, pointer.y);
                                    sprite.select();
                                    this.game.input.addMoveCallback(this.gemMove, this);
                                } else {
                                    this.gemSelect(sprite);
                                }
                            }

                        }, this);

                        this._gameArray[i][j] = sprite;
                    } else {
                        let grass: Phaser.Sprite = this.game.add.sprite(x, y, "grass");
                        grass.width = grass.height = GameConfig.BLOCK_SIZE;
                        grass.anchor.set(0.5);
                        this.addChild(grass);
                        this._gameArray[i][j] = new Gem(this.game, x, y, 0);
                        this._gameArray[i][j].row = i;
                        this._gameArray[i][j].col = j;
                    }

                    // this._container.add
                }
            }
        }

        private gemSelect(gem: Gem): void {
            if (this._selectedGem == null) {
                console.log("something strange");
                return;
            }

            if (this._selectedGem.isSame(gem)) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                return;
            }

            if (this._selectedGem.isNext(gem)) {
                this._selectedGem.deselect(); // set scale
                this._pickedGem = gem;
                this.swapGems();
                console.log("swap!");
            } else {
                this._selectedGem.deselect();
                this._selectedGem = gem;
                gem.select();
            }

        }

        private swapGems(swapBack: boolean = true): void {
            // ugly swap
            this._canClick = false;
            this._coolAnimation = true;
            this._verticalAnimation = false;
            this._gameArray[this._selectedGem.row][this._selectedGem.col] = this._pickedGem;
            this._gameArray[this._pickedGem.row][this._pickedGem.col] = this._selectedGem;

            let rowSel: number = this._selectedGem.row;
            let colSel: number = this._selectedGem.col;
            let rowPic: number = this._pickedGem.row;
            let colPic: number = this._pickedGem.col;

            this._selectedGem.row = rowPic;
            this._selectedGem.col = colPic;
            this._pickedGem.row = rowSel;
            this._pickedGem.col = colSel;
            // save poistions
            let posPicked: Phaser.Point = new Phaser.Point((colPic + 0.5) * GameConfig.BLOCK_SIZE, (rowPic + 0.5) * GameConfig.BLOCK_SIZE,);
            let posSelected: Phaser.Point = new Phaser.Point((colSel + 0.5) * GameConfig.BLOCK_SIZE, (rowSel + 0.5) * GameConfig.BLOCK_SIZE);

            // add tweens
            let tweenSelected: Phaser.Tween = this.game.add.tween(this._selectedGem).to({
                x: posPicked.x,
                y: posPicked.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            let tweenPicked: Phaser.Tween = this.game.add.tween(this._pickedGem).to({
                x: posSelected.x,
                y: posSelected.y
            }, GameConfig.SWAP_TIME, Phaser.Easing.Linear.None, false);

            // temp information
            let type1: number = this._selectedGem.mytype;
            let type2: number = this._pickedGem.mytype;


            tweenPicked.onComplete.add(() => {
                 if (this.isMatchOnBoard(rowPic, colPic, type1, rowSel, colSel, type2)) {
                     console.log("DESTROY!!!");
                     this.handleMatches();
                     this._selectedGem = null;
                     this._pickedGem = null;
                 } else if (swapBack) {
                     this.swapGems(false);
                 }
            }, this);

            tweenPicked.start();
            tweenSelected.start();

            if (!swapBack) {
                this._selectedGem.deselect();
                this._selectedGem = null;
                this._pickedGem = null;
                this._canClick = true;
            }


        }

        private isMatchOnBoard(row1: number, col1: number, type1: number, row2: number, col2: number, typr2: number): boolean {
            return this.isMatch(row1, col1, type1) || this.isMatch(row2, col2, typr2);
        }

        private isMatch(row: number, col: number, type: number): boolean {
            return this.isHorizontal(row, col, type) || this.isVertical(row, col, type);
        }

        private isHorizontal(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;


            for (let i: number = 0; i < 3; i++) {
                streak = 0;
                start = col + i;
                end = start - 2;
                if (start >= this._gameArray[0].length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let j: number = start; j >= end; j--) {
                    if (this._gameArray[row][j].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }

            return false;
        }

        private isVertical(row: number, col: number, type: number): boolean {
            let start: number;
            let end: number;
            let streak: number = 0;

            for (let j = 0; j < 3; j++) {
                streak = 0;
                start = row + j;
                end = start - 2;
                if (start >= this._gameArray.length) {
                    continue;
                }
                if (end < 0) {
                    continue;
                }
                for (let i: number = start; i >= end; i--) {
                    if (this._gameArray[i][col].mytype == type) {
                        streak++;
                    }
                }
                if (streak > 2) {
                    return true;
                }
            }


            return false;
        }

        private debugShow(): void {
            for (let i = 0; i < this._gameArray.length; i++) {
                let str = "";
                for (let j = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j] !== -1) {
                        str += this._gameArray[i][j].mytype + " ";
                    } else {
                        str += " -1 "
                    }

                }
                console.log(str);
            }
        }

        private handleMatches(): void {
            this._removeMap = [];

            for (let i: number = 0; i < this._gameArray.length; i++) {
                this._removeMap[i] = [];
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    this._removeMap[i].push(0);
                }
            }

            this.handleHorizontalMatches();
            this.handleVerticalMatches();
            this.destroyGems();
            this._coolAnimation = false;
        }

        private destroyGems(): void {
            if (this._destroyed > 0) {
                debugger;
            }
            this._destroyed = 0;
            let condensationPoint: Phaser.Point;
            let condensationType: number;
            if (this._selectedGem != null) {
                if (this._removeMap[this._selectedGem.row][this._selectedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._selectedGem.x, this._selectedGem.y);
                    condensationType = this._selectedGem.mytype;
                } else if (this._removeMap[this._pickedGem.row][this._pickedGem.col] > 0) {
                    condensationPoint = new Phaser.Point(this._pickedGem.x, this._pickedGem.y);
                    condensationType = this._pickedGem.mytype;
                } else {
                    condensationPoint = new Phaser.Point(1, 1);
                    condensationType = 0;
                    console.log("something went wrong");
                }
            }
            this._coolAnimation = this._coolAnimation && (this._counters[condensationType] > 0) && (this._selectedGem != null);
            if (this._coolAnimation) {
                console.log("create cool animation");
                // debugger;

                this.condensate(condensationPoint);
            } else {
                let gemsToDestroy: Gem[] = [];
                let tweens: Phaser.Tween[] = [];
                let delay: number = 0;
                for (let i = 0; i < this._removeMap.length; i++) {
                    for (let j = 0; j < this._removeMap[i].length; j++) {
                        if (this._removeMap[i][j] > 0) {
                            this._destroyed++;
                            let gem: Gem = this._gameArray[i][j];

                            if ((gem.mytype == 1 || gem.mytype == 2) && this._counters[gem.mytype] > 0) {
                                let point: Phaser.Point;
                                if (gem.mytype == 1) {
                                    point = GameConfig.APPLE_POS;
                                } else {
                                    point = GameConfig.GRAPES_POS;
                                }
                                // debugger;

                                // remove sprite from current layer
                                this._container.removeChild(gem);
                                this.addChild(gem);
                                let flyTween: Phaser.Tween = this.createFlyTween(gem, point, delay);
                                delay += 100;
                                tweens.push(flyTween);
                            } else {
                                gem.onDestroy = () => {
                                    this._destroyed--;
                                    this._counters[gem.mytype] -= 1;
                                    this._cb(gem.mytype, 1);
                                    if (this._destroyed == 0) {

                                        this.makeGemsFall();
                                        // this.makeGemsFall();
                                    }
                                };
                                gemsToDestroy.push(gem);
                            }

                        }
                    }
                }
                gemsToDestroy.forEach(x => x.playDestroy());
                tweens.forEach(x => x.start());
                // if (gemsToDestroy.length == 0) {
                //     this._canClick = true;
                // }
            }
        }

        private handleHorizontalMatches(): void {
             for (let i: number = 0; i < this._gameArray.length; i++) {
                  let colorStreak: number = 1;
                  let currentColor: number = -100500;
                  let startStreak: number = 0;
                  for (let j: number = 0; j < this._gameArray[i].length; j++) {
                       if (this._gameArray[i][j].mytype == currentColor && this._gameArray[i][j].mytype > 0) {
                            colorStreak++;
                       }
                       if (this._gameArray[i][j].mytype != currentColor || j == this._gameArray[i].length - 1) {
                            if (colorStreak >= 3) {
                                console.log("HORIZONTAL :: Length = "+colorStreak + " :: Start = ("+i+","+startStreak+") :: Color = "+currentColor);
                                if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                    // debugger;
                                    // this._cb(currentColor, colorStreak);
                                    this._coolAnimation = false;

                                }

                                for(let k: number = 0; k < colorStreak; k++) {
                                    this._removeMap[i][startStreak + k]++;
                                   // this._gameArray[i][j].sprite.tint = 0xff0000;
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[i][j].mytype;
                       }
                  }
             }
        }

         private handleVerticalMatches(): void {
            for (let i: number = 0; i < this._gameArray[0].length; i++) {
                    let colorStreak: number = 1;
                    let currentColor: number = -100500;
                    let startStreak: number = 0;
                    for (let j: number = 0; j < this._gameArray.length; j++) {
                        try {
                        if (this._gameArray[j][i].mytype == currentColor && this._gameArray[j][i].mytype > 0) {
                            colorStreak++;
                        }
                        } catch(e) {
                            debugger;
                        }
                        if (this._gameArray[j][i].mytype != currentColor || j == this._gameArray.length - 1) {
                            if (colorStreak >= 3) {
                                // this._coolAnimation = false;
                                console.log("VERTICAL :: Length = "+colorStreak + " :: Start = ("+startStreak+","+i+") :: Color = "+currentColor);
                                 // console.log("HORIZONTAL :: Length = "+colorStreak + " :: Start = ("+i+","+startStreak+") :: Color = "+currentColor);
                                // this._cb(currentColor, colorStreak);
                                if (this._counters[currentColor] <= 0 || !this._coolAnimation) {
                                    // this._cb(currentColor, colorStreak);
                                    // this._counters[currentColor] -= colorStreak;
                                    this._coolAnimation = false;
                                } else {
                                    this._verticalAnimation = true;
                                }
                                for(let k: number = 0; k < colorStreak; k++) {
                                    this._removeMap[startStreak + k][i]++;
                                }
                            }
                            startStreak = j;
                            colorStreak = 1;
                            currentColor = this._gameArray[j][i].mytype;
                       }
                  }
             }
        }

        private checkGlobalMatch(): boolean {
            for (let i: number = 0; i < this._gameArray.length; i++) {
                for (let j: number = 0; j < this._gameArray[i].length; j++) {
                    if (this._gameArray[i][j].mytype <= 0) {
                        continue;
                    }
                    if (this.isMatch(i, j, this._gameArray[i][j].mytype)){
                        return true;
                    }
                }
            }
            return false;
        }

        private swap(x1: number, y1: number, x2: number, y2: number, dx:number = 0): void {
            let temp: Block = this._gameArray[x1][y1];
            temp.row = x2 - dx;
            temp.col = y2;

            this._gameArray[x1][y1] = this._gameArray[x2][y2];

            this._gameArray[x1][y1].row = x1 - dx;
            this._gameArray[x1][y1].col = y1;

            this._gameArray[x2][y2] = temp;
        }

        private makeGemsFall(): void {
            if (this._destroyed > 0) {
                this.game.time.events.add(200, this.makeGemsFall, this);
            }
            if (this._turnOffMask) {
                this._turnOffMask = false;
                this._container.mask = this._myMask;
            }
            let holes: Hole[] = this.findHoles();
            let tweens: Phaser.Tween[] = [];
            let fallen: number = 0;

            // holes.forEach((hole: Hole) => {
            for (let k: number = 0; k < holes.length; k++) {
                let hole: Hole = holes[k];
                let above: Hole;
                let start: number;
                let end: number;
                if (hole.hasHoleAbove) {
                    above = holes[k + 1];
                    start = above.row + above.length - hole.length;
                    end = hole.row - start;
                } else {
                    start = 0;
                    end = hole.row - this._fieldUpperBound[hole.col];
                }
                for (let i: number = 0; i < end; i++) {
                    this.swap(hole.row - 1 - i, hole.col, hole.row + hole.length - 1 - i, hole.col);
                }
                if (!hole.hasHoleAbove) {
                    for (let i = this._fieldUpperBound[hole.col]; i < this._fieldUpperBound[hole.col] + hole.length; i++) {
                        let gem: Gem = this._gameArray[i][hole.col];
                        let rnd: number = this.game.rnd.between(1, 5);
                        gem.mytype = rnd;

                        gem.x = (hole.col + 0.5) * GameConfig.BLOCK_SIZE;
                        gem.y = (gem.row - hole.length + 0.5) * GameConfig.BLOCK_SIZE;
                    }
                    start = this._fieldUpperBound[hole.col];
                    end = hole.row + hole.length;
                } else {
                    let len: number = hole.row - (above.row + above.length - hole.length);
                    start = hole.row + hole.length - len;
                    end = hole.row + hole.length;
                }

                // add tweens
                for (let i: number = start; i < end; i++) {
                    try {
                        this._gameArray[i][hole.col];
                    } catch (e) {
                        debugger;
                    }
                    let gem: Gem = this._gameArray[i][hole.col];
                    fallen++;
                    let tween: Phaser.Tween = this.game.add.tween(gem).to({
                        y: gem.y + hole.length * GameConfig.BLOCK_SIZE
                    }, GameConfig.FALLDOWN_TIME, Phaser.Easing.Back.Out, false);
                    tween.onComplete.add(() => {
                        // block.row += hole.length;
                        this._gameArray[gem.row][gem.col] = gem;
                        fallen--;
                        if (fallen == 0) {
                            if(this.checkGlobalMatch()){
                                // this.game.time.events.add(250, this.handleMatches, this);
                                this.handleMatches();
                            } else {
                                this._canClick = true;
                                this._selectedGem = null;

                                for (let i = 0; i < this._gameArray.length; i++) {
                                    let str = "";
                                    for (let j = 0; j < this._gameArray[i].length; j++) {
                                        str += this._gameArray[i][j].mytype + "\t";
                                    }
                                    console.log(str);
                                }
                            }
                        }
                    });
                    tweens.push(tween);
               }
            }

            tweens.forEach(x => x.start());
        }

        private createFlyTween(gem: Gem, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {
            let scaleTween: Phaser.Tween = this.game.add.tween(gem.scale).to({
                x: GameConfig.PANEL_GEM_SCALE,
                y: GameConfig.PANEL_GEM_SCALE
            }, GameConfig.FLY_TIME, Phaser.Easing.Sinusoidal.InOut, false);

            scaleTween.onComplete.addOnce(() => {
                gem.scale.setTo(1);
            }, this);

            let flyTween: Phaser.Tween = this.game.add.tween(gem).to({
                x: flyawayPoint.x,
                y: flyawayPoint.y
            }, GameConfig.FLY_TIME, Phaser.Easing.Linear.None, false, delay);

            flyTween.onStart.addOnce(() => scaleTween.start());

            flyTween.onComplete.add((obj: Phaser.Sprite, tween: Phaser.Tween) => {
                this.removeChild(gem);
                this._container.addChild(gem);
                this._destroyed--;
                this._cb(gem.mytype, 1);
                this._counters[gem.mytype]--;
                if (this._destroyed == 0) {
                    this.makeGemsFall();
                }
            }, this);

            return flyTween;
        }

        private generateFlyAwayTween(block: Gem, condensationPoint: Phaser.Point, flyawayPoint: Phaser.Point, delay: number): Phaser.Tween {
            this._container.removeChild(block);
            this.addChild(block);

            let flyAwayTween: Phaser.Tween = this.createFlyTween(block, flyawayPoint, delay);

            if (this._verticalAnimation) {
                return flyAwayTween;
            } else {
                let condensateTween: Phaser.Tween = this.game.add.tween(block).to({
                    x: condensationPoint.x
                }, GameConfig.CONDENSATION_TIME, Phaser.Easing.Linear.None, false);
                            // condenstationStack.push(condensateTween);
                condensateTween.onComplete.add((sprite: Phaser.Sprite, tween: Phaser.Tween) => {
                    flyAwayTween.start();
                });
            return condensateTween;
            }
        }

         private condensate(point: Phaser.Point): void {
            let condenstationStack: Phaser.Tween[] = [];
            let delay: number = 0;
            for (let i: number = 0; i < this._removeMap.length; i++) {
                for (let j: number = 0; j < this._removeMap[i].length; j++) {
                    if (this._removeMap[i][j] > 0) {
                        this._destroyed++;

                        let block: Gem = this._gameArray[i][j];
                        let flyAwayPoint: Phaser.Point;
                        if (block.mytype == 2) {
                            flyAwayPoint = GameConfig.GRAPES_POS;
                        } else {
                            flyAwayPoint = GameConfig.APPLE_POS;
                        }
                        condenstationStack.push(this.generateFlyAwayTween(block, point, flyAwayPoint, delay));
                        delay += 100;

                    }
                }
            }
            condenstationStack.forEach((el: Phaser.Tween) => {
                el.start();
            });
        }

        private findHoles(): Hole[] {
            let result: Hole[] = [];

            for (let j: number = 0; j < this._gameArray[0].length; j++) {
                for (let i: number = this._gameArray.length - 1; i >= 0; i--) {
                    if (this._removeMap[i][j] > 0) {
                        // let's find length of the hole
                        let length: number = 1;
                        let row: number = i;
                        let found: boolean = true;
                        let wasPushed: boolean = false;
                        for (let k = i - 1; k >= 0; k--) {
                            if (this._removeMap[k][j] >0) {
                                length++;
                                row = k;
                                found = true;
                                wasPushed = false;
                            } else if (found) {
                                result.push({row: row, col: j, length: length, hasHoleAbove: false});
                                found = false;
                                wasPushed = true;
                                let id: number = result.length - 2;
                                if (id >= 0 && result[id].col == result[id + 1].col) {
                                    result[id].hasHoleAbove = true;
                                }
                                // debugger;
                            }
                        }

                        if (!wasPushed) {
                            result.push({row: row, col: j, length: length, hasHoleAbove: false});
                            let id: number = result.length - 2;
                            if (id >= 0 && result[id].col == result[id + 1].col) {
                                result[id].hasHoleAbove = true;
                            }
                        }

                        break;
                    }
                }
            }

            return result;
        }


    }
}