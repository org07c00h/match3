module TProject {
    export class GameConfig {
        public static readonly BLOCK_SIZE: number = 45;
        public static readonly SWAP_TIME: number = 150;
        public static readonly DESTROY_TIME: number = 150;
        public static readonly FALLDOWN_TIME: number = 400;
        public static readonly FLY_TIME: number = 300;
        public static readonly CONDENSATION_TIME: number = 300;
        public static readonly COUNTERS: number[] = [0, 6, 3, 0, 0, 0];
        public static readonly NAMES: string[] = ["apple", "apple", "grapes", "flower", "pear", "leaf"];
        public static readonly PANEL_GEM_SCALE: number = 0.8;
        public static readonly APPLE_POS: Phaser.Point = new Phaser.Point(45, -25 + 2);
        public static readonly GRAPES_POS: Phaser.Point = new Phaser.Point(250 - 2, -25);
        public static readonly UNLOCK_OBJ_PER_FRUITS: number = 3;
    };
}