module TProject {
    export class Gem extends Phaser.Sprite {
        private _mytype: number;
        private _sprite: Phaser.Sprite;
        private _spriteName: string;
        private _destroyCallback: Function;
        private _destroyAnimation: Phaser.Animation[];
        private _row: number;
        private _col: number;
        private _onCompleteFired: boolean;
        constructor(game: Phaser.Game, x: number, y: number, type: number) {
            super(game, x, y);
            this._mytype = type;
            this._spriteName = GameConfig.NAMES[this._mytype];
            if (type > 0) {
                this._sprite = this.game.add.sprite(0, 0, "fruits", this._spriteName + "0");
                this._sprite.anchor.set(0.5);
                this.addChild(this._sprite);
                this._destroyAnimation = [null];
                for (let i: number = 1; i < GameConfig.NAMES.length; i++) {
                    this._destroyAnimation.push(this._sprite.animations.add("set off" + i, Phaser.Animation.generateFrameNames(GameConfig.NAMES[i], 0, 9), 24, false));
                    this._destroyAnimation[i].onComplete.add(() => {
                        if (!this._onCompleteFired) {
                            this._onCompleteFired = true;
                            this._sprite.alpha = 0;

                            if (this._destroyCallback) {
                                this._destroyCallback();
                            }
                        }
                    }, this);
                }
            }
        }

        public set row(val: number) {
            this._row = val;
        }

        public set col(val: number) {
            this._col = val;
        }

        public get col(): number {
            return this._col;
        }

        public get row(): number {
            return this._row;
        }

        public get mytype(): number {
            return this._mytype;
        }

        public set onDestroy(val: Function) {
            this._destroyCallback = val;
        }

        public set mytype(val: number) {

            if (!this._onCompleteFired) {
                // debugger;
                this._destroyAnimation[this._mytype].stop();
                // this._destroyCallback();
                this._onCompleteFired = true;
                console.log("TRIGGERED!!!");

            }
            this._destroyAnimation[this._mytype].stop();
            this._mytype = val;
            this._spriteName = GameConfig.NAMES[this._mytype];
            this._sprite.loadTexture("fruits", this._spriteName + "0");
            this._sprite.alpha = 1;
            this._onCompleteFired = false;
        }

        public playDestroy(): void {
            this._destroyAnimation[this._mytype].play(24);
        }

        public select(): void {
            this._sprite.scale.set(1.2);
        }

        public deselect(): void {
            this._sprite.scale.set(1);
        }

        public isSame(a: Gem): boolean {
            return (this._row == a.row) && (this._col == a.col);
        }

        public isNext(gem2: Gem): boolean {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        }

        public update(): void {
            console.log("it works");
        }
    }
}