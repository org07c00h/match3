module TProject {


    export class Body extends OState {

		private _field: Field;
        private _panel: Panel;

        private _container: OSprite;
        private _bg: OSprite;
        private _bgSprite: string;
        private _initLandscape: boolean;
        private _objectToUnlock: Phaser.Sprite[];
        private _objectNames: string[];
        private _nextObjectToUnlock: number;

        constructor() {
            super(true); // true - и будут отслеживаться повороты
            this._objectNames = ["bg_bench_", "bg_flow_", "bg_fontan_"];
            this._nextObjectToUnlock = 0;

        }

        create(): void {
            _.log("Start game!");
            this.game.time.advancedTiming = true;
            this._objectToUnlock = [];

            // this._bg.anchor.set(0.5);
            this.initBG(this._bgSprite);
            this.game.world.addChild(this._bg);


            if (this.game.device.iPad) {
                this._container = new OSprite(Core.centerX - 70, Core.centerY - 210)
                    .myScale(1.05)
                    .end();
                this._container.otherXY(70, 240).end();
                this._container.otherBottomOffset(30).end();
            } else {
                this._container = new OSprite(Core.centerX - 50, Core.centerY - 220);
                this._container.otherXY(7, 0).end();
                this._container.myScale(1.2).otherScale(1.3).end();
                this._container.otherBottomOffset(30).end();
            }
            if (this._initLandscape) {
                this.setLandscapePivot();
            } else {
                this.setPortretPivot();
            }
            this.game.world.addChild(this._container);

            this._panel = new Panel(this.game, 0, 0);
            this._panel.unlockCallback = this.unlockObject.bind(this);

            this._container.addChild(this._panel);

            this._field = new Field(this.game, 0, 50);
            this._container.addChild(this._field);

            this._field.destroyCallback = (type: number, delta: number) => {
                this._panel.updateCounter(type, delta);
            };

            this._panel.initFruits();
        }

        private setLandscapePivot(): void {
            this._container.pivot.set(0.0, 0);
            this._objectToUnlock.forEach(x => x.position.set(-Core.centerX / 2 - 80, -Core.centerY / 2 - 40));
        }

        private setPortretPivot(): void {
            this._container.pivot.set(0, 700 * 0.5);
            // this._bg.pivot.set(0, -10);
            this._objectToUnlock.forEach(x => x.position.set(-Core.centerX / 2, -Core.centerY));
        }

        private initBG(spriteName: string): void {
            this._bg = new OSprite(Core.centerX, Core.centerY , spriteName).enabledBgMode().end();
            let type: string = spriteName.split("_")[2];
            if (this.game.device.iPad) {

                this._bg
                    .otherXY(Core.centerX - 70, Core.centerY + 80)
                    .myScale(1.5)
                    .otherScale(1.2)
                    .end();
                // this._bg.myScale(1.5).end();
            } else {
                // this._bg = new OSprite(Core.centerX, Core.centerY, spriteName).enabledBgMode().end();
                this._bg.otherXY(Core.centerX - 80, Core.centerY + 80).end();
                this._bg.myScale(1.35).otherScale(1.1).end();
            }
            this._objectNames.forEach(x => this.addObject(x, type));


        }

        private addObject(name: string, type: string): void {
            let sprite: Phaser.Sprite = this.game.add.sprite(-Core.centerX / 2 - 80, -Core.centerY / 2 - 40, name+ type);
            sprite.alpha = 0;
            this._bg.addChild(sprite);
            this._objectToUnlock.push(sprite);
        }

        public unlockObject(): void {
            console.log("FIRE");
            if (this._nextObjectToUnlock < this._objectToUnlock.length) {
                let obj: Phaser.Sprite = this._objectToUnlock[this._nextObjectToUnlock];
                this.game.add.tween(obj).to({
                    alpha: 1
                }, 300, Phaser.Easing.Sinusoidal.InOut, true);
                obj.y -= 100;
                this.game.add.tween(obj).to({
                    y: obj.y + 100
                }, 300, Phaser.Easing.Bounce.InOut, true);
                this._nextObjectToUnlock++;
            }
        }


        update(): void {
            this._panel.update();
        }

        public render(): void {
            this.game.debug.text(this.game.time.fps.toString(), 10, 20, "#00ff00");
            if (this.game.renderType == Phaser.CANVAS) {
                this.game.debug.text("CANVAS", 10, 40, "#00ff00");
            } else {
                this.game.debug.text("WEBGL", 10, 40, "#00ff00");
            }

        }

        // а это то самое отслеживание поворотов
        onPortret(): void {
            this._initLandscape = false;
            _.log("Portret");
            if (this._bg) {
                this._bg.loadTexture("bg_base_ver");
                for (let i = 0; i < this._objectToUnlock.length; i++) {
                    this._objectToUnlock[i].loadTexture(this._objectNames[i] + "ver");
                }
                this.setPortretPivot();
            } else {
                this._bgSprite = "bg_base_ver"
            }
        }

        onLandscape(): void {
            this._initLandscape = true;
            _.log("Landscape");
            if (this._bg) {
                this._bg.loadTexture("bg_base_hor");
                for (let i = 0; i < this._objectToUnlock.length; i++) {
                    this._objectToUnlock[i].loadTexture(this._objectNames[i] + "hor");
                }
                this.setLandscapePivot();
            } else {
                this._bgSprite = "bg_base_hor";
            }
        }

    }
}