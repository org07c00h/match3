module TProject {

    export class Boot extends Phaser.State {

        static readonly PATH_IMAGES: string = "./assets/images/";

        preload () {

            _.log("Loading...");

            this.game.load.atlas("fruits", Boot.PATH_IMAGES + "fruits.png", Boot.PATH_IMAGES + "fruits2.json");
            this.game.load.image("grass", Boot.PATH_IMAGES + "grass.png");
            this.game.load.image("panel", Boot.PATH_IMAGES + "panel.png");

            this.game.load.image("bg_base_hor", Boot.PATH_IMAGES + "bg_base_hor.jpg");
            this.game.load.image("bg_base_ver", Boot.PATH_IMAGES + "bg_base_ver.jpg");

            this.game.load.image("bg_bench_hor", Boot.PATH_IMAGES + "bg_bench_hor.png");
            this.game.load.image("bg_flow_hor", Boot.PATH_IMAGES + "bg_flow_hor.png");
            this.game.load.image("bg_fontan_hor", Boot.PATH_IMAGES + "bg_fontan_hor.png");

            this.game.load.image("bg_bench_ver", Boot.PATH_IMAGES + "bg_bench_ver.png");
            this.game.load.image("bg_flow_ver", Boot.PATH_IMAGES + "bg_flow_ver.png");
            this.game.load.image("bg_fontan_ver", Boot.PATH_IMAGES + "bg_fontan_ver.png");



            this.game.load.onFileComplete.add(this.loadingUpdate, this);

            // Здесь загружаем ресурсы для прелоудера или вообще, если прелоудера нет

        }

        create () {

            Core.begin(this.game, !this.game.device.desktop);

        }

        private loadingUpdate(progress: number, cacheKey: string, success: boolean, totalLoaded: number, totalFiles: number) {

            if (progress >= 100.0) {
                this.game.load.onFileComplete.removeAll();

                this.game.state.start("Body", true);
            }
        }

    }
}