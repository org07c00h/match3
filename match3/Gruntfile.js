
module.exports = function(grunt){
	grunt.initConfig({
		ts: {

			default: {
				outDir: 'js',
				src: ['ts/**/*.ts', '!node_modules/**/*.*'],
				baseDir: 'ts',
				options: {
					fast: 'never'
				}
			},

			dev: {
				outDir: 'js',
				src: ['ts/**/*.ts', '!node_modules/**/*.*'],
				baseDir: 'ts',
				options: {
					fast: 'watch'
				}
			},

			options: {
					allowJs: true,
					sourceMap: false,
					//fast: 'never',
					target: "es5",
					declaration: false,
					noLib: false,
					comments: false

				}

		},
		watch: {
			options: {
				livereload: true
			},
			scripts: {
				files: ['ts/**/*.ts', '!node_modules/**/*.*'],
				tasks: ['process']
			}
		},
		concat: {
			dist:{
				src:['js/**/*.js', '!node_modules/**/*.*'],
				dest:'dist/data/body.js'
			}
		},
		uglify:{
			dist:{
				options:{
					banner: '/* Created by Olphabet */'
				},
				files:{
					'dist/data/body.min.js':['dist/data/body.js']
				}
			}
		},
		express: {
			all: {
				options: {
					port:9339,
					hostname: 'localhost',
					bases: ['.'],
					livereload:true
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib');
	grunt.loadNpmTasks('grunt-ts');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-newer');
	grunt.loadNpmTasks('grunt-express');

	//grunt.registerTask('process', ['newer:ts', 'concat', 'uglify']);
	//grunt.registerTask('default', ['ts', 'concat', 'uglify', 'express', 'watch']);

	grunt.registerTask('process', ['ts:dev', 'concat']);
	grunt.registerTask('default', ['ts:default', 'concat', 'express', 'watch']);

	grunt.registerTask('release', ['ts:default', 'concat', 'uglify']);
}