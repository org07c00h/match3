/**
 * Created by DEaDA on 3/25/17.
 */

module mygame {
    import ScaleManager = Phaser.ScaleManager;
    import Sprite = Phaser.Sprite;
    import Game = Phaser.Game;
    import Group = Phaser.Group;
    import Tween = Phaser.Tween;
    import Sound = Phaser.Sound;
    import PhaserTextStyle = Phaser.PhaserTextStyle;
    import Point = Phaser.Point;

    let version: string = '1.0.0';
    // let bg: BackGround;
    export class PlayState extends OState {
        private _field: Field;
        private _panel: Panel;
        private _logo: Phaser.Sprite;
        private _container: OSprite;
        private _playFree: Phaser.Sprite;
        private _winTweens: Phaser.Tween[];
        private _canShowEndScreen: boolean;

        private _logoOsprite: OSprite;
        private _gamePanelOsprite: OSprite;
        private _playFreeBtnOsprite: OSprite;
        private _bushes: Phaser.Sprite[];
        private _bench: Phaser.Sprite;
        private _rake: Phaser.Sprite;

        private _backOverlay: Phaser.Graphics;
        private _panelContainer: OSprite;
        private _panelSprite: Phaser.Sprite;
        private _sunraySprite: Phaser.Sprite;
        private _congrateText: Phaser.BitmapText;
        private _plotText: Phaser.BitmapText;

        private _tutorialOver: Phaser.Sprite;
        private _hand: Phaser.Sprite;
        private _handTween: Phaser.Tween;

        private _tutorialContainer: OSprite;
        private _charTutorialSprite: Phaser.Sprite;
        private _bubbleTutorialSprite: Phaser.Sprite;
        private _bubbleTutorialText: Phaser.BitmapText;

        private _winscreenShowed: boolean;
        private _fruitFallEmitter: Phaser.Particles.Arcade.Emitter;
        private _timerInstall: number;

        constructor() {
            super(true);
            this._winTweens = [];
        }

        create() {
            GameConfig.COUNTERS_PUBLIC = [0,window["firstElement"],0, window["secondElement"], window["thirdElement"], 0, 100, 100];
            this.game.time.advancedTiming = true;
            this.game.stage.backgroundColor = "#64941A";

            //console.log("FDREFRF");

            this._winscreenShowed = false;

            this.game.physics.startSystem(Phaser.Physics.ARCADE);

            this.game.stage.smoothed = true;
            this._canShowEndScreen = false;

            Controller.Instance.width = getSize().width;
            Controller.Instance.height = getSize().height;


            this._container = new OSprite(Core.centerX + 200, 0)
                .otherXY(Core.centerY, 0)
                .end();
            this.game.world.addChild(this._container);

            this._bushes = [];
            this._bushes[0] = this.game.add.sprite(Core.width, Core.height, "atlas", "Back-03");
            this._bushes[0].anchor.set(1, 1);

            this._bushes[1] = this.game.add.sprite(this._bushes[0].width, this._bushes[0].height, "atlas", "Back-03");
            this._bushes[1].scale.set(-1, -1);

            this._bushes[3] = this.game.add.sprite(Core.width - this._bushes[0].width, this._bushes[0].height, "atlas", "Back-03");
            this._bushes[3].scale.set(1, -1);
            //this._bushes[3].alpha = 0;

            this._bushes[2] = this.game.add.sprite(this._bushes[0].width, Core.height - this._bushes[0].height, "atlas", "Back-03");
            this._bushes[2].scale.set(-1, 1);

            this._bench = this.game.add.sprite(Core.width, 0, "atlas", "Back-04");
            this._bench.anchor.set(1, 0);
            this.game.world.addChild(this._bench);

            this._rake = this.game.add.sprite(Core.width, Core.height, "atlas", "Back-05");
            this._rake.anchor.set(1, 1);
            this.game.world.addChild(this._rake);


            this._bushes.forEach(x => this.game.world.addChild(x));

            // init button
            this._playFreeBtnOsprite = new OSprite(Core.centerX + 250, 0).myBottomOffset(70).myScale(0.65).end();
            this.game.world.addChild(this._playFreeBtnOsprite);

            this._playFreeBtnOsprite.otherXY(Core.centerY, 0).otherBottomOffset(73).otherScale(0.65).end();

            this._playFree = this.game.add.sprite(0, 0, "play_free");
            this._playFree.scale.set(1.3);
            this._playFree.anchor.set(0.5, 0.5);

            this._playFree.inputEnabled = true;
            this._playFree.events.onInputDown.add(() => {
                window["trackClick"]();
            }, this);


            this._playFreeBtnOsprite.addChild(this._playFree);
            if (!window["collectable"]) {
                this._playFree.alpha = 0;
                this._playFree.y = 300;
            }

            // init panel
            if (window["collectable"]) {
                this._gamePanelOsprite = new OSprite(0, 0)
                    .myTopOffset(640)
                    .otherXY(Core.centerY, 0)
                    .otherTopOffset(230 + 400 - 20)
                    .myLeftOffset(300)
                    .end();

                this._panel = new Panel(this.game, 0, 0);

                if (this.game.device.iPad) {
                    this._gamePanelOsprite.myTopOffset(750).end();
                }

                this._gamePanelOsprite.addChild(this._panel);

                this._panel.initFruits();
            }

            GameConfig.gameArray = GameConfig.deepClone(GameConfig.gameArrayLandscape);

            this._logoOsprite = new OSprite(900, 0)
                .myTopOffset(10)
                .myScale(0.65)
                .otherXY(Core.centerY, 0)
                .otherTopOffset(10)
                .otherScale(0.6)
                .end();

            this._logo = this.game.add.sprite(0, 0, "atlas", "Logo-01");
            this._logo.anchor.set(0.5, 0);
            this._logoOsprite.addChild(this._logo);

            if (window["collectable"]) {

                this._container = new OSprite(Core.centerX + 400 - 85, Core.centerY - 70)
                    .otherXY(Core.centerY +50 - 2, Core.centerX + 220)
                    .end();

                this._field = new Field(this.game, 0, 0, this);

                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    this._panel.updateCounter(type, delta);
                };
                this._field.finalCallback = this.showWinScreen.bind(this);

            } else {

                this._container = new OSprite(Core.centerX, Core.centerY )
                    .otherXY(Core.centerY, Core.centerX - 80)
                    .end();

                this._field = new Field(this.game, 0, 0, this);
                this._container.addChild(this._field);
                this._field.destroyCallback = (type: number, delta: number) => {
                    type + 1;
                };


                this._container.myScale(1).otherScale(1.15).end();
            }
            this._field.firstMatchCallback = () => {
                this.game.add.tween(this._playFree.scale).to({x: 1.45, y: 1.45}, window["buttonPlaySpeed"], Phaser.Easing.Sinusoidal.Out, true, 0, -1, true);
            }

            this._field.pivot.set(100 * GameConfig.gameArray[0].length / 2, 100 * GameConfig.gameArray[1].length / 5);

            if(window["particleOn"]) {
                this._fruitFallEmitter = this.game.add.emitter(0, -100, Math.ceil(4600 / window["fruitSpawRate"]));
                this._fruitFallEmitter.makeParticles("atlas", ["F_1", "F_2", "F_3", "F_4", "F_5"]);
                //this._fruitFallEmitter.setYSpeed(200 * window["fruitSpawSpeed"], 300 * window["fruitSpawSpeed"]);
                //this._fruitFallEmitter.gravity.set(0, 0);
                this._fruitFallEmitter.minParticleSpeed.setTo(0, 200 * window["fruitSpawSpeed"]);
                this._fruitFallEmitter.maxParticleSpeed.setTo(0, 300 * window["fruitSpawSpeed"]);
            }

            if (!Core.isLandscape) {
                this.onPortret();
            }

            if(window["tutorial"]) {

                this._tutorialOver = this.game.make.sprite(-630, -160, "atlas", "bannerMask");
                this._tutorialOver.scale.set(10);
                this._tutorialOver.alpha = 0.8;
                this._field.addChild(this._tutorialOver);

                let tutorialUP: Phaser.Graphics = this.game.add.graphics(0, -128);
                this._tutorialOver.addChild(tutorialUP);
                tutorialUP.beginFill(0x000000, 1);
                tutorialUP.drawRect(0, 0, 256, 128);
                tutorialUP.endFill();

                let tutorialDOWN: Phaser.Graphics = this.game.add.graphics(0, 72);
                this._tutorialOver.addChild(tutorialDOWN);
                tutorialDOWN.beginFill(0x000000, 1);
                tutorialDOWN.drawRect(0, 0, 128, 128);
                tutorialDOWN.endFill();

                let tutorialLEFT: Phaser.Graphics = this.game.add.graphics(-128, 0);
                this._tutorialOver.addChild(tutorialLEFT);
                tutorialLEFT.beginFill(0x000000, 1);
                tutorialLEFT.drawRect(0, 0, 128, 128);
                tutorialLEFT.endFill();

                let tutorialRIGHT: Phaser.Graphics = this.game.add.graphics(128, 0);
                this._tutorialOver.addChild(tutorialRIGHT);
                tutorialRIGHT.beginFill(0x000000, 1);
                tutorialRIGHT.drawRect(0, 0, 128, 128);
                tutorialRIGHT.endFill();

                this._hand = this.game.add.sprite(100 * 4 - 20 - GameConfig.BLOCK_SIZE * 2, 100 * 2 - 60, "atlas", "Help-Arrow-01");

                this._hand.anchor.set(0.5);
                this._handTween = this.game.add.tween(this._hand).to({
                    y: 80
                }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);
                this._field.addChild(this._hand);

                this._tutorialContainer = new OSprite(Core.centerX, Core.centerY)
                    .myBottomOffset(0)
                    .myLeftOffset(35)
                    .otherXY(Core.centerY, Core.centerX)
                    .otherLeftOffset(10)
                    .otherBottomOffset(0)
                    .otherScale(0.8)
                    .end();

                this.game.world.addChild(this._tutorialContainer);

                this._charTutorialSprite = this.game.make.sprite(0, 0, "character");
                this._charTutorialSprite.anchor.set(0, 1);
                this._tutorialContainer.addChild(this._charTutorialSprite);

                this._bubbleTutorialSprite = this.game.make.sprite(window["bubbleX"], window["bubbleY"], "atlas", "tutor_bubble");
                this._bubbleTutorialSprite.anchor.set(0, 0.5);
                this._charTutorialSprite.addChild(this._bubbleTutorialSprite);

                this._bubbleTutorialText = this.game.add.bitmapText(this._bubbleTutorialSprite.width/2 + 10, 0, "font2", Lang.Instance.TUTORIAL[window["lang"]], 32);
                this._bubbleTutorialText.anchor.setTo(0.5, 0.5);
                this._bubbleTutorialText.align = "center";
                this._bubbleTutorialText.tint = 0x8a5b27;
                this._bubbleTutorialSprite.addChild(this._bubbleTutorialText);
            }

            setTimeout(()=>{this._field.endGame();}, window["endGameTimer"]);

            this._timerInstall = 0;
            window['gameStarted'];
            this.game.time.events.loop(Phaser.Timer.SECOND, this.tick, this);
        }

        private tick() {
            this._timerInstall ++;
            window['playTime'] =  this._timerInstall;
        }

        public getGurrentElementPosition(type: string): Phaser.Point {
            return this._panel.getElementPosition(type)
        }

        public update(): void {
            if (window["collectable"]) {
                this._panel.update();
            }

            if(this._field) {
                this._field.update();
            }

            if(window["particleOn"]) {
                //this._fruitFallEmitter.update();
            }
        }

        public destroyTutorial() {
            this._handTween.stop();
            this._hand.destroy();
            this._tutorialOver.destroy(true);
            this._tutorialContainer.destroy(true);
        }


        private repositionEmitter(): void {
            let dx: number;
            if (Core.isLandscape) {
                dx = Core.centerX - 20;
            } else {
                dx = Core.centerY - 20;
            }
            this._fruitFallEmitter.emitX = this.game.rnd.between(-dx, dx);

            this.game.time.events.add(window["fruitSpawRate"], this.repositionEmitter.bind(this));
        }

        public showWinScreen(): void {

            if(!this._winscreenShowed){
                this._winscreenShowed = true;

                this._playFree.visible = false;
                this._playFree.inputEnabled = false;

                this._playFreeBtnOsprite.removeChild(this._playFree);

                this._panelContainer = new OSprite(Core.centerX, Core.centerY)
                    .otherXY(Core.centerY, Core.centerX)
                    .end();
                this.game.stage.addChild(this._panelContainer);

                this._backOverlay = this.game.add.graphics(0, 0);
                this._panelContainer.addChild(this._backOverlay);
                this._backOverlay.beginFill(0x251d1a, 0.8);
                this._backOverlay.drawRect(-1024, -1024, 2048, 2048);
                this._backOverlay.endFill();

                if(window["particleOn"]) {
                    this._panelContainer.addChild(this._fruitFallEmitter);
                    if (Core.isLandscape) {
                        this._fruitFallEmitter.emitY = -Core.centerY - 100;
                    } else {
                        this._fruitFallEmitter.emitY = -Core.centerX - 100;
                    }
                    this._fruitFallEmitter.start(false, 4600, window["fruitSpawRate"]);
                    this.repositionEmitter();
                }


                this._panelSprite = this.game.make.sprite(-7, 0, "atlas", "Popup-Back-01");
                this._panelSprite.anchor.set(0.5);
                this._panelContainer.addChild(this._panelSprite);



                this._sunraySprite = this.game.make.sprite(190, 0, "atlas", "Popup-Back-02");
                this._sunraySprite.anchor.set(0.5);
                this._panelSprite.addChild(this._sunraySprite);
                this.game.add.tween(this._sunraySprite).to({angle: 360}, 18500, Phaser.Easing.Linear.None, true, 0, -1)

                this._panelSprite.addChild(this._playFree);
                this._playFree.position.set(0, 177);
                this._playFree.anchor.x = 0.5;
                this._playFree.scale.set(0.85, 0.85);
                this._playFree.visible = true;
                this._playFree.inputEnabled = true;

                this._congrateText = this.game.add.bitmapText(10, -195, "font2", Lang.Instance.CONGRAT[window["lang"]], 42);
                this._congrateText.anchor.setTo(0.5, 0.5);
                this._congrateText.align = "center";
                this._congrateText.tint = 0xfdf1eb;
                this._panelSprite.addChild(this._congrateText);

                this._plotText = this.game.add.bitmapText(190, 0, "font2", Lang.Instance.PLOT[window["lang"]], 32);
                this._plotText.anchor.setTo(0.5, 0.5);
                this._plotText.align = "center";
                this._plotText.tint = 0xb87b52;
                this._panelSprite.addChild(this._plotText);

                this.game.add.tween(this._playFree.scale).to({x: 0.9, y: 0.9}, window["buttonPlaySpeed"], Phaser.Easing.Sinusoidal.Out, true, 0, -1, true);

                this._panelContainer.alpha = 0;
                this._panelSprite.scale.set(0.1, 0.1);

                this.game.add.tween(this._panelSprite.scale).to({x: 1, y: 1}, 700, Phaser.Easing.Back.Out, true);
                this.game.add.tween(this._panelContainer).to({alpha: 1}, 700, Phaser.Easing.Sinusoidal.Out, true);
            }
        }

        public onLandscape(): void {
            if (this._rake) {
                this._rake.position.set(Core.width - 50, Core.height);
                this._rake.angle = 0;
                this.game.world.setChildIndex(this._rake, 0);
                this._bench.position.set(Core.width, 0);
                this._bench.scale.set(1, 1);

                this._bushes[2].position.set(this._bushes[0].width, Core.height - this._bushes[0].height);
                this._bushes[1].position.set(this._bushes[0].width, this._bushes[0].height);
                this._bushes[0].position.set(Core.width, Core.height);
                this._bushes[3].alpha = 0;
                this._bushes[0].alpha = 1;
            }

            if (this._panel) {
                this._panel.setLandscape();

                if(window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -Core.centerY - 100;
                }
            }
        }

        public onPortret(): void {
            if (this._rake) {
                this._rake.position.set(0, 1200);
                this.game.world.setChildIndex(this._rake, this.game.world.children.length - 6);
                this._rake.angle = 90;
                this._bench.position.set(Core.width, Core.height);
                this._bench.scale.set(1, -1);
                this._bushes[2].position.set(this._bushes[0].width, Core.height - this._bushes[0].height);
                this._bushes[1].position.set(this._bushes[0].width, this._bushes[0].height);
                this._bushes[0].position.set(Core.width, Core.height);
                this._bushes[3].alpha = 1;
                this._bushes[0].alpha = 0;

            }

            if (this._panel) {
                this._panel.setPortrait();

                if(window["particleOn"]) {
                    this._fruitFallEmitter.emitY = -Core.centerX - 100;
                }
            }
        }

    }

    function getSize(log = false) {
        let w = 0;
        let h = 0;
        let deW = 0;
        let deH = 0;
        if (!(document.documentElement.clientWidth == 0)) {
            deW = document.documentElement.clientWidth;
            deH = document.documentElement.clientHeight;
        }

        w = deW;
        h = deH;
        if (window.innerWidth > window.innerHeight) {
            w = window.innerWidth;
            h = window.innerHeight;
        }
        return {width: w, height: h};
    }

    function sendEvent(value, params = null) {
        window["trackEvent"](value, params);
    }

    function ClickInstall() {
        window["trackClick"]();
    }
}