module mygame {
    export class Gem extends Phaser.Sprite {
        private _mytype: number;
        private _sprite: Phaser.Sprite;
        private _spriteName: string;
        private _destroyCallback: Function;
        private _destroyAnimation: Phaser.Animation;
        private _row: number;
        private _col: number;
        private _onCompleteFired: boolean;
        private _tempTweenArray: Phaser.Tween[];
        private _bombTween: Phaser.Tween;
        private _border: Phaser.Sprite;
        private _boomAnimation: Phaser.Animation;
        private _bombBoomAnimation: Phaser.Animation;

        private _boom: Phaser.Sprite;

        constructor(game: Phaser.Game, x: number, y: number, type: number) {
            super(game, x, y);
            this._tempTweenArray = []
            this._mytype = type;
            this._spriteName = GameConfig.NAMES[this._mytype];
            if (type > 0) {
                this._sprite = this.game.add.sprite(0, 0, "atlas", "F_" + this._mytype);
                this._sprite.anchor.set(0.5);
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
                this.addChild(this._sprite);

                this._boom = this.game.add.sprite(0, 0, "boom_anim", "b_1");
                this._boom.anchor.set(0.5);
                this._boom.visible = false;
                this.addChild(this._boom);
                this._destroyAnimation = this._sprite.animations.add("set off", Phaser.Animation.generateFrameNames("e_", 1, 7), 18, false);
                this._destroyAnimation.onStart.add(() => {
                    this._sprite.alpha = 1;
                }, this);

                this._boomAnimation = this._boom.animations.add("bomb", Phaser.Animation.generateFrameNames("b_", 1, 10), 18, false);

                this._boomAnimation.onComplete.add(() => {
                    this._boom.visible = false;
                    this.animationEnd();
                }, this);

                this._bombBoomAnimation = this._boom.animations.add("b_bomb", Phaser.Animation.generateFrameNames("bb_", 1, 10), 18, false);
                this._bombBoomAnimation.onComplete.add(() => {
                    this._boom.visible = false;
                    this.animationEnd();
                }, this);

                this._border = this.game.add.sprite(0, 0, "atlas", "Help-Frame-01");
                this._border.anchor.set(0.5);
                this._border.scale.set(1);
                this.addChild(this._border);
                this._border.alpha = 0;

                this._destroyAnimation.onComplete.add(() => {
                    this.animationEnd();
                }, this);
                // this._bombTween = new Array(2);

                this._bombTween = this.game.add.tween(this._sprite.scale).to({
                    x: 1.1,
                    y: 1.1
                }, 500, Phaser.Easing.Sinusoidal.InOut, false, 0, -1, true);
            }
        }

        private animationEnd(): void {
            if (!this._onCompleteFired) {
                this._onCompleteFired = true;
                this._sprite.alpha = 0;

                if (this._destroyCallback) {
                    this._onCompleteFired = true;
                    this._destroyCallback();
                }
            }
        }

        public boomAnimation(): void {
            this._boom.visible = true;
            this._sprite.alpha = 0;
            if (this._mytype >= 6) {
                this._bombBoomAnimation.play();
            } else {
                this._boomAnimation.play();
            }

        }

        public spawnBoom(streakCounter: number): void {
            if (streakCounter > 3) {
                this._border.alpha = 0;
                this._sprite.scale.set(0.1);
                if (streakCounter == 4) {
                    this._sprite.loadTexture("atlas", "rocket");
                    this._mytype = 6;
                } else {
                    this._sprite.loadTexture("atlas", "bomb");
                    this._mytype = 7;
                }
                this.game.add.tween(this._sprite.scale).to({
                    x: 0.7,
                    y: 0.7
                }, 400, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    // this.game.time.events.add(100, () => {
                        this._sprite.scale.set(0.7);
                        this._bombTween = this.game.add.tween(this._sprite.scale).to({
                            x: 1.1,
                            y: 1.1
                        }, 500, Phaser.Easing.Sinusoidal.InOut, true, 0, -1, true);

                    // }, this);

                }, this);

                this._sprite.alpha = 1;
                this._onCompleteFired = false;
                this._destroyAnimation.stop();
            }
        }

        public stopTween(): void {
            this._bombTween.pause();
            this._sprite.loadTexture("atlas", "F_2");
            this._sprite.alpha = 0;
        }

        public set row(val: number) {
            this._row = val;
        }

        public set col(val: number) {
            this._col = val;
        }

        public get col(): number {
            return this._col;
        }

        public get row(): number {
            return this._row;
        }

        public get mytype(): number {
            return this._mytype;
        }

        public set onDestroy(val: Function) {
            this._destroyCallback = val;
        }

        public get isBomb(): boolean {
            return this._mytype >= 6;
        }

        public set mytype(val: number) {
            this._border.alpha = 0;

            if (!this._onCompleteFired) {
                this._destroyAnimation.stop();
                this._onCompleteFired = true;
            }

            this._destroyAnimation.stop();
            this._mytype = val;


            if (val < 6) {
                this._bombTween.pause();
                this._sprite.position.set(0, 0);
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE, GameConfig.GEM_DEFAULT_SCALE);
                this._spriteName = GameConfig.NAMES[this._mytype];
                this._sprite.loadTexture("atlas", "F_" + this._mytype);

            } else {
                this._bombTween.start();
                this._spriteName = "rocket";
                this._sprite.loadTexture("atlas", "rocket");
                this._sprite.scale.set(2, 2);
            }
            this._sprite.alpha = 1;
            this._onCompleteFired = false;
        }

        public playDestroy(): void {
            this._border.alpha = 0;
            if (this.mytype == 6) {
                this._bombTween.pause();
            }
            let tween: Phaser.Tween = this.game.add.tween(this._sprite.scale).to({x: 0.15, y: 0.15}, 100, Phaser.Easing.Sinusoidal.Out, true);
            this._tempTweenArray.push(tween);
            tween.onComplete.addOnce(() => {
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
                this._destroyAnimation.play();
            });



        }

        public select(): void {
            if (!window["tutorial"]) {
                this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE + 0.2);
                if (this.mytype < 6) {
                    this._border.alpha = 1;
                }
            }
        }

        public deselect(): void {
            this._sprite.scale.set(GameConfig.GEM_DEFAULT_SCALE);
            this._border.alpha = 0;
        }

        public isSame(a: Gem): boolean {
            return (this._row == a.row) && (this._col == a.col);
        }

        public isNext(gem2: Gem): boolean {
            return Math.abs(this.row - gem2.row) + Math.abs(this.col - gem2.col) == 1;
        }
    }
}