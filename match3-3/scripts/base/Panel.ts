module mygame {
    export class Panel extends Phaser.Sprite {
        private _sprite: Phaser.Sprite;
        private _myCounter: number[];

        private _apple: Phaser.Sprite;
        private _grapes: Phaser.Sprite;
        private _leaf: Phaser.Sprite;

        private _appleText: Phaser.BitmapText;
        private _grapeText: Phaser.BitmapText;
        private _leafText: Phaser.BitmapText;

        private _delta: number[];
        private _timer: number;
        private _sleepTime: number;
        private _fruitsCounter: number;
        private _finishCallback: Function;
        private _finishWasFired: boolean;

        private _appleTween: Phaser.Tween;
        private _grapesTween: Phaser.Tween;
        private _leafTween: Phaser.Tween;

        private _fruitScale: number;
        private _border: Phaser.Sprite;
        private _fountain: Phaser.Sprite;
        private _myMask: Phaser.Graphics;

        private _leafs: Phaser.Sprite[];

        private _leaf1: Phaser.Sprite;
        private _benches: Phaser.Sprite[];
        private _brightBenches: Phaser.Sprite[];
        private _fontains: Phaser.Sprite[];
        private _trash: Phaser.Sprite;
        private _trash2: Phaser.Sprite;

        private _unclocksCounter: number;


        constructor(game: Phaser.Game, x: number, y: number) {
            super(game, x, y);
            this._unclocksCounter = 0;
            this._benches = new Array(2);
            this._brightBenches = new Array(2);
            this._fontains = new Array(2);

            this._leafs = [];
            this._leafs[0] = this.game.add.sprite(240, -450, "atlas", "Back-02");
            this._leafs[0].angle = 25;
            this._leafs[1] = this.game.add.sprite(240, 0, "atlas", "Back-02");
            this._leafs[1].angle = 25;
            this._leafs[2] = this.game.add.sprite(0, 100, "atlas", "Back-02");
            this._leafs[2].angle = -45;
            this._leafs.forEach(x => this.addChild(x));



            this._myMask = this.game.add.graphics(-23, -16);
            this.addChild(this._myMask);



            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-500 / 2, -600, 545, 688 - 2, 30);
            this._myMask.endFill();

            this._fountain = this.game.add.sprite(0, 110, "atlas", "Build-05");
            this._fountain.anchor.set(0.5, 1);
            this._fountain.mask = this._myMask;
            // GameConfig.enableDrag(this._fountain);
            this.addChild(this._fountain);

            let bench = this.game.add.sprite(180, -505, "atlas", "Build-02");
            bench.alpha = 0;
            bench.anchor.set(0.5);
            this._benches[0] = bench;

            this._fountain.addChild(bench);

            let bench2 = this.game.add.sprite(-200, -505, "atlas", "Build-02");
            bench2.scale.set(-1, 1);
            bench2.alpha = 0;
            bench2.anchor.set(0.5);
            this._fountain.addChild(bench2);
            this._benches[1] = bench2;

            let bench3 = this.game.add.sprite(0, 0, "atlas", "Build-02");
            bench3.anchor.set(0.5);
            bench3.blendMode = PIXI.blendModes.ADD;
            bench.addChild(bench3);

            let bench4 = this.game.add.sprite(0, 0, "atlas", "Build-02");
            bench4.anchor.set(0.5);
            bench4.blendMode = PIXI.blendModes.ADD;
            bench2.addChild(bench4);

            this._brightBenches[0] = bench3;
            this._brightBenches[1] = bench4;





            this._border = this.game.add.sprite(0, 70 - 688 / 2, "atlas", "Build-01");
            this._border.anchor.set(0.5, 0.5);
            this.addChild(this._border);

            this._fontains[0] = this.game.add.sprite(-115 + 221/2, -455 + 120 / 2, "atlas", "Build-03");
            this._fontains[0].anchor.set(0.5);
            this._fontains[0].alpha = 0;
            this._fountain.addChild(this._fontains[0]);

            this._fontains[1] = this.game.add.sprite(0, 0, "atlas", "Build-03");
            this._fontains[1].anchor.set(0.5);
            this._fontains[1].blendMode = PIXI.blendModes.ADD;
            this._fontains[1].alpha = 0;
            this._fontains[0].addChild(this._fontains[1]);
            this._fountain.addChild(this._fontains[0]);

            this._trash = this.game.add.sprite(35, -395, "atlas", "Build-04");
            this._trash.anchor.set(0.5);
            this._fountain.addChild(this._trash);

            this._trash2 = this.game.add.sprite(35, -395, "atlas", "Build-04");
            this._trash2.anchor.set(0.5);

            this._trash2.blendMode = PIXI.blendModes.ADD;
            this._trash2.alpha = 0;

            this._fountain.addChild(this._trash2);

            this._sprite = this.game.add.sprite(0, 0, "atlas", "Quest-Board-01");
            this._sprite.anchor.set(0.5);
            this.addChild(this._sprite);
            this._fruitsCounter = 0;
            this._finishWasFired = false;
            this._fruitScale = 0.8;

            this._leaf1 = this.game.add.sprite(-300, -200, "atlas", "Back-01")

            this._border.addChild(this._leaf1);


            this._myCounter = [0,window["firstElement"], 0, window["secondElement"], window["thirdElement"], 0];

            GameConfig.COUNTERS_PUBLIC = this._myCounter.slice();


            this._appleText = this.game.add.bitmapText(-140 - 30, 10, "font", "0/" + this._myCounter[1].toString(), 32);
            this._appleText.anchor.setTo(0, 0.5);
            this.addChild(this._appleText);

            this._leafText = this.game.add.bitmapText(-5, 10, "font", "0/" + this._myCounter[3].toString(), 32);
            this._leafText.anchor.setTo(0, 0.5);
            this.addChild(this._leafText);


            this._grapeText = this.game.add.bitmapText(5 + 125 + 30, 10, "font", "0/" + this._myCounter[4].toString(), 32);
            this._grapeText.anchor.setTo(0, 0.5);
            this.addChild(this._grapeText);

            this._delta = new Array(this._myCounter.length);
            for (let i: number = 0; i < this._delta.length; i++) {
                this._delta[i] = 0;
            }

            this._timer = 0;
            this._sleepTime = 1;
        }

        public scaleFruits(val: number): void {
            this._apple.scale.set(val);
            this._fruitScale = val;
            this._grapes.scale.set(val);
        }

        public set finishScreenCallback(val: Function) {
            this._finishCallback = val;
        }

        public replay(): void {
            this._finishWasFired = false;
            this._myCounter = [0,window["firstElement"],0, window["secondElement"], 0, 0]
            this._delta = new Array(this._myCounter.length);
            for (let i: number = 0; i < this._delta.length; i++) {
                this._delta[i] = 0;
            }
            this._appleText.text = "0/" + window["firstElement"].toString();
            this._grapeText.text = "0/" + window["secondElement"].toString();
        }

        public updateCounter(type: number, delta: number): void {
            if (window["collectable"]) {
                if ((type == 1 || type == 3 || type == 4) && this._myCounter[type] > 0) {
                    this._delta[type] += delta;
                    this._sleepTime = 200;
                }
            }
        }

        public setLandscape(): void {
            this._border.angle = 0;
            this._myMask.clear();
            this._trash.scale.set(1);
            this._trash2.scale.set(1);

            this._fountain.y = 110;

            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-500 / 2, -600, 545, 688 - 2, 30);
            this._myMask.endFill();

            this._border.position.set(0, 70 - 688 / 2);

            this._leaf1.position.set(-300, -200);
            this._leaf1.angle = 0;
        }

        public setPortrait(): void {
            this._border.angle = 90;
            this._trash.scale.set(1.2);
            this._trash2.scale.set(1.2)

            this._fountain.y = 185;;

            this._border.position.set(0, 70 - 688 / 2 + 65 + 10);

            this._myMask.clear();
            this._myMask.beginFill(0xffffff);
            this._myMask.drawRoundedRect(-500 / 2 - 70 + 1, -600 + 57 + 90, 688 - 5, 545 - 2, 30);
            this._myMask.endFill();

            this._leaf1.position.set(-230, 360);
            this._leaf1.angle = -90;
        }

        public getElementPosition(type: string): Phaser.Point {
            if(type == "first") {
                return new Phaser.Point(this._apple.worldPosition.x, this._apple.worldPosition.y);
            } else if (type == "second") {
                return new Phaser.Point(this._leaf.worldPosition.x, this._leaf.worldPosition.y);
            } else {
                return new Phaser.Point(this._grapes.worldPosition.x, this._grapes.worldPosition.y);
            }
        }

        public initFruits(): void {
            this._apple = this.game.add.sprite(this.x - this._sprite.width / 2 + 50, this.y + 2, "atlas", "F_1");
            this._apple.scale.setTo(0.8);
            this._apple.anchor.setTo(0.5);

            this.parent.addChild(this._apple);

            this._grapes = this.game.add.sprite(this.x - 40 + (this._sprite.width / 2 - 90), this.y + 2, "atlas", "F_4");
            this._grapes.scale.setTo(0.8);
            this._grapes.anchor.setTo(0.5);

            this.parent.addChild(this._grapes);

            this._leaf = this.game.add.sprite(this.x - 40, this.y + 2, "atlas", "F_3");
            this._leaf.scale.set(0.8);
            this._leaf.anchor.set(0.5);
            this.parent.addChild(this._leaf);

            this._appleTween = this.game.add.tween(this._apple.scale).to({
                x: this._fruitScale + 0.2,
                y: this._fruitScale + 0.2
            }, 150, Phaser.Easing.Linear.None, false);
            this._appleTween.onComplete.add(() => {
                this._apple.scale.set(this._fruitScale);
            });

            this._leafTween = this.game.add.tween(this._leaf.scale).to({
                x: this._fruitScale + 0.2,
                y: this._fruitScale + 0.2
            }, 150, Phaser.Easing.Linear.None, false);
            this._leafTween.onComplete.add(() => {
                this._leaf.scale.set(this._fruitScale);
            });

            this._grapesTween = this.game.add.tween(this._grapes.scale).to({
                x: this._fruitScale + 0.2,
                y: this._fruitScale + 0.2
            }, 150, Phaser.Easing.Linear.None, false);
            this._grapesTween.onComplete.add(() => {
                this._grapes.scale.set(this._fruitScale);
            }, this);
        }

        private unlockObject(): void {
            switch (this._unclocksCounter) {
                case 0: {
                    this.game.add.tween(this._trash2).to({
                        alpha: 0.7
                    }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                        this.game.add.tween(this._trash).to({
                            alpha: 0
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                            this._trash.visible = false;
                        }, this);
                        this.game.add.tween(this._trash2).to({
                            alpha: 0
                        }, 750, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                            this._trash2.blendMode = PIXI.blendModes.NORMAL;
                        })
                    }, this);

                } break;

                case 1: {
                    this._benches.forEach((el: Phaser.Sprite, i: number) => {
                        el.scale.multiply(1.3, 1.3);
                        // el.y -= 500;
                        el.alpha = 1;
                        // this.game.add.tween(el).to({
                        //     y: el.y + 500
                        // }, 500, Phaser.Easing.Sinusoidal.InOut, true, 200 * i).onComplete.addOnce(() => {
                            this.game.add.tween(el.scale).to({
                                x: 1 - 2 * i,
                                y: 1
                            }, 1000, Phaser.Easing.Back.Out, true);
                            this.game.add.tween(this._brightBenches[i]).to({
                                alpha: 0
                            }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                                this._brightBenches[i].blendMode = PIXI.blendModes.NORMAL;
                            });
                        // }, this);
                    });
                } break;

                case 2: {
                    let el: Phaser.Sprite = this._fontains[0];
                    // el.y -= 500;
                    el.alpha = 1;
                    el.scale.set(1.3);

                    this._fontains[1].alpha = 0.7;

                    // this.game.add.tween(el).to({
                    //     y: el.y + 500
                    // }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                        this.game.add.tween(el.scale).to({
                            x: 1,
                            y: 1
                        }, 1000, Phaser.Easing.Back.Out, true);

                        this.game.add.tween(this._fontains[1]).to({
                            alpha: 0
                        }, 1000, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                            this._fontains[1].blendMode = PIXI.blendModes.NORMAL;
                        }, this);

                    // }, this);

                } break;
            }
            this._unclocksCounter++;
        }

        private hideText(text: Phaser.BitmapText): void {
            if (text.alpha > 0) {
                this.game.add.tween(text).to({
                    alpha: 0
                }, 500, Phaser.Easing.Sinusoidal.InOut, true).onComplete.addOnce(() => {
                    let checkMark: Phaser.Sprite = this.game.add.sprite(text.x + 30, text.y - 10, "atlas", "Quest-Board-02");
                    checkMark.alpha = 0;
                    this.game.add.tween(checkMark).to({
                        alpha: 1
                    }, 200, Phaser.Easing.Sinusoidal.InOut, true);
                    checkMark.anchor.set(0.5);
                    this.addChild(checkMark);
                });
            }
        }

        public update(): void {
            let dt: number = this.game.time.elapsedMS;

            if (this._timer > 0) {
                this._timer -= dt;
            }

            if (this._delta[1] > 0 || this._delta[3] > 0 || this._delta[4]) {

                this._timer = this._sleepTime;
                this._fruitsCounter++;

                if (this._delta[1] > 0) {
                    this._delta[1]--;
                    this._myCounter[1]--;

                    if (this._myCounter[1] <= 0) {
                        this._appleText.text = window["firstElement"].toString() +"/"+ window["firstElement"].toString();
                        this.hideText(this._appleText);

                        this.unlockObject();

                    } else {

                        this._appleTween.start();
                        this._appleText.text = (window["firstElement"] - this._myCounter[1]).toString() +"/"+ window["firstElement"].toString();

                    }
                }

                if (this._delta[3] > 0) {
                    this._delta[3]--;
                    this._myCounter[3]--;
                    if (this._myCounter[3] <= 0) {
                        this._leafText.text = window["secondElement"].toString() + "/" + window["secondElement"].toString();
                        this.unlockObject();
                        this.hideText(this._leafText);
                    } else {

                        this._leafTween.start();
                        this._leafText.text = (window["secondElement"].toString() - this._myCounter[3]).toString() + "/" + window["secondElement"].toString();
                    }
                }

                if (this._delta[4] > 0) {
                    this._delta[4]--;
                    this._myCounter[4]--;
                    if (this._myCounter[4] <= 0) {
                        this._grapeText.text = window["thirdElement"].toString() + "/" + window["thirdElement"].toString();
                        this.unlockObject();
                        this.hideText(this._grapeText);
                    } else {

                        this._grapesTween.start();
                        this._grapeText.text = (window["thirdElement"].toString() - this._myCounter[4]).toString() + "/" + window["thirdElement"].toString();
                    }
                }
            }
        }
    }
}