/**
 * Created by DEaDA on 3/25/17.
 */
module mygame {
    export class Preloader extends Phaser.State {
        preloadBar;
        preloadBarBackground;

        preload() {
            this.preloadBarBackground = this.add.image(0, 0, 'preloaderBarEmpty');
            this.preloadBar = this.add.image(0, 0, 'preloaderBar');
            this.preloadBarBackground.alignIn(this.game.world.bounds, Phaser.CENTER);
            this.preloadBar.x = Math.floor(this.preloadBarBackground.x);
            this.preloadBar.y = Math.floor(this.preloadBarBackground.y);
            this.load.setPreloadSprite(this.preloadBar);


            let baseURL = window['baseURL'];

            this.game.load.atlas("boom_anim", baseURL + "assets/boom_anim.png", baseURL + "assets/boom_anim.json");
            this.game.load.atlas("atlas", baseURL + "assets/atlas.png", baseURL + "assets/atlas.json");
            this.game.load.image("character", baseURL + "assets/" + window["characterName"] + ".png");



            if (window["lang"] == "en") {
                this.game.load.image("play_free", baseURL + "assets/Button_Eng_" + window["buttonPlayColor"] + ".png");
            } else if (window["lang"] == "de") {
                this.game.load.image("play_free", baseURL + "assets/Button_Ger_" + window["buttonPlayColor"] + ".png");
            }

            this.game.load.bitmapFont('font', baseURL + "assets/font.png", baseURL + "assets/font.fnt");
            this.game.load.bitmapFont('font2', baseURL + "assets/font2.png", baseURL + "assets/font2.fnt");
        }

        create() {
            this.game.state.start('PlayState');
        }

        ChangeSize() {
            this.game.scale.setGameSize(window.innerWidth, window.innerHeight);
            this.preloadBarBackground.width = window.innerWidth;
            this.preloadBarBackground.scale.x = this.preloadBarBackground.scale.x - 0.1;
            this.preloadBarBackground.scale.y = this.preloadBarBackground.scale.x;
            this.preloadBar.scale.set(this.preloadBarBackground.scale.x, this.preloadBarBackground.scale.y);
            if (this.game.scale.width > window.innerWidth) {
                this.game.canvas.style.marginLeft = '-' + (this.game.scale.width - window.innerWidth) / 2 + 'px';
            }
            this.preloadBarBackground.alignIn(this.game.world.bounds, Phaser.CENTER);
            this.preloadBar.x = this.preloadBarBackground.x;
            this.preloadBar.y = this.preloadBarBackground.y;

        }
    }
}